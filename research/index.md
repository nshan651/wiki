# Research Notes 

## Loyola SSL

[Sparkbadge](notes-2023-02-28)
[Sparkbadge](sparkbadge)
[Meeting Notes](loyola-ssl/meeting-notes)
[xml to arm](xml-to-arm)

## Pollution Autoencoders

[Related Works Notes](pollution-autoencoders/source-notes)
[Notes](pollution-autoencoders/notes)
