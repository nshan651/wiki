# Sources Notes

## "Spatio-Temporal Clustering Analysis for Air Pollution..." - Doreswamy2021

* Focuses on Taiwan for its massive air pollution problem, coupled with the fact that the area's unique topography makes it difficult to map the pollution distribution.

* Utilizes approximately six years of censor data in Eastern Taiwan to analyze the spatio-temporal qualities of pm2.5, as well as the secondary effects of its presence.

    - **NOTE: Uses k-means clustering, look into doing that again??**

* Structure:
    - Uses an autoencoder
    - residual CNN to improve precision and improve "learning organization" 
    - Spatial autoencoders to capture "surface estimations"
    - AE-architecture: 

## "A Deep Autoencoder Model for Pollution Map Recovery with Mobile Sensing Networks" - Ma2019

* Focuses on Pollution mapping recovery with mobile sensing data

* Seperates processes of pollution generation and data sampling allows the model to deal with irregular time and space intervals

* A deep learning approach using autoencoders was used due to the irregular sampling of mobile sensor data

* Additionally, a convolutional long short-term memory (ConvoLSTM) structure was used to model the pollution generation once the internal conenctions to an atmospheric dispersion model are revealed

* Modeled on 3 months of data in Tianjin, China

* Use autoencoders to describe the conditional probability of complete data given partial observations

* "framework" consists of three modules: encoder, decoder, and sampling imitator

* ConvLSTM is useful because it allows for the capture of temporal and spatial dependencies

* Five-fold cross validation to evaluate performance of the model
    - Used three metrics to evaluate results: Root mean squared error, mean absolute error, and Pearson correlation

* Use three algorithms as baseline performance benchmarks:
    - Inverse Distance Weighting (IDW), Artificial Neural Network (ANN), and Gaussian Process Regression(GPR)

    - **NOTE: Try graphing MSE, Mean Absolute Error, Root Mean Squared. Compare using Box plot??**

## "Segmentation of Oil Spills on Side-Looking Airborne Radar Imagery with Autoencoders" - Gallego2018

* Uses Side-Looking Airborne Radar (SLAR) imagery and deep selectional autoencoders to detect oil spills

* In this case, autoencoders dealt with noise present in the imagery caused by the aircraft's maneuvers (from which the sensor is mounted), different weather conditions, and look-alike images caused by natural phenomena

* Evaluated using F1 score (F1 score of 93.01%)

* The proposed model uses a segmentation autoencoder, which learns a codification of relevant pixel information

* This is opposed to how traditionally autoencoders learn the identity function, or how denoising autoencoders learn the underlying error

* Considers a convolutional Selectional Autoencoder (SelAE)  

* Uses cross-entropy loss function to optimize the network

* K-fold cross validation

## "Detection and Identification of Organic Pollutants in Drinking Water...Using Convolutional Autoencoder" - Yu2021

