# Data Visualization Ideas

* Group cities by geographic region (west, south, midwest, northeast)

* Pick out specific States

* Coastal vs non-coastal

* north vs south

* Cities outside the US

* Heatmap of vector values of each city placed in their respective geographic location
    - Cities marked on map of US colored by vec value
    - Compare with similar heatmap of pollution values in the US. Not sure if this is a good idea tbh?

* Plot scatter of first and LAST dimension, could be interesting?

* 3D scatter for gases that do not have high enough scores in the first two dims
