# Autoencoder Notes Part 2

* Autoencoders are *feed-forward* neural networks trained to reconstruct their input

* Goal is to learn the identity function $f(x) = x$

* We impose some restrictions to generate a compressed intermediate representation

* Uses intermediate layers (dimensions) with size smaller than the input layer

* Bottleneck architecture forces network to extract the most representative features, and generates a meaningful intermediate representation

* **Batch Normalization**
    - Regularization technique to allow for faster convergence

* **Dropout**
    - Regularization technique that approximates training a large number of neural networks with different architectures in parallel
    - Randomly drops some layer outputs, which in effect makes certain layers look like others to the model and "changes the view"

* Loss function is usually called **reconstruction loss** for autoencoders
    - This is the squared error between the ground truth and the generated output

* We use scaling to unit length normalization (unit normalization)


