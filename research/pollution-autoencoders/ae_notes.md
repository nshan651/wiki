# Notes

- Autoencoders are designed to be unable to learn to copy perfectly

- Usually restricted in ways that allow them to copy approximately, and copy only input that ressembles training data

- Model is forced to prioritize which aspects of the input should be copied,
and it often learns useful properties of the data

- We hope that the copying task will result in h (hidden layer/layers) taking on useful properties

- Constraining h to have smaller dimensions than x is one way to extract useful features. This is called an undercomplete autoencoder


## Undercomplete AE:

- IMPORTANT! When the decodr is linear and L (loss function) is MSE, undercomplete AE learns to span the same subspace as PCA. 
This means that the AE trained to perform the copy has learned the principle subspace of the training data as a side effect

- IMPORTANT! ae with nonlinear encoder and decoder functions can tlearna  more powerful nonlinear generalization of PCA. 
BUT, if the encoder/decoder are allowed too much capacity the AE can learn to copy wihout extracting useful info about the distribution of 
the data


## Regularized AE:

- Overcomplete AE means that the hidden code has dimensions greater than the input

- Regularized AE allow us to choose the code dimension and capacity of encoder/decoder based ont he complexity of distribution to be modeled

-Regularized AE use a loss function that encourages the model to have other properties besides the ability to copy its input to its output

- These properties include sparsity of the representation, smallness of the derivative of the represnetation, and robustness to noise or to missing inputs

- Regularized AEs can be nonlinear AND overcomplete but still learn something useful about the data distribution, even if the model capacity is large enough to learn a trivial identitty function

- Most generative models with latent variables and inference procedures are considered a type of AE

- Examples: variational autoencoder & generative stochastic network. These models learn high-cap, overcomplete encodings of the input and do not need regularization for these encodings to be useful. Encodings are useful because the models were trained to max the probability of the training data rather than to copy the input to the output


## Sparse AE:


## Denoising AE:

- Rather than adding a penalty to the cost function, can get ae that learns something useful by changing the reconstruction error temr of the cost function

- Denoising AE will make a copy of input that has been corrupted with noise

- Denoising AE will attempt to undo this corruption rather than simply copying the input


Regularizing by Penalizing Derivatives:
	[Also called a Contractive Autoencoder (CAE)]

- Another strategy for regularizing is to use a sparse penalty function

- This forces the model to learna  function that does not change much when input changes slightly

- Penalty is only applied to training ex. so it forces the AE to learn features that capture info about the training distribution


## Representational Power, Layer Size, Depth:

- AE typically trained with single layer encoder and decoder

- NOT A REQUIREMENT. Deep AE offer many advantages

- Encoder and decoder are feedforward networks, so each componen can individually benefit from depth

- Single layer AE mapping from input to code is shallow, which means we are not able to enforce arbitrary constraints, such as that the code itself should be sparse

- Deep AE with 2+ layers can aprox. any mapping from input to code (given enough hidden layers)

- Depth can exponentially reduce the computational cost of represnting some functions.

- Depth can also exponentially decrease the amount of training data needed to learn some fucntions

- Experimentally, deep autoencoders yield much better compression than corresponding shallow or linear autoencoders

- A common strategy for training a deep autoencoder is to greedily pre-train the deep architecture by training a stack of shallow AE
 




