# Meeting Notes

## 2022-06-06

- Python/JS
- Integrate with CI pipeline and github actions
- Like shields.io but more dynamic
- Sparkline (mini graphic, tiny chart)
    - Fits in a single excel cell, or word
    - Could be a preview (that links to project site, full-fledged viz)
    - Also could be self-contained
- Research symposium
- FDfind, ripgrep
- Travis (hosted CI provider)
    - Use github actions instead

- Read up on sparklne concept
- Research shield badge format
- Quickchart

- Sparkline reverse()
    - Reverse x-axis (oldest to newest)

- Hosted on heroku (free-tier)

- Check to see if shield.io does support sparkline
    - Also: pybadges. Check to see if we can use any of their work...?

## 2022-07-06

* Issues with kerning spaces
    - Would like to increase the font size by 100. However, it seems like pybadges default does not support this.
    - I would need to calculate the kerning lengths myself
    - The normal behavior leaves it as monospaced...and looking a little weird as a result
    - I tried taking the default-lenghts.json file and simply doubling, but this did not work

* Font definition
    - Font cache (might have the kerning lengths)
    - Mock-ups for different style charts
