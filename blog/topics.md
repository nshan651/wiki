# Blog Topic Ideas

## [Let's give kids more respect](drafts/more-respect)

* NPR podcast about reading economics books to kids
* Moment where the journalist is reading "Sneeches" by Dr. Seuss, and she is stopped by a school admin
    * Admin says that it is "inappropriate" for the children
    * WHAT?
    * Kids are smart, they can talk about complicated themes like racism and classism
    * We can't just pretend like it doesn't exist
    * They were having a great discussion until they were interrupted
    * Plus, there is a TON of economic lessons to be learned in that book, so what even...

## Modern Liberal Education

* Modern liberal arts institutions don't care about educating - they care about moralizing
* More concerned with cultivating ethical virtues than epistemological ones 
* Is moral/ethical virtue a bad thing? No, of course not!
* Should it be the main focus of the university? Absolutely not
* Epistemological virtues are much better at cultivating moral/ethical virtues than the other way around
* Jesuit values
    
## Networks vs Communities

## First world problems: Or, why everyone else is more normal than us

* Recently traveled abroad to a "developing" country, and I came to the realization that Americans are weird
* We live extremely isolated, unnatural lives.

## The fascination with the grotesque

## The importance of memorization
* As a kid, I went to a Lutheran elementary school where each week we were required to memorize certain passages of the bible
    * By the end, I memorized most of the book of Phillipians
* I do not believe in memorization for the sake of school (dates, medical terminology, etc.)
    * But, I want to learn more about how memorization as a mental *exercise* affects the brain
        * Positive benefits?

## On the importance of written records

* Refer to Borges' story of the man who never forgets, and how his brain allows him an almost encyclopedic, codified almanac of events
* Most do not have this gift. The deliberative process of intellectual growth must involve writing
    * Need to boil down the most succinct thoughts and record them
* This ties in with producing
    * We get tangled in our own mindscape, and assume that people know the things that we do
    * Why am I not more eager to share these wonderful things

## [Take more risks](drafts/take-more-risks)

## [The failings of public education in the United States](drafts/public-education-in-US)
 
* Anecdote about RWHS (anonymized, of course)

## Something on GEB...?

## The importance of having an opinion
 
* My own experience with the issue of abortion going to a conservative christian elementary school

## Why you should listen to the other side
 
* Actively challenge your beliefs
* Dorothy's story of Rabbi and Christian conference
    * Should listen to both, evaluate both sides of the coin
* Qanon shaman vid: identify moment when grounded truth becomes ridiculous
* Why do people settle for dogmatism

## Deconstructing the University
 
* Universities in their current form are too generalized. It doesn't make sense to bundle differing outcomes into one expensive package. Here are some of the differing goals that I have seen people seek out: 
    * Four years of partying and socializing
    * Job outcomes and financial security
    * Academic rigor and interests
    * Creation/development of art and artistic endeavors
    * etc...
* Why not create separate institutions to fulfill these needs in a significantly more efficient way?
* One counterargument is that some people are looking for a mix of these.
    * I would argue that a mix of norms and stigmas surrounding having/not having a college degree force people on the path to a degree, a path in which people only *really* care about one or two of the above points.
    * For instance, I don't think that most people actually care about points 3 and 4, an education for its own sake or for the development of a craft or skill. For most it is a means to an end, or a gateway to some other experience. We're already seeing alternatives cropping up online that allow one to hone a craft or skill completely for free, which underlines the clear inefficiency of the current system. Why not take this a step further and apply this to other areas of the university experience?
    * While this may be true, the current model has colleges trying to fulfill all of these rolls simultaneously, which passes on large costs to students. What inevitably happens is that students pay exorbitant tuition prices for buildings, programs, and services they never utilize or even asked for in the first place.
    * Why not create a modularized system in which students can pick and choose from what they want? Students should actually have control over shaping their college experience, rather than the cable television-like model of channels they'll never watch.

## Kafka on the Shore and Synchronisity

## [The Kafka-esque Beaurocracy of Getting My Tax Documents](drafts/tax-document-nightmare)
