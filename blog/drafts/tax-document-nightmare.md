# The Kafka-esque nightmare of getting my tax documents


It's everyone's favorite time of year again. No, not Easter, but a holiday that involves its own sort of Easter egg hunt. If you guessed "tax day," you'd be correct. It's a time of high anxiety and tracking down documents that you should have recieved months ago, all in an effort to avoid the ire of the IRS.

## Quick Notes

* Timeline:
    - Late-February to Early March:
        - I realize that I have not recieved any tax documents from a certain university (University X) that I had worked at. Tried calling once or twice but got no response. I get wrapped up in midterms and put it off for a while.
    - Mid-March:
        - After speaking about it with my dad, I realize that I probably should have recieved a 1099 form. I redouble my efforts and end eventaully get through 
