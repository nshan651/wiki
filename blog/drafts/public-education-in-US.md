# The failings of public education in the United States

## Notes

* State has excellent social security benefits

* Small Midwest town, we get a lot of people coming in from the larger cities like Detroit and Chicago looking for benefits

* Results in a very transient population

---

Something I've been thinking a lot about recently is the state of public education in the United States. While I wouldn't go so far as to claim that the current system is a *complete* failure, I do think there are some glaring issues with the economic structure of public education. I'd like to do a case study surrounding some of the social and economic factors affecting the school system in my hometown, a small city in the Midwest.

First, I want to highlight how certain circumstances made it difficult for EBD students to get proper attention, which leads to overwhelmed teachers and distracted students. In theory, kids with behavioral problems or learning disabilities receive special "placement" according to their educational and emotional needs. In practice, the process can take years of close evaluation and documentation from multiple teachers. 

Children and teens diagnosed with Emotional and Behavioral Disorder (EBD) display certain traits including (but not limited to) constant arguing, impulsivity, angry and unhappy moods, short attention spans, failure to respect authority, and difficulties maintaining relationships with peers, teachers, and family members. As you might imagine, this laundry-list of negative behaviors makes it extremely difficult for teachers to mediate a class of 30+ students. 


