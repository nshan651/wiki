# Let's give kids more respect

I recently listened this fascinating NPR Planet Money podcast where one of the reporters went around asking top economists for children's books that teach economic lessons. The idea is to see if kids can begin to understand economic concepts if they're broken down in simple ways. Among the list are classics such as **[..., ..., and ...]**.

The reporter and her team visit a middle school, where they have received permission from the administrators to read books from the list to a class of 3rd graders. The administration's one request? **Keep politics out.** We'll see how that goes. It starts off really well. The reporter asks if they spend a lot of time thinking about matching markets in labor economics, which predictably gets a lot of responses amounting to "that sounds like a lot of grown-up words."

**[para abt kids beginning to comprehend after it's broken down into language they understand]**

The final book on the list: "The Sneetches," by Dr. Seuss. This was far and away the most recommended, and if you've read it before it's not hard to see why. It touches on (class economics), supply and demand, a smattering of game theory, and more. I loved the story growing up, but I have to admit that at the time the economic lessons didn't fully sink in. I went back and re-read it before writings this though, and I can really appreciate what a masterpiece it is. It's certainly up there with the Little Prince as one of the deepest and thematically rich children's books I've read.

The reading starts out great. [...] has barely started and already the kids are asking questions. One precocious boy realizes that the way the star-bellied sneetches mistreat the no-stars reminds him of "a super long time ago, when black people were treated really really bad." The discussion is fantastic - a huge success in my opinion. The kids are fully engaged and wrestling with some challenging themes of racism and classism. Everything is going to well, but it all comes to an end when an administrator sitting in on the reading suddenly stands up and interrupts the journalist. 

> Umm, excuse me, but I don't think this is appropriate for the kids. I really don't see what this has to do with economics. Yeah, let's do a different book instead, this isn't really appropriate. [paraphrasing]

Wow... 

Where do I begin? Even if there was absolutely, positively, not a scrap of economics to be learned from this book, why stop a thought-provoking, intelligent discussion dead in its tracks?

Not appropriate? We've already demonstrated that it's on topic. Was it because they can't handle talking about race and class? I have to strongly disagree here too. Eight and nine year olds **are** capable about having this discussion, and they should be having it too. They have more than likely encountered racism and slavery in history class, and all of them, even at their young age, are have and will continue to experience the impacts of race and class.


