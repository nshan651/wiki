# CME Group 

## Job Description

Responsibilities
SOFTWARE ENGINEER I - ROTATIONAL DEVELOPMENT PROGRAM

* We are looking for motivated and talented Software Engineers who want to help us solve some of the most complex challenges in financial technology. Technology teams at CME write and maintain ultra low-latency electronic trading platforms, risk management systems, and front-end trading interfaces, all of which are central to CME's business. We are focused on designing, building and deploying customer focused solutions with passion and precision.
* Software Engineers at CME Group will gain unparalleled experience in electronic trading and financial technology. They will have the opportunity to work on a variety of applications and projects that directly impact the global trading ecosystem. Our engineers work on a wide variety of projects including developing cloud infrastructure, researching blockchain technologies, and applications that directly impact the trading lifecycle.
* The Software Engineers will participate in a Rotational Development program for 18-24 months to gain experience working with several teams, on different projects, and different technologies. This program will also include social events, professional development and knowledge sharing sessions on different aspects of CME's business functions.
* These are full-time, hybrid roles, typically based out of our state-of-the-art headquarters in Chicago, which includes a complimentary coffee and snack bar, as well as a game room and social space.

* MINIMUM QUALIFICATIONS:
    * Experience in Object-Oriented design and software development
    * Proficiency in Java or another programming language
    * Strong communication and collaboration skills and ability to work in a team environment
    * Strong troubleshooting and problem-solving skills
    * Curiosity, critical thinking, and analytical skills
    * Experience with source control and build tools (e.g. GIT, Stash)
* BONUS QUALIFICATIONS:
    * Experience in developing server-side applications
    * Experience working in Financial Technology
    * Proficiency in UNIX/Linux and shell scripting
    * Experience in javascript or REACT
    * Experience working with database or SQL
    * Experience working with cloud technologies

* We know the confidence gap and imposter syndrome can get in the way of meeting spectacular candidates, so please don't hesitate to apply - we'd love to hear from you.

* CME Group: Where Futures Are Made

* CME Group (www.cmegroup.com) is the world's leading derivatives marketplace. But who we are goes deeper than that. Here, you can impact markets worldwide. Transform industries. And build a career shaping tomorrow. We invest in your success and you own it, all while working alongside a team of leading experts who inspire you in ways big and small. Joining our company gives you the opportunity to make a difference in global financial markets every day, whether you work on our industry-leading technology and risk management services, our benchmark products or in a corporate services area that helps us serve our customers better. With 3,500 employees located around the world, we're small enough for you and your contributions to be known. But big enough for your ideas to make an impact. The pace is dynamic, the work is unlike any other firm in the business, and the possibilities are endless. Problem solvers, difference makers, trailblazers. Those are our people. And we're looking for more.
Industry
Financial Services
About Us

* CME Group is the world's leading derivatives marketplace. But who we are goes deeper than that. Here, you can impact markets worldwide. Transform industries. And build a career shaping tomorrow. We invest in your success and you own it, all while working alongside a team of leading experts who inspire you in ways big and small. Problem solvers, difference makers, trailblazers. Those are our people. And we're looking for more.

## Interview

* What?:
    * Interested in developing customer use software
* Why?:
    * Because it lends itself to fast-paced, results-driven work where the product is iteratively refined through feedback
* How?:
    * Currently, I am approaching this interest through my work with the Loyola Software and Systems Lab, where I am designing and creating a tool for developers that enables them to analyze project activity progress over time. 
        * TODO: pybadges edit that emphasizes a love of collaboration
    * Independently, I have built a terminal-based citation generator, the idea for which emerged from a shared frustration among me and my peers with how slow and clunky browser-based citation websites are.
    * Build software bc always trying to learn new techniques and tools. Not only for application itself, but also for other aspects of dev pipeline such as CI services, package manager, etc.
