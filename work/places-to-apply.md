# Places to Apply

| Company           | Date Applied |
| ------            | ------------ |
| Redhat            |              |
| IBM               |              |
| Cboe              |              |
| Atlassian         | 2022-12-08   |
| CVS Health        | 2022-12-08   |
| Amazon            | 2022-12-08   |
| Texas Instruments | 2022-12-08   |

* 
* IBM
* Cboe
* Atlassian
* Gitlab
* Leidos
* Walgreens
* CVS
* Cotsco
* Kroger
* Airbnb
* Uber/Lyft
* Duolingo
* United
* Spirit
* Delta Air
* HashiCorp
* Texas Instruments
* Cisco
* Adobe
* Intel
* AMD
* NVIDIA
* Dell
* HP
* Apache
* 66degrees
* Automattic (wordpress devs)
* Canonical
* Databricks
* Redis Labs
* Samsung
* SUSE
* Confluent
* JetBrains
* Digital Ocean
* 66degrees
* SiFive
