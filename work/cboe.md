# CBOE Interview Notes

## What is CBOE

* The Chicago Board Options Exchange, located at 400 South LaSalle Street in Chicago, is the largest U.S. options exchange with annual trading volume that hovered around 1.27 billion contracts at the end of 2014. 

## Job Description

Job Description

Job Title: Software Engineer – Regulatory

Department: Regulatory

Location: hybrid - locations in either Lenexa, KS or Chicago, IL

Job Summary

The successful candidate will design, develop and monitor systems or data analysis applications, based on the trading & market activities, for internal company or external market regulations compliance or investigative purposes. The position includes design & development in data processing, rules engine, RDBMS, and scripting technologies on the back-end. This opportunity requires an individual to have strong problem solving, data analysis, and software systems design skills.

Duties Include

Working with end-users to gather and define/refine functional requirements
Producing project documentation – high-level charter, functional requirements, technical design and test documents
Designing, coding, testing and implementing data processing, rules engine, database, and scripting surveillance applications
Working closely with infrastructure team on server systems
Collaborating and coordinating with various systems groups for new application development, problem troubleshooting and systems maintenance
Working as a team/project lead or individual team contributor or individual ownership role flexibly based on the project needs
Mentoring/Guiding new or less experienced/knowledgeable team members
Researching or evaluating software/tools and alternate design techniques for new or existing system improvements
Providing on-going application support to end-users and communicating technical information clearly to an end-user audience
Participating in production 24/7 on-call support rotation

Job Requirements

Proven experience with the below technologies:
    Experience with Python or experience in another language with a strong desire to learn Python.
    Experience with Java or experience in another language with a strong desire to learn Java.
    Experience with Linux or UNIX-like operating systems and knowledge of at least one scripting language.
    Experience with Hadoop and/or SQL desirable.
    Strong knowledge of data structures and algorithms.
    Excellent written and verbal communication skills
    Must be self-motivated and have the ability to work independently as well as part of a team, requiring minimal direction and follow-up
    Experience in working on multiple projects concurrently
    Experience in financial, trading, exchange, banking and/or related fields is a plus
Bachelor’s Degree in Computer Science or related field preferred.

    Cboe Global Markets is an Equal Opportunity Employer. For more information, please click the following links:

    Equal Employment Opportunity is The Law (in English)

    Equal Employment Opportunity is The Law (in Spanish)

    Equal Employment Opportunity is The Law (Supplement)

    E-Verify Participation Poster (English & Spanish)

    Right to Work Poster (English)

    Right to Work Poster (Spanish)
    
    
