# Leidos Negotiation

Mr. Miley,

Thank you so much for this offer. I am really excited about the opportunity to join the team at Leidos and to work on the ERAM system. Thus, I would love to discuss the possibility of a higher starting salary.

I currently have an offer with an annual total compensation of $110,000 for a job in Chicago doing software engineering for a finance firm. However, based on everything I've learned, I really do want to work for this team here at Leidos. I understand that the cost of living in the Twin Cities is lower, but even after that adjustment, I was wondering if we could explore a salary for this position that is closer to $100,000. I would be more than happy to hop on a call and discuss this further.

I am also finishing up a couple other interview processes that I imagine will be done in the next few weeks. As a result, I would greatly appreciate an extension on my offer acceptance deadline until the end of December in order to follow through on these processes and tie up loose ends.

Thank you so much in advance for your understanding. I look forward to hearing from you soon.

Best,

Nick Shannon
