# 66degrees Interview


## Link 

https://meet.google.com/pam-iwst-wjg

## Details

* Rotational program
    - cloud engineering
    - data
    - app dev

* One manager/ diff project managers

* 40 hrs/week

* June 6 - Early August

* On-site
    - Proof of vax

* Director of data team/chief tech architect

* final with ceo

## Email

```
Thanks for submitting your availability for the Engineering Summer Internship position here at 66degrees.

You're confirmed for your interview on: Date/Time: Apr 25, 2022 11:00am-11:25am CDT
Interviewers: Eric Lannert
Google Meet: https://meet.google.com/jep-atgv-pyx

Date/Time: Apr 25, 2022 2:00pm-2:25pm CDT
Interviewers: Jason Blythe
Google Meet: https://meet.google.com/mnn-zirk-bek

**Please note: this interview is not automatically uploaded to your calendar, you will need to do that manually**

Let us know if you have any other questions before your interview.
```

## Interview Questions

* Will it be 100% in-person, or a hybrid system?

* Based on our conversation this afternoon and my resume, what are the key things that I could work on and learn about so I can be as prepared as possible to be successful by the start-date of the internship

* Is the work fairly procedural and top down, or are there opportunities for creativity and innovation 

* Management structure. How do check-ins, progress reports, and meetings work?
    - More top-down (assigned tasks from management) or bottom up (some say in what to work on)?

* What is the coolest and/or most impactful project you have worked on at 66degrees
