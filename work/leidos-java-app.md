# Leidos Remote Java Dev Application 

## Description

locations
    6314 Remote/Teleworker US

time type
    Full time

posted on
    Posted 2 Days Ago

job requisition id
    R-00099833

At Leidos, we deliver innovative solutions through the efforts of our diverse and talented people who are dedicated to our customers’ success. We empower our teams, contribute to our communities, and operate sustainable. Everything we do is built on a commitment to do the right thing for our customers, our people, and our community. Our Mission, Vision, and Values guide the way we do business.

Leidos is looking for a Software Developer to join a high performing agile team to support a nationally significant program. This position offers the opportunity to work remote approximately 100%.The program is using test automation tools alongside a full suite of team collaboration tools. Program execution follows DEVOPS best practices and employs robust development, test, and production environments. The program provides system engineering, development, test, integration, and operational support to a large, fast-paced program, and is developing infusing new technology and adding advanced capabilities while continuing to support an on-going mission and operational system.

The incumbent will be responsible for creating maintainable, adaptable, scalable, defect free code using software development best practices and coding standards (i.e. software reuse, refactoring, continuous integration, test automation, secure development, TDD, static code analysis, peer/code review). He/she will support the software configuration management process and understand software components and their interface requirements.
This position requires someone who is proactive and passionate about ensuring the quality of software deliverables and continuous improvement, as well as, and can communicate with the development teams what is required to achieve this effectively on tasks.

Primary Responsibilities:

    Work with the overall program team in the design and development of new applications as well as integrating new solutions on existing applications within the cloud
    Analyze complex application development needs, reason logically, and identify solutions to include potential new tools/technologies and appropriate recommendations
    Provide development support to the IRS including creation of automation scripts, coding, testing, deployment, and integration with the cloud
    Maintain all source code in conformance with best practices and customer specifications
    Provide commanding knowledge of building and deploying systems on the Cloud, using CI/CD frameworks and infrastructure automation
    Adapt to changes in a multi-cloud DevSecOps environment
    Understand user needs and develop software solutions through clear communication with product owner and scrum team
    Design and develop within a cloud environment customized to IRS requirements, with the aim of optimizing operational efficiency

Qualifications

    Bachelor’s degree or equivalent experience and 2 or less years of professional experience.
    Experience with Java, JavaScript, and with Node/Or willingness to learn Node
    Experience in Cloud operations such as AWS, GCP and Azure.
    Experience working in a DevOps and Agile environments using code delivery mechanisms, continuous build systems, code repositories, and continuous delivery solutions
    Demonstrated knowledge of cloud-based application development
    Requires ability to obtain a Minimum Background Investigation, similar to a Public Trust Clearance and up to date on all tax filings and tax payments

Preferred Qualifications:

    Experience working on projects in an IRS/Treasury environment
    Certification in Agile methodology
    Security+ equivalent or higher certification (IAT Level II+)
    Experience in working with MS Office, Middleware Management, documentation, Java, JSP (HTML/XML), RESTful Web Services, Cloud, Containers, DevSecOps, Oracle SQL, Linux/RedHat, Agile, Continuous Integration/Continuous Delivery (CICD), CA SiteMinder, 508 compliance, IBM Collaborative Lifecycle Management (CLM)

Pay Range:
Pay Range \$53,300.00 - \$82,000.00 - \$110,700.00

The Leidos pay range for this job level is a general guideline only and not a guarantee of compensation or salary. Additional factors considered in extending an offer include (but are not limited to) responsibilities of the job, education, experience, knowledge, skills, and abilities, as well as internal equity, alignment with market data, applicable bargaining agreement (if any), or other law.
