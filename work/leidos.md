# Leidos Application

## Thank You Email

Mr Andersh,

I wanted to thank you for your time and for the fantastic interview experience. I enjoyed learning about the organization and your team's specific project, as well as about your specific role. I also really appreciated that you and Shane kept the process conversational and comfortable; it left me with a really positive impression of the company culture that I can expect going forward. I'm delighted to see that I have moved on to the background check and offer letter stage. Of course, nothing is final yet, but it would be a pleasure working with you in the future. In any case, thank you again!

Best,

Nick Shannon

## Security Info

* Foreign Travel:
    * 2022-08-13 to 2022-08-27 -- Costa Rica (San Jose, Puerto Jiminez, Drake Bay, Monteverde)  -- Vacation
    * 2022-08-13 to 2022-08-14 -- Panama City -- Layover en route to San Jose, Costa Rica
    * 2019-06-13 -- Gibraltar Spain -- Vacation, 
    * 2019-06-14 to 2019-07-03 -- Tangier, Morocco -- Vacation
    * Start of June 2017 to Start of July 2017 -- Tangier, Morocco -- Vacation

### Job Application

Job Description and Responsibilities:  

    Leidos is seeking an Software Development Engineer who will be a member of a dynamic team working on the Federal Aviation Administration’s En-Route Automation Modernization ERAM) technology, which is the heart of the Next Generation Air Transportation System and the pulse of the National Airspace System helping to advance our transition from a ground-based system of air traffic control to a satellite-based system of air traffic management. ERAM is vital to the future of air navigation, providing the foundational platform required for FAA to enable NextGen solutions, via modernization programs such as System Wide Information Management, Data Communications, and Automatic Dependent Surveillance- Broadcast.
    ERAM will provide benefits for users and the flying public by increasing air traffic flow Flight Deck (FDK) functions include: 
        Real-Time Programming on High Availability Systems
        Multi-threaded programming
        Low Level Operating Systems Customization (RedHat, AIX)
        Low level communication software (TCP-IP, UDP, SCTP)
        Support of Common Services utilized by ERAM Application teams
        System Maintenance Concepts  
    Primary Responsibilities:
        Modernizing the AT control system by improving the availability and performance of the ERAM system.
        Supporting ERAM as it continues to migrate from legacy system using the AIX operating systems to HP Hardware running Linux (Red Hat).  
        Detailed knowledge of Linux OS and OS concepts desirable. 
        Developing and maintaining the FAA air traffic control software (primarily Ada95/C++) on a UNIX/Linux platform.
        There are a number of support/maintenance functions that utilize PERL/PYTHON.
        Performing software development as needed for the ERAM system including FlightDeck “middleware” capabilities, System Management Concepts, and CHI for the Maintenance and Control Position.
        Coordinate Solutions with Systems Engineering Coordinate the development of new capabilities and upgrade to common services provided the applications via the FlightDeck API.
        CAS Integration Concepts ERAM Tools Development

Preferred Qualifications: 

    This is an entry level position.
    Bachelor's degree from an accredited college in related engineering discipline, or equivalent experience and less than 2 years prior relevant experience. (Computer Science or Computer Engineering Degree)
    Your GPA needs to be included on your application and resume.
    US Citizenship Requirements
    Candidate must be able to obtain an SF-85P Public Trust clearance from the FAA customer.
    Demonstrated Software Development Skills/Capabilities
    Excellent Verbal and Communication Skills
    Demonstrated ability to debug and solve problems
    A Strong Desire to Learn
    Work Experience in a Technical organization performing Software Engineering Tasks
    Leadership Skills & Experience

### Pre-Screen Questions

1. This is an entry level position.  Is that position acceptable to you?
    * Yes.

2. What is your current employment situation?  What has prompted you to start looking for a new opportunity?
    * I am currently a full-time student at Loyola University Chicago. I am eager to begin working as a full-time software developer upon my graduation in May of 2023.

3. What type of work are you interested in doing?  (What is it about this position that you like?) 
    * I am very interested in low-level programming and operating systems, specifically in a Linux environment. I enjoy analyzing and improving existing codebases; it is extremely rewarding to be able to break down a complex piece of software and understand how the individual components work. I would like to be a part of something impactful, and I believe that working on the mission-critical ERAM system with a team of talented and experienced developers is a perfect opportunity to do just that.

4. Do you have:
    * Demonstrated Software Development Skills/Capabilities
        * I have strong software development fundamentals which include knowledge and experience in version control, continuous integration/development, unit testing, design patterns, and project organization.
    * Excellent Verbal and Communication Skills
        * I have delivered an investment pitch to the executive board of the Rambler Investment Fund (RIF), as well as to over one hundred students. The following semester, I presented an algorithmic trader to the RIF board and undergraduate cohort. At the end of the 10-week Deep Learning REU, I presented a research poster to a large audience and answered audience member questions.
        * I also have a large amount of experience with non-verbal communication and technical writing from my research work, creating documentation for various projects, and a detailed stock report I created for RIF.
    * Demonstrated ability to debug and solve problems
        * In my time as a programmer, I have developed the confidence to overcome any challenge I encounter through a combination of persistence and methodology. My approach to debugging is to break the problem one step at a time into smaller sub-problems and to utilize all the resources available to me, including online resources, documentation, and peer guidance. 
    * A Strong Desire to Learn
        * I consider myself to be a life-long learner. I enjoy reading, writing, and sharing new things with others. I stay up-to-date on the latest developments in tech through articles, forums, videos, and podcasts. I love learning about new ways to solve problems as well as the lessons shared by more experienced and skilled developers than myself.
    * Work Experience in a Technical organization performing Software Engineering Tasks
        * I am currently a member of the Loyola Software and Systems Laboratory (SSL) where I am working on a project that allows users to create and use longitudinal status badges in their projects on GitHub or other hosting platforms.
        * I participated in a 10-week research program at the University of North Texas where I built a deep neural network to characterize and predict urban air pollution in the US. I developed a pipeline to retrieve data from OpenWeather's pollution API, cleaned, and analyzed the data. I presented a poster at the end of the program, and continued to work on the paper for another eleven months.
        * I led a team of students in the creation of an algorithmic stock trader and an automated stock report generator as a part of Quantitative Analysis Club (Quant).
        * I contributed several non-trivial poll requests to the Google Pybages project, as I needed additional extensibility for a project I was working on.
    * Leadership Skills & Experience
        * I have led a team of students in the creation of an algorithmic stock trader and an automated stock report generator. I was responsible for hosting weekly meetings, assisting my teammates, and setting goals for the projects.
        * I led a group of undergraduates in a deep learning research project. I was responsible for assigning tasks, project management, and providing updates during daily program-wide stand-ups.

5. Briefly list 2-3 core skill sets based on your experience.
    * I am confident in my familiarity with Linux and Unix tooling. I have been running a Linux distribution natively on my computer for about 2 years, and over time have learned a lot about scripting, development tools, and the operating system.
    * Over the course of my experience with software development, I have made it a focus to learn tools and techniques to deliver high quality software. I strive to utilize version control, unit testing, and continuous integration to create reliable programs.
    * I highly value my verbal and non-verbal communication skills. My leadership experience, research work, and documentation writing have given me the confidence to embrace responsibility in these areas.

6. What is your Major?
    * Computer Science

7. What is your major GPA and cumulative GPA?
    * My major GPA is 3.82 and my cumulative GPA is 3.85

8. What is your expected graduation date (MM/YYYY)?
    * 05/2023

9. What is your desired salary range?
    * Negotiable

10. How soon are you available to start a new position?
    * I am available to start any time after my graduation in May of 2023.

11. This contract also requires you to be a US Citizen.  Do you meet that requirement? 
    * Yes.

12. Do you have an active security clearance?  If so, what level clearance to you currently hold?
    * I do not hold any active security clearances.

13. Please provide us with a Word version of your most updated resume.

14. This position is in the Gaithersburg, MD area.  Are you able to secure reliable transportation to and, if needed, housing at this location?  Are you willing to self-relocate?
    * Yes, if necessary I would be willing to relocate and secure transportation/housing in Gaithersburg, MD.

15. Do you have any other preferred work locations?
    * Yes, remote, Chicago, Il., Egan, MN., or Denver, CO. 

## Questions to Ask

1. Can you give me an overview of ERAM?
2. What piece of it would I be working on and can you give me a little more in depth description of that?
    * How many other engineers work in this area?   Are they broken up into sub-groups/teams?

3. What are the other pieces (of ERAM) and where are they done?   (note:  I think there are a lot of separate parts to ERAM)
4. What is the biggest schedule item in process now?  When is it due?
5. Do people ever move around from one part of ERAM to another?   From ERAM to other Leidos projects?
    *  What other Leidos projects are worked out of Eagan?
6. Is there any opportunity to travel?  Where are the customer sites?  Are there Leidos people working “in the field now”?  Anybody from your group?
7. How long is your typical and/or current contract?  Knowing how the federal government works, is there ever any concern over potential government shutdowns or budget resolution delays?
8. How do you perceive the future of ERAM (short and longer term)?
9. How many new engineers do you typically hire a year?
10. If you talk to another engineer...
    * What is the most fun part of your job?
    * What is the most frustrating aspect of your job?
    * Do you feel like you have a good future with Leidos?

* Shane Hanlon / Craig Andersh

* VAT sim lab
