# TODO:

## $HOME 

- [ ] Apply to Java position (100% Remote!!!)
- [.] Utility cancellation:
    - [ ] Wi-Fi
    - [X] Electricity
- [X] Follow up on email to keep .luc email
- [X] Move-out
- [X] Flight on 2023-06-10
- [ ] Sign up for credit card

## $FUN

- Haber-Basch Process for increasing nitrogen fixation in soil by over 200%
- [ ] Morse market
- [ ] Install script to recreate setup
- [ ] Password manager/encrypt files
- [ ] Yocto Project
- "As man danced so the drums were beaten for him" - Achebe, *Things Fall Apart*
- [ ] RSS feeds all in one place
- [ ] Remote SSH server
- [ ] Containerize zoom
- [ ] Groff
- [ ] Trapezoidal numeric integrals in Rust
- [ ] Domain sockets
- [ ] Islam, **riba**, or the prohibition of loans and debts
- [ ] Paper a week challenge
- [ ] Fix broken release of excite 
- [ ] Bookmarking with dmenu
- [ ] Work on website
- [ ] Program to transcribe audio to text on linux
- [ ] SSH tunneling
    - https://www.ssh.com/academy/ssh/sshd_config
- Jean Giraud (Moebius)
