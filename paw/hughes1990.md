# Hughes1990

## Why Functional Programming Matters  

### 2022-07-22

* FP is characterized by various levels of functions, with language primitives at the bottom:
    * Main function -> Other functions -> Even more functions -> Language primitives
* FP contains no side-effects
    * Purpose of a function is simply to compute its result
* Relieves programmer's burden of establishing a control flow
* Conventional programs consist of 90% assignment statements, which can be omitted in FP
* This characterization above is inadequate to describe FP
* Structured vs unstructured programming:
    * Structured programs do not contain `goto` statements
    * They are more mathematically tractable
    * Most important difference: structured programs are designed in a modular way
* Modular programming ~= divide-and-conquer:
    * Modular program solves a problem by breaking it up into sub-problems
    * Sub-problems are solved, then solutions are combined
* Functions can be easily glued together
* A list defined (basically) recursively:
    * `listof *::= Nil | Cons * (listof *)`
    * Where * is a list of type *, or a Cons of a * and another list of *s
    * [] = Nil
    * [1] = Cons 1 Nil
    * [1,2,3] = Cons 1 (Cons 2 (Cons 3 Nil))
* Author constructs an entire semantic system on this structure, continuing to use Turing's *Miranda* language
* Big picture:
    * In FP, indivisible functions can be expressed as a combination of parts
    * Higher order function and some specializing function
    * This makes it easy to write extensible programs, while also localizing knowledge about the representation details
* Lazy evaluation is **great** when you don't have to deal with side-effects
    * While adding lazy eval to an imperative language is possible, it would actually make the programmer's life harder because of the side effects of imperative langs
* Next example program - Newton-Raphson Square Roots 
* Formula: `a_{i+1}=(a_i + n/a_i)/2`
* Conventional program is indivisible
* FP allows us to break this down into atomic subcomponents:
    * Function mapping one approximation onto the next `next`
    * Function to compute the list of approximations `repeat`
        * `repeat` is an example of a function with "infinite output", but only the approximation that the program requires will actually be computed
    * Function called `within` that finds the square root
* This combined function will look like this:
    * `sqrt a0 eps n = within eps (repeat (next n) a0)`
* Third example - differential equation solver:
    * Within $\epsilon$ can be used to select the first approximate value that is accurate enough, without the risk of rounding error
* Same thing can be done with integrals
* Start with formula `easyintegrate f a b = (f a + f b) ∗ (b − a)/2`:
    * By itself is not super accurate, unless a and b terms are close together
    * Can be improved by dividing the interval from a to b, estimating area on each half
    * This can be done *ad infinitum* for a more accurate result
* Formula for this is: 
    
```integrate f a b = Cons (easyintegrate f a b)
(map addpair (zip2 (integrate f a mid )
(integrate f mid b)))
where mid = (a + b)/2
```
* `zip2` is a standard list-processing function, combines two lists and returns a list of pairs
* The above integral function is inefficient because it continually recomputes f
* This can be improved by using the following instead, which never recomputes f:

```integrate f a b = integ f a b (f a) (f b)
integ f a b f a f b = Cons ((f a + f b) ∗ (b − a)/2)
map addpair(zip2 (integ f a m f a f m)
(integ f m b f m f b)))
where m = (a + b)/2
f m = f m
```
* This algo can also be improved - it converges slowly
```
super (integrate sin 0 4)
and
improve (integrate f 0 1)
where f x = 1/(1 + x ∗ x)
```
* The above algorithm takes advantage of lazy evaluation and glues together several previously mentioned functions
* Final example - Alpha-Beta ML Algorithm:
    * Represents the current player's position in a game
    * Structured as a tree with nodes and edges
    * Lazy evaluation allows for the tree structure to be evaluated and manipulated without worrying about memory constraints
    * Only a small part of the tree exists in memory at any given time
    * The rest is garbage collected
* Further reading: 
> [2] Henderson, P. “Purely functional operating systems”. In Functional Programming and its Applications. Cambridge University Press, Cambridge,
1982
