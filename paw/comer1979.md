# Comer1979

## The Ubiquitous B-Tree

### 2022-08-03

* Begins with a metaphor about a business's employee files
* Ordered by last name; each entry has a unique key
* The B-tree is defined as the organization for indexes in a database system
* Three main B-tree types:
    * B, B*, and B+
* A set of basic operations is expected of a filesystem:
    * insert
    * delete
    * find
    * next (Retrieve record immediately following)
* Came after BSTs
* Each B-tree of order $d$:
    * Contains at most $2d$ keys and $2d+1$ pointers
    * Must contain at least $d$ keys and $d+1$ pointers
* Multiple key approach allows for more branches to be taken:
    * 2 keys accommodates three branches
* Large multikey storage nodes can't be kept in main memory
* Longest path in a balanced B-tree of n keys has at most about $log_d n$ nodes
* Balancing a B-tree takes computational time, so it is important to have an efficient method
* Balancing scheme restricts changes to a single path from a leaf to the root
* Balancing method uses secondary storage to lower the balancing cost
* Storage-for-speed trade-off
* Example: B-tree of order 2 has between 2 and 4 keys (*d* and *2d*)
* When a key needs to be inserted in a full node:
    * A "split" occurs
    * Of the $2d+1$ keys...
        * Smallest are placed in one node
        * Largest are placed in another
        * Remaining value is "promoted" to the parent node where it serves as a separator
    * Parent node will usually accommodate another key
    * If the process happens again, another split occurs
* A B-tree only increases in height due to a **split at the root**
* Deleting a value means one of two things: that the key is in a leaf or a non-leaf node
    * If its in the latter, an additional adjacent key is required to be swapped into the empty position so that the tree still functions
    * To find an adjacent key, you must search for the leftmost leaf in the right subtree
* If less than *d* keys are left, then an **underflow** is said to occur, and redistribution of the keys becomes necessary
* If a redistribution is to happen between two neighbors, there must be at least $2d$ keys to distribute
* Else, a **concatenation** must happen, which is when the keys of the two nodes are combined into one, and the other node is discarded
* Concatenation is the inverse of splitting
* B-trees exhibit low retrieval costs 
* Insertion and deletion times are $O(log_d n)$
* Standard B-tree is not as good for sequential processing
* Considerations toward improving speed:
    * Concatenation and redistribution to delay the need for splitting
    * Knuth suggests to use a binary search for large nodes and a sequential search for small nodes
* Pointers in leaf nodes waste space that should be eliminated
    * Knuth creates variant where nodes have different node sizes, especially for the root
    * The downside of this is the maintenance costs 
* B* tree
    * Tree where each node is required to be at least 2/3 full (rather than just 1/2)
    * Delays splitting until two sibling nodes are full, at which point they split into 3 nodes each at 2/3 full 
    * Guarantees that space utilization is at least at 66%
* B+ trees:
    * All keys reside in the leaves
    * Upper levels only contain an index
    * Linked list of leaves is the *sequence set*
* Searching a B+ tree starts from the root and proceeds through the index to a leaf
* Deletion is simplified because of the fact that non-key values can be part of the index part of the tree
* Key to be deleted must **always** be located in a leaf
* When a leaf splits into two, the only difference is that a **copy** of the middle node is promoted, and the actual key is put into the right node
* In a search, the search proceeds all the way down to a leaf if it has located a matching query value 
* B+ trees can satisfy a next operation in 1 step
* Good reasons to *not* use keys as the indexes. Keys take up more space
* Prefix B+ tree:
    * System in which separators are given a unique prefix that relate to the key in some way
    * Works by giving separator the shortest possible unique prefix
    * Sometimes saves space, not always
* Virtual B-trees :
    * Address space of user's virtual memory is divided into pages which are saved on secondary storage and loaded into main memory automatically when they are referenced
    * This technique is called *demand paging*
    * B-tree is carefully allocated to one page of virtual address space
    * Accessing nodes not currently in memory cause the node to "page-in" the required node from secondary storage
    * Most paging algos remove the least recently used (LRU) page when making room for a new one
    * Most active nodes are the ones close to root
    * Virtual B-tree advantages:
        * Special hardware performs transfers at high speeds
        * The memory protection mechanism isolates other users, and
        * Frequently accessed parts of the tree will remain in memory
* Compressed keys and pointers:
    * Pointers can be compressed via base displacement from the node rather than an absolute address value
    * Key and pointer compression both increase the capacity of each node and decrease the retrieval costs
* Binary B-tree:
    * B-tree of order 1. Has 1-2 keys with 2-3 pointers
* Concurrency problem: What happens when multiple users access and alter a B-tree
* Solution - locking model:
    * Process where a find locks a node once it has been read, then releases it once it moves to the next depth
    * This allows for concurrency without interference between user operations
* Concurrent update model:
    * An update process will establish reservations on nodes that may be altered
    * This continues all the way down to some leaf node
    * At this point, absolute locking begins and the update happens
    * After changes are made, the locks are released and the nodes are once again available
* Two alternate proposals to make this better:
    * Bayer - proposes generalized locking protocol; a parameterized model that both allows concurrency to fully utilize technology while also wasting very few resources on restarting reservations
    * Guib - suggests a top-down splitting model, which would mean that only one pair of nodes would need to be locked at a given time, trading an increase in storage for more flexibility and speed  
* IBM's practical B-tree implementation - VSAM:
    * A B+ tree
    * Each leaf is called a *control interval*, and forms the basic unit of data transfer in an I/O operation
    * Pointers are compressed using base/displacement method
    * Novel part of VSAM is that one data format is used throughout the system
* Other offerings of VSAM:
    * Efficient insertion of a large contiguous set of records
    * Data protection
    * File backup
    * Error recovery
* 
