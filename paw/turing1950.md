# Turing1950

## Computing Machinery and Intelligence

### 2022-07-14

* "Can machines think?"
* Assumes that the natural best strategy for a machine to win the imitation game is to give answers naturally given by a person
* Digital computers consist of...
    - Store of info - corresponds to a "human" computer's paper
    - Executive units which carries out individual operations
    - Control - ensures that the instructions are clearly obeyed
* Random number generation $\neq$ free will
* Use of electricity is **not** an important factor when considering if a machine has free will
    - Despite the similarities between electricity in human brain and digital computer, C. Babbage's mechanical computer proved this doesn't matter
* Given any input of a finite state machine, it is possible to predict all future states
* Pro determinism!
    - Turing refers to Laplace's view of the universe as a FSM.
* A deterministic universe is very sensitive to inputs, down to the subatomic level
* Purpose of creating discrete state machines (DSMs) is to avoid this need for absolute precision
* A digital computer with adequate speed and storage capacity would have to be reprogrammed for every new machine it is desired to mimic
* "I believe that in about fifty years' time it will be possible, to programme computers, with storage capacity of about 109, to make them play the imitation game so well that an average interrogator will not have more than 70 per cent chance of making the right identification after five minutes of questioning."
    - Turing was almost dead on! 64 years later, the test was just barely passed by an [AI](https://www.theatlantic.com/technology/archive/2014/06/for-the-first-time-ever-a-computer-passed-turing-test-for-artificial-intelligence/372392/)
* Theological argument: 
    - Turing not impressed by orthodox view of man's intelligence attributed to soul
    - The difference between the animate and inanimate is **much** larger than between man and other animals
* Heads in the sand objection:
    - Belief that consequences of machines thinking would be too horrible. Would take away humans' "commanding position."
    - Not a compelling argument either
* Mathematical objection:
    - Godel's theorem: statements within any sufficiently powerful mathematical system can be formulated which can neither be proven nor disproven within the system, unless the system itself is inconsistent
    - Turing applies this concept to machines, saying that given an infinite capacity machine, there have to 
* Argument for consciousness:
    - Only way to be sure that the machine thinks is to **be** the machine and to feel oneself thinking
    - Solution is for the machine to respond complexly, giving sufficiently detailed and logical responses
* Arguements from various disabilities (i.e. machine cannot do X)
    - Assumption that machines cannot be kind, resourceful, friendly etc. 
    - Stems from inductive reasoning of humans who have interacted with thousands of machines, none of which are capable of these things
    - Most machines are designed with a limited purpose in mind
    - Assumption inability to do X is a human failing disregards humans who can't do X
* Creativity:
    - "The appreaciation of something as surprising requires as much of a 'creative mental act' whether the surprising event originates from a man, a book, a machine or anything else."
* The nervous system:
    - Nervous system is not a discrete-state machine (rather, it is continuous)
    - Questionable whether a machine can mimic the continuity of the nervous system 
* Argument for informality:
    - No prescribed list of valid "human" responses to a question. Normative
* ESPs! 
    - "Unfortunately the statistical evidence, at least for telepathy, is overwhelming."
    - Possibility of a telepath guessing a random card statistically more than a machine...
* Machine learning and imitating the human brain
    - Three components:
        - Initial state of mind
        - The education subjected to it
        - Other experiences subjected to it
    - Start from the initial state of mind and teach
    - Punishment/reward system, RL
    - Include an element of randomness 
