---
title: "Great Smoky Mountains Trip"
author: "Nick Shannon"
date: 2023-03-02
---

# Trip Outline 

# Packing List

## Sleeping

- 2-man tent
- Sleeping bag 
- Mat
- Pillow

## Gear

- Rechargeable lantern
- Flashlight(s)
- Swiss army knife
- 2-3 *laminated* maps
- Garmin (maybe)
- Compass (maybe)
- Watch
- Phone
- Chargers + portable battery

## Food

- Dried apples
- Jerky
- Oats
- Instant coffee
- Dried veggies
- Almonds + raisins
- Granola
- **Lots** of water

## Clothes

- 1 sweatshirt
- 1 pullover
- 4 pairs of sweatpants
- 4 shirts (2 long, 2 short)
- Raincoat
- Underwear / socks

## Misc

- First-aid kit
- TP
- Water filter (straw, solvents)
- Loyola water bottle + metal canteen

# Blogs

- [Trail guides](https://www.theoutbound.com/united-states/tennessee/great-smoky-mountains-national-park/backpacking)
- [Hiking the Smokies](https://hikinginthesmokys.com/blog/ultimate-guide-to-backpacking-in-the-great-smoky-mountains/)

## Official Resources 

- [Things to know](https://www.nps.gov/grsm/planyourvisit/bc-things2know.htm)
- [Backcountry permits](https://smokiespermits.nps.gov/index.cfm?BCPermitTypeID=1)
- [Map](https://www.nps.gov/grsm/planyourvisit/upload/GSMNP-Trail-Map_JULY21.pdf)
