# Godel, Escher, Bach: An Eternal Golden Braid
## Douglass Hofstadter

To say that this book is not for the faint of heart would be a huge understatement. I spent the better part of three months tackling this beast, but I can safely say that it was well worth it. Hofstadter presents a myriad of connected and complex topics, but he does so in a direct, easily comprehensible way. He even adds bits of humor in the form of dialogues between Achilles, the Tourtoise, and the Crab, which was sort of hit or miss for me.

The heart of this book is in Godelian set theory and systems of logic.
