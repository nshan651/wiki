# 21 Memories of You

Dot, After the past ~~six and a half~~ year of knowing you and the (almost) a year dating, I can confidently say that I am more in love with you than ever. I am so eternally grateful to have you in my life. You make myself and everyone else around you better with your curiosity, intelligence, and radiating kindness. 

Things haven't always been easy, but I can always be sure that we will find a way. 
I wanted to give you something that you can look at and remember some of our fondest memories together, so I made this list of twenty-one of my favorite memories of you.

* 1. First time meeting.
    * Sitting on the frozen-over rocks overlooking Lake Michigan, talking about "To Build a Fire" by Jack London, our first kiss.

* 2. First date.
    * Dressing up and going out on our first fancy date. 

* 3. A Capella show.
    * Sitting in the audience laughing our butts off with all your friends. Seeing Smitha absolutely slay. Boys group was pretty garbage, except for the one guy at the end who absolutely KILLED IT.

* 4. "Monkeying" around!
    * I cherish all the times we just goof around and laugh at ourselves. Playing on the kids playground in Rogers Park with all those toys lying around, spinning each other on the merry-go-round, hanging upside down on the pull-up bar.

* 5. Abandoned building in Roscoe Village.
    * Going to the abandoned building near Roscoe Village in the freezing cold only to find that it was all boarded up. Going back and snuggling in the warmth of your apartment.
    * It was so cold that day! We had to search for a while, and when we finally found the building it was all boarded up! I felt really bad, but it was a pretty memorable adventure anyway. Plus, we had some great food.

* 6. Maria Beach.
    * Skinny dipping after a long night out; guy walking past us and going "I didn't see anything!"

* 7. Frisbee at Tobey-Prinz.
    * Playing an impromptu frisbee game; your first time playing Ultimate. This one guy making up the absolutely BS rule that the frisbee was still in play after touching the ground.

* 8. Picnic at Bahai.
    * Gone wrong, kinda? We ended up walking to a nearby park for the picnic instead. Damn Evanston NIMBY police.

* 9. Alfonso Ponticelli.
    * Alfonso Ponticelli & Swing Gitan at the Green Mill. Alfonso strumming his guitar with a beer cup was pretty legendary.

* 10. Breakfast with your family.
    * Getting breakfast with your parents and Paulina and talking about the things you did growing up. Your epic ice skating pics.

* 11. Our philosophical debates.
    * I love all the intellectual discussions and debates we have! Our debate about whether a set could be disjoint and mutually exclusive when dealing with infinities (I totally won that argument xD).

* 12. Art Institute.
    * Our many trips to the art museum (probably one of the best free activities Chicago has to offer). That first time going with you to see *Farm Near Duivendrecht* and the Chagall windows with you.

* 13. Garfield Park Conservatory.
    * We had a little mishap with the tickets, but fortunately they let us in. It was such a relief to see all the plants and vegetation in the dregs of spring. My favorite had to be the desert room with all the different species of cacti.

* 14. Tree climbing!
    * More monkey business!

* 15. Board games with Jeremy and Henry.
    * We had a blast playing 7 Wonders and jamming out to some music with Henry on the guitar.

* 16. Cleaning your apartment with Smitha.
    * Complete with some dancing and classic rock.

* 17. Northerly Island.
    * Northerly Island is such a hidden gem. Probably one of the only places in the city that still feels relatively untouched by civilization. There's something special about that. Oh, and we took some fire pics!

* 18. Concussion + mono 
    * The double gut punch. You were so tough though, you still absolutely killed it on your finals!

* 19. Trip to RW
    * It was fun to show you where I grew up, despite the limited things to do. I loved our last day, where we hiked the bluff at sunrise (okay, a little after sunrise) and hammocked.

* 20. Climbing up on the parking structure.
    * I had the huge gash in my knee so I didn't join you in the climbing, but it was still a gorgeous view.
 
* 21. Morning breakfast and snuggles.
    * I cherish the our lazy Sunday mornings of snuggling and making a yummy breakfast. It feels so good to spend the whole day together with nothing else to get in the way.
 




