# Date Ideas

## Sushi + Jazz

* Tuesday 6/14
* Chicago Cellar Boys at the Green Mill
    - 8-11 pm
    - https://www.chicagojazz.com/jazz-calendar
* Agami Sushi
    - 4-9:30 pm
    - https://www.agamisushi.com/menus
