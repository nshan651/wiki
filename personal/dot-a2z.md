# I love you from A to Z

Dot,
One year already??? 
I've lost track of time.

Please don't get your hopes up,
not all this will rhyme.

We've shared many laughs 
and shed lots of tears.

Here's to another year,
of love, laughter, and cheer.

Love you Dora! Here are 26 words (from A to Z) that I would use to describe you and our relationship this past year:

* A is for Ameliorate:
    * Meeting you a year ago definitely ameliorated my life
* B is for Bibliophile:
    * For your acquired love of reading and books
* C is for Circumambulate:
    * To walk around something. For taking a stroll around the sun with me
* D is for Demarcate:
    * To set the boundaries of; delimit. I love that in our relationship, we communicate openly and don't set boundaries on what we discuss
* E is for Epiphenomenon:
    * A secondary phenomenon that results from another. Your love of vegetables caused me to appreciate them much more
* F is for Felicitousness:
    * Your life is marked by happiness and good fortune
* G is for Gastronomy:
    * You are an expert in the art of good eating and cooking
* H is for Horticulture:
    * You treat your body and mind as a garden, always looking to cultivate and grow yourself in new ways
* I is for Inquisitiveness:
    * I love that you are always curious and asking questions about the world around you
* J is for Juxtaposition:
    * The juxtaposition of before and after I met you is great
* K is for Kleptothermy:
    * I don't even mind when you steal my body heat.
* L is for Lithological:
    * Pertaining to the character of a rock. You're the rock that I lean on.
* M is for Metastasize:
    * What happens to the warm fuzzy feeling I get inside whenever you're around.
* N is for Nitroglycerin:
    * You're the bomb!
* O is for Ontological:
    * For all of our deep philosophical debates.
* P is for Phantasmagoria:
    * Our time spent together feels like a dream come true.
* Q is for Quantify:
    * It's hard to even quantify how excited I am to spend yet another year with you!
* R is for Rhododendron:
    * You're beautiful as a rhododendron.
* S is for Sanguine:
    * For being optimistic and cheerful.
* T is for Transmutation:
    * We are able to turn even the worst situations into positives.
* U is for Utilitarianism:
    * All of our discussions on Utilitarianism.
* V is for Vectors:
    * Because our relationship has given me direction and magnitude
* W is for Witticism:
    * Your cleverness.
* X is for Xenophile:
    * A person attracted to that which is foreign, especially foreign peoples, manners, or cultures. You are always eager and willing to try new things and experiences. 
* Y is for Yoctosecond:
    * One septillionth ($10^-{24}$) of a second, or the amount of time it took me to fall in love with you!
* Z is for Zeal:
    * For all the fervor I have for you!
