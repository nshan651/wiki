# Books List

## Science

* Helgoland: The World of Quantum Theory - Carlo Rovelli

### Computer Science

* An Introduction to GCC - Brian Gough
* The Mythical Man-Month - Frederick Brooks Jr.
* The Cathedral & the Bazaar

### Mathematics

* Algebraic Topology - Allen Hatcher
* Glass bead game - Herman Hesse

## Philosophy

* Rationality for Mortals - Gerd Gigerenzer
* The Just City - Jo Walton
* Hannah Arendt
    - Origins of Totalitarianism 
    - Eichmann in Jerusalem 
* Jean-Jacques Rousseau
    - The Social Contract
* Max Weber
    - The Protestant Ethic
    - Economy and Society
* Ride the Tiger - Julius Evola
* Better to Never have Been - David Benatar

## Psychology

* Aion - Carl Jung
* Predicatably Irrational - Dan Ariely

## Sociology/Anthropology

* Emile Durkheim
    - Suicide: A Study in Sociology
    - The Division of Labor in Society
* The Timeless Way of Building - Christopher Alexander

## Religion

* The Elementary Forms of Religious Life - Emile Durkheim

## Economics

* Economic Calculation in the Socialist Commonwealth - Ludwig von Mises
* Rational Decisions - Ken Binmore

## Nonfiction

* Rise of the Warrior Cop - Radley Balko
* The Meaning of the Library: A Cultural History - Alice Crawford
* After Babel - George Steiner

## History

* How to Hide an Empire - Daniel Immerwahr
* New Rome: The Roman Empire in the East - Paul Stephenson
* Albion's Seed - David Fisher

## Fiction 

### Science Fiction

* The Wind's Twelve Quarters: Short Stories - Ursula Le Guin
* Instantiation - Greg Egan
* The Three-Body Problem - 
 
### Literary

* Salman Rushdie
    - Languages of Truth
    - Midnight's Children
* The Aleph - Borges
* Foucault's Pendulum - Umberto Eco
* Colson Whitehead
    - The Underground Railroad
    - The Nickel Boys
    - The Intuitionist
    - Harlem Shuffle
* Stephen King
    - The Mist
    - 11/22/63
    - The Gunslinger
* Jack Kerouac
    - On the Road
    - Big Sur
    - The Dharma Bums
* Albert Camus
    - The Stranger 
    - The Fall
    - The Myth of Sisyphus
* The Hellbound Heart - Clive Barker

### Short Stories

* Standard Loneliness Package - Charles Yu
