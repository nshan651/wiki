# Thoughts

* Inductive learning:
    - Start by doing and *experimenting*, form set of heuristics that solidify into formal rules
    - Instant feedback is helpful
    - Sometimes it is useful to work backwards and think about the inverse of a problem
    - Feynman Technique: teach something you want to understand better
    - Learning should be like a game

* Don't let perfectionism become an identity; it leaves little room for failure and growth
    - It deludes one to chase unreasonably high standards
    - Perfectionism destroys confidence. It's better to fail confidently and learn from it
    - Don't rest on your successes

* Mind over matter
    - Positively reinforcing thoughts and self-confidence lead to palpable changes
    - The mind is adaptable and open to change

* We spend energy when it's available, in accordance with the laws of entropy

* Self-actuation, personal narrative
    - sisyphus55: should we commit ego death?
    - Research the topic of Self-disorder, ego death
    - Hypothesis: the world would be a more postive place if we all committed ego suicide
    - pros and cons of self-disorder
        - pros
            - Absoulutely always empathetic in the sense that one thinks about things from another perspective. However, this perspective is one of disconnect, neutrality
            - Outstanding ability to self-reflect, consider one's purpose
            - Looking at oneself from 30k feet. Almost like in Flatland where the sphere *lifts* the square out of flatland and into geometry land
        - cons
            - Constant feeling of disconnection
    
* On doing difficult things
    - At first I believed that I was doing hard things for myself to please others and to fulfill expectations
    - I couldn't even articulate why I felt that this was incorrect, nor that I did not feel like it was an attempt at self-validation
    - I now feel that it was rather that putting myself into physically or mentally strenuous situations are what put my own being as the central narrative
        - In those situtations, the meta thoughts about me thinking about myself went away, and I just **was** myself
        - In times of stress I think about myself in relation to such things quite naturally, and my inner dialouge becomes more effortless
        - I do believe that I always have had an inner dialogue, but there are sustained periods where there is noise that causes my inner self to constantly stumble and hesitate
        - Pushing myself brings out my inner narrative, and consequently puts me in greater touch with the people around me

* Some topics are too difficult for me to even put into words, to exercise my working memory on
    - See if the blog topic below of memorizing for a month helps
    - Seeing things as a written thoguht solidifies that it is a thought of my own, which gives comfort
              
* Do not lose the thread, continue the trains of thought

* Follow the thread
    - Be a productive force, even if the direction of your energy does not always lead to something fruitful
    - Don't be afraid to share things, throw things out there

* Deliberately set aside time to write, to read, to think
    - Don't just tell yourself that you will, *actually fucking do it*
    - Craft the perfect plan to maximize your time on this earth 

* Metaphysics
    - You are a collection of cells that persists for an insignificant flash in the infinitudes of time, live however the hell you want
    - Do something impactful. Have empathy. Be a better human
    - Learn what it means to be yourself and a human being
    
* **Fight** for what has meaning, persist with relationships
    - Constantly check in, call, strenghthen, build upon

* Blog Topic: Be a producer
    - Cell phones and streamlining in tech have turned us into consumers. Utilize platforms to share and create new knowledge/art
    - Have cell phones pacified us? Find short story about the future society where everyone wheres shock collars to prevent thougt

* The best high? Wellness
    - Getting high is just an artificial way of inducing introspection
    - Best way to create sustainable happiness is sleep, balanced diet, exercise, self-reflection, sunlight
    - People will try anything except what works
        - The secret is not at all a secret to anyone, yet it is a truth that we refuse to believe
 
* On the subject of getting high...
    - Getting high is an excuse to act as your authentic self
        - Why not just be yourself all the time? People around you want it, you want it, but you're too scared
    - People who are assholes and do terrible things when they are high/intoxicated are terrible people at heart
        - An proxy with which to defer terribles deeds onto
        - Those actions were still within you, still produced you to act on them
        - Nothing, no substance or experience, can fundamentally change you in such a way where you become incapable of rationalizing

* Stop waiting for inspiration
    - Build on momentum and enjoyment. Waiting for divine inspiration leads to inaction and stress
    - Lighten the allostatic and just do

* Who cares if you fail?
    - Fail often and fail confidently, stop attacking yourself for doing things sub-optimally
    - Don't let yourself freeze up

* On confidence
    - Confidence is an unbroken chain of loyalty in the self
    - Trust yourself and others will trust you
    - Don't try to think about how you are percieving the situation, just **be** 
    - Dumb people are successful because they are confident, smart people fail because they lack confidence
        - Of course, the converse is also true. Confidence in oneself leads to results
    - Self-actualizing and comfortability are an excellent first step
        - Now, recognize that you have important things to say that **need** to be said by you
    - Get used to people diagreeing with you, and especially...disagreeing with your past self
        - Recognize situations where you were wrong and need to reevaluate
        - Treat these moments as an opportunity to reify your self-confidence, lack of ego when recontextualizing
    - Diagree with others
        - Recognize when you need to double-down and defend your beliefs, without letting it lead to insecurity
        - Defending your values and cloistering yourself with your insecurities are **not** the same

* Why do so many people produce things for others' sake and not for their own?
    - How could you not love your life and what you are doing?

* Blog Topic: The importance of memorization
    - As a kid, I went to a Lutheran elementary school where each week we were required to memorize certain passages of the bible
        - By the end, I memorized most of the book of Phillipians
    - I do not believe in memorization for the sake of school (dates, medical terminology, etc.)
        - But, I want to learn more about how memorization as a mental *exercise* affects the brain
            - Positive benefits?

* Blog Topic: On the importance of written records
    - Refer to Borges' story of the man who never forgets, and how his brain allows him an almost encyclopedic, codified almanac of events
    - Most do not have this gift. The deliberative process of intellectual growth must involve writing
        - Need to boil down the most succinct thoughts and record them
    - This ties in with producing
        - We get tangled in our own mindscape, and assume that people know the things that we do
        - Why am I not more eager to share these wonderful things

* Don't fear sharing thoughts that have been produced before, for fear of being unoriginal
    - Within reason, production and sharing is always a good thing
    - Even if re-treading old ground in some respects, **someone** out there has not thought of it in the same way that you have, or even at all
    - Share for their sake
    - Relates to the 
    
* It's okay to leave some things as personal interior thoughts
    - Like this file

* The beauty of language is that it's reactive and can be curated to form meaning, especially the written word
    - The written word has such an austere beauty, a mind-bending infinitude that transcends across time
    - Exists as a time capsule of the inner machinery
    - It allows us to speak with the greatest women and men in history
        - We can directly talk to artists, scientists, leaders...
        - Rather unlike any other form of communication
    - Writing is deeply personal, yet infinitly more communal than any other medium

* Life is a lot like rock climbing
    - Path of discrete decisions you must face
    - Being successful means treating the discrete *handholds* as a decision tree of fluid, continuous actions
    - We have a finite amount of time and energy, and thus a limited time to deliberate
    - Make deliberations intentional and with great purpose

* Worship yourself and rejoice in your capacity for action

* Righteous anger
    - Be angry about injustices done toward you
    - Manifest those thoughts, share with others

### Stream of consciousness entry/rant
    - Loyola's cs department sucks
    - There are no expectations communicated nor articulated to us
    - Why even have class if there is no theory/praxis nor coding discussed, only incoherent rambling concerning a dried-up mixture of theory and practicality
        - Heavier/more hard-hitting theory, more code examples too
    - I have almost never gotten a poor grade and felt that it was a result of my own ineptitude. Nearly every instance has been a poor communication of instructions/standards
    - Online resources are **vastly** superior to most of the lecture materials
        - Part of the reason for this is the abject lack of passion in profs' code
        - How the fuck can you have the passion to lead you to academia yet have such absolutely mind-bendingly terrible coding style???
        - No comments/documentation, inconsistent use of spacing/tabbing, what???
        - Absolutely cannot believe that I am paying 32k a year for this
        - CS study should be 2 years of fundamental learning/theory of computing, and 2 (plus) years of independent learning at one's own pace
            - How have I learned more from a finance club/10-week research/indpendent study than in almost 3 years of schooling
        - I hate that we have to take this ridiculous core where we talk about the mere shadow of an intelligent idea
            - Not a single person wants to be there. Every single kid that sits in front of me is playing some absurd browser game on there laptop
            - Why do we keep telling ourselves that this is the education system working for us
            - Some people just don't give a fuck and don't want to learn. Fine. Don't make them
            - Let them persist in being uproductive, and let those that want to create and improve continue to do so. Why create the false premise of education
            - Why can't I just take whatever the fuck I am interested in?
        - There is so much fucking bureacracy in this whole institutionalized education cartel; I feel like a character in Kafka's short story *The Castle*, where a land surveyor arrives to a nearby town's summons, but discovers that no one knows who requested him
        - Kafka had a lot of great thoughts on the ridiculousness of bureaucracy, and I feel like I am constantly comparing his work to education
    - Everything I have learned has been a product of my own effort/volition, I cannot credit any institution for that (especially not Loyola)
    - Why are these archaic ways of doing things still around, when we have such opportunity for academic agility

* Exams where you cannot go back to previous questions --> **HATE**

* RFID: Chip reader
    - gov wanted to put it in currency
    - In passports, *maybe* realIDs
        - Cover of passport is radio-opaque, can only be read when it is open

* Michaelangelo on the David:
    - *When asked how it was created*: "I just cut away every part that doesn't look like David"
