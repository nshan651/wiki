# Goals

### Set up an email server
    - Move away from gmail

### Cloud + physical storage solution

### Crypto: Monero and BTC

### Clean up git repos
    - Remove commits that expose API keys!
    - Move large files on git to outside the repos
   
### Git-Annex

### Move this over to org mode Emacs?

### Secure Firefox
