# Presentation Notes

* Note: to compile this, use command `pandoc --extract-media . lua-presentation.md -t beamer -o lua-presentation.pdf`

* Lua:
    * Created in 1993 at the University of Brazil
    * Means "moon" in Portugese
* Lightweight (~150KB) 
* Virtual machine maps closely to C
* Considered to be the fastest (or one of the fastest) scripting languages in the world
* Ideal for emedding into apps
* Real world production Lua:
    * Roblox:
        * Lua is embedded INTO THE GAME, so users can create their own content very easily
        * Basically, Roblox is Turing complete
    * Activision Blizzard uses it in World of Warcraft
    * Neovim
* Features of Lua:
    * 21 reserved keywords
    * 1 data structure - tables!
* Tables can represent anything:
    * arrays, dictionaries, graphs, trees, etc.
* Supports coroutines, although technically the language is single-threaded
* Standard library is VERY limited
* Large ecosystem thanks to the Luarocks package manager
* No class keyword, but you can easily add class-like structures
* Functions are first-class objects, which means they can be passed to other functions and can support a functional programming paradigm
* Table is technically an *associative array*
* Arrays start at 1
* Lua has a very easy-to-use C API
    * Allows you to run C code in Lua and vice-versa

## Lexical Conventions

* Reserved keywords:

```
     and       break     do        else      elseif
     end       false     for       function  if
     in        local     nil       not       or
     repeat    return    then      true      until     while

```

## Small Language

> The tarball for Lua 5.4.4, which contains source code and documentation, takes 353K compressed and 1.3M uncompressed. 
> Under 64-bit Linux, the Lua interpreter built with all standard Lua libraries takes 281K
 
## Submission Guidelines

1. (1-2 slides) Background and rationale, based on corresponding chapter from the text by Biancuzzi (or other relevant sources)$^{[1]}$ 
2. (1 slide) Brief code sample (going beyond "hello world" by including input and/or processing logic, e.g., average of numbers entered on standard input or a simple interactive click counter)
3. (2-3 slides) Classification with respect to this$^{[2]}$ taxonomy and these principles (https://lucproglangcourse.github.io/principles.html)
4. (2-3 slides) Evaluation with respect to these criteria (please see this reference for details): 

    Readability
    Writability/Productivity
    Reliability
    Cost
    Others, such as
        Portability
        Generality
        Well-definedness
        Community, books, industry backing, etc.
        Major projects (e.g., C used for Unix and lots of other stuff)
        Ecosystem (availability of tools and libraries for the language or the platform it represents)
        Coolness

5. (1 slide) Conclusion
6. Questions and/or discussion

Texts: 
* Masterminds of Programming : Conversations with the Creators of Major Programming Languages
by Federico Biancuzzi and Chromatic Chromatic
* Programming Paradigms for
Dummies: What Every
Programmer Should Know
Peter Van Roy

