---
theme:
- Berlin
---

# Knowledge and Technology

## Question: 

Is technology making us dumber?

# Extended Cognition

\begin{itemize}
    \item<1-> Extended cognition:
        \begin{itemize}
            \item<1-> What is it?
            \item<1-> How does it differ from "regular" cognition? 
        \end{itemize}
    \item<2-> Calculators:
        \begin{itemize}
            \item<2-> Extended processes put distance between oneself and the answer
            \item<2-> In a way you are accepting that the layers of abstraction between yourself and the answer are valid, and that the answer is true
        \end{itemize}
    \item<3-> Biotech that bridges the gap between ourselves and the cognitive process
        \begin{itemize}
            \item<3-> Lenses correcting vision
            \item<3-> Neuralink
        \end{itemize}
\end{itemize}


# Extended Knowledge

* What about **extended knowledge?**:
    * Is there such a thing?
    * Calculators: Does it count as knowledge if the computation doesn't happen inside our heads?

# Knowledge and AI

* Artificial Intelligence:
    * We are extremely dependent on AI systems for banking, medicine, and entertainment
    * How much can we know?
    * Black box problem

# Conclusion

* Whichever side you take, we are epistemically dependent on technology

