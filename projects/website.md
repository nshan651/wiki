# Personal Website Notes

## General Notes

* **Use Brave browser for testing new styling!!!**

* Image example:

```html
{{ $original := .Page.Resources.GetMatch (.Get 0) }}
{{ $options := .Get 1 }}
{{ .Scratch.Set "image" ($original.Fit $options) }}
{{ $image := .Scratch.Get "image" }}
{{ $title := .Get 2 }}
<img src="{{ $image.RelPermalink }}" width="{{ $image.Width }}" height="{{ $image.Height }}" alt="{{ $title }}">
```
 
## Hugo

* Add a new post:
    * hugo new posts/*name-of-post*.md
* To start a webserver:
    * `hugo server -D --disableFastRender`

## Colorscheme

* Decided to go with `everforest medium dark` pallete

```css
primary {
    bg0:           #2f383e   
    bg1:           #374247
    bg2:           #404c51
    bg3:           #4a555b
    bg4:           #525c62
    bg5:           #596763
    bg_visual:     #573e4c
    bg_red:        #544247
    bg_green:      #445349
    bg_blue:       #3b5360
    bg_yellow:     #504f45
}

light {
    bg0:        #fdf6e3  
    bg1:        #f3efda   
    bg2:        #edead5
    bg3:        #e4e1cd
    bg4:        #dfdbc8
    bg5:        #bdc3af
    bg_visual:  #eaedc8
    bg_red:     #fbe3da
    bg_green:   #f0f1d2
    bg_blue:    #e9f0e9
    bg_yellow:  #faedcd
}
 ```
