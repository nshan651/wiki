# Shell Notes

### Setup static IP

https://www.makeuseof.com/raspberry-pi-set-static-ip/

## Quickly find and replace text in nested directories

1. `cd` into desired parent directory
2. `grep -RiIl 'search' | xargs sed -i 's/search/replace/g'`

### Quick Access

* `$ awk '/<g fill/{c+=1}{if(c==2 || c==6){sub("<g fill=\"#ffffff\"","<g fill=\"none\"",$0)};print}' input.svg > output.svg`
 
## Awk

* Find and print nth occurance of a pattern
    * `$ awk '/jack/{c+=1}{if(c==2){sub("jack","jill",$0)};print}' file`

## Sed

* Find and replace nth occurance of a pattern
    * `$ sed ':a;N;$!ba;s/jack/jill/2' file` 
      
      
      


