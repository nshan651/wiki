# List of potential OSS contributions

| Project           | Language | Description                         |
|-------------------|----------|-------------------------------------|
| Alacritty         | Rust     | OpenGL Terminal Emulator            |
| *rustlings*       | Rust     | Intro Rust exercises                |
| RustScan          | Rust     | Port Scanner                        |
| eureka            | Rust     | CLI tool to write ideas in terminal |
| static-web-server | Rust     | Asynchronous web server for fs      |
| *astro-rust*      | Rust     | Astronomy in Rust                   |
|-------------------|----------|-------------------------------------|
| lua-log           | Lua      | Asynch logging library              |
| Pegasus.lua       | Lua      | http web server                     |
| *ansicolors.lua*  | Lua      | print to the console in color       |
| Lumen             | Lua      | coroutine multitasking env          |
| luaproc           | Lua      | concurrent programming library      |
| *luajson*         | Lua      | JSON parser/encoder                 |

## List of Contributions for COMP 312

### Top 5

1. yfinance:
    - Used it before, and already familiar with the API
2. OpenAssistant:
    - Very new project, a lot of the open issues are just calling for datasets and other non-coding stuff
3. youtube-dl:
    - cli program to download yt videos. I've used this and it's awesome
4. Tqdm:
    - Progress bar for loops and CLI. I'm using this for something right now, it's pretty neat
5. sqlite3, pymongo, redis:
    - Database stuff

### Other Options?

- DuckDB (C++):
    - Ok, this one's written mostly in C++, but it's a python library with some of its source code in python. I'm really interested in this project, so could be fun to work on
- Dash:
    - Plotting library for python
- Arrow:
    - Pretty cool library that gives you better datetimes
- Yapf:
    - Python formatter
- Graphene:
    - python GraphQL framework
- PythonRobotics or rospy:
    - The only two robotics libs I could really find for python. The first one is basically just some algo examples, so idk if these will be that fun
- Ansible:
    - IT automation
