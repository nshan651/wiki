# Project Ideas

## Rust Projects

1) Write a Lisp dialect in Rust
2) Writing a compiler (Could build off of what we did in PL with Scala maybe?)
3) Something with OneAPI. I feel like George and Laufer could offer some direction on this, but I'm sure we could do some sort of distributed systems/HPC project

* [Ray Tracer](ray-tracer) 
* Password entropy analyzer
* Compiler Lua compiler written in Rust
* [Web browser](web-browser)
* Image decoder
* Drivers
* Web server:
    * https://beej.us/guide/bgnet/
* Something with IPFS
* P2P video app using WebRTC
    * XMPP
    * Matrix

* Ropes data structure
* Pinetime watch from pine64

## Projects

* [Excite](excite) 
    - Minimalist citation engine written in JS
    - No ads, just citations
    - CLI? Website? Electron App?
    
* Markdown-based resume

* [fingraph](fingraph)
    - Finance visualizer using InfluxDB

* Doctrine
    * Ncurses-based offline documentation
    * Could include code docs and stack overflow queries
    * Allow for custom inputs

* Interactive Calc (IC)
    - Programming language specializing in interactive math
    - Inspired by `J`
    - Data structs based on matricies 
    - Terse syntax ideal for speedy on-the-fly calculations

* Euadamo (Based on Greek word "Eudaemonia"):
    * Wellness app based on principles of stoicism

* Privacy-focused period tracking app
 
* Travel planning website
    - World map that allows the creation of different "nodes," with each node representing a destination.
    - Nodes and edges can be labeled with activities, times, etc.
    - Simple yet effective way to plan a trip
    
* Laser Chess
    - Chess pieces represented by light trails, show past n moves
    - Could be visually interesting, could also be useful
    - Visualize different board states by showing the *entire* play at once, not just each individual move

* Words with Woods 
    - Site that flips through a random billy woods or ELUCID quote
    - Accompanying pictures, database of pre-selected quotes

* Public domain custom books
    - eCommerce site dedicated to a small collection of public domain prints
    - Custom covers and typesetting

 
