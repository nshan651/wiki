# Yahtzee

# TODO

## Pygame Integration

* Title slide and game over slide

* Singleplayer/multiplayer select

* Keypress event handlers for rerolls, freezes, selection

* Scorecard marking

* (Optional) dice roll animation

## Singleplayer Bot

* Bot selects based on *optimal* strategy

* (Optional) different difficulties

## Core Features

* Add bonuses/special cases in scoring
