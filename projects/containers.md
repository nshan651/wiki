# Container Notes

## Podman

### Build

* Build an image:
    * `podman build -t nshan651/myimage .`
    * `--no-cache` flag to rebuild from scratch
* List images:
    * `podman images`

### Run

* Create a container from an image:
    * `podman run -it --name mycontainer myimage`
    * To create a backround container:
        * `podman run -d --name mycontainer myimage`

### Stopping/Removing Containers
 
* Stop a running container:
    * `podman stop mycontainer`
* Remove a container:
    * `podman rm mycontainer`
    * Use the `-f` flag to force remove a running container
