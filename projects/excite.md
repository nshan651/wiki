# excite

## TODO

* [X] Add main.lua to excite.lua
* [ ] Properly switch api from crossref to official doi site

## Notes

* The argument `INPUT_KEY` is must be sliced as `input[1]`
    * Changed this so that this slice only occurs in top-level excite.lua
    * This makes it more testable and legible, as subsequent uses of it don't require slicing

## Pseudo Code For Removing Require Statements

```python
file_list = "SRC_DIR/*.lua"

for file in file_list:
    basename = file.base
    target = f'local {basename.capitalized} = require \"{basename}\"'
    os.execute(f"sed -i 's/{target}//g'
```
