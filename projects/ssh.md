# SSH 

## SSH Tunneling

- Inspired by [this](https://iximiuz.com/en/posts/ssh-tunnels/)
- Install OpenVPN:
    - https://wiki.archlinux.org/title/OpenVPN
- Encryption:
    - https://wiki.archlinux.org/title/Easy-RSA
- Guide to secure computing:
    - https://www.secure-computing.net/openvpn/howto.php#pki
- Linksys Smart Wi-Fi:
    - `http://10.102.83.204/ui/1.0.99.182871/dynamic/login.html` `http://10.102.83.204/`

## Remote SSH 

1. To set up a remote SSH tunnel on your Raspberry Pi 4, follow these steps:
    - Start by enabling SSH on your Raspberry Pi 4. You can do this by running the following command in a terminal window:

```sh
sudo systemctl enable ssh
sudo systemctl start ssh
```
4. Next, determine the port number you want to use for the remote SSH tunnel. For this example, we will use port 9000.
3. From your Raspberry Pi 4, open a terminal window and run the following command to create the remote SSH tunnel:
    ```sh
    ssh -R 9000:localhost:22 <username>@<public_ip_address>
    
    # This command gets the router's public ip address:
    # curl ifconfig.me
    
    # Run this 
    ssh -R 9000:localhost:22 pi@96.87.72.41
    ```
    - Replace <username> with your username on the Raspberry Pi, and <public_ip_address> with the public IP address of your router. If your router has a dynamic IP address, you may want to use a dynamic DNS service to get a domain name that always points to your router.
4. Once the SSH connection is established, leave the terminal window open and connected to the Raspberry Pi.
5. From a different network, open a terminal window and run the following command to connect to the Raspberry Pi via the remote SSH tunnel:
    ```
    ssh <username>@localhost -p 9000
    
    # Run this
    ssh pi@localhost -p 9000
    ```
    - Replace <username> with your username on the Raspberry Pi.
6. You should now be connected to your Raspberry Pi via the remote SSH tunnel. You can use this connection to run commands on the Raspberry Pi as if you were sitting in front of it.
- Note that you may need to configure your router's firewall to allow incoming traffic on the port you have chosen for the remote SSH tunnel.

### Other answer

```
# On pibox to create the tunnel (wrong command!)
ssh -f -N -T -R 2222:localhost:22 user@your_raspberry_pi_ip

# Right command (???)
ssh -f -N -L 2222:localhost:22 user@your_raspberry_pi_ip
# Run this:
ssh -f -N -L 2222:localhost:22 pi@10.102.83.113

# Access the tunnel
ssh user@localhost -p 2222
```

- Here, "2222" is the port number that you will use to connect to the remote machine, "localhost" refers to the Raspberry Pi, and "22" is the port number used by the SSH service. Replace "user" with the username of the Raspberry Pi and "your_raspberry_pi_ip" with its IP address.

## Pivpn

- `pivpn add`
- 7

## Instructions

- Source:
    - https://www.tunnelsup.com/raspberry-pi-phoning-home-using-a-reverse-remote-ssh-tunnel/

- Remote ssh:

```
# Remote ssh using router ip
ssh -N -R 2222:localhost:22 96.87.72.41 

# Access it like so

ssh -l piUser -p 2222 localhost
```

$ ssh -fNT -R  20022:localhost:22   source-user@202.10.135.4


`ssh -f -N -L 2222:localhost:22 pi@96.87.72.41`
