# Notes on Rust

## Snippets

* Ordering enum has three possibilities when comparing two values:
    * Less than, greater than, or equal to

```rust
use rand::Rng;
use std::cmp::Ordering;
use std::io;

fn main() {
    
    // ...other code above
    
    println!("You guessed: {guess}");

    match guess.cmp(&secret_number) {
        Ordering::Less => println!("Too small!"),
        Ordering::Greater => println!("Too big!"),
        Ordering::Equal => println!("You win!"),
    }
}
```

* Convert String to Int:
    * This is variable shadowing, where you redeclare a variable as a diff type
    * Handles invalid input

```rust
let num: u32 = guess.trim().parser().expect("Please type a number!") 
{
    Ok(num) => num,
    Err(_) => continue,
};
```

* Function arguments **must** have a type annotation:
    * `fn print_labeled_measurement(value: i32, unit_label: char)`

* Expressions and statements:
    * A statement does not have a return value, expressions do
    * Leave out the semicolon to allow for the return of an expression 
    * i.e. `x+1` vs `x+1;`
* Loop through collections:
    * `for element in a {
        println!("the value is: {element}");
    }`

