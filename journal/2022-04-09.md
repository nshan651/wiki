# 2022-04-09

* Most people have politcal heresies in their brains
    - Even the far right and left

* Positive self-talk
    - But keep it genuine
    - Must be true to who you actually are

* Point to a time that you felt like you were at your best, and at your most confident
    - Practice saying these things so they become familiar
    - Value who you are

* **Confidence and self-advocacy are probably the largest predictors of success**

* Boredom *literally* isn't real
    - If you find yourself getting bored, you should probably check yourself
    - Not even close to enough time to experience everything in this life
