# 2022-03-31

* Memoryless Property of the Exponential Distribution
    - Property that the probabilty of some future event occuring is not affected by the occurrence of past events
    - Applies to exponential distribution with non-neg real numbers

