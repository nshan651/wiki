# 2022-03-23

* If you understand the "why," it becomes much easier to understand "how" - Neitzsche

* Make a list of installed programs/packages 
* Switch from ranger to lf

* Internet cable boom

* Million dollar idea: dating show using match-making algorithms

* Work on scripts

* Do captchas even really work?
