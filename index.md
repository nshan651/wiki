# Wiki Index

- moneyguidepro

- Vertico + Consult + Orderless + Embark + Marginalia + Corfu.
- Use Quicklisp to load personal projects from arbitrary locations:
    - `(pushnew (uiop:getcwd) ql:*local-project-directories*)`

## Table of Contents

- 📃 [Class Notes](#class notes) 
- 🎯 [Work](#work)
- 🌎 [Blog](#blog)
- 📟 [Programming](#programming) 
- 📊 [Research](#research)
- 📚 [Books](#Books)
- 🏦 [Finance](#Finance)
- 📜 [Journal](#journal)
- 🧭 [Travel](#travel)
- 🔒 [Personal](#personal)
 
## Class Notes

- [ENVS 101](classes/envs101/index.md)
- [COMP 312](classes/comp312/index.md)
- [ANTH 101](classes/anth101/index.md)
- [COMP 301](classes/comp301/index.md)
- [COMP 339](classes/comp339/index.md)

## Work

- [Work](work/index)
 
## Blog

- [Posts](blog/Posts)
- [Blog Ideas](blog/topics)

## Programming 

- [Dot's Thesis](thesis)
- [Projects](projects/projects) 
- [Colors](projects/colors)
- [Containers](projects/containers)
- [Rust Notes](projects/learn-rust)
- [SSH](projects/ssh)
- [webserver](projects/webserver)
- [Website](projects/website)
- [Paper a Week (PAW)](paw/index.md)
- [OSS Contributions List](projects/contributions)
- [Shell Notes](projects/shell-notes)

## Research 

- [Research Notes](research/index)

## Books

- [Reading List](personal/books.md)
- [Personal Library](library/index) 

## Finance

- [Personal Finance](finance/index)

## Journal

- [Journal Index](journal/index)

## Travel

- [Travel Index](travel/index)

## Personal 

- [dad](personal/dad)
- [acc](../private/acc.md)
- [Housing](personal/housing)
- [A to Z](personal/dot-a2z.md)
- [Dot's card](personal/dot-card.md)
- [Dates](personal/dates)
- [Youtube channels](personal/channels)
- [Quotes](personal/quotes)
- [Goals](personal/goals)
- [Random Thoughts](personal/thoughts)
