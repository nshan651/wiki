# PHIL 275 W3 

## Euclid's Elements

* Foundational text in geometry
* Defines several axioms related to geometric figures
* Builds propositions off of those

## Epistemic Rationality

* Form of rationality that concerns truth or true belief
* Connection between ER and knowledge:
    * only epistemically rational beliefs count as knowledge (maybe)
* ER and justification:
    * ER beliefs are justified (rational == justified)
* Proper epistemological norm - norm that is **truth conducive** and **error avoidant**
* Two aims of epistemological rationality:
    * Maximize true belief
    * Minimize false belief
* In a vacuum this is good, but we often have conflicts
* **Only** minimizing false belief leads to just not choosing to believe anything:
    * Does not lead to acquisition of knowledge
* Same with only maximizing true beliefs. Leads to inevitable false beliefs

## Internalism and Externalism

* Internalism:
    * The factors that provide justification must be **Internal** to the subject
    * Must be accessible to the subject's consciousness/awareness
* Externalism:
    * Factors that provide justification need **not** be internal to the subject
    * They may be external. Need not be subject to the subject's consciousness/awareness

## Evidentialism and Reliabilism

* Evidentialism:
    * What justifies as evidence? Must be internalist
    * Evidence that is accessible to the subject
* Reliabilism:
    * What justifies is the reliability of the belief forming process
    * The reliability of the process need not be known to the subject
    * Not even required that the process itself be known to the subject
    * Reliability of the process is **external**
* Both are examples of internalist and externalist views 

## Perception

* Sensory perception
* Direct realism (common sense realism):
    * Direct objects of perception are external to the mind
    * External objects are (mostly) as we perceive them
* Reasons to doubt direct realism:
    * Perceptual error (refraction of light
    * Perceptual illusion (double vision, hallucination, etc.)
    * Secondary qualities (color is just light of a certain wavelength)
        * Ex) Color is an object of perception
        * Other examples: Doppler effect with noise
    * Primary qualities
