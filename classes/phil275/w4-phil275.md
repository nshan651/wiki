# PHIL 275 W4

## Indirect Realism

* Indirect Realism - direct objects of (sense) perception are mental objects (sense data, screenings) that represent rational objects to us - External objects are perceived indirectly using mental objects
* Problems with indirect realism:
    * Skepticism
    * One needs to have experience of how objects *actually* are to ascertain their realism
    * We need to accept direct realism to have any access to the external world
        * Therefore we can *never* compare the mental objects to the physical objects, because we can't experience the latter
    * Judgements about the real world become guesses
* *A priori* - to known independent of experience
    * Empirical truth
* *A posteriori* - to know through experience
* Different kinds of truths:
    * Mathematical
    * Definitional 
    * Logical
    * Philosophical
        * These are sometimes debatable
    * Moral truths
        * Also debatable
* Theorems - built upon a set of axioms

## Induction

* Inductive reasoning:
    * Reasoning *from* what is true of a set of observed cases *to* what is true of all cases of that type
* Enables us to extend knowledge from observed cases to unobserved cases
* Assumes the *inductive principle*:
    * What is true of a sufficiently large and varied set of cases of a certain type is true of all cases of that type
