# PHIL 275 W5

## Science and Knowledge

* Science - comes from the Latin word *scientia*, which means knowledge
    * Similar to the word *epistime*, which also means knowledge
* The problem of demarcation:
    * What characterizes science as science?
    * What distinguishes science from non-science
* This causes challenges with subjects such as creationism vs evolutionary theory
* Landmark case (McLean v. Arkansas):
    1. Guided by natural law
    2. Explainable in terms of natural law
    3. Testable against the empirical world
    4. Conclusions are tentative
    5. Scientific theories are falsifiable
* Science is *naturalistic*
    * Meaning all explanation must be in terms of natural forces and the principles within that govern them
* Scientific knowledge is subject to change, religious dogma is not (as much) 
* Theories must be falsifiable
* Karl Popper
* Verificationism:
    * Mistaken view that science is when theories are formulated and *then* confirmed through gathering evidence
* Falsificationism:
    * Much better, subjecting a theory to falsification
    * Every good theory takes on falsifiable risk

## Religion

* Is there religious knowledge?
* Knowledge *about* religious truths, but not *of* them
* Reasons to doubt
* If God doesn't exist, no one can know that God exists
* God exists argument:
    * Not true, or
    * Not justified, or
    * Neither justified nor true
* Evidentialist challenge:
    * A religious belief is justified/rational only if there is adequate evidence to support it
* What counts as evidence for religious belief?:
    * The evidence must be independent from religious belief or commitment
* Common access to evidence is important in evidentialism
* Natural theology
    * Attempt to demonstrate the existence and nature of God by appeal to natural evidence 
    * Evidence that can be guided by observation and reason
    * Evidentialist challenge can be met
* Anselm(1090) - ontological argument for the existence of God
* Fideism:
    * Comes from *fides*, "faith" in Latin
    * Does not try to meet the evidentialist challenge - it rejects it
    * Religions belief not subject to standard epistemic evaluation (Rationalism justified/unjustified)
    * Demand for justifying evidence is misplaced
* Reformed epistemology:
    * Religious belief is (often) formed spontaneously without regard to justifying evidence
    * Religious beliefs are not unique in this respect
    * As other spontaneous beliefs are considered rational so too should religious beliefs be considered rational
    * Basic beliefs
