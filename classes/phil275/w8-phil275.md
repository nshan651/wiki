# PHIL 275 W8

## Skepticism

* Skepticism:
    * View that knowledge is **not possible**
* Global vs local skepticism:
    * Global - no knowledge at all
    * Local - no knowledge of a particular domain (i.e. the future)
* Skepticism with respect to other minds:
    * No knowledge of the existence or workings of other minds

### Problem of Other Minds

* Problem of accounting for how we know the existence and workings of other minds
* The mind itself is present to its own consciousness
* We have privileged access to one's own mind
* We can interact with someone's physicality, but not their mind
* Do we know others have minds?:
    * Not by direct observation
    * By inference?

### Arguments from Analogy (J.S. Mill)

* I observe:
    * Correlation of my body behavior with mind/consciousness
* I infer:
    * Correlation of body/behavior with mind/consciousness in others 
* Inductive generalization
* Problem - we can only observe *one* case of correlation:
    * Can't generalize from a single observed case
    * Generalization requires *multiple* and *varied* observations
* Therefore, knowledge of other minds is **impossible**

### Similar Minds?

* Can we know that other minds are like our own?:
    * Even if we assume there are other minds
* Inverted spectrum problem:
    * Phenomenological experiences could be completely different between people, and we'd have no way of knowing
* Possible Solution:
    * Attribution of mind/consciousness is spontaneous, basic, justified 

## Global Skepticism

### Skeptical Hypothesis
 
* Hypothesis that we are subject to deception but that all our experience is the same as it would be if we were not subject to deception
* Brain-in-a-vat hypothesis (BIV):
    * You are a brain in a vat
    * Skeptical hypothesis because all our experience is the same regardless of whether or not we are subject to the deception
* Formal argument:
    1. If I am to know that P, I must be able to rule out all skeptical hypotheses that relate to P
    2. But I cannot rule out all skeptical hypotheses that relate to P
    3. Hence I cannot know P
* This is the general argument for skepticism

## Strategies Against Skepticism

* Two strategies:
    1. We can rule out some skeptical hypotheses
    2. We don't need to rule out skeptical hypotheses in order to know
* Why should we accept (1)?:
    * It seems to demanding, unrealistic, restrictive

## The Closure Principle

* Closure principle abstract form:
    * If S knows that P, and if S knows that P entails not Q
    * Then S knows that not Q
* Example:
    * I know that I am in Cuneo
    * If I'm in Cuneo, then I'm not in Crown
    * I'm not in Crown

### Skeptical Hypothesis and the Closure Principle

* It seems that we must accept (1) if we are to agree with the closure principle
* But, if we can't know that P entails not Q, we cannot know that not Q

## Fallibilism

* Fallibilism:
    * Reject (1)
    * One need not rule out **all** skeptical hypotheses in order to know
        * We only need to rule out *relevant* hypothesis
    * Allows us to have knowledge even in the face of the possibility of error

## Objectivism vs Relativism

* Objectivism:
    * View that the truth or falsity of a proposition is independent of our belief
* Relativism:
    * View that truth corresponds to belief
    * Same claim turns out to be true and false
    * Contradictory
