# PHIL 275

## Relativism vs Skepticism

Although relativism and skepticism can both pose challenges to knowledge, I argue that skepticism poses a much more serious existential threat. 

Let us begin by defining the two. Skepticism is a framework for questioning claims to knowledge by considering all possibilities of falsification, however improbable they may be. Relativism holds that there are no objective or absolute truths. 

It is important to note that these two epistemological frameworks are not necessarily opposites. One could hold both skeptic and relativist beliefs. Relativism does not deny the possibility of knowledge outright, it is simply a way of classifying knowledge into several distinct views. On the other hand, skepticism questions whether knowledge of certain types is even possible. In essence, both systems of logic boil down to perspective truth, but skepticism is opinionated on whether certain "truths" should be included under the category of knowledge.

Conversely, skepticism presents an existential threat to knowledge by presenting nearly irrefutable possibilities. In Descartes Meditations on Philosophy, he underscores three main possible deceptions that can refute our knowledge. The first is the deception of the senses, which are not on their own systemically misleading. Generally, there are only certain scenarios where the senses deceive us, such as optical illusions or faraway objects. Next, there is the deception of dreams. Dreams can mirror the experiences of reality, which makes them nearly impossible to rule out. The only thing that remains constant across dreams is our intellectual perceptions. However, even our intellectual faculties are not immune to deception. Descartes describes the possibility of some evil being that deceives all of our perceptions with illusory experiences. This could extend to our intellectual experiences as well.

Although skepticism presents us with a pessimistic view on whether we will ever have fully objective knowledge, it is the framework that will lead us to the asymptote toward truth. Even if we can never fully reach knowledge, having a skeptical eye and constantly testing and rebuilding our core beliefs will strengthen said beliefs, like an athlete breaking down muscle through exercise and rebuilding even stronger through rest and recovery. Conversely, relativism introduces more noise that doesn't necessarily lead us in the right direction. 

If, as Protagoras claims, man is indeed "the measure of all things," we can be lead down many disparate paths of acquiring knowledge. Even if knowledge is an object of one's perception, there is no question over the existence of such knowledge, unlike with the skeptic model. There is the question of whether we can really "know" others, as they are privileged to their own perceptions. This possibility of a philosophical zombie, however, seems to be more of a skeptical hypothesis.
