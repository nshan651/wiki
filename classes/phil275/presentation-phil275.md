# Presentation Notes

## Topic Choices

Oct. 3 – Technology

* Pritchard, What Is This Thing Called Knowledge?, ch. 14

* Sloman & Fernbach, “Thinking with Technology” [sakai]

Oct. 5 – Education

* Pritchard, What Is This Thing Called Knowledge?, ch. 15

## Notes

* Ability to seek out the most salient truths

### Extended Cognition

* Is technology making us dumber?
* Extended cognition:
    * Idea that technology usage is not simply a matter of making use of technology as a tool
    * Rather, it should be thought of as a genuine part of one's cognition
* Socially distributed cognition:
    * Some proponents believe that other people can form part of a joint cognitive process
* Skeptics of extended cognition argue that cognition is a purely internal phenomenon. That is, an internal process that occurs in the brain
* Proponents state that it shouldn't matter where the cognitive process takes place, whether internally or externally
* If there is such a thing as extended cognition, should there also be extended knowledge?

### Extended Knowledge

* The cognitive process looks very different for extended vs non-extended cognitive tasks
* Extended processes put distance between oneself and the answer:
    * In a way you are accepting that the layers of abstraction between you are valid, and that the answer is also true
    * Calculator example
* There are examples of biotech that are intertwined between us and the cognitive process
    * Example of the lenses, NEURALINK!
    * It becomes difficult to disentangle when we are engaging with extended vs non-extended processes
* Cognitive augmentation:
    * View that technology dramatically enhances our cognitive abilities
    * No longer limited by our own biology

### Conclusion

* Whichever side you take, we are still epistemically dependent on technology
* Intellectual virtues - never innate, must be acquired over time through training
* Intellectually conscientious person:
    * Forms appropriate judgements from evidence available
    * Seeks additional evidence when sufficient evidence is not available to them

## Presentation Outline

1. Is technology making us dumber?:
    * Preliminary discussion, get 1-2 comments
2. Extended cognition:
    * What is it? 
    * How is it different from "regular" cognition?
    * Pro and contra positions
3. Extended knowledge:
    * Is there such a thing?
    * Discuss how cognitive process looks vastly different between extended and non-extended cog tasks (Calculator example)
    * Technology that becomes intertwined with our own biology (biotech)
4. Cognitive augmentation:
    * View that tech enhances our cognitive abilities
    * Our own biology is no longer a limitation
5. Brief mention of artificial intelligence:
    * Author doesn't discuss this but I think it's really important to at least mention
6. Conclusion:
    * Whichever side one takes, we are epistemically dependent on technology
