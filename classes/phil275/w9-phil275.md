# PHIL 275 W9

## Theaetetus

* What is knowledge?
* Theoretical vs productive knowledge:
    * Theory:
        * Math, music, astronomy
    * Productive:
        * Cobbling, carpentry, engineering, crafts
* Propositional vs skill
* This categorizes different kinds of knowledge, but it does not tell us **what** knowledge is

## Perception

* Knowledge is perception:
    * In the narrow sense, *sense perception*
* Protagorus (490-420 BCE):
    * "Man is the measure of all things - of things that are, that they are. Of things that they are not, that they are not."
* One in the same thing can not just *appear* different to different people, it can also *be* different to different people
    * How something appears to you **becomes the measure**
* Soy milk and almond milk are not really milks
* Under Protagorus, things are for the individual one perceives them

### Twin Births

* Objects in relation to a sense perception will **be** one thing or the other:
    * Ex.) Eyes seeing an object
    * The object only has a determinant color in relation to one's perception
* One has priviledged access to one's own perceptions:
    * Therefore, things will be judged in relation to one's own perception
* Problem: we are not the only animals with sense perception
* How can anyone be "wise" if everyone holds ultimate authority over their perceptions/reality
    * Wisdom means expertise in an area that others lack
    * Wise person will have better judgements than others    
* Relativist doctrine puts everyone on par 
