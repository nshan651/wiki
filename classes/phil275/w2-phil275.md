# PHIL 275 W2

## The Structure of Knowledge

* Justified true belief (JTB) analysis:
    * According to the model, these three conditions are **independently** necessary and **jointly** necessary for knowledge
* Necessary and sufficient conditions:
    * Necessary - A is necessary for B just in case B cannot occur without A. If A does not occur, neither does B
        * Ex) Citizenship is necessary for voting
    * Sufficient - A is sufficient for B just in case if A occurs B must occur as well
        * Ex) Decapitation is sufficient for death
* `Only if == necessary`
* `if == sufficient`
* `and == independently necessary`
* Gettier: JTB is necessary but **not** sufficient for knowledge
* Agrippa's trilemma:
    1. Our beliefs are unjustified
    2. The chain of justified beliefs is infinite
    3. Or the chain of justified beliefs is circular
        * Every member justifies itself
* Several proposed solutions to Agrippa's trilemma
* Foundationalism:
    * Popular in the middle ages but not really accepted now
    * Basic vs non-basic belief:
        * Basic - axiomatic belief
        * Non-basic - derived belief
* Foundationalist thesis:
    * Some beliefs are basic, and all justified non-basic beliefs get their justification **ultimately** from basic beliefs
* Classical foundationalism:
    * In order to be justified, a basic belief must be **infallible**
        * No possibility of error
    * In order to justify non-basic beliefs, basic beliefs must **guarantee** their truth 
    * Works well for mathematics, not as well for everything else
* Modest foundationalism:
    * Basic beliefs need not be infallible
    * Basic beliefs need not provide a guarantee of truth to non-basic beliefs
