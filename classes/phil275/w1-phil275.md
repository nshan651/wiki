# PHIL 275 W1

## Introduction to Epistemology

* The theory of knowledge:
    * How do we know what we know?
* Episteme - Greek word for knowledge
* Logos - account
* What kinds of knowledge are there?
* Propositional knowledge:
    * Either true or false
    * Has a truth value
    * Different from commands or questions
* Ability knowledge:
    * Know-how
* Acquaintance knowledge:
    * First hand experience or familiarity with something
* Emotion:
    * Doesn't really count as a type of knowledge
    * But can lead to one of the three main types
        * Ex) I feel pain = *I'm aquainted with pain at the moment*
* Knowledge comes from two things:
    * Truth and belief
* The prerequisite for knowledge is:
    1. Belief in the relevant proposition
    2. Belief must be true
        * False belief can be called "mistaken belief"
        * Mistaken belief is **not** a kind of knowledge
* Belief and truth are often mistakenly contrasted
    * Belief is still required for knowledge, that is, belief **in** the truth
* It is important to factor in the element of luck
    * "A broken clock [12-hour clock] is always right twice a day"
    * True knowledge or skill *usually* leads to the predicted outcome
* True belief alone is **not** sufficient for knowledge
* The source of the knowledge is also important

## The Value of Knowledge

* Why should we care if we have knowledge:
    * Because we value truth
    * Truth has instrumental value
* Instrumental value:
    * Value that accrues to something in virtue of some further useful purpose that it serves
* Things that are of intrinsic value are valuable for their own sake:
    * AKA intrinsic value
    * Ex) friendships, health, etc.
    * Valuable for their own sake *as well as* perhaps for some further end 
* True belief is always good *ceterus paribus*
* Not all knowledge has instrumental value
* We cannot argue from the instrumental value of true belief that all value must therefore be instrumental
* Knowledge is more stable than mere truth
* Daedalus statues:
    * True belief may easily run away, but knowledge remains tethered
* Better to have the stability of knowledge rather than merely having true belief
* Wisdom:
    * A kind of instrumental knowledge

## Excerpt From "Meno" by Plato


>PLATO, MENO
>
>[Socrates and Meno have been discussing whether moral virtue can be taught]

>

>SOCRATES: And is there anything else of which the professors are affirmed not only not to be teachers of others, but to be ignorant themselves, and bad at the knowledge of that which they are professing to teach? or is there anything about which even the acknowledged 'gentlemen' are sometimes saying that 'this thing can be taught,' and sometimes the opposite? Can you say that they are teachers in any true sense whose ideas are in such confusion?

> MENO: I should say, certainly not.

> SOCRATES: But if neither the Sophists nor the gentlemen are teachers, clearly there can be no other teachers?

> MENO: No.

> SOCRATES: And if there are no teachers, neither are there disciples?

> MENO: Agreed.

> SOCRATES: And we have admitted that a thing cannot be taught of which there are neither teachers nor disciples?

> MENO: We have.

> SOCRATES: And there are no teachers of virtue to be found anywhere?

> MENO: There are not.

> SOCRATES: And if there are no teachers, neither are there scholars?

> MENO: That, I think, is true.

> SOCRATES: Then virtue cannot be taught?

> MENO: Not if we are right in our view. But I cannot believe, Socrates, that there are no good men: And if there are, how did they come into existence?

> SOCRATES: I am afraid, Meno, that you and I are not good for much, and that Gorgias has been as poor an educator of you as Prodicus has been of me. Certainly we shall have to look to ourselves, and try to find someone who will help in some way or other to improve us. This I say, because I observe that in the previous discussion none of us remarked that right and good action is possible to man under other guidance than that of knowledge;—and indeed if this be denied, there is no seeing how there can be any good men at all.

> MENO: How do you mean, Socrates?

> SOCRATES: I mean that good men are necessarily useful or profitable. Were we not right in admitting this? It must be so.

> MENO: Yes.

> SOCRATES: And in supposing that they will be useful only if they are true guides to us of action—there we were also right?

> MENO: Yes.

> SOCRATES: But when we said that a man cannot be a good guide unless he have knowledge, this we were wrong.

> MENO: What do you mean by the word 'right'?

> SOCRATES: I will explain. If a man knew the way to Larisa, or anywhere else, and went to the place and led others thither, would he not be a right and good guide?

> MENO: Certainly.

> SOCRATES: And a person who had a right opinion about the way, but had never been and did not know, might be a good guide also, might he not?

> MENO: Certainly.

> SOCRATES: And while he has true opinion about that which the other knows, he will be just as good a guide if he thinks the truth, as he who knows the truth?

> MENO: Exactly.

> SOCRATES: Then true opinion is as good a guide to correct action as knowledge; and that was the point which we omitted in our speculation about the nature of virtue, when we said that knowledge only is the guide of right action; whereas there is also right opinion.

> MENO: True.

> SOCRATES: Then right opinion is not less useful than knowledge?

> MENO: The difference, Socrates, is only that he who has knowledge will always be right; but he who has right opinion will sometimes be right, and sometimes not.

> SOCRATES: What do you mean? Can he be wrong who has right opinion, so long as he has right opinion?

> MENO: I admit the cogency of your argument, and therefore, Socrates, I wonder that knowledge should be preferred to right opinion—or why they should ever differ.

> SOCRATES: And shall I explain this wonder to you?

> MENO: Do tell me.

> SOCRATES: You would not wonder if you had ever observed the images of Daedalus; but perhaps you have not got them in your country?

> MENO: What have they to do with the question?

> SOCRATES: Because they require to be fastened in order to keep them, and if they are not fastened they will play truant and run away.

> MENO: Well, what of that?

> SOCRATES: I mean to say that they are not very valuable possessions if they are at liberty, for they will walk off like runaway slaves; but when fastened, they are of great value, for they are really beautiful works of art. Now this is an illustration of the nature of true opinions: while they abide with us they are beautiful and fruitful, but they run away out of the human soul, and do not remain long, and therefore they are not of much value until they are fastened by the tie of the cause; and this fastening of them, friend Meno, is recollection, as you and I have agreed to call it. But when they are bound, in the first place, they have the nature of knowledge; and, in the second place, they are abiding. And this is why knowledge is more honorable and excellent than true opinion, because fastened by a chain.

> MENO: What you are saying, Socrates, seems to be very like the truth.

> SOCRATES: I too speak rather in ignorance; I only conjecture. And yet that knowledge differs from true opinion is no matter of conjecture with me. There are not many things which I profess to know, but this is most certainly one of them.

> MENO: Yes, Socrates; and you are quite right in saying so.

> SOCRATES: And am I not also right in saying that true opinion leading the way perfects action quite as well as knowledge?

> MENO: There again, Socrates, I think you are right.

> SOCRATES: Then right opinion is not a whit inferior to knowledge, or less useful in action; nor is the man who has right opinion inferior to him who has knowledge?

> MENO: True.

> SOCRATES: And surely the good man has been acknowledged by us to be useful?

> MENO: Yes.

>SOCRATES: Seeing then that men become good and useful to states, not only because they have knowledge, but because they have right opinion, and that neither knowledge nor right opinion is given to man by nature or acquired by him— do you imagine either of them to be given by nature?

>MENO: Not I.

>SOCRATES: Then if they are not given by nature, neither are the good by nature good?

>MENO: Certainly not.

>SOCRATES: And nature being excluded, then came the question whether virtue is acquired by teaching?

>MENO: Yes.

>SOCRATES: If virtue was wisdom (or knowledge), then, as we thought, it was taught?

>MENO: Yes.

>SOCRATES: And if it was taught it was wisdom?

>MENO: Certainly.

>SOCRATES: And if there were teachers, it might be taught; and if there were no teachers, not?

>MENO: True.

>SOCRATES: But surely we acknowledged that there were no teachers of virtue?

>MENO: Yes.

>SOCRATES: Then we acknowledged that it was not taught, and was not wisdom?

>MENO: Certainly.

>SOCRATES: And yet we admitted that it was a good?

>MENO: Yes.

>SOCRATES: And the right guide is useful and good?

>MENO: Certainly.

>SOCRATES: And the only right guides are knowledge and true opinion—these are the guides of man; for things which happen by chance are not under the guidance of man: but the guides of man are true opinion and knowledge.

>MENO: I think so too.

>SOCRATES: But if virtue is not taught, neither is virtue knowledge.

>MENO: Clearly not.

>SOCRATES: Then of two good and useful things, one, which is knowledge, has been set aside, and cannot be supposed to be our guide in political life.

>MENO: I think not.

>SOCRATES: And therefore not by any wisdom, and not because they were wise, did Themistocles and those others of whom Anytus spoke govern states. This was the reason why they were unable to make others like themselves—because their virtue was not grounded on knowledge.

>MENO: That is probably true, Socrates.

>SOCRATES: But if not by knowledge, the only alternative which remains is that statesmen must have guided states by right opinion, which is in politics what divination is in religion; for diviners and also prophets say many things truly, but they know not what they say.

>MENO: So I believe.

>SOCRATES: And may we not, Meno, truly call those men 'divine' who, having no understanding, yet succeed in many a grand deed and word?

>MENO: Certainly.

>SOCRATES: Then we shall also be right in calling divine those whom we were just now speaking of as diviners and prophets, including the whole tribe of poets. Yes, and statesmen above all may be said to be divine and illumined, being inspired and possessed of God, in which condition they say many grand things, not knowing what they say.

>MENO: Yes.

>SOCRATES: And the women too, Meno, call good men divine—do they not? and the Spartans, when they praise a good man, say 'that he is a divine man.'

>MENO: And I think, Socrates, that they are right; although very likely our friend Anytus may take offence at the word.

>SOCRATES: I do not care; as for Anytus, there will be another opportunity of talking with him. To sum up our enquiry—the result seems to be, if we are at all right in our view, that virtue is neither natural nor acquired, but an instinct given by God to the virtuous. Nor is the instinct accompanied by reason, unless there may be supposed to be among statesmen someone who is capable of educating statesmen. And if there be such a one, he may be said to be among the living what Homer says that Tiresias was among the dead, 'he alone has understanding; but the rest are flitting shades'; and he and his virtue in like manner will be a reality among shadows.

>MENO: That is excellent, Socrates.

>SOCRATES: Then, Meno, the conclusion is that virtue comes to the virtuous by the gift of God. But we shall never know the certain truth until, before asking how virtue is given, we enquire into the actual nature of virtue. I fear that I must go away, but do you, now that you are persuaded yourself, persuade our friend Anytus. And do not let him be so exasperated; if you can conciliate him, you will have done good service to the Athenian people.

### Note: Vi Expression to Create Dialogue

```
:49,139/^/>/s       # '^' is a special char for blank line
:49,139s/.*\n/\0\r/g    # Insert a new line ('\r', for some reason) after every other line
```

## The Problem of Knowledge

* The problem of the criterion:
    1. One can only identify instances of knowledge if one already knows what the criteria for knowledge are
    2. One can only know what the criteria for knowledge are provided one is already able to identify instances of knowledge
* Methodism - The way in which we define specific criterion of knowledge
* Chisholm argues that we should adopt **particularism**
* Particularism - rather than make the assumption that one can identify the criteria of knowledge independently of examining particular instances of knowledge, one should instead assume that one can correctly identify particular instances of knowledge and proceed on this basis

## Propositional Logic

* `S knows that P if and only if:`
    1. `S believes that P` <- belief condition
    2. `P is true` <- truth condition
    3. `P is justified for S` <- justification
* True belief is tied down by justification
* This is classical analysis
