# PHIL 275 W6

## Moral Knowledge

* Cultural relativism: 
    * Moral standards vary across diverse cultures
        * Different cultures -> different standards
    * There are no standards independent of those standards that arise within and operate within various cultures
    * Every culture has its own norms, no higher moral standard
    * Moral judgements are correct only insofar as they align with the standards of the relevant culture
* Problems with cultural relativism:
    * No possibility of forward progress
* Subjectivism:
    * Moral judgements are **not** assertions of fact
    * Rather, they are:
        1. Statements of approval or disapproval
        2. Statements of emotion
        3. Issuing of a command 
* Moral Objectivism:
    * There are moral facts that are not indirectly or culturally determined
    * Such facts are objective features of the world
* Moral skepticism:
    * Even if there are objective moral principles, knowledge of those principles is impossible
    * Irresolvable moral disagreement (i.e. abortion)
        * Unlike scientific debates, which are *generally* resolvable by observation/experimentation
        * There can still be right and wrong, but there's usually no way to *determine* it
* Moral foundationalism:
    * Foundationalism with respect to moral justification

## Technology and Knowledge

* Implications of tech on knowledge:
    * Possible loss of knowledge
    * Possible loss of skill
    * Less information in your head:
        * Offloading memory
* Extended cognition
* Extended mind:
    * Extension of one's own mind through the use of external devices
    * *Your* cognitive process
* What can't we offload:
    * Intellectual virtue - enables us to deploy cognitive abilities in an excellent way
        * That is, in a way that leads to truth
