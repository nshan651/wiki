# PHIL 275 W7

## Education

* The goals of education
* Economic:
    * The individual has better job prospects (maybe?)
    * The economy needs skilled workers
* Political aims:
    * Helps inform people for the purposes of democracy
    * Gives us the expertise that we need as a people to make intelligent decisions
    * I.e. understanding debt, tax brackets, voting, etc.
* Epistemological:
    * Institutional level:
        * Knowledge production (research)
        * Knowledge transmission (teaching)
    * Individual level:
        * Skills
        * Content information
        * Intellectual virtues:
* Intellectual virtues:
    * Habits of the intellect that allows us to deploy our resources in a productive way
    * Independence of thought, curiosity, carefulness, tenacity, etc.
