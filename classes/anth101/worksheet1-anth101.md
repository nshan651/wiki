---
title: "PBS Evolution, Chapter 5 Why Sex? Questions"
author: "Nick Shannon"
date: 2023-02-10
---

# Part 1

## Mendelian Genetics Lab

![lab1](~/Downloads/anth101/lab1.JPG)

# Part 2

## PBS Evolution, Chapter 5 Why Sex? Questions

1. What are three disadvantages of sexual reproduction?:
    - It requires both a male and a female, each of whom can only pass on 50% of their genes.
    - Lots of effort and energy is required to be put into the "courting" process.
    - Males cannot bear offspring, and often do not invest in them.
2. Describe the Red Queen hypothesis:
    - Evolution doesn't stop at some point - there is no "final" adaptation. Species must adapt and evolve just to keep up. If they stop, they are at risk of going extinct.
3. Given the characteristics of sperm and eggs, why does evolution favor females that are selective or "choosy"?:
    - Eggs take a large energy and time investment. Additionally, only a certain number of eggs may be produced, therefore it is advantageous for females to be selective.
4. How did a change in feeding ecology influence chimpanzees and bonobos differences?:
    - The bonobos live in forests where herbs are more abundant on the ground. Conversely, chimps have to rely on food from trees, because other animals such as gorillas compete with them for the easily accessible ground food. Male and female chimps are focused on foraging around, and as a result have less time to form bonds. Competition is also fierce and leads to much more aggressive behavior.
5. What are the evolutionary implications of shared parenting?:
    - Shared parenting implies a monogamous relationship, whereby both males and females collaborate to raise offspring. They share the burden of raising young.
6. What single behavior do humans have that is unique to the animal world?:
    - We take care of offspring that we do not sire. This demonstrates the fact that we are a deeply cooperative society.
