---
title: "ANTH 101: Hominins Worksheet"
author: "Nick Shannon"
date: 2023-04-04
---

# Part 1. [Myths about Hominins](https://www.sapiens.org/archaeology/stone-age-myths/)

- What is a new fact you learned about hominin evolution from this article?:
    - This article really challenges the popular assumption that adult males were the first and only humans to use stone tools during the stone age. Not only did women work with tools, but they used a variety of materials other than stone. This assumption is largely based on the fact that stone tools preseve much better than bone, wood, or fiber, and as a result we have much more archealogical evidence.
- What was the most interesting aspect of this article to you and why?:
    - This article does a great job highlighting how the roles of women and children in prehistory have been drastically downplayed, just as they have in the historical study of different cultures and societies. We tend to create a false dichotomy of men as hunters and women as homemakers, when in reality the lines were much less defined
- What questions are you left with?
    - What can we do to educate people on the Stone Age and how can we erase some of the false mythology surrounding it? Change the name?

# Part 2. [Hominin Diet & Brain Size](https://www.sapiens.org/biology/brain-evolution-fat/)

- What is a new fact you learned about hominin evolution from this article?:
    - It's interesting that are origins and "rise" as homo sapiens was a lot less glamorous than initially thought. Rather than becoming dominant by using flaked tools to hunt, we were likely scavengers who subsisted on the remains and bone marrow of carcases.
- What was the most interesting aspect of this article to you and why?:
    - I think this article calls into question our hubris as a species and our need to be "special." It makes sense that we would have started out as scavengers - it was both easier and safer. But, I think this is a much more humble origin story than our mythologized rise to dominance.
- What questions are you left with?:
    - I am curious about the evolution of our diets and how we have become much less tolerant to raw meat. Were early homo sapiens more resistant to the diseases we would get today if we tried eating an animal's brains?

# Part 3. [H. erectus Migration](https://www.sapiens.org/archaeology/ubeidiya-homo-erectus/)

- What is a new fact you learned about hominin evolution from this article?:
    - I learned that h. erectus sought many of the same resources that modern humans do, such as bodies of water and thriving wildlife.
- What was the most interesting aspect of this article to you and why?:
    - I thought the aspect of human collaboration as a competitive advantage was interesting. One part of the article discusses how h. erectus would assist eachother in hunting, gathering, and even the preparation of raw food
- What questions are you left with?
    - Did h. erectus pave the way for h. sapiens to flourish? How would our own development have occurred had they not existed?
