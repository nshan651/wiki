---
title: "ANTH 101: Human Variation, Diet, and Agriculture"
author: "Nick Shannon"
date: 2023-04-17
---

# Part 1. Hunting Down the Facts About Paleo Diets

- link: https://www.sapiens.org/biology/paleo-diet-inaccurate/
- What is a new fact you learned about human evolution from this article?:
    - I think the section in the Q&A about H. sapiens being strategic with their meals was interesting. They had to be intelligent with what foods they go after, given finite time and resources. However, they can only go after foods that are available in their local environment.
- What was the most interesting aspect of this article to you and why?:
    - It's interesting that there's this romanisation about the "macho caveman" hunter that eats almost entirely meat and few carbs. Most variations of the modern paleo diet tend to emphasize this, even though evidence suggests that there were in fact a lot of carbs in hunter-gatherer diets.
- What questions are you left with?:
    - The article discusses that people's diets varied based on locale, i.e. populations living near oceans and rivers ate lots of fish, populations living near forests and vegetation focused on plants. My question is, then, is there a certain region that lent itself best to optimal nutrition?

# Part 2. Why Don’t More Humans Eat Bugs?

- link: https://www.sapiens.org/culture/eat-bugs/
- What is a new fact you learned about human evolution from this article?:
    - I didn't realize how important insects were to the ancient human diet. Not only this, but more than a quarter of the world's current population eat insects regularly.
- What was the most interesting aspect of this article to you and why?:
    - It's interesting to hear the role that culture and colonialism play in this. The attitude of Christopher Columbus and other colonists reflects a certain ethnocentric bias, where other cultures who had to rely on insects as a fallback were seen as inferior and alien to them.
- What questions are you left with?:
    - Will eating bugs become more accepted/normal in Western countries?Or is our insect averseness to ingrained in our culture to change.

# Part 3. Why Can’t Most Humans Drink Milk?

- link: https://www.sapiens.org/biology/lactose-intolerance-cheese-evolution/
- What is a new fact you learned about human evolution from this article?:
    - It is normal for most humans to lose the ability to consume milk after they are infants. This is reflected today in that nearly 2/3 of all people today are lactose intolerant. The ability to continue consuming lactose after infancy did not appear until around the shift to agricultre.
- What was the most interesting aspect of this article to you and why?:
    - I did not consider that lactose persistence offered an advantage to some people who were able to pass this trait on to their offspring. It wasn't only advantageous from the perspective of giving them more options for calories, but also because of the social status that dairy foods held.
- What questions are you left with?:
    - Has the proportion of lactose persistence changed since we first began consuming dairy? If so, how?

# Part 4. With So Few Farmers, Why Are Video Games About Farming So Popular?

- link: https://www.sapiens.org/archaeology/farming-simulator-anthropology/
- What is a new fact you learned about human evolution from this article? :
    - In keeping with the idea that evolution is not progressive, the agricultural evolution was not strictly nor immediately beneficial to early farmers. Many of the earliest farmers actually lived shorter, less-healthy lives.
- What was the most interesting aspect of this article to you and why?:
    - I think that one thing that one thing the article doesn't quite touch regarding farming games is their ability to allow people to safely experiment with satisfying their basic physiological needs in a risk-free way. Modern society has given many people the privilege of all but assuring that their most basic needs will be met. I think that this gives people a way to "simulate" providing food, water, and security, in a way that they are not normally required to do.
- What questions are you left with?:
    - Will games like farming simulator spark an interest in agriculture in the next generation?
