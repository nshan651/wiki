---
title: "ANTH 101: Week 7 Activity"
author: "Nick Shannon"
date: 2023-02-28
---

# Key Features of Living Primates

![Primate Taxonomy](/home/nick/Downloads/anth101/primates.png)

## Conversations in Human Evolution

1) What was the coolest thing you learned about Dr. Kalan's work/research?
    - The coolest aspect of her work to me is that she doesn't quite know what to expect when embarking on field work. There's an element of excitement and adventure to it.
2) What is one question you would like to ask her about her work/research?
    - What are the implications of your study of chimp acoustic communications on the potential for biomonitoring? Are there any useful human applications?
