# ANTH 101 W7

## Communication

- Tactile communication:
    - touch, _very important_ in all primate species
    - Afilliative behavior:
        - Non-aggressive social interactions and associations between individuals

## Primate Culture

- Transmission of behavior from one generation to the next through observation and imitation
