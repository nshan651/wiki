# ANTH 101 W9

## Several Hypotheses for Primate Evolution

- Angiosperm-primate co-evolution hypothesis:
    - Primates evolved anatomical traits that made them more efficient fruit foragers

## Key Terms

- Paleocene epoch
- Plesiadiformes:
    - Archaic primates
- Adapoides

## Eocene Epoch

- Platyrrhine Dispersal
- Rafted to S.America
- Sweepstakes dispersal from Africa

## Miocene: Planet of the Apes

## Study Guide

- Know the primate types
- Types of traits:
    - Ancestral vs derived
- Sight:
    - Physiology, etc.
- Smell:
    - Reduced olfaction
- Brain:
    - Large brain and neocortex
    - Wrinkly brain
- Primate life history and society
- Clades:
    - African
    - Asian
