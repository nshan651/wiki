# ANTH 101 W2

## Scientific Basis

* Epistemes - culutral assumptions:
    * Naturalism:
        * Universe divisible into natural and supernatural
    * Rationalism:
        * Miracles are not explanatory in the natural world
    * Empiricism:
        * We learn about nature by collecting data under controlled circumstances, replicability is important

## Intellectual Trends

* Monogenism:
    * All people share common origin
* Polygenism:
    * Different people have different origins, share no common ancestry
* Geological stories were beginning to be told
* Biblical scholarship changed:
    * Shift from whether they actually happened to the meaning of the stories 

## Charles Darwin

* 1859: The Origin of Species is published
* Transmutation hypothesis:
    * Pre-Darwin, states that life forms spontaneously generated, not descendants from common ancestor
* Engine for adaption is competition
* 2-step process:
    * Random generation of variation + non-random process by which an environment favors organisms with certain features
* Never mentions people in his books, wanted to focus on the process:
    * Was a godly man, didn't want to rock the boat

## Herbert Spencer and Co.

* Coined the term "survival of the fittest":
    * Equated it to being the same as evolution, it is **not**
    * Used it to forward his agenda on eugenics
* Edward Tylor:
    * Wanted to separate evolution and study of culture from biological properties of people
    * Also problematic and used for malicious purposes
* Ernst Hackel:
    * Argued we don;t need fossil record because Europeans are linked to apes through non-white peoples
    * Humanity of blacks considered by him less important - morally repugnant concept today

## Theories of Inheritance & Disputes

- Nat. Sel. is just a theory at this point
- Blending inheritance:
    - Dominant theory of heredity at the time
    - Natural selection cannot work if you lose variation each generation
- Lamarck's **inheritance of acquired characteristics**:
    - Use it or lose in
    - Short and long-necked giraffe analogy
- Gregor Mendel pea plants:
    - Showed evidence that blending is not how it works
    - Offspring are not midway between parents' phenotypes
- Mendelian genetics:
    - Simple examples for complex variation
    - Binary states of existence
    - Charles Davenport -> binaries -> reinforced eugenics
- Eugenics:
    - An idea that was popular in the 1920s that American society should be improved by breeding "better" kinds of people

## The Synthetic Theory of Evolution

- Extension of Mendelian genetics to population genetics
- **Explains the evolution of life in terms of genetic changes occuring in the population that leads to the formation of new species**
- Successfully reduced evolution to genetics (1960s):
    - Gene: sequence of DNA providing coding for the construction of proteins
    - Genotype: set of alleles that an individual has for a given gene
    - Gene pool: entire collection of genetic material ina  breeding community that can be passed from one generation to the next
- More definitions:
    - Phenotype: set of outwardly observable characteristics for an individual
        - Genotypic changes and environment controls phenotypic changes
    - Genetic drift: random changes to the gene pool through multiple generations
- 1980s: now possible to study the DNA sequence of genes directly
- **Anatomical variation tracks adaptive divergence of the species, but genetic variation more closely tracks the time since the species diverged from one another**
- **We find more differences across species in DNA between genes than in DNA within genes**
- Genes are tracking time, anatomy is tracking adaptation
- Most mutations that occur are non-adaptive and neutral
- Molecular evolution:
    - Human genome project
    - Still don't understand how DNA really functions to create organisms
    - Animals are adaptive, reactive beings full of complexity

## Organismal & Multilevel Evolution

- Plasticity: tendency of growing organisms to react developmentally
- Canalization: tendency of a growing organism to be buffered toward normal development despite environmental changes
- Epigenetics: study of how genetically identical cells and organisms, can nevertheless differ in stability inherited ways
- **There is no normal human form, just context-specific ranges of human forms**
- Human adaptability:
    - Developmental plasticity due to epigenetic modifications to DNA
- Niche construction:
    - Active engagement by which species transform their surroundings in favorable ways, rather than passively inhabiting them

## Adaptation

- Features can be isolated as evolutionary units
- There is a reason fro the existence of any particular feature
- Such a reason can be discerned:
    - Population randomness

## Misconceptions About Human Evolution

1. NO SEPARATION of culture from science, or facts and values in human evo.
2. Equality is NOT identity
3. All humans are equally close to apes
4. Competition can take many forms other than over aggression
5. There is no "person of the future" - evolution is not progressive
6. Bible scholarship does NOT conflict with science

## Molecular Biology and Genetics

- 4 basic molecules necessary for cell structure and function:
    - Proteins
    - Lipids
    - .
    - .
- DNA
- 4 nucleotides
    - Includes A, T, C, and G
    - A & T, G & C make base pairs
    - Sequence is just the order of nucleotides
- 46 Chromosomes (23 pairs)
- DNA replication:
    - Initiation
    - Elongation
    - Termination

## Mitotic Cell Division

- Cell cycle:
    - cycle where cell undergoes diff checkpoints between phases to ensure that DNA replication and cell division occur properly
- Diploid:
    - Organism or cell with two sets of chromosomes
- Somatic cells:
    - Divide and grow and repair body tissues
- Mitosis:
    - Somatic cell division, gives rise to two diploid daughter cells
- Meiosis:
    - Gamete division, 2 rounds of division, 4 haploid daughter cells
    - Only applies to sperm and egg cells
- Genetic recombination:
    - Cellular process that occurs during meiosis in which chromosomes pair up and swap information

## Protein Synthesis

- Multistep process through which amino acids are brought together by RNA machinery read from a DNA template
- Introns:
    - Segment of DNA that do not code for proteins
- Exons:
    - DNA sequences within a gene that directly encode protein sequences
- Gene:
    - Sequence of DNA that provides coding information

## Public Acceptance of Evolution in the US (1985-2020)

- Level of public acceptance of evolution in the United States has increased in the last decade
