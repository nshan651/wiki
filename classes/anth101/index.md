# ANTH 101 Index

- Textbook: Shook B, Nelson K, Aguilera K, and. Braff L. 2019. Explorations: An open invitation to Biological Anthropology. https://pressbooks-dev.oer.hawaii.edu/explorationsbioanth/

## Notes

- [Week 1](w1-anth101)
- [Week 2](w2-anth101)
- [Week 3](w3-anth101)
- [Week 4](w4-anth101)
- [Week 6](w6-anth101)
- [Week 7](w7-anth101)
- [Week 9](w9-anth101)
- [Week 10](w10-anth101)

## Worksheets

[worksheet1](worksheet1-anth101)
[worksheet2](worksheet2-anth101)
[worksheet3](worksheet3-anth101)
[worksheet4](worksheet4-anth101)
[worksheet5](worksheet5-anth101)
[worksheet6](worksheet6-anth101)
[worksheet7](worksheet7-anth101)
[worksheet8](worksheet8-anth101)
[worksheet9](worksheet9-anth101)
