---
title: "ANTH 101: Hominin Comparative Morphology"
author: "Nick Shannon"
date: 2023-04-05
---

# Station 1: Gracile v. Robust Australopithecines

Complete the following table comparing the crania of these different Australopithecines:
[A. afarensis](https://sketchfab.com/2d-models/australopithecus-afarensis-lucy-da45bb3252fd4de39607639571dfa25e) 
[P. boisei](https://sketchfab.com/3d-models/australopithecus-boisei-4effe8a1a18140e3beccdb14f013538a) 

| Feature                                                | A. afarensis | P. boisei |
| -----------------------------------------------------  | ---------    | --------- |
| Sagittal cresting (present/absent)                     | absent       | present   |
| Degree of prognathism (1=most, 2=middle,3=least)       | 2            | 3         |
| Size of teeth (larger v. smaller)                      | smaller      | larger    |
| Overall robusticity (1=most robust, 2=medium, 3=least) | 3            | 1         |

# Station 2: Australopith Mandibular Anatomy

Compare the mandibles of each species:

| Feature                                                 | A. afarensis | P. boisei          | H. sapiens |
| -------                                                 | ------------ | ---------          | ---------- |
| Dental arcade shape (U-shaped v. parabolic or V-shaped) | U-shaped     | u-shaped/parabolic | parabolic  |
| Canine size (small v. large)                            | small        | small              | small      |
| Chin (present/absent)                                   | absent       | absent (mostly)    | present    |
| Robust or gracile                                       | gracile      | robust             | gracile    |


# Station 3: Gracile Australopithecines and Homo

Complete the following table regarding the similarities and differences between the H. habilis and A. africanus crania:

| Features                               | A. africanus | H. habilis |
| --------                               | ------------ | ---------- |
| Degree of prognathism (more v. less)   | more         | less       |
| Foramen magnum (anterior v. posterior) | posterior    | anterior   |

# Station 4: Robust australopithecines and Homo

The focus of this station is to compare the crania of the robust australopithecines and early Homo.

| Features                                      | P. aethiopicus | H. habilis | H. erectus  |
| --------                                      | -------------- | ---------- | ----------  |
| Sagittal crest (present/absent)               | present        | absent     | absent      |
| Size of the teeth (bigger v. smaller)         | larger         | smaller    | smaller     |
| Zygomatics | flared         | none       | less flared |

# Station 5: Early Homo v. modern Homo sapiens

Compare the crania of the species and record the relative differences revealed in
your observations on the table below:

| Features                                                  | H. habilis          | H. erectus                        | H. sapiens   |
| --------                                                  | --------------      | ----------                        | ----------   |
| Supraorbital torus/brow ridge (1=most, 2=medium, 3=least) | 2                   | 1                                 | 3            |
| Degree of prognathism (1=most, 2=medium, 3=least)         | 1                   | 2                                 | 3            |
| Cranial capacity (1=greatest, 2=medium, 3=least)          | 3                   | 2                                 | 1            |
| Describe overall shape and size of cranial vault          | It is low and small | higher vault but receding forhead | high forhead |
