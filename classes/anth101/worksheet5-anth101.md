---
title: "ANTH 101: Week 10"
author: "Nick Shannon"
date: 2023-30-03
---

- https://theconversation.com/behind-closed-doors-what-the-piltdown-man-hoax-from-1912-can-teach-science-today-76967
 
# Part 1: Piltdown Man Hoax

- Given the outcome of the Piltdown situation: 
    a) Consider how society was "primed" to accept Piltdown so easily as fact. How can society address sensationalism and scientific literacy?
        - Part of the reason why the Piltdown man was so easily accepted was that it was seen as a landmark discovery, a "missing link" between apes and modern humans. Another reason was because the discovery was difficult to verify due to limited information. A select few gatekeepers kept the materials under wraps, which prevented researchers from independently corroborate their claims.
        - One way that we can address media sensationalism is to temper expectations and treat new discoveries (especially major breakthroughs) with a healthy degree of skepticism. I think this responsibility largely falls on scientific journalists, who act as the mediators between scientist and laypeople. When it comes to fresh discoveries, it's important to allow for some time for the story to develop and be verified. There's a difficult balance between sensationalism that draws attention and generates revenue, and scientific integrity. Another area that has room for improvement is in the public, open availability of information. As I mentioned in class, if science isn't done in public, its not real science. That said, there has been a lot of progress when it comes to open sourced research. Distribution services like arXiv give everyone full and open access to the latest research. Moreover, in the computer science space, there has been a huge push for open source software that has also spilled over into research, with code hosting services such as Github housing a lot of research efforts.
    b) Reflect and evaluate your own scientific literacy. For example, think of a time you jumped to a conclusion based on a science-related headline?
        - There have been many times where I've been overconfident in my scientific literacy. I think I have a lot of room for improvement and learning to do. One example of a time that I jumped to conclusions was my Freshman year when I read Steven Jay Gould's *The Mismeasure of Man*. I took a lot of the information in the book at face value, and it wasn't until much later that I learned about the controversy surrounding it. Even though it initially felt like I had wasted my time, I think that reading it and then reading some of the responses and criticisms gave me multiple perspectives and was a really good thought exercise.

# Part 2. Fossils

- What kind of information could you acquire from a single fossil? What could that fossil tell you about the broader environment?:
    - A single fossil can provide useful information about the organism and its environment. It can provide information on the taxonomy, anatomy, and age of the organism. It can also tell you about the ecosystem that the organism lived in, their diet, and even the population distribution of the species over time.

# Part 3. Paleoanthropologists

- Read both Conversations in Human Evolution articles. 
    1) Compare and contrast the research of Dr. Habiba Chirchir and Dr. Lauren Schroder.:
        - Although both scientists are interested in human origins in Africa, Dr Chirchir's focus is on primate anatomy and comparative studies of behavior, whereas Dr. Schroeder's research revolves around the internal evolutionary morphology of primates. Despite coming from different backgrounds, Dr. Chirchir and Dr. Schroeder's paths and interests seem to have led them both to Eastern and Southern Africa, where they study and engage in field work.
    2) Which approach to paleoanthropology is more interesting to you and why?:
        - I think that Dr. Schroeder's application of quantitative methods to hominin morphological evolution sounds really fascinating. Because there is so much that we can no longer directly observe, I really like the idea of using statistical methods to "fill in" those blind spots, where perhaps archaeological remains alone can't tell the full story.
