# ANTH 101 W6

## Clades

- Grouping of organisms based on evolutionary history
    - Examples: African clave of primates vs Asain clave
- Platyrrihines:
    - Spider and howler monkeys
    - Marmosets and tamarins
    - Capuchins
- Catarrhini
- Hominoidea:
    - Apes & humans
- Pongo:
    - Orangutans

## Primatology

- Jane Goodall:
    - Studied chimps in Tanzania
- Dian Fossey
- Basal metabolic rate(BMR):
    - Rate an individual uses energy
    - Larger animals eat more food
- Competition:
    - Direct (fighting)
    - Indirect (eat it first)
    - Occurs within and between groups
    - Home range: are group or individual uses at a given period of time
    - Territory: home range whose boundary is defended from intrusion by conspecifics
- Sympatric:
    - Multiple species that overlap in geographic distribution
- Allopatric:
    - Two or more species not overlapping
- Philopatric:
    - Remaining with your birth group
- 
