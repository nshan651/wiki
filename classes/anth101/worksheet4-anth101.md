---
title: "ANTH 101: Primate Behavior"
author: "Nick Shannon"
date: 2023-03-14
---

# Ritualized Behavior

- What is the importance of ritualized aggression?:
    - Ritualized aggression can serve as a way for animals to establish a dominance hierarchy within their respective social groups. This kind of symbolic violence can prevent real injury or death from occurring. Another possible explanation for ritualized violence is for it to act as a sort of "simulation", where individuals can act out aggressive scenarios in a controlled manor. Finally, ritualized aggression can be used as a part of a competition or sport, where aggression is channeled constructively.
- Provide an example (one not found in the text) of a ritualized behavior – your example can relate to humans, non-human primates, and/or any other animal:
    - An example of a ritualized behavior seen in humans is in sporting events. For instance,Loyola basketball games are an excuse for a bunch of Loyola students to gather and trash talk the opposing team. On a more serious note, Rambler games represent a community forming a shared emotional connection.

# Social Strategies: Behavioral "Currencies"

- Grooming: pretend you are an adult female monkey, why would you spend 
time grooming:
    a) An infant:
        - I would likely groom an infant monkey because I am its mother or close relative, and I would do so to establish or maintain a social bond, and to provide comfort for the infant.
    b) An adult male:
        - It would be because I have a social bond with him, such as a mating relationship or close friendship.
    c) Another adult female:
        - Grooming another adult female could be a way of establishing a social hierarchy, where perhaps I am looking to establish favor. It may also be because I wish to establish and maintain a bond with another adult female who has a strong social connection to me.
- Alloparenting: why might the following individuals partake in this energetically costly behavior?
    a) An adult female:
        - An adult female may be related to the infant they are caring for, such as for a niece or a nephew, and as such has a genetic interest in ensuring their survival. Alloparenting among females is also important for maintaining social connections within a group. Finally, there may be some intrinsic enjoyment to caring for and being around infants.
    b) An adult male:
        - Although males usually engage in alloparenting less often, they do so for many of the same reasons as females. Additionally, some males who engage in alloparenting may gain social status or increased mating opportunities.
    c) A juvenile:
        - A juvenile engaging in alloparenting can learn useful caregiving skills that can help them later on to ensure their own reproductive success. Additionally, juveniles may find joy in the company of younger relatives, and may use alloparenting as a way to gain social status.

# Group Living

- Name 3 reasons why a primate would want to live in a group:
    1. Protection - living in a group can provide protection from predators and other threats.
    2. Access to resources - living in a group can give access to resources such as food, water, and potential mates.
    3. Social interaction.
- Name 3 reasons why a primate would NOT want to live in a group:
    1. Competition.
    2. Disease transmission.
    3. Reduced autonomy.
- Name 5 types of social/mating systems & identify a primate which exemplifies each:
    1. Monogomy: Humans are a great example of a (mostly) monogmous species. In many (though not all) cultures, marriage and monogomy hold central social importance.
    2. Polygyny: Gorillas are an example of a primate that practices polygyny. One male gorilla will mate with multiple females within his group.
    3. Polyandry: Marmosets are an example of a primate that practices polyandry. One female marmoset will mate with multiple males within her group.
    4. Proiscuity:  Bonobos are an example of a primate that practices promiscuity. They have frequent sexual interactions with multiple individuals within their group and use sex to mediate social interactions.
    5. Polygynandry: Chimpanzees are an example of a primate that practices a multi-male, multi-female social/mating system. They live in large communities with multiple males and females and have complex social relationships and hierarchies.

# Dominance

- How do dominance hierarchies reduce aggression?:
    - By establishing a clear social hierarchy, individuals can avoid costly physical fights and instead rely on ritualized displays of aggression to establish their position in the group. This reduces the overall level of aggression and violence within the group.
- Name 2 costs & benefits associated with being dominant.
    - Costs:
        1. Increased risk of injury
        2. Increased energy expenditure in maintaining one's position in the hierarchy
    - Benefits:
        1. Easier access to resources for dominant individuals
        2. Improved reproductive success
- If, biologically speaking, the purpose of life is to reproduce and spread one’s genes, why don’t all subordinate male primates challenge the alpha male for dominance?:
    - The alpha is usually the physically strongest of the group, which poses risks to challenging them. Challenging the established alpha could also result in social isolation. Subordinate males may choose to utilize alternative reproductive strategies, such as through alliances with other subordinate males.

# Infanticide

- Why would a male primate commit infanticide?:
    - A male primate may commit infanticide as part of his reproductive strategy, in order to mate with the female and have his own offspring.
- Why would a female mate with a male after he killed her baby?:
    - A female may still mate with a male after an infanticide to prevent further infanticide, and for protection for herself and her future offspring. A third reason could simply be a lack of mating options.
 
# Conservation/effects of Humans

- Imagine a rainforest devastated by clear-cutting and the construction of roads, creating ‘pockets’ or islands of forests that were once continuous. How might this habitat destruction affect the primate inhabitants, not only in terms of reproductive potential and survival, but also in terms of social behavior?:
    - Deforestation reduces primates' access to resources
    - The fragmentation of the remaining rainforest increases competition over the limited resources
    - Deforestation disrupts primates' social order
- Provide 3 reasons that non-human primates do NOT make good pets:
    1. It disrupts their need for socialization with other non-human primates
    2. Keeping a primate as a pet poses health risks such as disease transmission. Additionally, primates are much stronger than us, and could easily hurt us (like by ripping our face off!)
    3. Legal restrictions
