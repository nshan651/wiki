---
title: "ANTH 101"
author: "Nick Shannon"
date: 2023-02-15
---

## Comic Strip

- Prompt:

> Imagine a population of common house mice (Mus musculus). Draw a comic strip illustrating how mutation, genetic drift, gene flow, and natural selection might transform this population over several (or more) generations. Upload a picture of your drawing. [yes it has to be a drawing, no you don't have to be an artist :)]

![Mutation](/home/nick/Downloads/pikachu.jpg)

![Drift, Gene Flow, Nat. Selection](/home/nick/Downloads/drift.jpg)

\newpage

## Matching

> Match the correct force of evolution with the correct real-world example:
> 
> a. Mutation                                       i. 5-alpha reductase deficiency
> 
> b. Genetic Drift                                ii. Peppered Moths
> 
> c. Gene Flow                                     iii. Neurofibromatosis Type 1
> 
> d. Natural Selection                        iv. Africanized Honey Bees

- a. Mutation --> iii. Neurofibromatosis Type 1
- b. Genetic Drift --> ii. Peppered Moths
- c. Gene Flow --> iv. Africanized Honey Bees
- d. Natural Selection --> i. 5-alpha reductase deficiency
