# ANTH 101

## What is Anthropology

* Several types:
    * Archaeology
    * Cultural anthro
    * Biological anthro
    * Linguistic anthro
    * Applied anthro
* Names to know:
    * Herbert Spencer and Lewis Henry Morgan:
        * Founders of phys anthro
    * Bronislaw Malinowski - participant observation
    * Franz Boas - "father" of 4 field anthro

## Types of Antrhopology

* Cultural anthro:
    * Similarities and differences among living societies
* Linguistic anthro:
    * Study of human language and speech
* Archeology:
    * Early cultures/lifestyles
* Applied anthro:
    * Practical application of anthro practices to solve real-world problems
* Biological Anthro:
    * Study of the human species from a biological perspective 
    * Physical anthro has deeply-racist roots

## Anthropolgy Articles

* Anthropology is the science of humanity
* Discussion questions:
    * What is something new that you learned about the field of anthropology?
        * I was surprised by the diversity of settings that Anthropologists study in. Although it's a science, they aren't confined to a laboratory
    * Has your perspective on the field of anthropology changed?
        * Yes, I feel like I have a better understanding of the importance of anthropology and its mission
    * How could you apply the practice of anthropology to your own life or interests?
        * I am interested in computer science and computing, and so I am most interested in humans' relationship to technology
