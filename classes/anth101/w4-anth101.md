# ANTH 101 W4

## Exam Review

- Participant observation:
    - Essential method of cultural anthro
- Know the six main subfields
- Evolution is not progressive
- Key points:
    - Humanity is biocultural
    - Inequalities are structural, caused by human choices, not biological inferiority
    - Competition does not have to be overt aggression
    - Structure of evolution is a tree
    - Biblical scholarship and evolutionary science are separate ways of thinking, they can and do coexist
- Genisms:
    - Mono -- all people share common origin
    - Poly -- different origins (used to justify slavery)
- Niche construction:
    - Active engagement by which species transform their surroundings in favorable ways, rather than passively inhabiting them
- Know the difference between Mitosis and Meiosis
- Speciation:
    - Species division
