---
title: "ANTH 101: Race, Racism, Sex and Gender"
author: "Nick Shannon"
date: 2023-04-17
---

# Articles

In addition to this podcast episode: https://www.humbio.org/sos-142-f-around-and-find-out-methodsandmeasuring-hair-morphology-with-dr-tina-lasisi/

Read the following articles:

- Race, racism, and sex 
    1. https://www.sapiens.org/biology/is-race-real/
    2. https://www.sapiens.org/biology/hair-race-evolution/
    3. https://www.sapiens.org/biology/biological-science-rejects-the-sex-binary-and-thats-good-for-humanity/

- Anthropology, museums, and human remains
    4. https://www.sapiens.org/archaeology/museums-human-remains/
    5. https://www.sapiens.org/archaeology/where-do-teaching-skeletons-come-from/
    6. https://www.sapiens.org/culture/anthropology-colonial-history/

# Questions

1. Should we continue to attribute any value in “race” concepts older than 1950, based on our current understandings of human biological diversity?
    
Ultimately, I think that the concept of "race" is a social construct that is used as a shortcut for categorizing a person by their outward appearance. It doesn't capture any of the nuance or complexity of their actual identity, which can be extremely problematic, especially when looking at the origins and history of the concept. An article from last week described human identity as a continuous set of traits, which I think is a great way of putting it. 
    
Unfortunately, I'm not as to whether the concept of race will ever die. It's something that's so ingrained in our culture that race and appearance have become interminably linked with how we identify ourselves. Perhaps the best thing that we can do is to acknowledge and teach that human biological diversity is a complex, continuous spectrum, that cannot be simplified down to race.

2. How should we communicate scientific findings about human biological variation more accurately and responsibly to those outside the anthropological discipline?
    
One important consideration when communicating scientific findings about biological variation is to be inclusive. That is, we should always strive to acknowledge the diversity of the human population and avoid generalizations that could perpetuate prejudice or stereotypes. Additionally, these communications should be communicated simply and clearly, and with ample context on any relevant background information.

3. What are some of the ethical considerations faced by forensic anthropologists and bioarchaeologists?

Because these professions deal with human remains, there are a number of ethical considerations that they face. First, it is important for forensic anthropologists to have respect for the deceased. This includes treating remains with respect by handling them carefully, seeking consent with the individual's next of kin, and doing so in a confidential manner that protects the privacy of all parties involved. These professions are also responsible for being sensitive to the cultural differences and beliefs of the deceased. From a scientific standpoint, they are obligated to use transparent, data-preserving methods.

EXTRA CREDIT – OPTIONAL (5 points) this is especially for students looking to regain some points lost over the course of the semester.

4. Synthesize what you learned from articles 1-3 and the podcast episode. What questions are you left with?

The articles and podcast all point toward the fact that many methods of classification, such as race and gender, can be problematic and inaccurate to a person's actual biological identity. Everything from skin tones to hair type is distributed on a continuous gradient. Even the human brain is a mosaic of what are stereotypically considered masculine and feminine traits. Nothing about humans can be categorized in such simple terms as race or gender without losing a person's true identity.

One question I have is whether people being globally connected will help people better understand the difference between geographic ancestry and race. For instance, will the ability to communicate and interact with anyone in the world help to clear up this misconception and establish a more nuanced understanding of identity.
 
6. Synthesize what you learned from articles 4-6. What questions are you left with?

The main idea of these three articles is the importance of respecting human remains under study. Bioarchaeology has unfortunately had a dehumanizing history with the collection of human remains, and specifically with the collection of African American bodies. Many institutions are re-examining their collections and developing policies to respectfully memorialize these remains. Even the question of "who is allowed to study who" is being reconsidered. Historically, there has been a huge imbalance of white European scholars who studied and categorized those in "developing" or "underdeveloped" nations. However, this creates a huge imbalance and lack of diversity. Today, people from diverse backgrounds are encouraged to be the "researcher" and the "researched", with the hope that they can mitigate the damage that colonial tradition has done.
