# STAT 203 Week 15

## Chapter 8: Hypothesis Testing

* Testing exisiting idea against an alternative
    1. Hyp. testing for population mean $\mu$
    2. ... for population $\rho$
---

* Null hypothesis $H_0$ vs alternative hypothesis $H_1$ (Or $H_a$)

* Refer to the sample, based on the data, arrive at a decision

* Decision: Reject $H_0$ **or** fail to reject $H_0$
    - Note: never say *accept* the null

| Truth/Decision | Reject $H_0$ | Fail to reject $H_0$ |
|----------------|--------------|----------------------|
| $H_0$ is true  | Type I error | $\checkmark$         |
| $H_0$ is false | $\checkmark$ | Type II error        |

* $\alpha =$ P(Type I Error)

* $\beta =$ P(Type II Error)

* Goal is to minimize both $\alpha$ and $\beta$

### Steps For a Hypothesis Testing Problem

1. Identify $H_0$ and $H_1$
2. Define test statistic and its distribution **under** $H_0$
3. Calculate observed value of test statistic
4. Decision based on rejection region
5. Conclusion

### Practice Problems

* *If $\alpha$* is not given, assume 0.05*

* Problem #1 
    - Part a
        - Already done
    - Part b
        - Test statistic:
        - $Z=\frac{\bar{X}-\mu_0}{\sigma/\sqrt{n}}$
    - Part c
        - $Z_{obs}=\frac{94.32-95}{1.2/\sqrt{16}} = -2.2667$
    - Part d 
       - ... 
   - Part e
       - Conclude that the melting point does not differ significantly from 95

* Problem #2
    - Part a
        - Case III (Pop not known, sigma known)
            - $H_0: \mu = 1600$
            - $H_1: \mu \neq 1600$
        - Test statistic
            - Calculate Z under $H_0$ 
         $Z_{obs}=\frac{1570-1600}{250/\sqrt{370}} = -2.3082$
        - Reject if $Z_{obs} > Z_{\alpha/2}$ **or**$Z_{obs} < -Z_{\alpha/2}$
            - $Z_{0.025}=1.96$
            - Observed is less than $Z_{\alpha/2}$, reject the null
        - Based on this sample that the true mean claimed paid differs *significantly* from $1600
    - Part b
        - $\bar{X} + Z_{\alpha/2}*\frac{\sigma}{\sqrt{n}} = 1570 + (1.96*\frac{250}{\sqrt{370}})$
        - 1600 is outside the range of the confidence interval
        - We conclude with 95% confidence that the true mean claim paid is $(1544.526, 1596.474)$
    - Part c
        - Yes, they do
    - Part d
        - $ME \leq 2 \rightarrow n \geq (\frac{Z_{\alpha/2}\sigma}{2})^2$
            - $Z_{\alpha/2}=2.575$
        - $=103603.5$
        - $n=103604$

* Problem #3
    - *Case IV*
    - $\mu$: true mean thickness of lenses
    - $H_0: \mu=3.2$ vs $H_1: \mu < 3.2$
        - Could also do $\mu > 3.2$, if properly explained
    - $T = ...$ under $H_0$
    - $\frac{3.05-3.2}{0.34/\sqrt{50}}=-3.11966$
    - Reject $H_0$ if $T_{obs} < -t_{n-1;\alpha}$

## Hypothesis Testing Cont.

1. $H_0:P=P_0$
    - vs $H_0: p > p_0 or p < p_0$ or $p \neq p_0$
2. Test statistic
    - No $p_0$ in confidence interval. Only in hyp. testing
    - $Z=\frac{\hat{p}-p_0}{\sqrt{\frac{p_0(1-p_0)}{n}}}$
3. $\hat{p}=\frac{x}{n}$ where $p_n$, n given

### Chapter 8 Practice Problems

* Problem #5
    - Part a
        - p: true number of ceramic potshards included
            - KW: **at least fifty**
        - $H_0:p=0.5,H_1:p<0.5$
        - $Z=\frac{\hat{p}-p_0}{\sqrt{\frac{p_0(1-p_0)}{n}}}$
            - $np \geq 10?\ n(1-\hat{p}) \geq 10$ **Yes**
                - Conditions are met ^^
            - $...=-2.3988$
        - Reject if $H_0$ if $Z_\alpha < -Z_{0.05}=-1.645$
        - Z observed is less than $Z_\alpha$
            - Therefore, reject the null hypothesis
    - Part b
        - For upper bound, use $Z_\alpha$ rather than $Z_{\alpha/2}$
