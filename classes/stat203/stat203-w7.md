# STAT 203 Week 7 Notes

* Pmf and cdf
    - $pmf = P(X) = P(X=x)$
    - $cdf = F(X) = P(X\leq x)$

* Mean and variance
    - $mean = \mu = E(X) = \sum_{x}^{X}x*p(x)$
    - $variance = \sigma^2 = V(X) = E(X^2) - (E(X))^2$
        - $= E(X^2)-\mu^2$

* Specific distributions

* For a binomial experiment
    1. n total trials, n is fixed
    2. Each trial has **two** possible outcomes
    3. $P(success) = \rho$ remains fixed from trial to trial
    4. Trials are independent

* Binomial-specific equations
    - $\mu = E(X) = n*p$
    - $\sigma^2 = V(X) = n*p*(1-\rho)$
    - $P(X=x)=p(x) = nCx * p^x(1-p)^{n-x}$
* $P(X>3) = 1-P(X\leq 3) = 1 - F(3)$

### Practice Problems 1

* Problem #8
    - $p=0.3, q=0.7$ 
    - $std=\sigma=\sqrt{n*p*(1-p)}$
        - $=\sqrt{25*0.3*0.7}=2.29$
    - $mean=\mu=E(X)=n*p$
        - $=25*0.3=7.5$
    - Part b
    - $P(X is more than 2\sigma away from \mu)$
        - Above **AND** below 2 std's
    - Two options: compliment between to stds, *or* less than 2 stds + greater than 2 stds
    - $\mu + 2 \sigma = 7.5+(2*2.29)=12.08$
    - $\mu - 2\sigma = 7.5-(2*2.29)=2.92$
    - $P(X<2.92 or X>12.08$
        - [0,1,2] or [13,14,...,25]
        - *Boil down to integers*
    - $P(X\leq 2) + P(X\geq 13)$
    - $=F(2) + 1-P(X \leq 12)$
    - $=F(2) + (1-F(12))$
    - $=0.009+1-0.983$
        - **Derive F values from table**

* Problem #9
    - Part a
        - X: # of customers who prefer oversized (success)
        - $P(X>=6)$
            - Convert
        - $=1-P(X<=5)=1-F(5)$
        - $=1-0.367$
    - Part b
        - $P(\mu - \sigma < X < \mu + \sigma)$
        - $\mu = n*p=10*0.6=6$
        - $\sigma = \sqrt{np(1-p)} = \sqrt{10*0.6*0.4}=1.55$
        - $P(4.45 < X < 7.55)$
            - [5,6,7]
            - Make sure that this includes 5!
            - So do F(4)
        - $P(5),P(8)$
        - $P(X<=8) - P(X<=4)$
            - Equivalently, $F(7) - F(4)$
        - Simplify using table...
    - Part c
        - $P(X\leq 7)$
        - $F(7)$

### Exam prep questions

* Hurricanes
    - $p=0.05, n=20$ 
        - X is counting hurricanes in a twenty-year period, so n is 20
    - $P(X<3)=P(X\leq 2)=F(2)$

* **TO READ PROBABILITY TABLES**
    - Row intersection column

* Actuary study on drivers
    - Y: driver is a young adult; A: at least one collision
    - $P(Y|A) = \frac{P(Y \cup A}{P(A)} = 0.08/0.32$

* Color blind study
    - M: male; M': female
    - C: color blind; C': not c.b.
    - $P(M)=100/1100; P(M')=1000/1100$
    - $P(C|M)=5/100; P(C|F)=100/1000$
    - Use Bayes
    - $P(M|C)$
        - $=\frac{P(M)P(C|M)}{P(M)P(C|M) * P(M')P(C|M')}$
