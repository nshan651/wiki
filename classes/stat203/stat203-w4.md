# STAT 203 - Week 4 Notes

## Probabilities

* For compliment formula, use an apostrophe

    --- Type up formula sheet

* When you have a compliment of multiple elements, think of the compliment of each individual item
    - (A U B)' = A' int B'
    - (A int B)' = A' U B'
        --- Add these to formula sheet ^^

* In counting probability, there is a base assumption that each event is equally likely
    - Else, could involve weighted probabilities

* Product Rule
    - Every choice includes the others
        - i.e. 3-digit lock, 10^3 combinations
            10 possible digits, all reliant on the others to make a unique combination

* Intersection probability table: shows intersection of probability
    - The intersection of these events (Ch2 Hw6 are disjoint)

* Union => "At least one"

* Combinatrics
    - Combination and permutation
        - Combination/permutation formulas ARE allowed (if supported by calculator)

* Combination
    - Number of ways to choose k objects from n objects
    - nCk (n combination k)
        - also written as a verticle stack
    - $nCk = n! / k!(n-k)!
        - nCk = nC(k-1)

* Permutation
    - Number of ways to order (permute) k objects from n objects
    - nPk = n! / (n-k)!
        - Shortcut: If you find the combination nCk, you can multiply by k! to cancel denominator and add a k ordering
    - Ex 1.) num of 3 letter words created from 5 (distinct) letters
        - 5P3 = 5! / (5-3)! = 120 / 2 = 60 permutations
    - Ex 2.) num of ways to select 9 players for a line-up from 15 players
        -- 15P9 = 15! / (15-9)! = ...

---

## Worksheet 2

* Problem 7
    - 8P3
    - 30C6
    - 8C2 * 10C2 * 12C2
        -- Multiple because we are looking for combinations
        -- If it were adding instead, question would be without replacement (union of each)
    - Divide combinations (found above) by total possibilities: (8C2 * 10C2 * 12C2) / 30
    - (8C6 + 10C6 + 12C6) / (30C6)
        -- Add the numerator because it is OR


## Conditional Probability

* P(A) unconditional probability; ways to improve on this 

* Given event B *has occured*, what is the probability that A will happen?
    - P(A|B)
        -- Given event is always on the right
    - The only way that A can happen is if you select the intersection of A and B
        -- If it is A but not B, that would be contradictory because B has occured
    - Sample space changes
    - P(A int B) / P(B)

* Ex 1.) 1 card randomly chosen, given it is a face card, what is the probability it is in hearts?
    - P(H|F) = P(H int F) / P(F) = (3/52) / (12/52) = 1/4

* Multiplicative Rule
    - P(A int B) = P(B) * P(A|B)

## Conditional Probability Part 2

* Problem 8: stolen ATM pin

* Given: 8 _ _ 1
    - No ascending or descending values, no sequences starting with 19
    - P(S1 U S1 and S2 U S1 and S2 and S3)
    - P(S1) + P(S1 int S2) + P(S1 int S2 int S3)
        - Intersection means multiplicative rule!!! (multiply)
    - 1/100 + P(F1)P(S2|F1) + P(F2)P(S3|F2)
    - = 1/100 + (99/100 * 1/99) + (98/100 * 1/98)
    - Each consecutive selection is the probability of failure (out of the original total), multiplied by the possible remaining success (minus 1 in the denominator for remaining correct guess) because you do not select the failure again

    - 10^4        
    - (10 identical, 14 ascending/descending,
        -- 10^9 -124 = 9876

* Part d, if given 1 _ _ 1
    - 11 prohibited pins, 1111 and 19x1
    - 89 remaining possible pins
    - 1/89 + (88/89 * 1/88) + (87/89 * 1/87)
    - Probability of failure for the first does not change in denominator

* Problem 9: Probability table (intersection probabilities)
            Small   Medium  Large
            --------------------
    Regular| 14%    20%     26% | 0.6
    Decaf|   20%    10%     10% | 0.4
    ---------------------------
            0.34    0.3     0.36

    - Because this displays intersections, add rows and columns to get joint probabilities

    - Part b
        - P(D|S) = P(D int S) / P(S) = 0.2 / 0.34 = 0.5882
        - Conditional probability
        - Of those people who purchased a small cup, find those who went decaf
        - Reduce population size
        - More info increases probability

    - Part c
        - P(S|D) = P(S int D) / P(D)
        - 0.2 / 0.4 = 1/2

* Problem 10
    - Do problem 10 as homework (Similar to above)
    - Leave out part c (Do for extra credit)

## Independence of events 

* While it is possible to condition any two events, the operants will not affect the chances of the other

* P(A|B) = P(A) = P(B) given A and B are independent

* P(A int B) = P(A) * P(B)

* Sampling with replacement is an example of an independent event

* Independent vs Disjoint events
    - Contradicting scenarios
    - Independent events cannot be disjoint, or vice versa
        - list of all odds and all evens? Maybe???

* P(A'|B) = 1 - P(A|B)
        - ADD THIS TO FORMULA SHEET
    - Cannot use comp formula on conditioning event!!!
    - P(A|B') will NOT work

* Problem 11
    - Part a 
        - P(B'|A') = P(B') = 30%
        - If the two are independent (which they are), it is simply the unconditional probability of B'
    - Part b
        - P(A U B) = P(A) + P(B) - P(A int B)
        - Because A and B are independent, we can use the intersection rule (multiply the two probabilities)
        - 0.4 + 0.7 - (0.4*0.7)

* Problem 12
    - Part a
        - P(A1) = 0.4, P(A2) = 0.35, P(A3) = 0.25 
        - P(B|A1) = 0.6
        - P(B|A3) = 0.5
        - P(A2 int B) = P(A2)P(B|A2)
        - 0.35 * 0.6 = 
        - The language of the problem gives the order, but, you can break it up in a way that makes sense given the info you have
