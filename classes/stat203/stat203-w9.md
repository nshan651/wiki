# STAT 203 Week 9 Notes

### Practice Problems

* Problem #1
    - Part c
        - X: # trees per acre
        - $Poisson(\mu = 80)$
        - Area of circle = $\pi r^2$

* Problem #2
    - Part a
        - X: # vehicles arriving in 2 hours
        - $Poisson(\mu = d*t = 10*2 = 20)$
        - $P(X=15) = P(X\leq 15) - P(X \leq 14) = 0.052$
    - Part b
        - A: 10 vehicles arrive; B: all 10 vehicles have no equipment violation)
        - $P(A \cap B) = P(A)P(B|A)$
            - P(A) is counting (**Poisson!**)
        - $P(A) = P(X=10) = P(X \leq 10) - P(X \leq 9) = ... = 0.125$
        - Y: # vehicles out of 10 with *no* violations
        - $Bin(n=10, p=0.5)$
        - $P(Y=10) = 0.5^{10}$
            - OR $P(X \leq 10) - P(X \leq 9)$

## Limiting Distributions

* Probability: "Area under the curve"

* In continuous probability, less than and less than or equal to are the **same**

* Probability density function (PDF)
    - Notation: f(x)
    - $= \int_{a}^{b} f(x)dx$
    
* Cumulative Distribution Function (CDF)
    - $F(x) = P(X \leq x) = \int_{-\infty}^{x} f(u)du$

* PDF given CDF
    - $f(x) = \frac{d}{dx}F(x)$

* Properties of PDF
    1. $f(x) \geq 0$
    2. $\int_{-\infty}^{\infty} f(x)dx = 1$
        - Total probability
    
### Practice Problems 2

* Problem #1
    - $\int_{-\infty}^{\infty} f(x)dx = \int_{0}^{\infty} (x/\sigma^2}e^{-x^2/2\sigma^2}dx = $
        - Substitution
