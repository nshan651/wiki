# STAT 203 Week 8 Notes

## Negative Binomial Distribution

* Distributions allow us to identify trends in our experiments

* Binomial is like a *fixed budget* experiment
    - Conversely, negative binomial is **not** a fixed budget (variable)

* Negative binomial
    1. $P(success)=$ r is fixed
    2. Each trial has 2 outcomes
    3. $P(success) = \rho is fixed$
    4. Trials are independent
        - Points 2-4 are the **same** for regular binomial experiments

* X: # failures before $r^{th}$ success
    - Total trials is $X + r$

* **Number of trials is random**
    - Totally depends on how many it takes before number of successes is reached

* Negative Binomial Probability Function
    - $P(X=x) = (r+x-1)Cx * (1-p)^{x}*p^{x-1}*p$
    - $X~NB(r,p)$; where r is number of successes and p is the probability of success
    - $\mu = E(X) = \frac{r(1-p)}{p}$
    - $\mu^2 = V(X) = \frac{r(1-p)}{p^2}$

* In negative binomial, variance is often **larger** than mean

* Overdispersion:
    - More variance than what is usual
    - Negative binomial dist can account for this

### Worksheet Problems

* Problem 1
    - P(5 failures before 3rd success)
    - X: # failures before 3rd sucess
    - $NB(r=3,p=0.7)$
    - $P(X=5) = (3+5-1)C5 * 0.7^3 * 0.3^5$
    - $=0.0175$
    - Part b
    - $E(x)=r(1-p)/p$

* Problem 2
    - P(success) = one or more accidents
    - $p=0.6$, trial is a month 
    - $r=4$ (at least one failure before the 4th success)
    - $P(X\geq 1) = 1 - P(X=0)$

* Problem 3
    - P(success) = striking oil
        - Not binomial, because the number of trials are NOT fixed
    - X: # of failures before 1st success
    - $NB(r=1, p=0.2)$
    - $P(X=2) = ...$
    - Because trials are related, the answer is just the product of all unconditional success/failures
        - $0.8*0.8*0.2$
    - Part b
        - $NB(r=3, p=0.2)$
        - $P(X=4) = (3+4-1)C4 * 0.2^3 * 0.8^4$
    - Part c
        - num of drills = # s + # f
        - $= r + X$
        - $E(X+3) = V(X+3)$


* Problem 4
    - $NB(r=4, p=0.44)$
    - $P(X=8) = ...$

* Problem 5
    - $NB(r=4, p=1/13)$
    - $P(X=16) = ... = 0.0094$
        - 16 failed attempts for the four successes

### Geometric Distribution

* When looking at first success, this is called a **geometric distribution**
    - $NB(r=1, p)$

## Poisson Distribution

* Count data
    - Ex.) number of times you blink
    
* X: count

* Poisson
    - $Poisson(\mu)$
    - $\mu = E(X) = \mu$
    - $\sigma^2 = V(X) = \mu$
    - Only distribution where mean and variance are *equal*
    - $P(X=x) = \frac{e^{-\mu} * \mu^x}{x!}; x=0,1,...$

* Fixed time is called a Poisson process
    - $Poisson(\lambda)$ in unit time, then...
    - $Poisson(\mu = \lambda * t)$ in time t
   
### Poisson Example Problems

* Problem 6
    - Part a
        - $P(X<=10) = F(10)$
    - Part b
        - $P(X>20) = 1 - P(X \leq 20)$
    - Part c
        - $P(10 \leq X \leq 20) = P(X\leq 20) - P(X\leq 9) = F(20) - F(9)$
        - $P(10 < X < 20) = F(19) - F(10)$ 
    - Part d
        - $P(\mu - 2\sigma < X < \mu + 2\sigma) = P(10 - 6... < X < 10 + 6...)=P(3.68 < X < 16.32) = F(16) - F(3) = 0.973 - 0.01 = 0.963$

* Problem 7
    - Part a
        - $P(X = 1) = F(1) - F(0)$
    - Part b
        - $P(X\geq 2) = 1 - P(X\leq 1) = 1 - F(1)$
    - Part c
        - For 1 disk, P(no missing pulse)
        - $P(X=0) = F(0) = 0.819$
    - Part d
        - $P_1 * P_2$
        - $=0.819 * 0.819$

* Problem 8
    - Part a
        - X: # of arrivals in one hour 
        - $Poisson(\mu = 8*1 = 8)$
        - $P(X=6) = F(6) - F(5)$
    - Part b
        - $P(X\geq 6) = 1 - F(5)$
        - $Poisson(\mu = 8*1.5 = 12)$
        - $\mu=12, \sigma^2 = 12, \sigma=\sqrt{12}$
    - Part c
        - $P(X\geq 20) = 1 - F(19)$
        - $P(X\leq 10) = F(10)$
        - *Use cdf table for both*

* Problem 9
    - Part a
        - X: # of trees in 1 acre 
        - $Poisson(\mu=80*0.25 = 20)$
        - $P(X\leq 16) = F(16)$
    - Part b
        - *Skip for now*
    - Part c
