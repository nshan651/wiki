# STAT 203 Week 14

## Sample Population Procedure

* Set up
    1. Population
        - mean and variance (unknown)
    2. Sample from population
        - $E(X_i)=\mu, V(X_i)=\sigma^2$

* The goal is to construct a $100(1-\alpha)$% **Confidence Interval (CI)** for $\mu$

* $\hat{\mu} = \bar{X}$ sample
    - $\bar{X}$~$N(\mu, \sigma^2/n)$

* You can look at positive and negative z values and just take the inverse, because the z values are **symmetric** around zero

* $P(-Z_{\alpha/2} < Z < Z_{\alpha/2})=1-\alpha$

## Distribution Percentiles

* $N(0,1)$ *z-distribution*

* General notation
    - $Z_{\alpha}$
    - Suffix ($\alpha$) is the percentage on the *right*
    - Therefore, probability on the left is $1-\alpha$

* Example
    - $90^{th}$ percentile
    - $Z_{0.1} = 1.28$
        1. Look on nromal table
        2. Find probability of 0.95
        3. Check what value of z corresponds to it

* $Width=2*Z_{\alpha/2}*\frac{\sigma}{\sqrt n}$

### Student's T Distribution

* Similar in nature to the z-distribution
 
* $t_v$: A *t*-distribution with *v* degrees of freedom

* Degrees of freedom is a parameter of the t-distribution

* Lower and upper bounds
    - Lower: $t_v,\alpha/2$
    - Upper: $-t_v,\alpha/2$
        - $\alpha/2=0.025$

* $\hat{\sigma} =$S (Sample standard deviation)

* $\frac{\bar{X}-\mu}{S/\sqrt n}$~$t_{n-1}$

* $P(-t_{n-1,\alpha/2} < t < t_{n-1,\alpha/2})$
    - $P(\bar{X}-t_{n-1,\alpha/2} < \mu < \bar{X} + t_{n-1,\alpha/2})$
    - *The margin of error*
    - $ME = t_{n-1,\alpha/2}*-\frac{s}{\sqrt{n}}$

## Non-Normal Populations

* We can use CLT (provided that $n \geq 25$)
    - Otherwise, there is nothing we can do

* If $\sigma^2$ is unknown...
    - We can still use CLT, but we need to calculate t-score
    - We can then replace $\sigma^2$ with $s^2$
    - Two layers of estimation here
    - $n \geq 40$
    - If n is not sufficiently large, nothing can be done
    - When $n \geq 200$, it doesn't really matter whether z or t is used

### Chapter 7 Practice Problems

* Problem #1
    - Part a 
        1. Find $\alpha/2$
            - 90% CI for $\mu$, true average yield point
            - Normal population, $\sigma$ known
            - $\bar{X}=8439$
            - $1-\alpha=0.9, \alpha=0.1$
            - $=\alpha/2=0.05$
            - We want the $95^{th}$ percentile
            - $\sigma=100, n=25$
        2. Normal table lookup
            - $Z_{0.05}=1.645$
        3. Find value of Z
            - $8439 \pm (1.645*\frac{100}{\sqrt 25})$
            - $(84061, 8471.9)$
            - *Based on this data, we are 90% confident that the true average yield point lies between 84061 ibs and 8471.9 ibs*
    - Part b
        - Changing the condidence interval changes $\alpha$
        - $100(1-\alpha)$% = 92%
        - $...z_{0.04}=1.75$
        - Look in z table at 0.96 CI ($1-\alpha/2$)
    - Part c
        - $width = 8471.9 - 8406.1 = 65.8$
            - *New width, reduced by 20*
        - $(\bar{X}-Z_{alpha/2}*\frac{\sigma}{\sqrt n}, \bar{X}+Z_{alpha/2}*\frac{\sigma}{\sqrt n})$
        - $width = 2Z_{alpha/2}*\frac{\sigma}{\sqrt n}=45.8$
            - *Solve for n*
            - $...n=52$
        - *To reduce the width by 20, we need an additional 27 data points*

* Problem #2
    - *Posted later*

* Problem #3
    - Part a
        - Case 2: Normal distribution, $\sigma$ not known (only told sample distribution)
        - $\bar{X} + t_{26-1};\alpha/2 * \frac{s}{\sqrt n}$
    - Part b
        - $(-\infty, \bar{X} + t_{26-1};\alpha * \frac{s}{\sqrt n})$
        - $=(-\infty, 378.8498)$

* Problem #4
    - Part a
        - **If no CI is mentioned, go for 95%**

## Population Proportion of $\rho$

* Sample proportion: $\hat{\rho}=\frac xn$

* By CLT:
    - $\hat\rho$~$N(\rho, \frac{\rho(1-\rho)}{n})$
<br><br>
* $\hat\rho \pm Z_{\alpha/2}*\sqrt{\frac{\rho(1-\rho)}{n})}$

