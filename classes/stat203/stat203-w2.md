# STAT 203 - Week 2 Notes

* Skewed left
    - mean < median < mode

* Skewed right
    - 

* Balanced (Bell)
    - mean ~= median ~= mode

* Mean squared deviations
    - Sample variance: $s^2 = 1/(n-1)\sum{1}^{n}(xi-xbar)^2$
    - Why do we divide by n-1 rather than n?
        - Because there is a slight underestimation that happens, so we subtract 1 from the denominator to correct for this

* Sample standard deviation 
    - $s = +-\sqrt{s^2}

* Variance is sigma sqrd, std deviation is sigma
