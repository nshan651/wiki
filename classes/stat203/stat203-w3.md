# STAT 203 - Week 3 Notes

## Ch. 2: Probability 

* Set theory 
    - Sample space: space of all possible outcomes
    - Events: subset within the sample space
        - Simple and composite events

* Compliments: Everything outside A (A')

* Union & Intersection

* Union
    - A U B: includes all, none, and both elements

* Intersection
    - A inter B: only includes shared elements

* Empty (null) set {}

* Disjoint (mutually exclusive) set
    - Two events A and B are disjoint or mutually exclusive if and only if the intersection is the null set
    - I.e., there is no overlap or common elements between the two

* Exhaustive set
    - Two events A and B are exhaustive if A U B is the universal set
    - If A and A' are the universal set

* How to deal with multiple events?
    - Distributed law

* Distributed law:
    - A U (B int C) = (A U B) int (A U C)
    - A int (B U C) = (A int B) U (A int C)
        - Prove these two laws by shading venn diagrams
        - For union, shade both circles involved
        - For intersection, shade overlap
        - If after complete and shading is equal, then the equation is true!
        - Create a sequence of drawings to show the steps

* Subsets 
    - A <_ B if every element of A is also in B

## Probability

* Probability of an event A and the chances that A would occur
    - P(A) = (# of outcomes for A) / (# total outcomes)

* Axioms of probability
    - P(A) >= 0
    - P(S) = 1
        - Corollary 1: Probability will be 0 >= P >= 1
        - Corollary 2:  P(A U A') = P(S) = 1
    - Let A1,A2,A3,...An ne a set of disjoint/mutually exclusive events...
        - Then, P(SUM_{i=1}^{INF} Ai)
        - Corollary 3: P(A U A') = 1 and P(A') = 1 - P(A)
        - P(A int A') = 0
    - For any disjoint events A and B...
        - P(A int B) = P(NULL) = 0

* Additive Rule 
    - P(A U B) = P(A) + P(B) - P(A int B)
        - Intersection is counted twice (overestimate, so we subtract intersection)
        - If the intersection is zero, then just add the two sets without subtracting intersection

* Sample space and counting events

* Product rule
    - The probabilities of two separate events can simply be multiplied to get the total possibilities
    - If A can happen in N1 ways and B can happen in N2 ways, then A and B can happen in N1 * N2 ways
    - Ex.) Instructors students
        - 3 instructors and 3 tutors
        - 9 different ways to construct the course (if paring of 1 student to 1 instructor)
        - Could add more and more nodes to the tree (for example, online vs offline)

* Tree diagram is useful for representing the product rule of outcomes

* All probabilities can be expressed in terms of union and intersection
    - Ex.) P(A) = 1/2
        - P(2 U 4 U 6) = 
          P(2) + P(4) + P(6) =
          1/6 + 1/6 + 1/6 = 1/2

* Union is OR, Intersection is AND 

* Ex.) tossing a coin AND rolling 6-sided fair dice
    - Sample space of 12 (2 sided coin * 6 sided dice)
    -  6/12 + 2/12 - 1/12 = 7/12 

    
