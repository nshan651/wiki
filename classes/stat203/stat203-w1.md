# Stat 203 Week 1 Notes

## Chapter 1: Experiments

* Research questions involve experiments

* There are several key ingredients involved in an experiment
    - A well-defined population
    - One or more variables that define what is measured

* Constraints on an experiment
    - Time, budget, level of access

* Sampling techniques
    - Simple, stratified, convenience

        *** UCI ML Data Repository: https://archive.ics.uci.edu/ml/datasets.php

* Variables
    - Univariate, Bivariate/Multivariate

* Data analysis
    - Preliminary/exploratory data analysis
        - EDA is a popular acronym

* Statistical inference methods
    - Estimation
        - Point and interval estimation
    - Hypothesis testing
        - Single and multiple sampling

### Sampling techniques

* Simple sampling
    - Simple random sampling with replacement (SRSWR)
        - Random sampling technqiue where a sample can be chosen multiple times
    - Simple random sampling without replacement (SRSOR)
        - Technique where duplicates are not included
    - Usually the best form of sampling

* Stratified sampling
    - Define the strata
    - Take a simple random sample from each strata
    - "Strata" are just the groupings of the samples
        - i.e. students by age, household income, race, gender
    - Can avoid bias, but can also lead to very misleading results, depending on the strata that are chosen
    - Generally not as preferable as simple sampling

* Convenience sampling
    - Usually not recommended
    - Sample is based on convenience of the data-aggregator
    - Usually leads to clustering which does not always give a full representation of the population
    - One good use case: useful in quality control, where random sampling can be destructive
        - i.e. brick layer testing a stack of bricks instead of bricks uin houses

### Data

* Discrete and continuous variables
    - Discrete: in a range of fixed (countable) values
    - Continuous: in a range, variable can take any possible value

* Secondary data: data collected earlier for a different purpose or experiment now being used for something else

* Univariate and multivariate (including bivariate) data

### Visualization

* Symmetric data

* Skewed data
    - Skewed right (positively skewed) and skewed left (negatively skewed)

* Stem & leaf display
    - Decide on stem and leaf based upon available data
    - To determine skew direction, draw a vertical curve along leaves

## Chapter 1 pset

* Question 1:
    a.) No, this group is not selected from a population. It is the population
        ** An example of a sample from this population would be students under a particular SI
    b.) To avoid bias, to make sure both groups are comparable in terms of level, size, race, etc.
    c.) Becuase it is extremely important to have a control group. Need a strong baseline to judge the relative difference

* Question 2:
    Human error, variance that naturally exists (i.e. environmental factors), measurement imprecision
