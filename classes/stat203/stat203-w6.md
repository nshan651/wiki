# STAT 203 Week 6 Notes

## Discrete random variables

* X vs x
    - Capital X is a sample population, little x is a given result drawn from the sample
    - x is random because it could be any value within population

* Probability mass function (PMF)
    - Probability at anything NOT in the set is always **zero**
    - For probability less than or equal to some x, find CDF using known probabilities

* Two kinds of probability questions
    - PMF (=)
    - CDF (<=)

* Examples
    - P(X<=3) = F(3) *CDF*
    - P(X<3) = P(X<=1) = F(1) *PMF*
    - P(1 <= X <= 4) = P(X<=4)-P(X<=1)

* P(X>1) = 1 - P(X <= 1)
* P(X>=3) = 1 - P(X < 3) 
  - OR P(X>=3) = 1 - P(X <= 1)

* General Rules:
    - P(X < x) = P(X <= x-1) = F(x-1)
    - P(X <= x) = F(x)
        - x-1 is the previous value
    - P(X >= x) = 1 - P(X <= x-1)

* PMF shows how the total probability is distributed

### Example Problems

* Problem #2
    - P(X<=3) = F(3)
    - P(X<3) = F(2)
    - P(X>=3) = 1 - P(X<=2) = 1 - F(2)
    - P(2<=X<=5) = F(5) - F(1)
    - P(X<=1 or X>=5) = P(X<=1) + P(X>=5) = F(1) + 1 - F(4)
    - P(X<=3) = F(3)
    
* Problem #3
    - P(X=2) = P(X<=2) - P(X<=1)
        - =F(2) - F(1) = 0.39 - 0.19 = 0.2
    - P(X>3) = 1 - P(X <= 3) = 1 - F(3) = 0.67
    - P(2<=X<=5) = P(X<=5) - P(X<=1) = 0.97-0.19 = 0.78
    - P(2 < X < 5) = P(X<=4) - P(X<=2) = 0.92 - 0.39

## Mean & Variance

* Mean of X === population mean === expected value of X
    - Represented by greek letter *mu*

* Population mean $\mu = E(X) = \sum_{x}^{X} x * p(x)$
    - E(X) is the expected value

* It doesn't always make sense to weigh probabilities evenly

* Properties
    - Let c be a constant...
    - E(c) = c
    - E(cX) = cE(X)
    - E(c+X) = c + E(X)
    
* General statement
    - For constants a,b, E(aX+b) = aE(X) + b

* Population Variance ($\mu^2$)
    - Variance of X = V(X)
    - $E(X - mu)^2$
        - Also valid...$E(X-E(x))^2$

* Calculating variance
    - $\mu^2 = E(X-\mu)^2 = E(X^2) - \mu^2$

* Properties of variance
    - Let c be a constant...
    - $V(cX) = c^2V(x)$
    - $V(c+X) = V(X)$ *Nothing changes!*
    - $V(c) = 0$
        - You can only caluclate variance of a **random** variable

### More Examples

* Problem #4
    - $\mu = E(X) = \sum_{x}^{X} x*p(x)$
    - $= (1*0.2) + (2*0.4) + 3*0.3) + (4*0.1)$
    - $= 0.2 + 0.8 + 0.9 + 0.4 = 2.3$

---

## Populations

* Inside expectation E and Variance V, always write a capital X 

* Variance formula
    - $\sigma^2 = E(X^2) - \mu^2$

* Properties of expectation and variance
    - Assume a and b are constants
    - $E(aX+b) = aE(x)+b$
    - $V(aX+b) = a^2V(X)$

### Even more examples

* Problem #4
    - $V(X) = E(X - E(X))^2 = E(X^2)-(E(X))^2$
    - $E(X^2) = (1^2*0.2) + (2^2 * 0.4) + ...$
    - $= 6.1 + 2.3^2 = 0.81$
    - STD $= \sqrt{0.81} \approx 0.9$

* Follow-up on linear combination...
    - Find Y
    - $Y = 100 - 5X$
    - $E(Y) = E(100-5X)$
    - $E(100 - E(5X)$
    - $100 - (5*2.3)$

* Problem #5
    - Find the expected value of $\frac{1}{X}$
    - $E(h(X)) = \sum_{x=1}^{6} \frac1X * p(x)$
    - $= (1*\frac16) + (\frac12 * \frac16) + (\frac13 + \frac16) + ...$
    - $=\frac{147}{360}$

## Specific Probability Distributions

1. *Binomial*
2. Negative Binomial
3. Poisson

* Key is in the identification of the distribution, the math is simple

* Binomial Experiment
    - n trials (mini experiments)
    - Bernouli trial $\equiv$ binary trial
        - Two possible outcomes
        - Outcome of one does not affect another
    - Simple example: n tosses of a coin

* Binomial
    1. n (fixed) number of trials
    2. Each trial has two outcomes (success and failure)
    3. $P(success)= \rho$ *is fixed from trial to trial*
    4. Trials are independent

* Notation:
    - $X\sim Bin(n, \rho)$
    - Tilde $\equiv$ has a, follows a, distribution of...
    - $P(X=x)$: Out of n trials, there 'x' sucesses
        - $(n-x)$ failures

* TIP: In calculating probabilities, try to reduce to less than or equal to, and from there to F(X)

* $\mu = E(X) = n * \rho$

### Examples

* Problem #1
    - $\rho = P(S) = 0.75$
    - n=10 *number of trials*
    - *Sucesses out of n trials*
    - $P(X=4) = P(X\leq4) - P(\leq3)$
        - *Consult the binomial table*
    - $=0.02 - 0.004 = 0.016$

* Problem #5
    - n is 5
