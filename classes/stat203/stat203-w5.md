# STAT 203 - Week 5 Notes

## Partition

* Partition: a sequence of events B1,B2,...,Bk is called a partition if
    - I.) Bi int Bj = {} (disjoint events, or mutually exclusive)
    - II.) U_{i}^{k}Bk = S (exhaustive, sigma)
        - Total union of all events is equal to the sample space itself

* Law of Total Probability
    - Can use partitioning to find a *new* partition's probability
    - If A is the partition, the new probability is the union of all intersections with each known partition 
    - P(A) = P(A int B1) U (A int B2) U ... (A int Bn)
    - P(A) = P(B1)P(A | B1) + P(B2)P(A | B2) ... P(Bn)P(A | Bn)
    - P(A) = SUM_{k=1}^{k} P(Bk)P(A | Bk)
    - Remember: this must be **exhaustive** in order to accurately represent partition
    - Also, must be disjoint to prevent double-counting
    - Very similar to a weighted average

* Bayes Theorem
    - Theorem is asking the *reverse* conditional probability
    - What is the conditional probability given P(A) 
    - P(B1 | A)
    - We know the conditional result, but not the unconditional base case
    - Ex.)
        - "Given that a person has x disease what is the probability that they belong to age group y"
    - P(Bi | A) = P(Bi)P(A | Bi) / P(A) (<-- total probability)
       
* Conditional probabilities do not need to add up to one, only partition probabilities do!!!

* Example:
    - Rare disease affects D people
        - P(D) = 0.01, P(D') = 0.99
        - P(A | D) = 0.95 (True positive)
        - P(A | D') = 0.03 (False positive)
    - How good is the test kit...?
        - Find P(D | A)

* If the affirmative is really rare, the conditional probability has to be *really* good

* Problem #12
    - Part b
        - P(A1) = 0.4, P(A2) = 0.35, P(A3) = 0.25 *Partition! Notice that probabilities add up to 100*
        - P(B | A1) = 0.3, P(B | A2) = 0.6, P(B | A3) = 0.5
        - P(A2 | B) = P(A2) * P(B | A2) 
            = 0.35 * 0.6
        - P(B) = *Use total probability*
    - Part c (reverse direction probability, Bayes)
        - Find P(A1 | B), P(A2 | B), P(A3 | B)
        - Use Bayes for each

* Problem 13
    - P(B) = 0.4, P(B') = 0.6, P(W | B) = 0.3, P( W | B') = 0.5
    - Find P(B | W) *Bayes*
        - Kind of a retrospective question
    - P(B | W) = P (B int W) / P(W) *Expand to multiplicative rule*
    - P(B)P(W | B) / P(B)P(W | B) + P(B')P(W | B')
    - = (0.3 * 0.4) / (0.4 * 0.3) + (0.6 * 0.5) = 0.2857

* Problem 14
    - P(T | G) = 0.95, P(T' | G') = 0.99 *Conditionals*
    - P(G) = 1/200 = 0.005; P(G') = 0.995
    - Find P(T) and P(T') *Use total probability*
    - P(A' | B) = 1 - P(A | B)
        - **IMPORTANT: ADD TO FORMULAS** As long as the conditions are the *same* you can apply the compliment rule for conditional probabilities

## Random Variables & Their Distribution

* Random variable:
    - A function that maps from the sample space to the set of real numbers
        - Sample space is all possible outcomes of an experiment

* Notation of a random variable is ALWAYS in caps
    - X, Y, Z
    - Lowercase characters denote *specific* values of the random variables
    
* x is a subset of X

* Discrete vs continuous variables
    - Blinking (discrete random var), height (continuous random var)

* Random variable means that it is a repeatable experiment which may produce variable results
    - Anchoring to a specific time will result in a specificknown value
        - ex.) experiment on a particular day will produce specific result that CANNOT be repeated.
        - To make the above case random, extend epoch to include any time/more generous time frame
    - Not sure of the outcome *a priori*

* Probability distribution table
    - Possible values of x sum to the total probability of sample space X (1.00)
        x | P(X=x)
        ---------
        0 | 1/4
        1 | 1/2
        2 | 1/4
    - Probability distribution table

* Probability mass function (PMF)
    - For a discrete random variable, probability that X takes a particular value x is given by its PMF
        - ex.) 1/4 + 1/2 + 1/4 = 1 are the masses

* Generalization:
    - $P(X=x) = (1/2)^2 * (1/2)^{1-x} | x = 0,1$
    - This models the probability as a function given inputs 0 and 1
    
* Abstraction:
    - $P(X=x) = (\alpha)^2 * (1-\alpha)^{1-\alpha} = 1-\alpha$

* Probability function = probability mass function = PMF

* Parameters
    - Probability function may involve such unknown parameters
    - These are unknown constants that define characteristics of the population

* PMF = P(X-x)
    - or...p(x) is also an acceptable notation
    - small p of small x. Particular probability for particular value
    - $\sum{all x} p(x) = 1$ *total probability*

* Cumulative distribution function (CDF)
    - For any X, CDF at x is the probability *up to* x
    - F(x) = P(X <= x)

* Example CDF:
        x | p(x) | F(x) = P(X <= x)
        ---------------------------
        0 | 0.2 | 0.2 
        1 | 0.4 | 0.2 + 0.4 = 0.6
        2 | 0.3 | 0.2 + 0.4 + 0.3 = 0.9
        4 | 0.1 | 0.2 + 0.4 + 0.3 + 0.1 = 1
            -- *Always ends equaling 1*
    - Can be represented as a histogram of probability densities 

* Formal CDF
    - $F(x) = \SUM{i=0}^{x} xi$

## Worksheet 3 Problems

* Problem #1
    - Ranging from 0 <= x <= 12 unbroken eggs for a given carton
    - Discrete, not random!
        - Specific known value
    - Discrete, U can be 1 to infinity
    - Continuous
    
