# HIST 212 - Week 4 Notes

## Triangle Shirtwaist Fire 

* "Shirtwaist" is a women's blouse or bodice styled like a tailored shirt

* Essentially no rules at this time regarding labor
    - If there are, it's relegated to state/municipality
    - No federal labor laws

* No gov support for workers

* Workers are motivated to unionize
    - One of the only concrete support mechanisms for workers' rights
    - Triangle factory was never unionized *fully* 

* Problems with unionizing
    - Many people consider it *socialism*
    - For many, goes against rugged American individualism

* Immigration laws at this time
    - No "illegal" immigrants except for the Chinese
    - They were the first group to have US immigration restrictions

* The workers were responsible for cleaning
    - But, when workers are already having their wages "sweated" out, there was no time nor energy to clean their spaces

* Atkins V the Childrens' Hospital
    - Court case arguing for basic minimum wage
    - Women workers would be able to work for minimum wage
    - DC Children's Hospital would NOT pay them the established minimum wage
    - Courts agreed that they should be paid minimum wage, BUT...they argued that they could not compel the hospital to pay them more

* Most important breakthroughs came for companies that recognized unions
    - This meant that the established union was able to approach the employers *as an entity*

## The Progressive Era and Taylorism 

* Frederick Taylor introduced the principles of scientific management
    - Principles were based on scienctific method

* Controversial among workers
    - Sometimes was helpful to them, other times it made their skills less valuable or created more work for them

* Progressive Era
    - Era of passing reforms and sweeping changes because of problems due to immigration and labor

* The arts and crafts movement
    - Movement among all the modernity
    - Rise of urbanization and increased technology brought an end to the movement

## Vocabulary

* Naturalization - process of becoming a citizen

* Alien - legal term refering to anyone not from the united states

* Free labor - referring to the status of the worker
    - Worker is free, working for pay

* Melting pot - describes different people assimilating into one homogenous society
        - Have a when for every answer in part 2! time frame to the year
        - Only care about the when of the primary sources

* Ratified year for the amendments!!!

* Q22 (corporation one, the corporate lawyer)

* Progressive era (1900s to 19-"teens")
