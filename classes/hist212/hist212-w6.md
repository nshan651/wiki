# HIST 212 Week 6 Notes

## Beginning of WW1

* Mothers were an icon for pacifism

## Prohibition

* Ban of alcohol

* Very popular for women to get into
    - Reason: one of the few movements women were permitted to get into

* Descrimination involved
    - Irish and German immigrants particularly known for drinking

* Overarching moral/social responsibilty
    - No drinking on Sunday

* Left up to Congress to determine the law
    - Created the Volstead act

* Prohibition resulted in a lot of increased illicit activity surrounding alcohol
    - speakeasies, moonshine, Al Capone

* Gov'n't purposely poisoned alcohol
    - Lots of people died
    - "You wouldn't have died if you didn't break the law" xD
