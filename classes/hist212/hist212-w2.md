# HIST 212 Week 2 Notes

* Reconstruction Era

* No question of union between North and South
    - This was a major pretence for the war

* Radical Republicans changed idea of citizenship; added newly freed men to the mix
    - Adding to the American working class as wage-laborers

* Free laborer
    - Voluntary labor that requires payment
    - Reinforced capitalism with the addition of more employability

* Civil rights act during Reconstruction
    - Law was written to give black civil rights
    - But, when Reconstruction was over, so too was the Civil Rights Law
        - Supreme Court overruled it

* Took another ~80 years to get strong enough Civil Rights protections (1964 Civil Rights Act)
    - This became an amendment to the Constitution

* Bloody shirt rhetoric
    - Both a personal appeal to emotion as well as an appeal to partisan loyalty

* Roger Prior states that slavery had nothing to do with the war. "slavery" to him was the fashion of the time. "Changes with the weather."
