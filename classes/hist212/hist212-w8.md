# HIST 212 Week 8 Notes

## Great Depression

* Recession was traditionally viewed as a "male" problem
    - Affected the working man
    - But, of course also affected the family

* Men wouldn't take jobs that women did
    - Women's jobs were largely unaffected, they went on with their tasks
    - They had to redouble their efforts to support the family
    - Men who lost their jobs would *not* help out at home

* Such rigid stereotypes around gender and race

* Social security was originally restricted along racial lines
    - Only provided through "worthy" work
    - Blacks were not permitted to have those jobs, and thus were not covered by SS

* Continued existential fear that there will not be enough social security funding 

