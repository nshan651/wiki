# HIST 212 Week 7 Notes

* Many people used WWI as an opportunity to demonstrate how they belonged in the US
    - Especially within minority groups

* Many blacks moved to African American communities that were popping up throughout the US 
    - Tulsa, OK, was one such place, which provided bountiful opportunities

* Black Wall Street
    - Tulsa's Greenwood District

* Took a lot of effort to resurface

* Black entrepreneur O.W. Gurley purchased over 40 acres of land in the area
    - Sold most of it to African Americans in an effort to create a strong black community

* Ironically, segregation was a motivation for the riots
    - Lower-class whites were jealous of black success, still believed they had some sort of *divine* rights

* In the wake of the massacre, the government and private businesses worked to drive down land prices
    - This forced black landowners to get less for their land
    - Concerted effort to take control away from Black Wall Street

## Great Depression

* Marked the first instance of stock speculation

* Also the beginning of the popularity of retail investing

* Nearly *every* bank failed
    - People did not get their money back

* Bright side: resulted in many additional financial rules to prevent a similar situation

* For those who lost money - there was no safety net


