# HIST 212 W1 Notes

## Civil War Context

* The Union
    - Lincoln (assasinated second term, April 1865)
        - First Republican (radical for the time)
    - First term V.P. Hannibal Hamlin (anti-slavery Gov of Maine)
    - Second term V.P. Andrew Johnson (opposed secession but supported slavery, Senator from Tennessee--state seceded, he stayed)

* The Confederacy
    - President Jefferson Davis (previous Sen. from Mississippi)
    - V.P. Alexander Stephens (previous Gov. of Georgia)


