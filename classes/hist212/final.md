# HIST 212 Final Exam

## Participation Grade

I believe that I have earned an 'A' in participation for the semester. I feel this is fair because I have deeply engaged with the material and I am consistently prepared to engage in thoughtful class discussions.

## Exam Questions

1. Executive Order 9066
    - February 19, 1942
    - Executive order 9066 gave the military authority to ban American citizens from a 50 to 60 mile-wide coastal area. Moreover, they were also authorized to transport citizens to camps. 3,200 Italians and 11,000 Germans were arrested. By far the largest impact was in the Japanese-American community, where entire communities were uprooted.
    - Japanese Americans suffered greatly from executive order 9066. The war-time measures targetted citizens and resident aliens alike. They were sent further inland to "relocation centers."

2. Compromises That Had To Be Made to Establish a Women's Branch of the Army.
    - 1941 (Bill that established first women's army corps.
    - The Women's Army Auxiliary Corps (WAAC) was originally established to work with the army in an auxiliary fashion. They made compromises with a lower relative ranking system and reduced pay compared to their male counterparts, even though they often had very comparable levels of responsibility. WAACs who served overseas did not receive overseas pay, government life insurance, veteran medical coverage, nor did they receive the same prisoner of war protections if captured.
    - Although the introduction of the Women's Army Auxiliary Corps represented a huge leap forward for womens' equality in the armed forces, it was not without its sacrafices. There was considerable backlash from congressmen and the general public, but the need for skilled communications operators was enough to convinnce most of the importance of the bill. Women who entered into the Auxiliary Core were still treated as comparably of lesser rank, even if their roles and responsibilities were functionally the same to that of their male compatriots.
    
3. “Social Factors” and Stereotypes that Led to Negative Public Images of Female Soldiers.
    - 1941-1943 (Inception of the WAAC to the signing of the WAC bill).
    - There were social factors and stereotypes that affected the WAAC before the bill was even passed. Conressional opponents of the bill wondered who would do the "cooking, the washing, the mending, the humble homey tasks to which every woman has devoted herself; who will nurture the children?" (Women's Army Corps 3). Unfortunately, the critisism did not end when the bill passed and the corps was assembled. Male soldiers were upset by the WAACs' involvement; they wanted everything to be the same when they returned to the US. WAACs' morals were constantly questioned. Perhaps most importantly was the fact that many soldiers who were already in comfortable non-combat roles were afraid of being replaced for combat.
    - There were a littany of reasons for WAACs to be criticized, most of which stemmed from gener stereotypes. Most men believed that the woman's place was at home, doing all the housework and taking care of the children. WAACs were seen as a threat to the traditional societal roles.
5. Four WWII Primary Docs: Numbers 14 and 20 and 23 and 46 on Women’s Work
(Hannah Woods of Ilinois Assn; Mechanic’s Suit: Alarm Clocks; Day Care)
5. Three WWII Primary Docs: Numbers 10 and 21 and 28 on Food Rationing
(Victory Garden; Meat Rationing; Sugar)
6. Three WWII Primary Docs: Numbers 04 and 19 and 32 on Collecting and Cooperating
(Scrap Drive; Rubber Conservation; Car Pooling)
1. Two WWII Primary Docs: Numbers 17 and 43 on Concern for Illinois Germans (Two Churches)
2. One WWII Primary Doc: Number 31 on Relocating Japanese-Americans in Illinois
3. Name two reasons why the post-WWII Red Scare was so successful
    - 1947-1957
    - One reason for the Red Scare's huge success was in the way that congressional hearings differed from ordinary criminal cases. Congressional hearings were not subject to the same due process, and so Communist accusations were much easier to make. The McCarthy era created an intense atmosphere of fear. Another reason for its success was the increased anti-Communist rhetoric. Many members of the public had changed their views by the end of WWII and the dawn of the Cold War. This allowed committees such as HUAC to achieve political hegemony
4. Name two ways HUAC investigations came to be a “symbolic ritual”
    - Early 1950s (When the "rituals" became a routine)
    - One of the ways in which HUAC turned into a symbolic ritual was in the routine "unmasking ceremonies" during the early 1950s. An expert witness would name an area of Communist infiltration that the committee was currently investigating, and would name several names. Subsequently, those people would be interviewed and asked if they were members of the communist party. The second way in which the "ritual" persisted was in the second phase of these interrogations, where witnesses would be asked to name others connected to the Communist party. Doing so would prove the intervewee's remorse and their commitment to Democracy.
5. Belonging to the Communist Party in the 1930s became perceived differently by the 1950s
    - 1938 (Inception of HUAC) through the 1950s
    - HUAC, established in 1938 as a response to backlash surrounding Roosevelt's New Deal, was far from the political fear-mongering powerhouse that it would become by the 1950s. In fact, they were seen as an irresponsible, fringe establishment who were driven more by ideological fervor than anything. In those days, being seen as a Communist was much less of a faux pas, and the public viewed most of HUAC's accusations as baseless.
6. The Federal Housing Administration’s role in solving the housing shortage for returning war veterans
    - 1945 through 1950 
    - The FHA's solution to solving the housing shortage post-WWII was to work with the VA to insure half of all new mortgages worldwide, and to guarantee mortgages for returning veterans. The two organizations worked together to create entire subdivisions and suburbs in bulk.
7. The Fair Housing Act’s impact on wealth equity between African Americans and Whites.
    - The VA and FHA created programs that perpetuated and strengthened segregation in the US. Although racial segregation was in direct violation of the Constitution and the Bill of Rights, there was next to no enforecement of these rights. Moreover, the housing disparity was a nationally organized project of the federal government - a deliberate effort by those in power to widen wealth inequality between African Americans and Whites.
8. The American Institute for Free Labor Development (AIFLD) was an educational program for labor leaders
    - 1962
    - The AIFLD was an educational program designed to instill democratic unionism and anti-Communist sentiment among Latin American labor leaders. The AIFLD staged trainings at their facilities in Front Royal, Virginia, which became known as the "Little Anti-Red Schoolhouse." the AFLID, in cooperation with the CIA, was found to have been involved in several "covert incursions" in Latin America. In particular, they aided a Guatemalan uprising backed by Colonel Carlos Castillo Armas, which toppled a pro-union Guatemalan government.
9. Two ways “Duck and Cover” protected America
10. President Harry Truman implemented the containment strategy (of whom?) with the Truman Doctrine
    - 1947
    - The Truman administration implemented the containment strategy of Communism in an effort to stop Soviet Russia. With the Truman Doctrine speech in 1947, the President outlined a dichotomous world - Communists and everyone else.
11. The Great Powers’ reaction to Ho Chi Minh’s declaration of Vietnam independence
12. President JFK’s decision to deepen the U.S. commitment in Vietnam collided with at least two factors.
13. Describe the Braceros arrangement begun during WWII
14. Activists Cesar Chavez and Dolores Huerta’s historic launch
15. Congress amended the Fair Labor Standards Act
16. Role of stereotypes in protecting status quo of anti-union and anti-voting rights
17. Role of non-violence in labor rights and civil rights
18. Getting mainstream America to pay attention to the fight for voting rights in Mississippi
19. Two kinds of lessons taught at Freedom Schools
20. President John Kennedy and the Moon Landing goal
21. President Lyndon Johnson and the Civil Rights Act
22. A common element of urban rioting in the 1960s
23. Impact of highway building on two communities in New Orleans
24. Two elements of highway building designed to improve central cities
25. Stokely Carmichael supported political “Black Power”
26. Stokely Carmichael did not support racial integration
27. Stokely Carmichael notes that poor whites are becoming more hostile as attention is focused on Blacks
28. Two arguments that business used against Rachel Carson as she fought to protect the environment
29. The Women's Liberation Movement did not see itself as “liberal" but as “radical”
30. Shirley Chisholm, in supporting the ERA, admits that “laws will not eliminate prejudice from the heart”
31. Two examples of the discrimination women faced in higher education, in the rights action by Bernice Sandler
32. Phyllis Schlafly was an important activist of the Twentieth Century

