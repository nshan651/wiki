# HIST 212 Week 5 Notes

## Progressive Era Labor Changes

* Photography is important for reform
    - Document moments

### Analysis Paper Quick Notes

* All photos taken during the Progressive era

* Photo worksheet for each photo
    - Use it as a rubric for taking notes
    - Will turn it in at the end
    - Focus on everything happening, not just the main focus of the photo
