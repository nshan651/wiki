# HIST 212 - Week 3

## Ida B. Wells

* Lynching is "and always was" against the law
    - "Lynch law" was mocking this fact
    - People were able to get varying degrees of support for there actions

* Ensuing violence in response to rapes was almost never directed toward justice for the victim
    - Excuse for lynching

* Ida was a race reformer
    - Also interested in women's rights, but her focus was on getting improvements for her race

* One of the methods of protests was not engaging
    - Leaving areas, not buying from places

* This got people in trouble!
    - People not supposed to have autonomy

* States are not allowed to create laws that go against the rights of its citizens
    - To deny any **person** within the jurisdiction within the laws

* Black codes were an example of this

* Right to due process
    - Including a fair trial

* Bill of Rights added these protections of the law

* Public perception of the "worthy" voter
    - Educated, white, male

* Federal anti-lynch law
    - Still does not exist
    - Lynch "mob" is three or more people

## Corporate Personhood

* Supreme court is "supposed to determine" the spirit of the Constitution

* Conkling's account of drafting the 14th amendment: persuassive case for corp personhood

* 14th amendment: guarantees equal protections from the law to all *persons*
    - Note that persons was originally citizens before being changed to include legal entities

* Dumping waste in the river

* San Mateo vs South Pacific
    - S. Pacific wanted personhood to deduct their mortgages
    - Would not have to pay taxes, California citizens upset by this
    - Also would not have the same risk of losing the land that an individual would
    - Also, Conkling (court reporter reporting) was HEAVILY biased
        - He was involved in drafting the 14th amendment

* 
