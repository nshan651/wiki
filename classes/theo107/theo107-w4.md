# THEO 107 - Week 4 Notes

## Sports as religion

* Loose definitions of religion
    - System of beliefs and behavior representing social ideals
    - System of distinctions made by the group for things sacred and profane
    - "Society worshipping itself" -> effervescence

* Sports is an example of the third, the celebration of one's self

* A totem in religion is any relgious icon that people can gather around
    - i.e. sports mascots

* Physical aspects of religion
    - Architecture, hierarchies, rituals, stories, etc.

* Once a group reached a certain size, totems were necessary to form trust with strangers
    - Anyone sharing the same totems/beliefs are automatically a little bit easier to trust

* Loyola community and religion

* Religion is seen as a mutually exclusive category
    - Leaving or considering multiple groups is considered "blasphemy"
    - Calling something religious changes the reality
    - This becomes a matter of politics

* The word "religion" still exists for political reasons, to codify different groups and beliefs  
    - However, there is no "unique" characteristic of religion 

## Functionalism and the Hermeneutics of Suspicion

* Functionalism: The function of religion

* Religion is not just a belief system

* Ways to understand religion  
    - Religion as culture
     
* Hermeneutic of suspicion
    - Interpretation of suspicion (and superstition) in religion
    - Looking at things through a lens of suspicion 

* The approach:
    - Consider a cultural element
    - Approach with herm of sus 
        - Methodological atheism
    - Seek functional explanation

* Functionalist structure
    - There are punishments and rewards for actions

* India and cows
    - Reprecussions for harming cows
    - Functional uses for cows
        - Animal products, ecosystem stimulation, methane/fuel products

* In defense of hermeneuticism
    - 
