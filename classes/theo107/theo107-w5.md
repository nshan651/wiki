# THEO 107 - Week 5 Notes

* Hermeneutic of Suspicion
    - The interpretation of events as being consequential

* Karma of Women
    - H of S ties into this; women were said to be born as such due to bad karma in a previous life
    - "Cause and effect" of the suffering women experienced from birth and menstration

* Thailand & their sex trade
    - Devoutly Buddhist
    - One of the larges sex trades of the world

* How do we find the "authentic" Buddhism?
    - Find the original sources
        - Religious texts, scholars, monks
    - Search for modern practicioners
        - For example, the Buddhist prostitutes in Thailand

* Issues with these approaches
    - Primarily an oral tradition
    - Many consider text to be a problem (especially Zen Buddhists)

* Being a Buddhist woman
    - Considered "bad karma" to be born a woman
    - To reach Nirvana, it was said that women had to be good in the current life and hope to be reborn a man 
        - This was also originally the only path to monkhood for women
    - Women were often stuck dealing with violence and unhealthy relationships because both they and others were told that it was a result of karma
        - Little interference on their behalf

* Eventually there was a female monastic order of nuns
    - The Budda was rather reluctant about the whole process

* Many rules:
    - Women cannot go off by themselves
    - All women have to bow before men
    - Women cannot make fun of male monks

* Many female monastic oders died out due to lack of funding
    - It was considered less cosmically rewarding to give to women

* Women who rebel against the system only create more bad karma for themselves
    - Lose-lose situation

* Theodicy
    - "Vindication of God"
    - Answers the question of why a Good God/deity allows for suffering
    - Karma provides the perfect theodicy
        - Infallable relationship between cause and effect
        - Makes it difficult for any real social justice or changes to occur

## Principles of Social Construction 

* First step in sophisticated scholarship...
    - Getting past common-sense view of language
    - But what is common-sense?
        - Based on base assumptions that words always map to reality

* "Rose by another name..." - Bill Shakes
    - Underlying biological material naturally occuring
    
* Social constructs
    - No "it"
    
* Religion is the sum of all its members
    - "There is no Buddhism, only Buddhists"

* No "it", so we study how people talk about "it"

* Cookie cutter analogy
    - Cutter = word, tool (religion, for instance)
    - Dough = all human expression

* Words are variable
    - No ultimate truth of words, just different uses

* Truth depends on the community

* Social facts are real in that they are felt
