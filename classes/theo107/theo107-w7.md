# THEO 107 Week 7 Notes

## Timeline of History

* Tree of knowledge
    - Cognitive revolution
    - Cause

* Consequences
    - Increased precision of communication
    - Gossip
    - Transmit information about things that do not exist (naturally)
        - Individually and collectively
        - Religion adaptive and maladaptive
