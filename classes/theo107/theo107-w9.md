# THEO 107 Week 9

## Snake Charmers

* Context
    - Geography, sociology, race/ethnicity

* Insider Perspective
    - Why follow directions?
    - Literal biblical interpretation
    - Spiritual gifts
    - Proof of relationship/favor of God
    - Proof of salvation

* Outside theory using hermaneutic of suspicion and Functionalism
    - Identity formation/flip the social order
    - Strict religion/costly ritual
    - Trance/other worlds

* Other questions
    - Legality
    - Secular state regulation/religious freedom
