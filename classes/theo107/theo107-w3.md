# THEO 107 - Week 3

* School lunches:
    - Is this a positive or negative example of secularism?

* Quebec religious clothing
    - Religious clothing not always "religious" necessarily
    - Also has cultural and traditional significance
    - Clothing example moves religion from mind to body

* Motivation for decision(s)
    - Economic
    - Preserve the secularist state
    - Marginalization

## Rational/Irrational Violence 

* Afghanistan invasion killed 9x as many people as 9/11

* Fine line between secular state and religious authority
    - Both had their own reasons for actions

## Spirituality vs Religiousness

* Spirituality can mean anything connecting to some larger purpose/meaning in life
    - Does not necessarily need to be organized as religion is
    - Looser principles

* Spirituality is individual, religion is communal (stereotype, does not necessarily always hold)

* Reasons to become Spiritual 
    - Avoid politics of religion
    - Move away from a religion because one had a previous negative experience
    - Religion is '''Irrational'''
    - Fits well with Capitalism
        - Pick and choose what works for you

* SBNR (Spiritual but not religious) 

* Pluralism

* Why the spirituality movement has been primarily driven by white people
    - Economic priviledge: wppl may have the autonomy to leave communities
