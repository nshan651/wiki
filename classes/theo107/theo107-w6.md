# THEO 107 Week 6 Notes

## Facilitator Sheet

### "A Trans-Species Definition of Religion"

* Symbolic behaviors are used as a more inclusive category, rather than looking at "religious" or "spiritual" behaviors which are less descriptive.

* Animals such as birds, baboons, monkeys, and mongooses show *reverance* in the morning and evening sun, suggesting some sort of "universal urge to worship," a "sun cult."

* Critics state that the descriptions of these behaviors "anthropomorphize" animals, and that there cannot be any meaning without language. Others fall somewhere in the middle, saying that observed primate symbolic behaviors could be a "precursor" to human spirituality and religion, but that this is not sufficient to definitively say that chimpanzees are indeed spiritual or religous.

* Assumptions in a religious definition
    - Anthropomorphic
    - Anthropocentric
    - Logocentric
    - Hypotheses on inner experiences that cannot be scientifically observed and verified

* Anthropomorphic definitions apply human-like experiences to distinctly non-human phenomena.

* The logocentric assumption requires that a religion feature a "language" with interpretable and semantically meaningful symbols.

* Human definition of religion/spirituality:
    - "Religion/Spirituality denotes a ritualization that binds individuals together or back together in empathic intimacy with respect to experiences of aliveness and animacy, which may secondarily involve the witnessing of this by a collective social group."

* Trans-species definition, including five defining behaviors:
    - Communion (To commune with empathic intimacy with respect to animacy)
    - Reverance (Intense love, deep respect, may involve silence)
    - Careful observance (May involve a remark or announcement)
    - Terror
    - Wonder

* Could be useful for studying evolutionary biology

* Religion determined from DNA??

* Understanding trans-species religious behavior allows us to empathize more with animal species (environmental argument).

### Key Arguments

Harrod argues that "religious" and "spiritual" behavior needs a more robust definition that is not human-centered and which can be applied across species.

### Vocab

1. *Anthropocentricim*: The belief that humans are the central element of the universe; interpreting reality exclusively in terms of human experience.
2. *Anthropomorphism*: Attribution of human characteristics to inanimate objects, animals, or other phenomena.
3. *Hominid*: Any member of family (Hominidae) of erect, bipedal, primate mammals.
4. *Animacy*: Expresses how sentient or alive the noun is

### Critical Thinking Questions

1. Why might a more universal, trans-species definition of religion and spirituality also prove to be useful when studying human religions?
2. What is the significance of comparing spirituality across species? What can we learn from it?
3. Is linguistic ability really necessary for being spiritual or religious?

## Essentialism

* Essentialism
    - Practice in assigning some defined "essential" characteristics to every object

* EB Tylor's "Primitives" to civilization
    - Animism -> polytheism -> monotheism -> rational science
    - Puts himself at the top
    - Social Darwinism

* JG Frazer (1854-1941)
    - Magic -> religion -> science
    - Magic is seen as less sophisticated

* Problems of Essentialism
    - An "essence" cannot be observed
