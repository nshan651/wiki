# Cheat Sheet

## Mestiza Langugages of Religion 133-135 - Anzaldua

* History of long walks: tradition of dislocation, migration, and resettlement.

* Anzaldua turns a critical eye toward the paradigms that celebrate plurality and diversity, but which fail to consider the historical consequences of cultural contact. Anzaldua points to the example of the "tradition of long walks," where the US-Mexican war left Mexicans isolated in Texas with nowhere to go

* Anzaldua, along with other mestiza, have been living in a hybrid world. She states that this has shaped an alternative ethics of "nepantla," an ethics of dwelling on the edge.
    - Refers to the liminal space that the mestiza inhabit

* Victor Turner - Rites of passage and pilgramage
    - Separation
        - Ordinary, everyday, predictible
    - Liminality
        - Dangerous, uncertain, uncomfortable
    - Aggregation
        - Reunited, new identity
    - Confines these to special rites of passage or sacred practices

* Contrasts by Anzaldua, who seems to exist liminally in a state of permanence

* Trauma in popular culture has become something productive. 
  
* Anzaldua argues that we shouldn't be so eager to transcend painful experiences

* She explains that trauma signifies a *rupture*, something so significant that one cannot begin to process the experience.
    - It is typified by its sudden quality

## The Sufi Mystic Way - Al-Ghazali

* The way of mystics involves both knowledge and deeds

* The goal is to eliminate any defects and vices of one's character, and to overcome any obstacles set up by one's own self.

* The only way to properly learn about the mystic state of the soul through first-hand experience.

* It is important for him to avoid all wealth and honors so that he does not get too attatched to anything of this world.

* Al-Ghazali reflects on his proudest achievement: his teaching and instructions. He realized that he was motivated out of personal glory, rather than for the glory of God.

* He feels conflicted between his secure, established position and abandoning what he has worked on to seek God.

* He finally submits himself to God, and travels to Damascus, and then to Jerusalem.

* The Way is opened by the "fusion of the heart with God's name," and with the annihilation of the self.

* Three degrees of knowledge:
    - Science: verification by proof
    - Experience or savoring: knowledge from ecstasy
    - Faith: acceptance of oral testimony and second-hand experience of others

* God transcends this hierarchy of knowledge.

* Another class of people is completely ignorant; these people deny everything.

## Key Arguments

* Anzaldua discusses living between worlds, and her inability to feel fully connected with either side of her heritage. She underlines the ways in which inhabiting these liminal spaces has shaped her ethics. Anzaldua is critical of the celebration of "cultural diversity," which often fails to account for the trauma attached.

* Al-Ghazali's writing is also related to identity. However, he is more interested in separating himself from his worldly achievments and identity in an effort to become closer with God. He works to disentangle his identity as a teacher with his relationship to God. Al-Ghazali identifies three different degrees of knowledge, of which God transcends.

## Vocabulary

* Nepantla - Aztec word that signifies an in-between state.

* Sacralization - The act of making something sacred

* Chicano/a - A person of Mexican origin living in the United States.
 
* Mestizo/a - Describes a person of both European and Indigenous American ancestry.

## Critical Thinking Questions

1. What are some similarities and differences between the ways in which Anzaldua and Al-Ghazali grapple with the concept of identity.
2. Why does popular culture seem to fetishize overcoming trauma; what would Anzaldua suggest as a healthy way to deal with trauma.
3. What causes some to form their ethics based on identity, and others to influence their identity based on ethics? What comes first?
