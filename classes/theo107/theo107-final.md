# THEO 107 Final Exam

## Table of Contents
---
* [Mar 15: Roots of Religion/Cognitive Revolution](#Mar 15: Roots of Religion/Cognitive Revolution)
* [Mar 17: Cultural Ev., Why Does Religion Spend So Much Time On Sex?](#Mar 17: Cultural Ev., Why Does Religion Spend So Much Time On Sex?)
* [Mar 22: Strict Religious Theory and Costly Rituals](#Mar 22: Strict Religious Theory and Costly Rituals)
* [Mar 24: Docility & Social Instincts Hypothesis - Does Marxist Theory Apply?](#Mar 24: Docility & Social Instincts Hypothesis - Does Marxist Theory Apply?)
* [Mar 29: Religion and Empire Theory/Morality and Sureillance](#Mar 29: Religion and Empire Theory/Morality and Sureillance)
* [Mar 31: Religion and Fitness Benefits Theory/Cooperation and Prosocial Behavior](#Mar 31: Religion and Fitness Benefits Theory/Cooperation and Prosocial Behavior)
* [Apr 5: Social Stability and the Individual Interplay](#Apr 5: Social Stability and the Individual Interplay)
* [Apr 7: Practicing Multiple Religions at the Same Time?](#Apr 7: Practicing Multiple Religions at the Same Time?)
* [Apr 12: Religion, Rites of Passage, and Assigned Behavior](#Apr 12: Religion, Rites of Passage, and Assigned Behavior)
* [Apr 14: Blurring the Binaries](#Apr 14: Blurring the Binaries)
* [Apr 19: Long Walks and Liminality](#Apr 19: Long Walks and Liminality)
* [Apr 21: Role of the Shaman](#Apr 21: Role of the Shaman)
* [Apr 26: What have we learned about "religion and its study?](#Apr 26: What have we learned about "religion and its study?)
---

### Mar 15: Roots of Religion/Cognitive Revolution
    - Harari - Timeline and tree of knowledge

* What is the human capacity that emerged to make religion possible? 
    - The cog. rev. enabled fictive language and the emrgence of social constructs

* What definition of religion emerges based on the above human capacity
    - The ability to share information and form collective abstractions. Creates one "shared" reality.

* When was the "cognitive revolution" (based on sapiens)?
    - 100,000 - 70,000 BCE

* What is the "truly unique feature of our language" that separates humans from other animals and allows for the growth of civilizations and large-scale religions?
    - See above ^^

### Mar 17: Cultural Ev., Why Does Religion Spend So Much Time On Sex?

* What does it mean to say that religion is "adaptive?" (Biology perspective).
    - The question is whether religion is biologically advantageous or disadvantageous. Does it improve our chances of reproduction?
    - Well...is it adaptive?
    - Religion is an amalgam of culture, beliefs, practices, and institutions. Entangled with art, social and political orgs, family life, practical knowledge, etc.

* Dawkins' argument for religion being maladaptive: theory of "parasitic memes."
    - Argues that religious ideas are maladaptive cultural elements, transmitted to impressionable young children 
    - While it is certainly an exageration to say that **all** religious ideas and beliefs are parasitic, it is true for some subsets

* Richerson and Boyd "tribal social instincts hypothesis"
    - Ev. of culture  created a process of group selection on cultural variation
    - Symbolic diffs create a limited flow of ideas between groups
    - Individuals who fail to conform to the strict culture are punished and do badly in the group dynamics
    - Moral framework created a very cooperative large group united by shared principles

* Religion and empire
    - Many large civs have a shared "universal religion, suggesting that there is some net advantage in terms of large-scale cooperation

* Fitness benefits theory
    - Culture is usually adaptive because it is shaped by human actors to be so
    - Examples of religion providing fitness benefits: 
        - Calvanism as a solution to corruption within the Catholic church
        - Muhammad's religious codes to regulate intertribal anarchy of the Arabs 

### Mar 22: Strict Religious Theory and Costly Rituals

* Why does the community take place in snake handling?
    - Liminality, creation of social identity through ritual, emotions of fear/joy through ritual, trance-like experience
    - High cost rituals intimately connect members by creating high barriers to entry; reduce risk of imposters and hostile infiltrators

### Mar 24: Docility & Social Instincts Hypothesis - Does Marxist Theory Apply?

* Historical context of Marx's ideas
    - Grew up in a Jewish household, surrounded by industrialization and key religious ideals that he felt were a means of controlling the masses
        - "Religion is the opiate of the masses."
    - Class struggle: Bourgeoisie vs Proletariat

* Mapping Marx
    - Economic inequality increases with the emergence of private property
    - This is ended with revolution (violent)

* Economic inequality is **also** correlated with religiosity

* Marx's definition of religion is a form of escapism; an inescapable fantasy

* Marx and "alienation"
    - Economcic sense: profit taken from working class
    - Religious: Asks people to give a part of themselves, never to get it back

* According to Marx, there have not been more revolutions because those in power use social tools like religion to sedate the masses

* Marx map theory
    - Economic inequality increases with the emergence of private property
    - This is ended with a revolution (violent)
    - Econ inequality is **also** correlated with religiosity

* Gallup poll and Marx
    - In some ways, the poll confirms his hypothesis that religiosity and poverty are correlated
    - The outlier that is the US could be explained by our incredibly high wealth disparity

### Mar 29: Religion and Empire Theory/Morality and Sureillance

* Religion is an effective tool for cooperation

* Allows for greater surveillance capacity and an extra dimension of control 

* As tribes became larger and larger, there was a greater need for a hierarchical order

* Religious hierarchy reinforces certain behaviors (what society considers to be acceptable)
    - Similar to biological natural selection, encourages certain behaviors
    - Social conditioning

### Mar 31: Religion and Fitness Benefits Theory/Cooperation and Prosocial Behavior

* The Call to Prayer study shows religion's capacity to generate prosocial behavior by looking at how shopkeepers showed increased prosocial behavior when call to prayer was audible

* Good Samaritan story might undermine this theory by reminding us that religiosity is not neccessarily correlated with helpfulness. People who are interested in religion for what it will gain them will be less likely to help than those who value religion for its own sake

### Apr 5: Social Stability and the Individual Interplay

* How does author argue for the power of social structures in ways that might question if we as individuals really have any autonomy?
    - Many social systems are inherited rather than given 

* Sam Harris - Free Will Exerpt
    - EFG signaling in our brains can detect impulses moments before they occur
    - fMRI scans of patients asked to push buttons proved that there was brain activity in those regions a full 7-10 seconds before they made those decisions

* Tendency not to question the social order we have inherited is good for social stability
    - Questioning the "nature of things" leads to hostility to other members of that social order

* Internalizing one's role means accepting it for the way things are

### Apr 7: Practicing Multiple Religions at the Same Time?

* Why does the stereotype that religions are mutually exclusive persist?
    - European dominant view that everyone had one, single religion
    - Assumed commitment to one religion

* The assumption has generated fear that people claiming to have converted did not truly reject their previous religious beliefs and practices

* Existing legal institutions preserve this cliche by assuming membership to one religion in legal documents

* Differing approaches to religious diversity
    - Exclusivist
        - My religion is the only truth
    - Inclusivist
        - Other religions have some truth
    - Pluralist
        - All religions are the same

* Some claim it is necessary in order to maintain commitment to a single religion

### Apr 12: Religion, Rites of Passage, and Assigned Behavior

* Naturalization takes place when things that are social are perceived as if they are natural

    * Social order only exits as a product of human activity, **not** because of the nature of things

    * Classifcatory concepts are descriptive by providing quick and non-nuanced explanations of things, as well as prescriptive by telling how things "ought" to be

* Social structure is both simultaneously constraining and enabling. Enabling because for ex. it allows professors to do what they love

* Meaning of following quotes:
    - "complete liberation from social constraints would result in us being very constrained."
        - Constraints provide positives in enabling us with needs, desires, enjoyments and capabilities
    - "Dominated groups sometimes willingly participate in their own domination"
        - Groups sometimes internalize their subordinate social role that they accept as natural, right, and necessary
        - Believed to be necessary for a stable social order
    - "Judging whether a group is oppressed by imposing our own particular ideas about what is 'liberating' may itself be a means of domination"
        - Prescribing the status of a group may be a means of domination by a culture that assumes itself to be superior

### Apr 14: Blurring the Binaries

* The border represents difference between groups and the merging of identity

* She endeavors to think beyond dominant Western epistemic binaries

* Anzaldua is frustrated by contemporary forms of multiculturalism
    - She sees many of these efforts as sanatized, superficial, and capitalist

* Modernity: simple, predictable, stable

* Post modernity: a critique of modernity. Probes the limits of the former

* Anzaldua is considered a post-modernist because she critiques many modern multiculturalist ideals
* 
### Apr 19: Long Walks and Liminality

* Liminality is the space in between, similar to the borders that Anzaldua discusses

* History of long walks; the tradition of dislocation, migration, and resettlement

* Victor Turner - stages of Rites of passage/pilgramage
    - Separation
    - Liminality
    - Aggregation

* The above are confined to special rites of passage

* Contrasted w/ Anzaldua, who seems to exist liminally in a state of permanence

* Nepantla - Aztec word meaning an in-between state

* Pilgramage is important to the mystics because it forces them to detach from worldly possesions and honors

* For Al-Ghazali, his pilgramage represents a distinct choice he has made to forgoe comfort. For Anzaldua, it is more of a forced state

### Apr 21: Role of the Shaman

* Trauma - an open wound

* Defined as a sudden, violent rupture

* Healing for Anzaldua means remembrance and mourning of the past in addition to moving toward less damaging ways of life

* Idea of borders similar to that of the Shamans
    - Religious psychoactic drugs induce a trance like, liminal state

### Apr 26: What have we learned about "religion and its study?

