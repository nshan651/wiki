# Theo107 Week 1 Notes

# 1/20/22

## Religion and the Problem of Definition

* The meaning of certain words changes over time
    - Ex.) Bush redefined the term "wetlands" to keep his campaign promise to environmenatlists while also making real estate developers a ton of money

* Religion falls under this same category - its definition depends on who does the defining

* Does Yoga fall under the category of religion?
    - Depends. Can be religious, but can also be taught secularly
    - Definition can also align with monetary/other incentives

* Even religion and laws/gov can overlap in its definition
    - Ancient Israel, Greece, Rome, etc.

* No other language or culture has a word quite like "religion", which makes translation difficult
    - Rome "religio" meant devotion, reverence, or rules, not in reference to a private institution as it is usually defined today

* Martin Luther was the first to make a jurisdictional distinction between religion and the state 

* John Locke was also in support of religious tolerance and seperation
    - Except when it came to Islam?? Lol

* Notion that "religion can make the world a better place" generally overlapps with social welfare
    - Thus, overlapps with jurisdiction of the nation state

* Religion used to be ranked on the basis of "correctness", with Christianity being rated at the top and "primite" religions beneath it.

* "White man's burden"

* Religion in the 19th and 20th century was seen as an "irrational" form of knowledge

* Organized religion vs spirituality
    - Religion is seen as oppressive, spirituality is seen as more openand accepting

* Meanings of Religion
    - Religion as a belief system
    - As something that specifically concerns "supernatural" matters
    - As "matters of faith"
    - As concerning the "meaning of life"
    - As concerning spirituality or spiritual well-being
    - As a communal institution oriented around a set of beliefs, ritual practices, and ethical or social norms

## "Hindu Group Stirs a Debate Over Yoga's Soul"
