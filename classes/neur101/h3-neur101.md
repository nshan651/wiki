# NEUR 101 HW3

## 2022-09-30

### Optogenetics

1. Define optogenetics
    * A method of stimulating the electric state of neurons by illuminating light-sensitive proteins
    * It is the combination of genetics and optics

2. Why was it developed?
    * It was developed to solve the problem of not being able to control well-defined events within specific cells

3. What has been the major challenge for neuroscience?
    * The major challenge facing neuroscience is the need to control one type of cell in the brain while leaving the others unaltered.

4. Why metal electrodes weren't good enough?
    * Metal electrodes weren't sufficient because they are too crude a tool to isolate a single cell type. They stimulate all circuitry at the site without precisely distinguishing between cell types.

5. How are viral vectors used in this technique?
    * The researchers spliced the gene for ChR2 (special protien that is responsive to light) as well as a promoter into a vector. The vector (a benign virus) transports the added genetic material into the cells. The promoters guarantee that only specific cell types will make and express the encoded proteins.

6. Which scientific questions have the researcher able to directly address?
    * These optogenetic methods can be used in brain imaging. For example, it can be used to verify fMRI results, since fMRI only shows a representation of brain activity based on blood-oxygen levels
    * It has been used to control specific neurons that are involved in narcolepsy
    * It has improved the understanding of Parkinson's disease
