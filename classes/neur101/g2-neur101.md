# NEUR 101 Group Exercise

## 2022-09-29

### Depolarizing and Hyperpolarizing Stimulae

* The hyperpolarizing graph represents the response from increasingly large stimulae
* The same thing happens with the depolarizing step: it will become increasingly positive as the strength of the stimulae become larger

### Graph of A1 and A2

* Stimulating the axon, which is already below the threshold (at -50mV) will result in the depolarization of the cell, and several jumps in positivity as Na+ gates begin to open
* Temporal summation
* First it will increase by summing charges A1 and A2
* Subsequently, we will add additional charges based on the increased stimulae
* In practice, we must remember that some of this charge will be absorbed in the charging of the membrane
