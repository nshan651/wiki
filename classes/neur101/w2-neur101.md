# NEUR 101 W2

* Glia - "The glue":
    * Astrocytes
    * Microglia 
    * Oligodendrocytes & Schwann cells
* Neuron functions:
    * Motor neurons - simulate muscles or glands
    * Sensory neurons - respond to environment stimuli (i.e light)
    * Interneurons - receive input from and send input to other neurons
* Astrocytes help clearance of substances released by the synapse, such as neurotransmitters
* Microglia - survey extracellular space (space between neurons):
    * They engulf debris, dead cells
    * Also engulf the Ab deposit thought to play a role in the pathogenesis of Alzheimer's disease
* Oligodendrocytes & Schwan cells:
    * Wrap layers of myelin sheets around axons
    * Oligodendrocytes are mostly found in the Central Nervous System; Schwan cells are found in the Peripheral Nervous System
* There are gaps in the myelin known as the Node of Ranvier:
    * Very important, this is where the action potential conduction occurs
    * Ions flow through this space
    * The rest of the myelin acts as an insulator; does not allow ion flow
* Nervous System breakdown:
![nervous system](nervous-system.png)
* Central nervous system - brain and spinal cord
* Peripheral nervous system - everything else:
    * Somatic and autonomic:
        * Autonomic is separated into sympathetic and parasympathetic
* Somatic and autonomic:
    * Somatic - movement of the body. Connects the brain and major muscles and sensory systems
    * Autonomic - visceral functions - to keep us alive, control the internal organs
* Motor neurons - carry information from CNS to muscles, organs, and glands
* Sensory nerves - convey information from the body (skin, organs). Info arrives at the spinal cord
* The spinal cord:
    * Cervical
    * Thoracic
    * Lumbar
    * Sacral
    * Coccyx
* First layer of bone structure is known as **vertebrae**; subsequent layers are the **meninges**
* Second layer is the DURA
* The next layer is the arachnoid membrane
    * Cushions the brain in cerebrospinal fluid
* The final layer is the Pia mater
* Ganglia is a cluster of neurons
* A nerve - cluster of many neuron fibers which themselves are made of axons:
    * Basically a bunch of axons
* A third nervous system besides para- and sympathetic nervous systems - **enteric nervous system**:
    * Local network of neurons that governs function of the gut
* Four cerebral hemispheres:
    1. Occipital - processes visual info
    2. Frontal - 
    3. Parietal -
    4. Temporal - 
* Gryi (singular: gyrus) - folds in the brain
* Sulci (singular: sulcus) - indentations or grooves
* Most structures in the brain are symmetrically arranged
* There is some lateralization in terms of function:
    * Main exception is the visual system
* Tract refers to a bundle of axons:
    * Myelinated, because the connections must be preserved across distances
* Nucleus refers to a collection of neurons
* Categories of tracks:
    * Short tracks - connect adjacent cortical areas
    * Long tracks run ipsilaterally
    * Corpus callosum - Made up of long tracks that end in the contralateral hemisphere
* The Thalamus:
    * One of the most complex nuclear structures of the brain
    * Acts as a relay. Takes info from different areas and send it to other areas
    * "Gatekeeper of the brain" (except for olefactory)
* Cerebrospinal fluid:
    * Breakdown of nutrient content
    * Very little protein
* Cerebrospinal fluid is created in the choroid plexus
* The circulatory system:
* Astrocytes - Provide nutrients in the CNS and form the **blood-brain barrier**
    * blood-brain barrier is a structure lining brain capillaries restricting passage of large molecules from the blood into brain tissue
