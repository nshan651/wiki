# NEUR 101 W3

## Neurochemistry

* Equilibrium potential for an ion:
    * $E_{ion} = RT/zF \cdot ln([ion]_o/[ion]_i)$
    * Where:
        * E = equilibrium potential
        * R = universal gas constant
        * T = temperature in Kelvin
        * F = Faraday constant
        * z = valence of the ion
* Nerst equation predicts what Voltage the ion will be in equilibrium:
    * $Vm = 61/z \cdot log(concentration\ of\ outside\ ion/concentration\ of\ inside\ ion)$

#### Excercise: Calculate Nerst equilibrium for given ions

* $Vm = \frac{61}{z}log(\frac{ion_o}{ion_i})$

* $Vm_{Na^+} = (61/1) \cdot log(145/15) = 60.0509 mV$ 

* $Vm_{K^+}= (61/1) \cdot log(5/140) = -88.2766 mV$ 

* $Vm_{Ca^{2+}} = (61/2) \dot log(2/0.0001) = 131.1814 mV$ 

## More on Membrane Equilibrium Potential

* Several necessary components:
    * Electrode - electrical conductor used to make contact with the cell
    * Measuring device (amp) - amplifier is a device used to increase power of a signal. Proportionally greater output based on amplitude
    * An output device
* A circuit used in Neuro:
    * Recording electrode
    * Ref electrode (zeroes the system):
        * Somewhere outside the sample, used for comparison
    * Sample
    * Amplifier
    * Extracellular solution
* Electrodes can be glass or metal, can have one or multiple points
* If recording outside cell, mV will be zero. If inside, then it will be negative
* Dependent variable is the current (or ionic flow)
* As Nerst potential increases, so does ionic flow
* Outward flow - negative nerst
* Inward flow - positive nerst
* When Nerst is zero, it has reached equilibrium potential
* IRL, the whole curve deviates when you increase ion flow:
    * This means that the **other** ion channels are playing a role in setting up the membrane potential
* Nerst only controls **one** ion channel. There are more that contribute
* Goldman Hodgkin Katz equation for membrane potential:
    * You must flip $CL_{in}$ and $CL_{out}$ because of valence issues. Chloride is negative, so must be flipped
* Includes Potassium, Sodium, and Chloride
* At permeability rest, there **will** be some Na and K leakage through their respective ion channels
* Neurons use a sodium-potassium pump to maintain resting potential:
    * Pumps out 3:2 $Na^+$ to $K^+$ ions
    * This is the **active transport**
* Passive transport (leak) channels vs active transport

#### Activity: Use Goldman Equation 

* Example:
    * $V_{m}=-60*log(\frac{(140*20)+14+120}{(4*20)+140+4})=-67$
* When $K^+$ and $Na^+$ are 10 and 100:
    * $V_{m}=-60*log(\frac{(140*10)+(14*100)+120}{(4*10)+(140*100)+4})=40.1mV$
* When $CL^-$ and $K^+$ are 50 and 51
    * $V_{m}=-60*log(\frac{(140*51)+(14)+(120*50)}{(4*50)+(140)+(4*50})=-76.2mV$
