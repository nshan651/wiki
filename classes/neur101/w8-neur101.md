# NEUR 101 W8

## Dopamine

* Two metabolic receptors:
    * D1R - depolarizing
    * D2R - hyperpolarizing

## Norepinephrine

* Subtypes:
    * Alpha, beta
* AKA noradrenaline
* Found in the locus coeruleus

## Serotonin

* Activates 7 receptors:
    * Only one is ionotropic, the rest are metabotropic
* Most cell bodies produced in the raphe nuclei
* Implicated in sleep state, mood, libido, and anxiety
* "Serotonin hypothesis" of clinical depression is ~50 years old
* **J. Moncrieff - The serotonin theory of depression: a systematic umbrella review of the evidence**
* Drugs to ease symptoms of psychiatric illness:
    * Antidepressants
    * Monoamine oxidase inhibitors (MAO) - prevent breakdown of monoamines
    * Accumulating monoamines and prolonging their activity is a major feature of antidepressants
    * Later: SSRIs
* Mike Tyson on DMT (YouTube)

## Opiates

* Opiates are peptides present in the brain
* Exercise releases opiates
* Act in some synapses, or as hormones
* Generally metabotropic G-protein coupled receptors
    * Peptides are derived from proteins
* LDVC and SCCV:
    * LDVC - large vesicles containing peptides
    * SCCV - small vesicles containing neurotransmitters
* Endogenous opioids:
    * Peptides produced in the body that bind to opioid receptors
* Opium:
    * Comes from poppy seeds
    * Morphine is derived from this

## Ligand Flavors: Agonists and Antagonists

* An Antagonist binds to a receptor but does not activate them
* Competitive and noncompetitive
* Binding affinity:
    * Degree of chemical attraction between ligand and receptor
* Two phases:
    * Association referring to the binding of the ligand to the receptor
    * A dissociation phase which refers to the unbinding
* Potency:
    * Refers to the amount of a drug needed to produce an effect
* Efficacy:
    * Refers tp the maximum response to the drug
* Begins in the center of the cell where concentration is the highest
* Dose-response curve (DRC):
    * Graph where the relationship between the concentration of the ligand and its effect is plotted
* ED_50 = Half maximum effective dose

## Pharmacokinetics

* Pharmacokinetics:
    * Branch of pharmacology concerned with measuring the movement of drugs within the body after its administration
* Brain Pharmacokinetics:
    * Entry to the brain, blood brain barrier must be crossed
