# NEUR 101 Review Session

* Action potential:
    1. Resting
    2. Depolarization
    3. Repolarization
    4. Hyperpolarization

## Resting 
* Resing potential:
    * For the pump...
    * Na+ goes out (pumped out), K+ goes in
    * Inside the cell is negative
    * Na+ and K+ pump a little bit (leaky channels):
        * A little Na+ is allowed in, and a bit of K+ is let out

## Depolarization

* Negative = polar, positive = less polar (IN NEURO)
* Depolarization occurs when Na+ gates open and Sodium outside is let in
* Big picture - two critical things to understand with neurons:
    * Concentration gradient (Difference in concentration between Na+ and K+ inside and outside)
    * Charge difference - cause channels to open

## Repolarization

* Voltage-gated K+ channels open:
    * More K+ inside than outside, so it leaves
    * Cell becomes negative again

## Hyperpolarization

* A lot of K+ is released
* Becomes **too** negative inside the cell (overshoots)

## AP 

* All sodium channels are active at the threshold 

#### Jeopardy Questions

* What are the two major cell types of the brain?:
    * Neurons and glial
* What does are pre- and postsynaptic neurons?:
    * Presynaptic sends a transmission, postsynaptic receives
* Difference between Nerst and Goldman equations?:
    * Nerst models flow (intra and extra cellular) of a single ion, Goldman accounts for multiple (Na+, K+, Cl-)
* What is the saltatory conduction?:
    * The way an action potential moves down a myelinated axon
    * Uses voltage to move quickly down the length of the axon. Myelin acts as insulation to prevent the loss of charge
* What is a CT scan?:
    * Computerized tomography uses x-ray imaging to create cross-sectional images of the body
* Who is the father of the neuronal doctrine, and what is it?:
    * Santiago Ramon y Cajal. Nervous system is composed of independent cells with gaps in between them
* Drawing of multipolar, bipolar, and unipolar neurons:
    * Multipolar has multiple dendrites coming out of the soma, and an axon coming out
    * Bipolar has one dendrite coming out of the soma as well as an axon
    * Unipolar has one projection branching into the dendritic branches and axon
* What are the two forces that influence neurons?:
    * Charge difference and concentration flow
* What is the resting potential
    * Resting potential is the default polar state of a cell before the AP sequence has been initiated
    * The **membrane potential** of a neuron at rest, when AP is not occurring
* What is the MRI? (Bonus for fMRI):
    * Magnetic resonance imaging is a technique that uses magnetic forces to displace the body's water molecules. The displacement is subsequently used to create a detailed map of the brain's architecture. Functional MRI specifically measures metabolic functions to visualize active parts of a brain during a given task
* What is the axon initial segment?:
    * The AIS is the axon hillock, which is the area between the cell body and the axon. Contains a significant number of Na+ channels which are responsible for generating the action potential
* What is the purpose of the cranial nerves, and how many pairs are there?:
    * They are responsible for controlling the five senses, along with head movement and autonomic functions. There are **12** pairs
* What is equilibrium potential?:
    * Equilibrium potential is the measurement of the default state of a cell, at which there is no net movement of a given ion. Where the charge is in equilibrium with concentration. K+ is most responsible for this level.
* What is the refractory period? What causes it?:
    * It is the short period right after an action potential during which it is harder to produce a new AP. This is due to the voltage-gated Na+ channels being inactive and K+ channels still being open
* What is PET? (Drawbacks?):
    * Positron emission tomography uses a radioactive tracer to visualize metabolic activity. The major downside is exposing the subject to potentially harmful radiation
* Major components of the nervous system:
    * TODO:
* Major sections of the spinal cord?:
    * Cervical, thoracic, lumbar, coccyx
* Calculate membrane potential for Na+ when following ion concentrations are present (values in problem):
    * $E=\frac{RT}{zF}ln(\frac{ion_o}{ion_i})$
* What are the major phases of AP, and what are the most important ion movements during each step?:
    * The steps go: resting -> depolarization -> repolarization -> hyperpolarization
    * First, sodium gates are opened and Na+ ions come into the cell which causes the cell to become more positive than usual. This causes more Na+ gates to open, and more Na+ to flow in
    * Next, membrane becomes more negative due to K+ gates opening and K+ ions flowing out of the cell
    * Hyperpolarization is where the membrane potential overcompensates and becomes **too** negative. A lot of K+ leaves, and it takes a little while for these channels to turn off
* What is DTI?:
    * Diffusion tensor imaging is a process that detects how water travels along the white matter tracts of the brain
* Draw a Neuron:
    * NOTE: Done
* What are the types of glial cells, and what are their functions?:
    * Astrocytes, microglia, oligodendrocytes, and Schwann cells
    * Astrocytes clear neurotransmitters and others from the synapse
    * Microglia destroy debris
    * Oligodendrocytes wrap myelin around axons in the CNS
    * Schwann cells wrap myelin around axons in the PNS
* Advanced membrane potential example:
    * 
