# NEUR 101 W1

## Levels of Biological Systems

| Levels    |
|-----------|
| Atomic    |
| Molecular |
| Cellular  |
| Tissue    |
| Organ     |
| System    |
| Cells     |

* Cells are the smallest organic building blocks
* Basic cell types of the brain:
    * Neurons (hundreds of cells)
    * Glia (fewer cell types)
* DNA must travel through different organelles of the cell
* Parts of the cell:
    * Cell membrane
    * Golgi complex
    * Membrane receptor
    * Cytosol
    * Mitochondria
    * Nucleus Ribosomes
    * Centrosome
    * Lysosome
    * Microtubules
    * Nucleolus
    * Chromatin
    * Peroxisome
* All of these parts (**organelles**) serve a unique purpose
* Cells of the brain - Neurons
* Information travels from cell to cell across synapses
* Santiago Ramon y Cajal was the father of the Neuron doctrine
* Able to accurately understand information transfer across synapses without modern electrophysiology techniques
* Anatomy of the neuron:
![dendrite](dendrite.png)
* Composed of:
    * Cell body
    * Dendrites:
        * Serve as the **input zone** of the neuron
        * Convert signals into small electric impulses inward until they reaches the cell body
        * Signals arrive at discrete locations
    * Axon
        * Collaterals
        * Terminals
* Control flow and Function of the Neuron:
    1. Input zone - area of the neuron receiving info
    2. Integration zone - area of the neuron combining all inbound inputs at a given time; made up of dendrites and cell body
    3. Conduction zone - AKA the **Axon**, carries info from the soma to the axon terminals
    4. Output zone - area of the neuron where info transfer occurs; also referred to as the axon terminals
* The nucleus:
![nucleus](nucleus.png)
* Nucleus:
    * Membranous organelle containing DNA
    * DNA is organized into chromosomes
    * DNA in Neurons exists in the form of **chromatin**
* DNA transcription and RNA translation:
![transcription translation](transcription-translation.png)
* Energy production in the cells:
    * Mitochondria converts carbs, fats, and proteins along with oxygen into ATP
* Cell body == Soma
* Transport system:
![protein transport](protein-transport.png)
* Two types of transport:
    1. Anterograde transport - away from the soma
    2. Retrograde transport - backward transport
* Na (Sodium) channels are responsible for generation of the AP (action potential)
* Physiology of the axon:
    * Surrounded by layers of fat known as the **myelin sheath**
    * Axons often split to generate **axon collaterals**
    * These further differentiate to form the presynaptic terminals
* What is a postsynapse?
    * Membranous structure that forms on the **receiving** neurons
    * Forms structures known as dendrites, dendritic spines
* The synapse:
    * Combination of the pre- and post-synapse
* The postsynaptic spine
![postsynaptic spine](postsynaptic-spine.png)
* **SYNAPSE = (PRESYNAPTIC) AXON TO (POSTSYNAPTIC) DENDRITES**
* Multipolar neuron:
    * Distinct section of the neuron dedicated for its dendritic fields and an area where the axons comes out
    * All of these structures come out of the soma
    * Most common neuronal shape
* Unipolar:
    * Single projection branching into dendritic fields and axons
