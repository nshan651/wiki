# NEUR 101 Homework 1

1. Define histology.
    * Histology is the study of the microanatomy of organs, cells, and tissues through a microscope

2. How do we use histological methods in biomedical research and in medical diagnostics?
    * In biomedical research, it is extremely important that the researcher have a decent understanding of the microanatomy organism they are studying. The most basic technique is the staining of cells so that particular parts are easily identifiable. Another useful technique is sectioning, which involves encasing cells in solid material and cutting it apart. Finally, there is electron microscopy, which allows one to see highly complex arrangements of cells. These techniques are extremely useful for forming medical diagnoses by identifying certain irregularities at the microscopic level.

3. How Nissl stain works and why it is useful for brain sciences?
    * The Nissl stain is one of the oldest techniques in neuronal imaging. It uses a violet or blue material to permanently stain genetic material. The technique is useful for identifying the cytoarchitecture across brain structures 

4. Which experimental techniques are used to study the localization of certain proteins in tissues? How that technique works?
    * The general technique for studying localized proteins is to use staining. There are many types of different staining techniques, which each serve to highlight different cells, proteins, and organelles.$^{[1]}$
    * Before proceeding with a stain, it is often necessary to prepare the tissue samples using the following four steps:
        * Fixation - Uses chemicals to preserve the natural structure of the tissue
        * Dehydration - Addition of ethanol to the sample; removes water and hardens tissue 
        * Embedding - Encasing sample in paraffin wax or plastic resin to enhance structure
        * Sectioning - Cutting specimen into sections
    * Occasionally it is also necessary to remove antigens that could have been covered in the fixation and embedding stages
    * There are numerous staining techniques, including the previously mentioned Nissl stain. Some of these include the Gram Stain (differentiates different bacterial species), Prussian Blue (Useful for identifying iron stores in the body), and the Golgi stain (Which was used to understand more about neuronal morphology).$^{[2]}$

## Sources

1. https://www.ncbi.nlm.nih.gov/books/NBK557663/
2. https://brainstuff.orWhg/blog/what-is-the-golgi-stain 
