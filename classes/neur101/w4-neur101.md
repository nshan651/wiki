# NEUR 101 W4

## Group Activity

* What is the first step in generating action potential?
    * The first step is depolarization, which is where the membrane potential rises and causes voltage gated sodium channels to open in the membrane, and for sodium ions to enter the cell
* What is the name of the zone for its initiation?
    * The integration zone
* What is the name of the point at which AP is triggered? What is its usual numerical value?
    * It occurs at the threshold. -40 mV.
* How many phases make up the Action potential
    * 4 phases

#### FIVE EXTRA CREDIT POINTS!

* Conductions of action potential:
    * Why doesn't the purple electrode have that threshold? Why isn't their a blip

## Action Potentials

* AP happens at the Nodes of Ranvier
    * The gaps in between the myelin of the axon
* AP (graduated fountain analogy):
    * Charge is only given to the next node when previous node has been filled completely
    * Moves on to the next part of the axon
    * This turning point is the **threshold**
    * AP will be generated with or without myelin
    * Analogous to resistors and capacitors
* Steps of Action Potential (AP):
    * Depolarization - brings the membrane potential of the axon initial segment closer to the threshold
        * Threshold is usually -40mV
    * Fast depolarization - 
