# NEUR 101 Index

## Notes

[Week 1](w1-neur101)
[Week 2](w2-neur101)
[Week 3](w3-neur101)
[Week 4](w4-neur101)
[Week 5](w5-neur101)
[Week 6](w6-neur101)
[Week 7](w7-neur101)
[Week 8](w8-neur101)
[Week 9](w9-neur101)

## Homework

[Homework 1](h1-neur101)
[Homework 2](h2-neur101)
[Homework 3](h3-neur101)

## Groupwork

[Group](g-neur101)
[Group](g2-neur101)
[Group](g3-neur101)
[Group](g4-neur101)

## Exams

[Exam 1](e1-neur101)
