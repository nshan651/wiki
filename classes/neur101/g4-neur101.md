# GROUP 4

## Agonists

* Dopamine - D1R and D2R
* Glutamate - AMPAR and NMDAR
* Acetylcholine - Nicotine
* Endocannabinoids - CBD and THC

## Antagonists

* Serotonin:
    * Serotonin - 5HT_2
* GABA:
    * GABA_A and GABA_B

## Binding and Unbinding

* Kinetic scheme:
    * Mathematical approximation of different conformational changes taken by the protein upon binding to its ligand
    * Arrows indicate the direction of transition
