# NEUR 101 H2

## 2022-09-14

### Brain Imaging Techniques

1. Describe how images obtained using CT, MRI, DTI, and PET are generated and collected?
    * CT (computerized axial tomography) - Imaging technique where a X-ray is moved in an arc around the head. The radiation passing through is measured and from this process the brain can be anatomically mapped
    * MRI (magnetic resonance imaging)- Rather than use radiation like X-rays do, MRIs use a radio frequency detection technique. A powerful magnet is used which lines up all of the protons in the brains tissue, which is then blasted by RF energy. The protons will return to their original positions and leave a trail of RF energy, which is then recorded. The recorded densities can be used to generate a detailed cross-sectional map of the brain
    * DTI (diffusion tensor imaging) - This imaging technique takes advantage of the fact that constrained water molecules inside tubes of axons cannot diffuse as readily. This allows one to create images of axon fiber pathways
    * PET (positron emission tomography) - The objective of this technique is to analyze brain activity rather than structure. Radioactive chemicals, such as glucose are injected into the patient, which can then be tracked when they are asked to do something mentally strenuous.
    
2. What are the benefit and limitations of each technique?

| Imaging Technique  | Pro                                                                         | Con                                                                        |
| ------------------ | --------------------------------------------------------------------------- | ---------------                                                            |
| CT                 | Creates a 3D image, allows one to see major brain regions, cheaper than MRI | Low resolution, radiation exposure                                         |
| MRI                | High resolution                                                             | No radiation exposure                                                      |
| DTI                | Easily view pathways, detects denegration of axons                          | Sensitive to water diffusion and motion, expensive, low spatial resolution |
| PET                | Allows one to see dynamic brain activity                                    | Expensive, radiation exposure, low resolution                              |



4. Knowing all that if you want to identify a deficit in one neurotransmitter system which imaging technique will you use?
    * I would use an MRI, because it can detect subtle changes in the brain, and how neurotransmitters affect different regions

5. If you want to study dynamic processes underlying cognitive processing, which imaging approach will you select?
    * The best technique for this case would be a PET scan, because it allows one to dynamically monitor cognitive processes

6. Lastly, which imaging approach will you use to observe a brain lesion?
    * I would use a DTI, because it can identify lesions with more fidelity than conventional MRI
