# Group Activity

* E1 and E2 fire simultaneously?:
    * There will be a net depolarization of +9 mV
* I1 and I2 fire?:
    * There will be a net negative hyperpolarization -14 mV
* E1, E2, and I1?:
    * There will be a net negative hyperpolarization of -1 mV (5+4-10)
* The above is **spatial summation**
