# NEUR 101

## 2022-09-22

### Group Activity

* What predicts the Vm at resting, peak of AP and the most negative value?
    * Peak of AP will be determined by inner and outer ion flows. Ratio of Inner and outer in Nerst.
    * Resting is predicted by Goldman. Takes into account all permeabilities 

* What permeabilities dominate at each of these points
    * Permeabilities usually start at a negative, then polarize to a positive after reaching a threshold, then return back to resting

* Explain why the depolarization occurs
    * Threshold is reached, which returns membrane potential back to resting potential

* What would happen (to the sodium permeability) if we bring Vm from +55mV all the way to -100mV (Permeabilities = conductance = a given open ion channel)
    * Sodium channel will shutdown because it will sense the negative potential and close

* What is the experimenter doing to this axon?
    * They are depolarizing (adding positive charge)
    * Charge will not go back (as much) because there are not as many ion channels in the dendrites

* Do we have ion channels in the myelinated portion of the axon?
    * No, the AP generated "jumps" the ions over the myelin
* What is the major difference between myelinated and unmyelinated axons?
    * Unmyelinated parts of the axon is a conductor, the myelinated portion acts as a capacitor to prevent charge from escaping
