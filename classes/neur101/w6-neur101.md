# NEUR 101 W6

## Synaptic Connection

* Excitatory and Inhibitory
* Strong synapses will depolarize by larger amounts
* Same for excitatory
* Spatial summation:
    * Multiple presynaptic neurons release neurotransmitters together to fire an action potential in the postsynaptic neuron
    * Refers to the summing of potentials that arrive at the axon hillock at different times 
* Temporal:
    * Neurotransmitters over a period fire action potentials
* If the overall sum is negative, the neuron will hyperpolarize
* The closer together in time they arrive, the greater the potential summation and the more possibility for an action potential to occur
* Ligands (endogenous and exogenous):
    * Former is neurotransmitter and hormones produced in the body
    * Latter is in toxins and drugs produced outside the body
* Acetocolyne
* Noreprenephrine
* GABA
* Glutamate
* Ionotropic receptors - open when bound by transmitter
    * Three ways to open - Ligands, ions, and .....a third way :)
* Metabotropic receptors - activate G proteins when bound by a transmitter
* IPSP and EPSP
* Inhibitory synapses CL- enters cytosol
* Ending synaptic transmission:
    * Degredation - rapid breakdown by an enzyme
    * Reuptake -transmitter is taken up by presynaptic receptors called transporters
* Axon creates signal that carries a vesicle fuse and then binds pre- and post- synaptic connection
* Synaptic spines do NOT generate APs:
    * Generate either EPSPs or IPSPs (excitatory or inhibitory post synaptic potential)
* Types of synapses:
    * Axo-:
        * Dendritic - terminal between synapses on a dendrite
        * Somatic - terminal synapses on the cell body
        * Axonic - Synapse between two axons
    * Dendro-dendritic - synapse between two dendrites
    * Retrograde - a gas (CO2 or NO) signals presynaptic cell to release transmitter

## Synaptic Neurotransmission

1. Action Potential travels down the axon and arrives at the presynaptic terminal 
2. Voltage-gated calcium channels open and Ca2+ ions enter
3. Synaptic vesicles fuse with membrane and release transmitter into the cleft
4. Transmitter crosses the cleft and binds to postsynaptic receptors, which opens ion channels
5. Transmitter is activated (degraded) by enzymes or removed by transporters-so transmission is brief
6. Transmitter may activate presynaptic autoreceptors, decreasing its release

* When you are scared:
    * Noreprenephrine and adrenaline are released
    * G proteins are responsible for this
    * Noreprenephrine channel flow will increase heart rate
* Neurotransmitter system:
    * ligand gated ion channel
    * metabolic gated receptor
* Depolarization of the postsynaptic spine results in an EPSP

## Acetylcholine

* Acetylcholine:
    * Major neurotransmitter at the neuromuscular junction
    * Presnet at the presynaptic terminal of motorneurons
* Areas producing Acetylcholine:
    * Brain stem
    * Basal forebrain

## Glutamate

* Glutamate - small amino acid neurotransmitter:
* Neurons using and producing glutamate is called glutamatergic
* The major excitatory neurotransmitter in the CNS
* Ionotropic and glutamatergic receptors involved at these synapses
* Excitatory pyramidal neurons assembled into a circuitry involved in spatial navigation
