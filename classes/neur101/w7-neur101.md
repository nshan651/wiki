# NEUR 101 W7

* Neurotransmitters:
    * Substance present in presynaptic term
    * Substance released  in respknse to presynaptic depolar
    * Specific receptors for the neurotransmitter must be present
* Ionotropic receptors:
    * NMDA - important receptor
        * Responsible for learning and memory
    * AMPA
* Metabotropic receptors:
* Glutamate is an autoreceptor

#### Activity

* Electrophysiological recording of a *excitatory neuron* showing *EPSP*?

## GABA

* GABA is the chief inhibitory neurotransmitter in the brain
* Neurons capable of synthesizing and releasing GABA are referred to as GABAergic Neurons
* Involved in most brain functions
* Activates ionotropic GABA_A and metabotropic GABA_B receptors
* Drugs binding to GABA:
    * Alcohol (ethanol)
    * Barbiturates
* Why do we call ethyl alcohol ethanol
* Origin of seizures:
    * Characterized by synchronized electrical activity in the brain

#### Group Work 2

* Vm -75 mV. No difference between recording and actual ion potential, so there will be no flow of ions 
* Starts inhibited, depolarized to -55mV, then repolarized. Will be inhibited again
    * Basically a spike

## Endocannabinoids

* Fatty molecules, highly insoluble in water
* Molecules synthesized from lipid bilayer

## Glycine

* Major inhibitory transmitter in the spinal cord

## Dopamine

* A catecholamine
* The motivator neurotransmitter
* Dopamine receptors are metabotropic
* Two major types:
    * D1R: depolarizing
    * D2R: hyperpolarizing
* Muhammad Ali:
    * Had Parkinsons, which caused major dopamine problems
