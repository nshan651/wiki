# NEUR 101 W9

## The Fear Response

* Anxiety -> fear -> panic
    * Simulated in a laboratory with electrical foot shocks
* Pavlovian conditioning:
    * Learning process where two stimuli are paired in time
    * Pavlov originally studied conditioning on dogs salivating over food based on the ringing of a bell
* Fear extinction:
    * Form of inhibitory learning
    * NOT forgetting
    * During fear extinction, the subject learns a new contingency where the rules of the game are changed
* The amygdala:
    * Part of the lymbic system, chiefly responsible for the fear response

## Activity

* What is the response of the trial
    * The number of fecal droppings
