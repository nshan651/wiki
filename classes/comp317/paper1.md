# Paper 1 Notes: Sci-Hub

#### Sources

* Academic copyright: https://cyber.harvard.edu/sites/cyber.law.harvard.edu/files/Copyright%207-17HLS-2009.pdf

* United States Copyright Law, ICS: https://copyrightservice.net/copyright/us

## Quick Notes

* Authors of academic papers are expected to do much of the work involved in producing the paper, including type setting
    - May even be charged a submission fee
    - Peer reviewers are not typically paid either

* Most journals ask for the copyright for your work, meaning you essentially turn over the rights to distribute it in order for the work to be published

* Sci-Hub is based in Kazakhstan, which is out of reach of copyright law
    - Servers could be distributed across multiple other locations

* Technically, Sci-hub is a *flagrant* violation of copyright law

---
### Two central arguments for the existence of Sci-hub

* Two main arguments: 
    - "The research in question is typically publicly funded; why shouldn't it be publicly accessible?"
        - A variation on this is that all scientific information should be free, period
    - "Nobody is claiming that Sci-hub will have any impact on the creation of new scientific research, or on the writing of new papers." (Taken from course website)
        - Sci-hub will not cause scientists to write fewer papers        - Failure of the utilitarianism argument against infringement
---

* Second argument (not Fair Use) runs counter to the Utilitarian justification for copyright
    - Copyright is intended to protect the original works of authorship
    
* Utilitarianism necessitates that the most ethical choice will produce "the greatest good for the greatest number of people"
    - It seems that obfuscating information behind steep paywalls is **not** doing the greatest good for the majority
    - In fact, far fewer people benefit from this. Neither society nor the original authors benefit from it

* The publications, the "middle men" in this situation, contribute comparatively little value.

* Paywall for journals can be prohibitive, especially for researchers of developing countries

* Arguments for closed-access publishers
    - Selective acceptance can create an image of prestige for a journal, which can also greatly benefit the reputations of authors whose publications are accepted
    - Traditional journals are also able to charge much less than open-access for submissions
        - They make most of their money from library subscriptions, whereas open-access journals need to charge more to maintain their services

* The social costs of abandoning traditional journals is simply unknown
    - Mirrors one of the major shortcomings of Utilitarianism, which is that the future consequences of present actions are not always clear
    - Some argue that switching exclusively to open-access would destroy the integrity of academic research
    - It is also possible that more highly selective open-access journals will begin to appear, which could replace traditional prestigious journals like Elsevier or Springer

## Philosophy of Consequentialism/Utilitarianism

* Hedonic calculus (Jeremy Bentham)
    - Intensity (I)--How intense is the pleasure or pain?
    - Duration (D)--How long does does the pleasure of pain last?
    - Certainty (C)--What is the probability that the pleasure or pain will occur?
    - Propinquity (nearness or remoteness) (N)--How far off in the future is the pleasure or pain?
    - Fecundity (F)--What is the probability that the pleasure will lead to other pleasures?
    - Purity (P)--What is the probability that the pain will lead to other pains?
    - Extent (E)--How many persons are affected by the pleasure?

* Act Utilitarianism (Bentham)

* Rule Utilitarianism or consequentialism (Mill)
    - Right thing to do (greatest good for greatest number)
    - Actions should promote pleasure, buttt, two types of pleasure
        - Bodily pleasures
        - Pleasures of the mind
    - The latter take higher priority
    - Rules needed for promoting most happiness
    - Rules to prevent immoral action
    - Rules should be obeyed no matter what

* Rule Utilitarianism problems
    - Rules may not fit every given situation
    - Soft rule utilitarianism becomes act utilitarianism

* Preference utilitarianism
    - takes into account preference of all individuals
    - still extremely difficult to calculate and way all ppls best interests

### Social Contract Theory (Locke, Rousseau, Rawls)

* Locke and Rousseau
    - We agree to move from the State of Nature to Civilization
    - Agree to certain social/ethical rules due to their consequences
        - Consequences benefit us
    - Ethics are in our long-term self-interest under the Social Contract

* Rawls
    - Veil of ignorance when navigating the Social Contract
        - ^ Not knowing others' socioeconomic position
        - Ethics would then be chosen based on this position
        - Typically thought of as a theory of justice, not ethics

### Other variants of Utilitarianism

* Zero-sum utilitarianism
    - Idea that we sum everyone's benefit numerically and add them up
        - Option with the best score wins
    - The issue is that one individual may be hurt a great deal at the cost of marginal benefit to the majority
    - Another question: do we only consider current scores, or future scores as well

* Min-max utilitarianism
    - Minimize the harm to those affected most

* Disinterested-person Utilitarianism
    - Idea that utilitarian decisions must be made by a disinterested party
    - I.e. a group that does not stand to gain or lose anything from the decision

* "The ends justify the means"
    - Position based on the argument that sometimes it's okay to take potentially harmful means when the ends are clearly an overall good

### General problems with Utilitarianism

* Do the needs of the many *really* outweigh the needs of the few

* How broad should the scope of ethical rules be?

* How do we judge "social utility?"

* "Utility monster" problem
    - Maximizing pleasure may violate the overall sense of justice of a situation
        - I.e. feeding a "monster" may empirically maximize utility, but in practice it is not the most just outcome
    - Diminishing returns is an important solution to this

## Deontological Ethics
    
* Kant
    - Categorical imperative: All principles should be universal
        - If it is okay for us, it must be okay for everyone
        - Must be able to be generalize to all contexts
        - Must choose ethical principles based on the idea of universality
    - Similar to the golden rule
    - People should not be treated as a means to an end
        - People should be "endpoints" of moral action

* WD Ross (more modern deontologist)
    - Argued against utilitarianism
    - Identified seven duties that we have to each other:
        1. fidelity [not lying, keeping promises]
        2. reparation [making up for accidental harm to others]
        3. gratitude
        4. non-injury [do no intentional harm others; includes harming their happiness]
        5. justice [or prevention of harm by others?];
        6. beneficence [do good to others. How much good?]
        7. self-improvement [perhaps "taking care of oneself"]

* Problems with deontological ethics:
    - When do certain rules conflict
    - What are the rules?
    - Are they consistent?
    - What if we are not all in agreement?

## Argument Outline 

* Copyright appears to be interpreted in a deontological manner; it is applied in a generalized way to to protect all forms of property (even when it is not the most beneficial to treat it as such)

* As the nature of copyright has evolved over time to encompass digital distribution (specifically of educational materials), copyright should no longer be considered a categorical imperative (Cite Kant).

* The Utilitarian justification for copyright in this case is insufficient

* Utilitarianism necessitates that the most ethical choice will produce "the greatest good for the greatest number of people"
    - It seems that obfuscating information behind steep paywalls is **not** doing the greatest good for the majority
    - In fact, far fewer people benefit from this. Neither society nor the original authors benefit from it

* The closed-access of scientific publications benefits a small group (publications) while marginally benefiting or outright harming a large majority of the public

* The journals themselves contribute very little value, aside from social credit and prestige to the authors and the the promise of quality 
    - Why can't there also be selective journals that provide these things?

* Open-access would increase readership, and thus would increase the potential recognition an author might gain from publishing
    - This would incentivize more publications, not fewer

* Despite there being a potentially higher submission cost to open-access, this may serve to incentivize more serious efforts
    - Unpublished work could still be made available on the internet

* Public research grants and universities would have a greater incentive to fund research efforts
    - This should not be a large issue, as they would be simply substituting copyright costs and subscriptions for publishing fees
        - Could actually save money on net

---
### Central Argument

1. Sci-Hub and other open-access databases should be allowed to exist because publicly-funded research should remain publicly accessible
2. A transition to open-access will not necessarily undermine the quality of scientific research, nor will it result in a decline in research endeavors
    - Peer review is largely unpaid, and done on a voluntary basis.
    - Open-access may actually result in an *increase* in research output, because of easier access to existing information and lower barriers of entry
---

## Quotes

* "Stoic, as well as Christian, elements require to be included. But there is no known Epicurean theory of life which does not assign to the pleasures of the intellect, of the feelings and imagination, and of the moral sentiments, a much higher value as pleasures than to those of mere sensation" (Mill 15). 

* "The creed which accepts as the foundation of morals, Utility, or the Greatest Happiness Principle, holds that actions are right in proportion as they tend to promote happiness, wrong as they tend to produce the reverse of happiness. By happiness is inteded pleasure and the absence of pain and the privation of pleasure" (Mill 13).
