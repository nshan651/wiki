# Paper 3 Outline

## Topic 2: Should Amazon, Facebook or Google be broken up?

#### Sources
* [Has Big Tech gotten too big for our own good?](https://www.marketwatch.com/story/has-big-tech-gotten-too-big-for-our-own-good-2018-07-02)
* [Mkt structure and antitrust subcommittee](https://research.chicagobooth.edu/-/media/research/stigler/pdfs/market-structure---report-as-of-15-may-2019.pdf?la=en&hash=B2F11FB118904F2AD701B78FA24F08CFF1C0F58F)
* [BU: Is breaking up big tech a good idea?](https://www.bu.edu/articles/2019/break-up-big-tech/)
* [Market Watch: Has Big Tech gotten too big?](https://www.marketwatch.com/story/has-big-tech-gotten-too-big-for-our-own-good-2018-07-02)

### FB Notes

https://www.wired.com/story/tim-wu-explains-why-facebook-broken-up/

#### Pro
#### Contra

* "Social Network" not a well-defined market
* Not a monopoly
* Non-essential
* Regulation would be more effective

#### Zuck's Arguments

1. Dividing the company would force the fragments into a growth-at-all-costs mindset, rather than a focus on privacy and security
2. Breaking up US tech gives the advantage to China and other foreign platforms to move in. Tech cold war
    - Historically, breakups have led to even more growth.
    - 1980s AT&T and IBM during a time when Japan was a serious technological threat. The US broke them up, which new industry growth


### Notes

* *Would society be better off?*
    - Or are we better off with big tech and the large-scale services that they can provide

* Horizontal vs Vertical Integration

* Privacy is best served by a pluralistic, heterogenous industry

* Big tech has a huge amount of control over our lives
    - Huge amounts of political power and influence
    - They control

* My argument for breaking them up isn't necessarily about market dominance, it's about social and political dominance

* Amazon has a huge advantage by allowing competitors on its marketplace. Not only do they have to pay a fee, but they are also giving away valueable pricing info away that AMZN can use against them
    - Amazon can also sell products at a loss and simply make it up in another area of their business

* Blurry boundaries between markets

* Many startups exist with the sole goal of getting bought out by big tech
    - Removes potential competition
    
* Critics may claim that these startups may not exist or get VC funding in the first place if there wasn't the promise of being bought out
    - I argue this isn't necessarily the case. Instead of a competition to see which startup can burn the brightest before running out of funding, they would be forced to think about their long term business strategy.

* Buuuuut, the assurance of getting aquired may have led to risks that wouldn't otherwise have been taken
    - Counterargument: While this may be true, startups make certain decisions tailored to impress big tech that might not be the best overall for innovation.

* Standard monopoly frameworks do not apply to most big tech

* FB closest to a *natural* monopoly
    - Network effect of social media makes it difficult for competing platforms

### Basic Pro/Con List

* Pro:
    - Opportunities for small biz
    - Reduced issues with privacy
    - Fewer security/privacy concerns
* Cons:
    - Less money put into important R&D
    - Market fragmentation could lead to higher service costs/worse user experience
