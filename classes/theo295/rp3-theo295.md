# THEO 295 Reaction Paper 3

## 2022-11-26

### Notes

Lorem ipsum dolor sit amet, officia excepteur ex fugiat reprehenderit enim labore culpa sint ad nisi Lorem pariatur mollit ex esse exercitation amet. Nisi anim cupidatat excepteur officia. Reprehenderit nostrud nostrud ipsum Lorem est aliquip amet voluptate voluptate dolor minim nulla est proident. Nostrud officia pariatur ut officia. Sit irure elit esse ea nulla sunt ex occaecat reprehenderit commodo officia dolor Lorem duis laboris cupidatat officia voluptate. Culpa proident adipisicing id nulla nisi laboris ex in Lorem sunt duis officia eiusmod. Aliqua reprehenderit commodo ex non excepteur duis sunt velit enim. Voluptate laboris sint cupidatat ullamco ut ea consectetur et est culpa et culpa duis.

* Paradoxical that God is very close, yet humanity is far from God. 
    * Faith brings one closer
* Participation in Sufism requires group participation. The teachings of the saints and individual reflection are important, but one must study with a group in order to develop.

#### The Many Wines (Rumi)
 
* The Many Wines by Rumi serves a warning for the many worldly pleasures that we may encounter. 

> God has given us a dark wine so potent that, 
> drinking it, we leave the two worlds

* The two worlds mentioned that Rumi refers to are likely the world of the body and the world of the mind.

> God has put into the form of hashish a power...

* Hashish is a drug made from compressing part of the cannabis plant, and achieves an effect not unlike that of marijuana.
*  Layla and Majnun:
    * https://en.wikipedia.org/wiki/Layla_and_Majnun

> There are thousands of wines that can take over our minds
> Don't think all ecstasies are the same!

* Although achieving a state of ecstasy and enlightenment is important to Sufis, they believe that achieving this state happens through abstaining from worldly pleasures, rather than indulging in them.

> Every object, every being, is a jar full of delight.
> Be a connoisseur, and taste with caution

* Every worldly object can be made into one of pleasure that stimulates the mind and the body. Rumi warns us to be cautious of indulging, and to only indulge in "the purest" of things.
