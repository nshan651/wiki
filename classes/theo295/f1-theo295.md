# THEO 295 Final

* Hadith
    * Hadith are sayings, actions, and teachings of the Prophet that have been passed down for generations of Muslims
* Hadith vs Sunna:
    * Hadith refers specifically to the sayings
    * Sunna refers to the Prophet's overall mannerisms in addition to his words and deeds
* What are they? What are the parts of a hadith chain (isnad) vs. matn. How are hadith evaluated by scholars? What are the main categories/ranking (true (sound), good, weak, and fabricated). Why is there some disagreement as to how extensively hadith should be applied?
    * Hadith are important sources of guidance and law for Muslims
    * Consists of two main parts: chain of transmission (isnad) and the text of the hadith (matn). Isnad tells us about the people transmitting the texts, the matn is the content of said text.
* Ranks of Hadith:
    * typically grouped into four categories: true, sound, good, weak, and fabricated
* Islamic Law (shari‘a)
    * Islamic legal system that is derived from the teachings of the Qur'an and the Prophet
    * Dynamic system of changing and evolving laws
    * Two main kinds: hudud (serious crimes and offenses) and tazir (less serious crimes that are punishable under law but that are not specifically mentioned
* Qur’an and law—the hudud—punishments for theft and adultery
    * Thievery and adultery are typically punished by cutting off the offenders hands and/or flogging
    * Part of the more serious set of hudud crimes
* Prohibition of interest (riba)
    * Fundamental principle of Islamic finance. Has to do with the prohibition of debts and loans
    * Also applies to transactions that involve the payment of an unequal amount in exchange for a deferred benefit
    * Islamic finance must be based on risk-sharing and profit-loss sharing, rather than the charging of interest
    * Achieved through financial instruments such as mudarabah (profit-sharing), musharakah (joint venture), and murabahah (cost-plus financing)
* What is the scope and importance of the Islamic shari‘a?
    * The scope and importance of Shari'a is vast and far-reaching. It is a set of comprehensive laws that include all aspects of Muslim life, including beliefs, morals, and daily actions
    * Provides Muslims with an important set of principles to live by
* What are the four roots of Islamic law: Qur’an, sunna, qiyas, ijma‘
    * Qur'an - the word of god
    * Sunna - the sayings and mannerisms of the prophet
    * Qiyas - process of using reasoning and analogy to derive new legal rulings from the Qur'an and sunna. Allows Muslim scholars to apply the principles of Islamic law to new situations that may not be explicitly addressed
    * Ijma' - consensus of the Muslim community. It is the agreement of Muslim scholars and legal experts on a particular issue, and it is considered to be a valid source of Islamic law
* What is ijtihad? Taqlid—their relevance to contemporary debates?
    * Itjtihad is the process of using independent reasoning to interpret and apply the principles of Islamic law 
    * Taqlid is the practice of following the legal ruling sof a qualified Islamic scholar without question. Based on the idea that ordinary Muslims are not qualified to interpret the Qur'an and the sunna on their own, and should therefore defer to the opinions of experts
* What is the importance of the madhhabs? (schools of law)
    * Important for providing Muslims with a diverse range of legal opinions and interpretations
    * Allows Muslims to choose the madhhabs that align best with their own beliefs
    * Named after different Islamic scholars that founded them
* What are the five categories of actions?
    * Fard - category of actions considered to be mandatory for **all** muslims
    * Wanjib - actions that are highly recommended and necessary for the practice of Islam
    * Mustahabb - recommended and encouraged in Islam. Not required, but seen as good and virtuous
    * Muhab - considered to be neutral and permissible in Islam. Neither encouraged nor discouraged
    * Haram - Forbidden in Islam
* What is a fatwa?
    * A fatwa is a legal opinion issued by a Muslim legal scholar
* How does shari‘a work in diverse Muslim contexts?  What is its appeal to contemporary Muslims?
    * Shari'a works so well in diverse contexts because of its adaptability and flexibility in the face of changing circumstances
* Issues in law: bank interest, gender justice, corporal punishments, should there by an Islamic government?
    * Bank interest (riba) has long been prohibited, but the interpretation and application has been the subject of debate among Muslim scholars
    * Gender justice is also a topic of discussion. One side claims that Islamic law does provide for gender justice and equality, while the other side believes that it is not sufficiently progressive on issues of gender
    * Use of corporal punishment, such as mutilation and flogging is a controversial issue. These punishments are prescribed as part of hudud, but there is debate on the appropriateness and effectiveness of these punishments in the modern world
    * The question of an Islamic government is another hotly debated issue. Some believe that an Islamic gov'n't is necessary in order to fully implement Shari'a, while others believe that Islamic law can be applied and upheld in a non-governmental context

## Sufism (tasawwuf)

* Is Sufism Islamic or not? Why? (an opinion question) 
    * Sufism should be considered Islam because it is based on the same core principles of the Islamic faith and the belief of the oneness with God
    * Though traditional Islam focuses more on a set of laws and Sufism on individual spiritual growth, they share the same core belief structure and have similar goals
* List historical stages in the development of Sufism (piety and fear of God, love and ecstasy, theory, philosophy and poetry, Sufi Orders, etc.).  
    1. The early stages of Sufism, which focused on the development of piety and fear of God through practices such as asceticism, solitude, and contemplation.
    2. The emergence of the concept of divine love and the experience of mystical ecstasy, which became a central aspect of Sufi thought and practice.
    3. The development of Sufi theories, philosophy, and poetry, which helped to further define and clarify the principles and practices of Sufism.
    4. The formation of Sufi orders, or brotherhoods, which provided a framework for the organization and dissemination of Sufi teachings.
    5. The spread of Sufism beyond its origins in the Middle East and its incorporation into various cultures and societies around the world.
    6. The modern era, in which Sufism has continued to evolve and adapt to changing social, political, and cultural contexts.
* Know about some major Sufis and their contributions such as: Rabia of Basra, al-Hallaj, al-Ghazzali, Ibn ‘Arabi, Jelaluddin Rumi
* What are some Sufi practices? Shaykh, dhikr, tariqa, stages and states
    1. Shaykh: A shaykh is a spiritual teacher or guide who helps to direct the spiritual growth of their students. Shaykhs play a central role in the transmission of Sufi teachings and often lead group practices and rituals.
    2. Dhikr: Dhikr is the practice of repeating the names of God or other phrases in order to focus the mind and heart on God. It is often done in a group setting, with the shaykh leading the repetition of the phrases.
    3. Tariqa: A tariqa is a Sufi order or brotherhood, which provides a framework for the organization and dissemination of Sufi teachings. Each tariqa has its own specific practices and rituals, and members of a tariqa often receive spiritual guidance from a shaykh.
    4. Stages and states: Sufism recognizes a series of stages and states that individuals pass through on their spiritual journey. These may include the states of poverty, detachment, and annihilation, as well as the higher states of fana (annihilation of the ego) and baqa (permanent union with God).
* Pre-Modern Reform movements
    * The Khawarij movement: The Khawarij were a group of early Muslims who rejected the authority of the first three caliphs and claimed that only those who were truly devout and just could lead the Muslim community. The Khawarij rejected many of the traditions and practices of mainstream Islam, and their strict interpretation of Islamic law laid the groundwork for later reform movements.
    * The Mu'tazilah movement: The Mu'tazilah were a group of theologians and philosophers who emerged in the 8th and 9th centuries. They believed in the rational examination of Islamic doctrine and rejected many of the superstitions and traditions that had become associated with mainstream Islam. The Mu'tazilah emphasized the importance of reason and justice in Islamic law, and their ideas influenced many later Islamic reform movements.
    * The Ash'ari movement: The Ash'ari were a group of theologians who emerged in the 9th and 10th centuries. They sought to reconcile the rationalist ideas of the Mu'tazilah with the traditional beliefs of mainstream Islam. The Ash'ari believed in the absolute oneness of God and the importance of following the teachings of the Prophet Muhammad, but they also emphasized the role of reason in interpreting Islamic doctrine. Their ideas influenced many later Islamic reform movements.
    * The Wahhabi movement: The Wahhabi movement was founded in the 18th century by Muhammad ibn Abd al-Wahhab. It sought to return to the strict monotheism of the early Muslim community and rejected many of the superstitions and practices that had become associated with mainstream Islam. The Wahhabi movement was particularly influential in Saudi Arabia, and its ideas continue to shape the beliefs and practices of many Muslims around the world today.

## Pre-Modern Reform movements

* Know something about each figure or movement--activities and/or ideas
* Ibn Taymiyya
    * 13th century scholar and theologian known for his strict conservative interpretations of Islamic law. 
    * Critical of other Muslim sects, such as Sufis and Shi'a
    * Emphasized importance of strict monotheism
    * Influenced later movements including the Wahhabi movement
* Wahhabi movement, M. ibn ‘Abd al-Wahhab
    * The Wahhabi movement was founded in the 18th century by Muhammad ibn Abd al-Wahhab. 
    * Sought to return to strict monotheism
    * Particularly influential in Saudi Arabi
* importance of the Crusades, the Mongol conquests.
    * Two major events in midieval Islamic history that had significant impacts on the Muslim world
    * Crusades were a series of holy wars fought between European Christians and Muslim armies in the Middle East
    * Mongol conquests were a series of invasions by the Mongol Empire that swept across much of Asia and Europe, including much of the Muslim world. Had a profound cultural, political, and economic impact on the lands
* What is colonialism? It effects on the Muslim world
    * Refers to the period in which European powers, such as the British and the French, established and maintained control over much of the Middle East and North Africa
    * Resulted in the loss of political sovereignty and the introduction of European cultural and economic systems

## Types of Islamic Responses to Modernity

* Pan-Islamism (its characteristics)--Jamaluddin Afghani
    * Movement that sought to unite all Muslims under a single caliphate or state
    * Originated in the 19th century and was influenced by the ideas of Jamaluddin Afghani, a prominent Islamic scholar who argued for the need to unite under the Muslim world order in order to resist European colonization and to protect the Islamic faith
* Nationalism - (what is it?) Ataturk, Reza Khan d. 1944, Reza Shah Pahlavi d. 1979
    * Nationalism in the Muslim world emerged as a response to Euopean colonization and imposition of foreign ideology and systems.
    * Prominent leaders of Muslim nations include Ataturk, who led the movement for Turkish independence
    * Reza Sha Pahlavi, who was instrumental in the modernization and westernization of Iran
* Modernism/Islamic liberalism
    * A movement that seeks to reconcile traditional Islamic beliefs and practices with modern liberal values such as freedom, democracy, and human rights
    * Emerged in the 19th and 20th centuries as a response to European colonization and the spread of western ideas
    * Prominent proponents include **Sir Sayyid Ahmad Khan** and **Muhammad Iqbal**, who sought to develop modern liberal interpretations of Islam that would be compatible with the values of the modern world
* Impact of modernity on religion
    * Rise of secularism and rationalism
* Fundamentalism/literalism/Islamism
    * Fundamentalism is a religious movement that seeks to return to the fundamental principles and beliefs of a religion, often by rejecting modern ideas and practices that are seen as incompatible with those principles
    * Literalism is the belief that religious texts should be interpreted literally, without consideration for the historical or cultural context in which they were written
* Idea of an Islamic state; Islamism
    * Islamism is a movement that seeks to establish an Islamic state or caliphate based on Shari'a law
    * Emerged in the 19th and 20th centuries. Islamists believe that Islam should not only be a personal faith, but also a political and legal system that governs all aspects of society
* Hasan al-Banna (1906)
    * Egyptian Islamist activist who was the founder of the Muslim Brotherhood
    * Grew up during a great political and social upheaval in Egypt
    * Muslim Brotherhood sought to promote Islamic values and revive the caliphate, and was one of the most influential modern Islamic organizations
* Sayyid Qutb, The Muslim Brotherhood
    * Egyptian Islamist writer and activist who was a leading member of the Muslim Brotherhood
    * Became a vocal critic of the Egyptian government and its failure to implement Islamic law
    * Arrested and imprisoned for his views, during which time he wrote his most famous work, "<ilestones"
* Syed Maududi, The Jamaat Islami
    * The Jammat Islami is a political party in Pakistan that was founded by Syed Maududi in 1941
    * Islamist party seeking to establish an Islamic state in Pakistan based on Islamic law
    * Powerful and influential political force in Pakistan
* Iranian Revolution-Khomeini
    * Political and religious movement in Iran that led tot he overthrowal of the Shah of Iran and the establishment of the Islamic state
    * Leader of the revolution was **Ayatollah Ruhollah Khomeini**, who became the first supreme leader of the Islamic Republic of Iran
    * Revolution was fueled by widespread dissatisfaction with the Shah's authoritarian rule and his efforts to westernize and secularize Iran
* Tablighi Jamaat
    * Global Islamic missionary movement that was founded in India in the 1920s
    * Encourages Muslims to return to practicing their faith in a more traditional and devout manner
    * Founded by Indian Islamic scholar **Maulana Muhammad Ilyas**.
* Extremism/radicalism
    * Radical views and religious ideologies in Islam
    * Al-Qaeda and ISIS are two prominent examples of extremist and radical Islamic groups who have carried out numerous terrorist attacks around the owrld
* More recent trends—progressive Islam, Islamic feminism, media and Internet
    * There have been trends within Islam that have pushed back against extreme and radical ideologies
    * This includes the rise of progressive Islam, which emphasizes the compatibility of Islam with modernity and democracy
    * Islamic feminism is another movement that has quickly risen to prominence. The movement advocates for women's rights within an Islamic framework
    * The role of the internet and media has allowed for the rapid spread of more modern and progressive ideas within the Islamic world 
* Issues of Islam and politics
    * There have been many movements within Islam for both the creation of an Islamic state and for the separation of religion and politics
* Role of shari’a—in changing world, in Islamic and nationalist movements 
* Islam and democracy
    * Often seen as incompatible, with critics arguing that the principles of Islam are at odds with democracy. However, many scholars and Muslims argue that the two can coexist
* Understand issues Relating to the Status of Women in Islam
    * Some argue that the role of women in Islam is inherently misogynistic, while others argue that Islam grants women a number of rights and protections
    * The Qur'an and hadith contain a number of statements on the role of women
* Legal rights given to women by Islam
    * Some of the statements in the Qur'an are seen as restrictive, others argue that women are granted a number of legal rights
    * This includes the right to own property, seek divorce, and to receive an education
* Hijab
    * A religious headscarf
    * Seen to some as oppressive, and to others as a symbol of religious piety
* State interference in women's choice of dress
    * Dress is often seen as a matter of personal freedom, but in some cases state authorities have attempted to interfere with women's clothing choices
* Changing family structures; issues of right to work, availability of divorce
    * The increasing prevalence of single-parent households and the rise of gender inequality have also had an impact on the status of women in Islam
    * Raises questions about the rights of women to work outside the home, their access to divorce, and their ability to make decisions about their own lives
* Responses to women's issues--traditional, conservative, liberal
    * Islamic feminism is one of the more liberal responses to women's rights in Islam
    * It seeks to promote the rights and equalit of women
    * Many Islamic feminists argue that the principles of Islam are in fact compatible with gender equality and the advancement of women's rights

## Muslims in The United States

* Muslims in The United States
    * Diverse religious and cultural group
* "Black" Muslim movement/s
    * Also known as the Nation of Islam, was founded by 20th century **Wallace Fard Muhammad**, and later by **Elijah Muhammad**
    * Movement sought to promote rights and dignity of African Americans
    * Placed emphasis on black self-reliance and self-sufficiency
* Malcolm X
    * One of the most prominent members of the Nation of Islam
    * Converted to Sunni Islam after a pilgrimage to Mecca
* Louis Farrakhan
    * Another notable figure in the nation of Islam
    * Took over leadership of the movement after the death of Elijah Muhammad
* Immigrant Islam
    * Many Muslim immigrants or descendants of immigrants
    * Has its own unique history and characteristics, and is often the subject of discrimination and Islamophobia
* Changing roles of Islamic institutions in the US
    * Islamic institutions have sought to adapt to the needs of the Muslim American community
    * Traditionally focused on providing religious education and services, but now have begun to play an active role in advocating for the rights and interests of Muslim Americans
    * Also involved in providing social services to the broader Muslim community, such as in helping refugees and immigrants

## TERMS and NAMES in addition to those mentioned above

* Al-Bukhari
    * Highly respected scholar of Islamic law. 8th century scholar know for his collection of hadith, which is considered to be one of the most authoritative collections in Sunni Islam
* Al-Shafi‘i
    * 9th century scholar who is know for his contributions to the development of Islamic legal theory and jurisprudence
* Ulama (ulema)
    * Refers to Muslim scholars who are experts in Islamic law and theology
    * Play a role in interpreting and the dissemination of Islamic teachings and are often seen as spiritual and intellectual leaders of the Muslim community
* Madrasa
    * Islamic religious school where students study the Qur'an, Islamic law, and other religious subjects
    * Play a central role in the transmission of Islamic knowledge
* Umayyads and Abbasids
    * Major muslim dynasties ruling over the Islamic empire of the Middle East and North Africa
    * Umayyads - ruled from the 8th to 13th centuries, first Muslim dynasty to establish a unified empire
    * Abbasids - ruled from 8th to 13th centuries, are known for their contributions to the development of Islamic science and learning and for their famous House of Wisdom constructed in Baghdad

## ISLAMIC LAW (terms)

* hadith
    * Hadith refers to the sayings and actions of the Prophet Muhammad, which are considered to be an important source of Islamic teachings and guidance.* sunna
* isnad
    * Isnad refers to the chain of transmission for a hadith, which includes the names of the people who passed down the hadith from one generation to another.
* matn
    * Matn refers to the text of a hadith, which contains the actual words and teachings of the Prophet Muhammad.
* Madhhab (school of law)—any of the school names (optional)
    * Madhhab, or school of law, refers to the various legal traditions within Islam, which are based on different interpretations of the Qur'an and hadith. The four main schools of law are the Hanafi, Shafi'i, Maliki, and Hanbali schools.
* Shari‘a 
    * Shari'a refers to Islamic law, which is based on the Qur'an, hadith, and other sources of Islamic teachings.
* qiyas
    * Qiyas refers to the process of applying reason and analogy to Islamic legal matters in order to arrive at a ruling or decision.
* fiqh
    * Fiqh refers to the study and understanding of Islamic law.
* fatwa
    * Fatwa is a legal opinion or ruling issued by an Islamic scholar in response to a question or problem.
* al-Shafi'i (Shafi‘i school)
    * The Al-Shafi'i school is one of the four main schools of Islamic law, named after the 8th century scholar Al-Shafi'i.
* ijma‘
    * Ijma' refers to the process of reaching a consensus among Islamic scholars on a particular issue or ruling.
* ijtihad
    * Ijtihad refers to the process of using independent judgment and reasoning to arrive at a ruling or decision in Islamic law.
* taqlid
    * Taqlid refers to the practice of following the rulings and opinions of a particular Islamic scholar or school of law.
* halal
    * Halal refers to actions or behaviors that are considered permissible or lawful under Islamic law.
* haram
    * Haram refers to actions or behaviors that are considered prohibited or unlawful under Islamic law.
* categories of actions
    * Categories of actions in Islamic law refer to the various types of actions that are classified as halal or haram, such as those related to food, clothing, and personal behavior.
* Sufism (tasawwuf)
    * Sufism, also known as tasawwuf, is a mystical movement within Islam that emphasizes the inner spiritual dimensions of the religion and the attainment of a closer relationship with God.

## Sufism (tasawwuf)

* tariqa
    * A tariqa is a Sufi spiritual path or order, which typically follows a particular shaykh or spiritual leader.
* shaykh
    * A shaykh is a spiritual leader or teacher within the Sufi tradition.

* dhikr 
    * Dhikr refers to the practice of repeating the names of God or other religious phrases as a means of focusing the mind and achieving a state of spiritual contemplation.
* fana
    * Fana refers to the Sufi concept of the "annihilation" of the ego in order to achieve a state of union with God.
* wali (saint)
    * A wali, or saint, is a highly respected and revered figure within the Sufi tradition, believed to be close to God and possessing special spiritual powers.
* Sufi shrines
    * Sufi shrines are places of worship and devotion for Sufis, often associated with the graves of revered Sufi figures.
* Individual Sufis
    * Individual Sufis refer to the many individual practitioners of Sufism throughout history, who have made significant contributions to the development of the tradition.
* Rabi'a of Basra, 
    * Rabi'a of Basra was an 8th century female Sufi saint and mystic who is known for her emphasis on the love of God and her rejection of worldliness.
* al-Hallaj
    * Al-Hallaj was a 9th century Sufi mystic who was executed for heresy, but is now widely revered as a martyr and a pioneer of the Sufi path.
* al-Ghazzali
    * Al-Ghazzali was a 11th century Sufi mystic and theologian who is known for his influential works on Islamic spirituality and his reconciliation of Sufism with mainstream Sunni Islam.
* Ibn ‘Arabi
    * Ibn 'Arabi was a 12th century Sufi mystic and philosopher who is known for his writings on the unity of God and the nature of reality.
* Jelaluddin Rumi 
    * Jelaluddin Rumi was a 13th century Sufi poet and mystic whose works are widely read and revered in the Sufi tradition.
* Mevlevi
    * The Mevlevi order, also known as the "Whirling Dervishes," is a Sufi order founded by the poet and mystic Rumi, known for their practice of whirling as a form of dhikr.
* Whirling Dervishes
    * Members of the Mevlevi order, a Sufi order founded in 13th century by mystic poet Rumi
    * Known for their practices of whirling as a form of dhikr, or the repetition of the names of God
* Bid’a
    * Bid'a refers to innovation or novelty in matters of religion, which is generally discouraged in traditional Islamic thought.
* Hijab, niqab
    * Hijab refers to the head covering worn by some Muslim women as a sign of modesty and piety. Niqab is a face veil worn by some Muslim women.
