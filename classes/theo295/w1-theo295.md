# THEO 295 W1

## Intro to Islam: Lingual Roots

* Islam - submission to God (Allah), acceptance of Allah
* Surrender to the will of God
* The same Arabic root s-l-m as "salaam" or "peace"
* Muslim - surrender to God:
    * Refers to cultural as well as religious
* Misconception: Arab and Muslim:
    * Arab - ethnic group, language
    * Muslim - religion, culture
    * Most Muslims are **not** Arab

## Ernst, Islam in the Eyes of the West

* Large gap in understanding of the Muslim world between The U.S. and Western Europe vs the rest of the world
* "The Muslim World" and the West are **not** opposites:
    * Plenty of Muslims in the West
* Pamona College: ceremony of the flame
* Huge emphasis on our Greco-Roman influences. Often our other diverse roots are forgotten
* Islamic civilization claims to be built on the same foundation:
    * Prophets of Israel and the Greek philosophers
* Popular view is that Europe = proper civilization and everyone else = barbarians 
* Christianity has clear Asiatic origins, but has been transformed by the West
* Who benefits from religious rhetoric
