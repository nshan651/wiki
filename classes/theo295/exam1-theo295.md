# THEO 295

## Exam 1

### Part 1

* What is Orientalism and what is the relevance of Orientalism for the contemporary study of Islam? (Ernst chapter)
    * The imitation or depiction of aspects from the Eastern world, specifically from Westerners
* What doss Ernst mean when he asserts that Israelite prophecy and Greek rationalism are shared by “Islam” and the West.
    * It is a common misconception among Westerners that we are unique in our greco-judaeo roots. Islam shares many of the same influences, and is in fact responsible for preserving much of the information for the West to later on rediscover
* What differentiates “ethnic” or local religion from universal ones?
* Concept of religions (practices and interpretations) changing over time (Ernst)
* Islamic resources for pluralism (Ernst/lecture)
* What impact did colonialism have on the perception of Muslims by the West?
    * Islam was seen as a "competitor" to Christianity, which led to it often being portrayed in a negative light
* Issues Facing Muslims Today—review issues covered in Ernst/Class discussion
* Terms: Orientalism, Islamophobia, studying about religion vs. studying of religion
* What is the significance of “islam” as small letter (generic) vs. “Islam” the historical religion
* Terms/concepts: Orientalism, Islamophobia, studying about religion, sacred history

### Part 2

* Jahiliyya Pre-Islamic Arabia: What was the life of the Pre-Islamic Arabs like?— their values and beliefs? Which elements did Islam reform (blood feuds, murder of infant girls, polytheism, etc.) Role of poetry and oral culture. See Brown 19-35
* Prophet Muhammad - Know the major events in his life and their significance for the religion. (Brown, lectures, ppt), Meccan and Medinan periods 
* Film: Empire of Faith What is the Islamic concept of sacred time, prophetology? Lecture/Brown
What is the importance of being a “Companion” of the Prophet and who were some of the major 
companions (Ali, Khadija, A'isha, Abu Bakr, Bilal, Umar, Fatima, Ali) and their roles. Brown 
55-75, Film, class discussion

* Qur'an- How did the revelations occur according to Muslim belief? How did the Qur'an achieve written form (collection and canonization)? What are the differences between the Meccan and Medinan chapters? What are some elements of Qur'anic style?

* What are the major themes of the Qur'an? God (Allah, tawhid, the divine names), human beings (the creation story, theological anthropology-concept of Adam—human beings’ not being perfect –need divine guidance) Brown and Hermansen lecture judgment (of individuals and in history-heaven, hell, eschatology), 

* Prophetology and other faiths? Who are the People of the Book?What are Muslim positions on other religions (Judaism and Christianity) and their scriptures? Which elements of the role of Jesus are important for Muslims?

### Part 3

* Lecture/Ernst/Brown 77-101
 
* How is oral transmission important in reciting and passing on the Qur’an? (lecture, film Qur’an by Heart)
 
* The Five Pillars  (terms: Shahada, Salat, Saum/Ramadan, Zakat, and Hajj) --Profession of Faith, Prayer, Fasting, Alms tax (Zakat), pilgrimage. What is the nature and importance of each pillar?
 
* Documentary: Nightline Hajj
 
* Bowen chapter—the idea of religious rituals- both “working on the self” and “social performance.”
 
* Expansion of Islam
 
* Roles of the Four Caliphs-Abu Bakr Umar Uthman, Ali 
 
* Umar's policies the dhimma, dhimmi
 
* Shi'ism  
 
* What is the Shi'a branch of Islam? Why is the martyrdom of Husayn, son of ‘Ali significant for Shi‘ism? 
    * It gave rise to the rejection of the caliphate 
 
* Compare and contrast Shi'a and Sunni Islam-- where found, concept of religious authority (imam vs. khalifa)
 

### Part 4

* Terms for the Mid-Term Islam Theology 295 Any terms/names listed above +

* Quraysh
    * Arabic tribe controlling the city of Mecca and the Kaaba
* Hira 
    * The name of the cave where the word was revealed to the Prophet
* Khadija 
    * Wealthy businesswoman who proposed to Muhammad. His first wife.
* Wudu
    * Islamic ritual for purifying/cleansing the body
* A’isha 
    * Muhammad's third wife. Know to be very intelligent, a renowned lawyer, and a great leader. Narrated many hadiths. Daughter of Abu Bakr
* qibla
    * Direction toward the Kaaba which Muslims face during prayer 
* Fatima 
    * Daughter of Muhammad and Khad. Shi'i - their only daughter, but Sunni - one of four daughters
* Abu Bakr
    * Father of Aisha. One of the first converts. Contributed his wealth to Muhammad's work. Became the first caliph 
* Abu Talib 
    * Muhammad's uncle. Leader of the Banu Hashim, a Meccan tribe
* ‘Umar
    * Second caliph. Father-in-law of Muhammad. Great military strategist
* ‘Ali 
    * Muhammad's cousin. The first child to convert to Islam. Later in life became a Caliph (4th)
* Hijra 
    * The prophet Muhammad's migration (622 ad) from Mecca to Medina
* Eid
    * Holiday of breaking the fast, celebrated at the end of the month of Ramadan
* Hajj 
    * The annual pilgrimage to Mecca. Mandated by one of the pillars of Islam.
* Salat 
    * Prayer session held five times a day, the second pillar
* Sunna
    * The body of Islamic custom and practice based on Muhammad's words and deeds
* Zakat 
    * Almsgiving. Collected by the Muslim Ummah. A mandatory pillar.
* Shari‘a
    * Traditional Islamic law
* Jihad 
    * Struggle
* Tawhid 
    * Unification of God in Islam
* Ka‘ba
    * "The cube." Building at the center of Islam's most important mosque
* Jahiliyya 
    * Age of ignorance preceding the Prophet's revelation
* Kufr 
    * A person who willfully disregards God
* Shirk 
    * The sin of idolatry or polytheism
* Ramadan
    * Fasting holiday
* Iblis /shaytan
    * The devil
* Sura 
    * The chapters of the Qur'an
* Qur'an 
    * Central religius text of Islam
* People of the Book
    * Refers to the other religions that Muslims regard as having been guided by previous revelations
* Jihad 
    * The struggle. Any effort both personally or sociallly to conform to God's guidance
* Nabi
    * Word for prophet. Can be someone following a previous message or a direct message from God
* Rasul
    * Someone who has certainly received a direct message from God
* Halal and Haram
    * Permissible and forbidden. Mostly refers to foods one can and cannot eat. Also refers to other lifestle choices
* Sunni
* 
* Shi’a
* 
* Caliph (khalifa)
    * Political leader of Islam, successor of Muhammad
* Imam (for Shi‘a 12 Imams)
    * Sunni - the leader of worship at Mosques. For Shi'a, exemplaray individuals who rule over community and are able to keep and interpret Shar'ia
* Husayn son of Ali
    * Grandson of the Prophet. Hero of Shi'a, he is the third Imam
* Mahdi
    * Messianic figure to appear at the end of time 
* Hidden Imam
    * The twelfth Imam, said to be the spiritual guide of Shi'i
* Kerbala
