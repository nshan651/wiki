# THEO 295 W3

## Ali Asani

* "In the history of conflict and antagonism, the dichotomy thrives on stereotypes. What stereotypes do is help to mask the humanity of the others. So you think of them as barbaric, you don't think of them as human beings"
* Ali's personal *jihad*:
    1. On how Islam is portrayed in the West
    2. Making Muslims understand that there are many ways of being Muslim
* Many Muslims look at all the diverse ways of identifying as Muslim to be a threat to the essential Muslim identity 
* Asani argues that there "is strength in tradition that you have all this diversity"
* Many *Imams* from the Middle East only speak about Islam from a narrow point of view of their own belonging, not of the religion as a whole
* It's the extreme outlier cases that linger in peoples' minds. The experience at the very end of the article is one such example
