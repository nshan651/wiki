# THEO 295 W11

## Mysticism

* Learning through direct experience of the ultimate reality
    * St. Ignatius was a mystic
* Monism:
    * One unified *thing*
* Sufism
* Rise of political Islam:
    * Generally saw mysticism as irrelevant

## Mystic Origins

* Etymology:
    * *Tasawwuf* - wool?
    * People of the bench - suf
* Determining if something is Islamic:
    1. Qur'an
    2. Sunna (What did the prophet do?) 
        * Sufis believe that Muhammad was the first
    3. Consensus
* Reasons for Sufism being Islam:
    * Verses in the Qur'an justifying
    * Sufis follow all of the same standard practices
* Reasons against:
    * Equating divine experience reserved only for heavenly beings
        * *shirk*
    * Absorbed local, pre-Islamic practices
        * Many of these could be described as shamanistic

## Development of Sufism

* How to become a Sufi
    * *Initiatic chain* 
        * `Person 1 (initiated by) -> Person 2 (initiated by) -> ..... Person N (initiated by) -> The Prophet Muhammad`
* Asceticism:
    * Turning away from distractions
    * Can include depriving yourself of various pleasures
* Stages of Sufism:
    * Asceticism - fear of God
    * Ecstasy - love of God
        * Fana - annihilation of the divine
    * Sufi Theorists
        * Manuals of the *stages and states*
        * Sufi psychology
        * Al-Ghazali (1111 AD)
* Al-Ghazali:
    * Sufi, legal scholar, professor
