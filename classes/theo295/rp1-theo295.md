# Reaction Paper 1: Ernst on Religion

## Prompt 

> Typed double-spaced in Word, (not a pdf) length 1-2 pages. Include bibliography of any sources used. For footnotes or in-text citations you may use APA, Chicago Manual of Style, or MLA format [your choice].
>
> Links to these various reference formats are posted on Sakai at the end of the Lessons tabs
>After reading the chapter by Carl Ernst “Approaching Islam in terms of Religion” [available in pdf format on Sakai] write a reaction paper in which you discuss either or both of the following passages:
>
> In your response you may draw from material in this chapter or on other sources. There should be some discussion of Islam as part of the response but it does not need to be the exclusive focus.

## Informal Observations

### Prompt A)

> The postmodern and postcolonial world, however, calls for a different approach to religion. Except for the diehard fanatics who are intent on converting the world to their doctrine, it should be apparent to everyone that pluralism is a fact of life. Not only do we have to accept the existence of multiple religions, but we must also acknowledge the nonreligious option as a significant and legitimate choice. [p. 44]

* Postmodernism has shown us that religious pluralism is a fact of life
* Moreover, nonreligious positions should also be seen as an equally legitimate choice
* Asserting one's religious dogma is an abuse of power which is not compatible with modern pluralism 
Christianity is characterized as belief-focused, whereas Muslims and Jews emphasize legal and ethical practices more than theology and doctrine
    * "Differences of opinion is a mercy of my community" - Prophet Muhammad
    * Qur'an is accepting of the authority of multiple other prophets, such as Abraham, Moses, and Jesus
    * The focus on the law leads often leads to relevant discussions of one's day-to-day religious life
* The shift to pluralism has changed the way in which we study and view religion:
    * It has changed from a study of political legitimacy and doctrine to one of historical understanding and interpretations
* World religions (as opposed to folk or local religions) are explicitly classified as competitive missionary religions on a global scale:
    * The traditional Western view compares other global religions to Christianity and ranks them by "legitimacy" and how much of a "threat" they are to Christian evangelism

### Prompt B)

> One of the important insights that has emerged from the study of religion as a historical and cultural reality is the realization that religions change; they are not timeless, eternal essences. [p. 50]

* Followers of a religion, particularly those focused on religious scriptures, often have a hard time accepting that religions are mutable
* Studying religions from a historical and an interpretive position clearly illustrates that change 
