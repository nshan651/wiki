# THEO 295 W4

## Quiz Questions

> The first questions on this quiz are based on the Film: Islam: Empire of Faith (2000). The PBS documentary features the commentary of academic scholars of Islam as well as recreations of life in Pre-Islamic Arabia and certain aspects of the biography of Muhammad. Note that the Prophet himself is never depicted by an actor due to Muslim sensitivities about this. In some cases pre-modern miniature illustrations are used to provide background.

1. What does one commentator in the film say in this regard (about how the Prophet tends to be depicted in Islamic art). (2 facts)
    * Muslims felt that the only just depiction of God or the prophet Muhammad was the word of God itself, the Quar'an. They felt that God could not be representable in picture form, and Muhammad should not be represented because he was not divine. When Muhammad was represented, he would be portrayed with a white cloth covering his face
2. List one contrast between the beliefs of the Pre-Islamic Arabs and the teachings of Islam.
    * Pre-Islamic Arabs followed various animist religions. It was only after the Quar'an had been revealed to Muhammad that Islam became a religious and creative force, which eventually led to an empire of faith.
3. List two facts that the documentary imparts about Pre-Islamic poetry (themes, importance, social role, etc.).
    * Pre-Islamic poetry was known for its richness and symbolic depth. People originally hailed Muhammad as a poet, but there is a passage in the Quar'an explicitly stating that "Muhammad is not a poet, poets speak through desire and this is the word of God."
4. In the documentary, which fact, statement, or image was the most interesting to you? This could work as inspiration for a blog post this week.
    * What I found most interesting was the spiritual and economic importance of the Kaaba, and how it acted as a place of peace where Bedouin tribes had to lay down their arms. It's also interesting how much cultural and religious diversity was present there. The people coming through were Christians, Jews, and Animists.
5. Based on the documentary or Brown's Chapter--name three major events in the life of Muhammad and state the religious significance of each.
    * The birth of Muhammad involved several miraculous events, including a revelation a king had in a dream that was later confirmed by two soothsayers and divine words spoken to Muhammad's mother.
    * Muhammad received the divine word of God from the angel Gabriel in a cave above Mecca. This would later become the Quar'an
    * Muhammad and several of his followers embarked on a pilgrimage from Mecca to Medina. This event, the Hijra, marks the beginning of the Islamic calendar
9. Formulate two questions that you would like to ask or have discussed in class. This could also lead to a blog post.
    * 

#### Week 4 Blog: Pre-Islamic Poetry

One thing from the documentary and this week's reading that really struck me was pre-Islamic poetry. The documentary describes its richness and layers of symbolism. Brown lists several examples, and indeed, they are extremely elegant, despite being English translations. However, the poetry is not only important for artistic reasons. It also serves as the primary historical documentation of pre-Islamic thought, religion, and culture. The importance of poetry is evident even in the time of Muhammad, when he was often praised for the elegance of his verbal expression. I thought the response to this in the Qur'an was interesting. There is a Surah that states "Muhammad is not a poet. Poets speak through desire." This seems to be in line with the emphasis on the message rather than the prophets.
