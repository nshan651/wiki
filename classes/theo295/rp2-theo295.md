# THEO 295

## Reaction Paper 2

### Notes

* Salat:
    * One of the five pillars
    * The ritual worship five times a day
* Salat is not always the most everyday ritual to Muslims
    * Some find that other rituals, such as fasting or holidays are more significant (Bowen 44)
* However, it still holds religious primacy because it is doctrinally central (Bowen 44)
* Salat can be public or private, individual or communal
* "Felecity" conditions of Salat:
    * Ritual cleanliness (ablutions)
    * Right state of mind (generally meaning the right intentions)

---

* Individual benefits of worship:
    * Way of self-control and ordering their lives
* For the collective, it benefits the moral domain
* Socially equalizing, everyone, regardless of class, must submit themselves to God five times a day
* Worship does not commemorate any specific historical event

---

(Bowen 50-51)
* Ritual as a social performance
* Worship *accomplishes* something:
    * Communion or commemoration
    * Merit
    * Communication with God
    * Obedience
* The mechanics of worship in a religion can shape the structure of the religion
    * Bowen gives the example of separation between priest and worshipper in Catholicism
    * This is in contrast with the "universal egalitarianism" that Salat promotes
* This is not to say that status is entirely absent from Salat:
    * A worshipper can still display their status by clothing, gestures, etc. among others during prayer
* Another part of the social performance is in correctness:
    * Doing or not doing certain things (such as saying the "Bismillah" aloud or not) signals one's position on these issues
    * One "marks" themselves as part of a specific movement within Islam

## Draft
