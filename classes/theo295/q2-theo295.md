# Quiz 3: Islamic Law in Nigeria

1. List two things you learned about Islamic Law and its being being applied in one region of Nigeria through watching this documentary.
    * Shariah Law has been introduced alongside British law in several states in Northern Nigeria with a Muslim majority. The two systems work together in parallel
    * Shariah law divided previously united communities when it was first integrated. There was a lot of tension between Muslims and Christians living in these areas.

2. Comment on the attitude of local Nigerian Muslims to the application of shari'a law in this society. (discuss two specific cases or comments based on the film)
    * Judge Essaw explains that many Muslims see Shariah as a peaceful, fair system. There is a woman who appears in the court because her husband is not paying her alimony. She ends up getting justice.
    * The UK reporter visits a women's group in the village to ask them about their views on Shariah. The first woman, when asked about the hijab, says that she prefers the hijab over traditional african head covering because it doesn't fall off as easily. Another woman says that Shariah has gotten rid of a lot of gamblers, alcoholics and prostitutes, and has established new education centers.
3. What was the most new or surprising fact or impression from the film for you? Why?
    * It was interesting to see the contrast between the thoughts on Shariah that people in the UK have versus the Nigerians who have been living under Shariah law for several years. The people interviewed in the UK had rather negative views of Shariah, calling it conservative and oppressive. However, the people of Nigeria are more favorable, saying that it facilitates social harmony and fairness.
4. Formulate one question that you have after viewing this documentary?
    * I was wondering why there are no upperclass or nobles who seem to appear in Judge Essaw's court. When asked about this, he responds that there is no discrimination in the court, and that anyone can be made to make an appearance. However, I wonder if that there might be some bribery going on.
5. T or F? According to the Brown text, ones of the categories for human actions in Islamic law is "neutral" (p. 151)
    True (p. 308)
6. T or F? According to Brown, after the 11th century Muslim legal scholars were pessimistic abut the efficacy of human reason in making moral decisions
    True (p. 310)
7. _____ is the term for a scholar's exercising effort and reason in attempting to discern the divine intent in a matter and hence the correct ruling in a matter of law  
    Qiyas
