# THEO 295 W2

## Ernst Ch. 2, Islam and the Structure of Religion

* St. Augustine on "True Religion":
    * Did not allow room for consideration of multiple religions
* Grotius writes a text on the truth of religion in 1627, which recognizes multiple "religions" as such, but still disparages the non-Christian religions
    * Characterizes the Islam (calls them "the Turkes") as "violent and false"
* Part of the misrepresentation of Islam by Christians is fueled by a "thirst to convert the world" (Ernst 44)
* Post-modernism has shown us that religious pluralism is a fact of life:
    * This includes the option of Atheism
* Modern Christianity is characterized as belief-focused, whereas Muslims and Jews emphasize legal and ethical practices more than theology and doctrine
* "Differences of opinion is a mercy of my community" - Prophet Muhammad
* Qur'an is accepting of the authority of multiple other prophets, such as Abraham, Moses, and Jesus
* The way that religion is studied as shifted from doctrinal authority/political competition to historical understanding and interpretation
* Fortunately, the separation of church and state has (mostly) prevented the favoring of one religion over another
* World religions (as opposed to folk or local religions) are explicitly classified as competitive missionary religions on a global scale
* Comparative religions originated in Protestantism:
    * Generally the three major players were Christianity, Islam, and Buddhism
    * These three were the "missionary" world religions. Everything else was local
* Classifying religion was compared to Linnaean biology, which took major categories and broke them down into sub-categories:
    * Presumed that there was a similar sort of taxonomy in the structure of religions
* The problem with this taxonomy was that it doesn't really allow for multiple conflicting sources of order within each religion:
    * I.e. Protestants vs Catholics, Judaism and Messianic Jews
    * Islam runs into the same sorts of problems

## Religions of Faith Versus Religions of Law

Something I'd like to talk about from this week's reading is the comparison that Ernst makes with the importance of religious dogma to Christianity relative to its importance to Islam and Judaism. Christianity is much more concerned with religious doctrine and belief, whereas Muslims and Jews place more emphasis on legal and ethical practices. I feel like the latter approach leads to very insightful, person-focused discussions of day-to-day religious life. In Judaism, for example, the Talmud (which translates to "learning") serves as a religious commentary and a guide to Jewish law and tradition. Muslims as well are much more amenable to different interpretations of Islamic law. There was a quote from Muhammad that supports this: "Differences of opinion is a mercy of my community" (Ernst 45). In contrast to this, Christianity has a much more narrow focus on belief. Even among different branches of Christianity there is often vehement disagreement. I remember the Lutheran church associated with my elementary school proudly refusing to participate in a city-wide service in the park because of a difference in how they viewed Holy Communion.
