---
title: "COMP 312: Final Project Report"
author: "Nick Shannon and Victor Rakotondranoro"
date: 2023-04-28
---

# Source Repository

> A link to your github repository (or other repository, if you chose a different one) that will allow me to download your source code.
> Your repository should have at least one of a mission statement code of conduct
> Your repository should have a modest amount of documentation, addressing how to use your new features.

- Our fork of youtube-dl (support-for-panopto):
    - https://github.com/nshan651/youtube-dl/tree/support-for-panopto
    - Commit history: https://github.com/nshan651/youtube-dl/commits/support-for-panopto

# Changes

> Overview of what changes you made, if you worked on a pre-existing project. You can also tell me about what changes you worked on, but didn't finish, or even what changes you contemplated.

It was a challenge to understand the structure of the youtube-dl source repository. We spent most of the time trying to figure out where to make the changes within the repository, as well as understanding the Panopto API. Some resources that were helpful were past contributions that involved adding support for other platforms (such as Zoom for example: https://github.com/Romern/youtube-dl-1/commits/zoomus-extractor), which is very similar to what we are trying to accomplish. It provided us a good starting point, but the code and implementation are different for each platform.

- We added a `panopto.py` file in the `youtube-dl/extractors/` directory. The extractor attempts to take a valid Panopto video URL, find its ID, and then use that ID to find the actual URL of the video on the Panopto server. 

```python
class PanoptoIE(InfoExtractor):
    IE_NAME = 'panopto'

    # Regex matching URLs of the webpages we want to extract
    _VALID_URL = '...'
    # Regex matching URLs of the actual video
    _VIDEO_URL = '...'
    # Tests go right here in the IE class
    _TESTS = [{
        # ...
    }]
    _NETRC_MACHINE = 'panopto'
```

- The other function we added in `panopto.py` was the actual extractor, `_real_extractor`:

```python
def _real_extract(self, url):
    # Match page URL (mobj is match object)
    mobj = re.match(self._VALID_URL, url)
    # Get the video ID
    video_id = mobj.group('id')
    # Download target webpage
    webpage = self._download_webpage(url, video_id)
    # Search the webpage for the correct video URL
    mobj = re.search(self._VIDEO_URL, webpage)
    if mobj:
        # ... then returns the webpage
```

- Finally, we added an the import `from .panopto import PanoptoIE` to `extractors.py` so that the entrypoint to youtube-dl can find the new extractor.
- We added support for login authentication. However, we did not end up having much sue for this as we were unable to access the direct video link, regardless of whether or not we were logged in.

The Panopto videos that are downloaded via the API are not using the URLs that are on the webapp, but the associated video URL on their server. Once the file added, we imported it in the youtube-dl/extractors/extractor.py file to It took us a long time to try to make this work, but we could not, unfortunately. When we run the command `python3 youtube-dl/__main__.py [url]`, it can identify the URL as a Panopto video, but it is not able to download it.


# Testing Procedure

> A document describing your testing procedure. This can be part of your repository, too. It is acceptable to have a manual testing procedure, in which case your tests will be a list of actions (eg button presses), and the expected result. You may be able to automate the testing of some arithmetic results.

Youtube-dl has an established test procedure. We define a test right inside the extractor class with the `_TESTS` variable like so:

```python
    _TESTS = [{
        'url': 'https://demo.hosted.panopto.com/Panopto/' +
        'Pages/Viewer.aspx?id=01234567-89ab-cdef-0123-456789abcdef',
        'info_dict': {
            'id': '01234567-89ab-cdef-0123-456789abcdef',
            'ext': 'mp4',
            'title': 'Video title',
        },
        'params': {
            'skip_download': True,
        },
    }]
```

Then, we can invoke the test runner using one of three commands:

```sh
python -m unittest discover
python test/test_download.py
nosetests
```

In addition, we also did a fair amount of ad-hoc testing with print statements, and commenting in and out various bits of code to get an understanding of the extractor's structure. We also made heavy use of `grep` and other cli tools for examining the downloaded webpage.

# Project Roles

> A written description of who on the team worked on what. I do not need to see specific time percentages, or how successful you were.

We had a handful of meetings over the course of the semester where we brainstormed ideas, pair-programmed on the extractor, and worked on the project write-ups. Victor set up multiple panopto accounts for testing, and he uploaded a public video for us to download as a sample. Nick researched various ways to get access to the direct video URL, such as Panopto's API or **[this](https://www.reddit.com/r/techsupport/comments/ohlg7k/assistance_downloading_videos_from_website_panopto/)** workaround that involves accessing the mp4 link with an RSS reader and screen recording.

Both of us were equally involved in the pair-programming, by iterating on and testing our ideas.
