---
title: "COMP 312: Libreoffice Bug"
author: "Nick Shannon"
date: 2023-03-23
---

# Bug Report 

Bug 125477 - Tabbed Compact NB incorrect refresh after using the ">>" to show hidden controls 
https://bugs.documentfoundation.org/show_bug.cgi?id=125477

## Description

I found this nearly immediately after opening libreoffice. The problem is that when you open Libreoffice at a certain window size (maybe 3/4 of my monitor, which is specifically a 27" 1080p display), the toolbar is shown in compact view with most of the options hidden. This normally wouldn't be a problem, but it looks quite strange when there's clearly plenty of room for additional buttons. If you click the ">>" to expand to the floating menu of hidden controls then click it again to minimize, all the options appear where they should normally be located on the main toolbar.

I have a suspicion that this bug is more apparent on tiling window managers, especially on dynamic tilers such as DWM where the default layout (at least for me) is one larger parent window alongside several smaller child windows.

## Reproducing

- I'm running DWM on Arch Linux, and I tested this on a 27" display. It may occur on other setups and platforms (a contributor suggested this was also a problem on Windows) but I unfortunately can't test it.

### Instructions

- First, open libreoffice while another window is open (terminal, web browser, whatever, as long as it's in "tiled" mode). 

![Fresh start](/home/nick/dl/bug/lo-2.png)

- Select file->new and open a new Writer text document. 
 
![file->new](/home/nick/dl/bug/lo-3.png)

- As you click around the menus, the navbar items should look a bit sparse. 
 
![Home menu is looking especially empty](/home/nick/dl/bug/lo-4.png)

![Insert has some options shown, but clearly there is more space](/home/nick/dl/bug/lo-5.png)

- Then, click the arrow once to open the floating menu, and again to close it. The items should then reappear in their correct positions.

![Floating menu](/home/nick/dl/bug/lo-6.png)

![Home bar is looking much better!](/home/nick/dl/bug/lo-7.png)
