# ENVS 101 Presentation Notes

## "Forest edges increase pollinator network robustness to extinction with declining area"

#### Definitions

- Nestedness is a property of a matrix in which the presence or absence of species in a community is organized such that the presence of species in smaller subsets (nested within larger ones) is predictable based on the species present in larger subsets.
- Modularity is a property of a network that describes the presence of densely connected subgroups (modules) within a larger, sparser network. In ecological systems, modularity can describe the organization of species interactions, where certain species form tight clusters of interactions, while interactions between clusters are looser.

### Abstract

- Edge effects on ecosystems typically amplify the negative effects of habitat loss
- In forested ecosystems, this may not necessarily be the case
    - The study shows that pollinators actually prefer open, sunny spaces
- In the study, when floral resources and plant richness declined at the edges, pollinators were still able to maintain a robust network:
- Pollinator species at the edges of forests are highly specialized, with **higher nestedness** and **lower modularity** than the interior networks
    - They maintained "high robustness to extinction" following the interior collapse

### Importance of the study

- Most studies focus on the effects of habitation loss of pollinators in grasslands or farmlands, in which effects are negative or neutral at best
- Why is this important to understand:
    - Habitation loss is one of the leading causes of species endangerment, and so this new empirical evidence that pollinators and flowers thriving in open, light-filled forest edges
    - This new information better equips us to understand how to allocate resources to protecting endangered species

### Introduction

- Habitat loss causes non-random loss of interactions
- This leads to **higher network modularity** and **lower nestedness**
- Pollinators are very good at maintaining robustness finding new "partners"
- Side tangent -- traveling salesman problem and the amazingness of honeybees' uncanny ability to estimate a "computational impossible" problem
- What does this mean:
    - Higher competition among species of similar trophic level
- Structural equation models (SEM) to test forest edges hypothesis:
    - Not super important to go into it, uses ~20,000 flower visitation records
    - Kind of like airline flight records
- Fig. 2 on page 3:
    - Squares represent measured variables
    - Arrows represent direction of hypothesized causal relationships
    - Double-headed arrows represent correlations

### Results

- The results suggest that the creation of forest edges does not exacerbate the negative effects of forest area loss on pollinators
- Fig. 3 (empirical results):
    - Shows that 24 paths remain in the final SEM model
    - Interaction effects between predictors are shown by one causal path intersecting another (with an open circle shown at the junction
    - Blue vs red represents the significant (P < 0.005) positive vs negative paths 
    - Lighter colors represent paths that don't deviate significantly from null
- Proximity to the forest edge had an even larger effect on plant and pollinator attributes than decreased areas:
    - These effects were uniquely positive
- Differences between small and large islands:
    - For interior communities, decrease in forest ares had negative effects on relative connectedness (higher relative connectedness within the smaller plant-pollinator groups on samall islands compared to larger islands)
    - Also had significantly lower nestedness and higher modularity on small compared with large islands
    - For forest edge communities, relative connectedness was substantially lower and nestedness substantially higher than observed for forest interior communtiies
    - These changes had a cascading effect on network robustness and extinction
    - In particular, higher nestedness had a strong positive effect on robustness
    - **While large islands had a higher robustness toward extinction, it was "only" a 10-20% better robustness, compared to 50-100% higher with respect to interior communities**
- Fig. 4 shows graphs of a bunch of the metrics that the study measured

#### Slides

- Nick:
    - 2,4,5,6,9
- Avery:
    - 1,3,7,8,[10](10)
