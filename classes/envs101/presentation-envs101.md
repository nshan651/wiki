# ENVS Presentation


- Technologies for hydrogen (H2) production fall into four main categories:
    - Thermal Processes: Thermal processes use the energy in various feedstocks (natural gas, coal, biomass, etc.) to release the H2 that is part of their molecular structure. The main hydrogen production technologies using fossil fuels are all thermal processes, and include reforming, gasification, and pyrolysis. Table 1 summarizes fossil fuel-based hydrogen production technologies.
    - Biological & Thermochemical Processes: Thermochemical processes use heat in combination with a closed chemical cycle to produce H2, while biological processes use microbes or fungi in fermentive reactions to produce gases from which hydrogen can be derived.
    - Electrolytic Processes: These processes use electricity to split water into its two chemical constituents, oxygen (O2) and H2,  using an electrolyzer. The cost and efficiency of producing H2 via electrolytic processes is directly dependent on the cost and efficiency of the electricity used in the process.
    - Photolytic Processes: These processes use light energy to split water into H2 and O2. These processes are currently in the early stages of development and currently are not viable for large scale production.
