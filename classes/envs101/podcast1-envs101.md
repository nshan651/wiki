---
title: "ENVS 101: The Clean Energy Revolution Podcast"
author: "Nick Shannon"
date: 2023-03-26
---

 
- [The Clean Energy Revolution Podcast](https://omny.fm/shows/the-clean-energy-revolution/a-revolution-at-our-fingertips-how-renewable-energ)
# Episode 1: A Revolution At Our Fingertips: How Renewable Energy Can Be Fair, Safe and Affordable

- When does the world need to halve carbon emissions to stay on track?:
    - By 2030
- In the UK- how much daily energy comes from electricity?:
    - About 20%
- How much energy goes to heating a home?:
    - About half
- When will the UK see its first fossil fuel free year?:
    - The year 2025 will be when they see first few hours and days, first year between 2030 and 2035
- How important is energy in fighting climate change?:
    - It is absoultely essential
- What does net zero mean?:
    - It means that the amount of greenhouse gasses added and removed are equal
- What happens when the US accelerates something? (Minute 7:20ish):
    - When the US accelerates something, it really accelerates
- What does ESO stand for? (After minute 9):
    - Electrical System Operator
- How do they forecast energy demand? (A little after min 11):
    - By looking at weather metrics, such as temperature
- A degree temp difference is how many megawatts? (Just before min 12):
    - A degree centigrade is equal to 500 megawatts of power
- When is the morning demand for energy highest on weekdays? (Just after min 12):
    - From 6-10 am when people are starting work
- At 15:30 what are the five key challenges to operating the grid at zero carbon?:
    1) Inertia
    2) Frequency management 
    3) Voltage control
    4) Thermal management of flows of the system
    5) Ways to restore the system from a complete shutdown
- Around 24:30 sustainability is all about balancing what?:
    - It's about balancing the needs for both today and tomorrow and for people and planet
- At 25:30 what is one of the risks of changing the system of the future?:
    - One of the risks is the ability to decarbonize people's homes, and how to afford being able support people in making the transition
- Since the privatization of the energy sector, who has borne the cost of decarbonization?:
    - The cost has been borne by consumers on consumer bills.
- Right around minute 28: What is it really about understanding?:
    - It's about understanding how to engage with multiple perspectives in ongoing work and building lasting relationships.
- 29:45 where does diversity and inclusion need to be with this?:
    - Diversity and inclusion need to be at the heart and center of this.
- What’s the next big milestone in the energy transition in the UK, according to Duncan?:
    - The operation of the network without fossil fuels is the next major milestone for the UK.
- What does Sharon say is the next milestone?:
    - She says the next major milestone is the decarbonization of heat.
- What is one thing new you learned from listening to this podcast that you will take with you?:
    - The transition to clean energy involves much more than just scientific innovation. It will require massive political and social cooperation, and discussions with people of diverse backgrounds and world views.
