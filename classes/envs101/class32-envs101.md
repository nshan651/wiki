---
title: "ENVS 101: Class 32 Assignment"
author: "Nick Shannon"
date: 2023-04-18
---

# In-Class Assignment

Discuss (out loud) the following two questions with your partner. Share your thoughts and opinions on the matter. They may differ. Please type in your own name below. 

1) Should rich nations pay for climate change ‘loss and damage’ in order to compensate for poorer nations suffering the impacts of climate change?:
    - Wealthier nations tend to offload the consequences of their consumption patterns and emissions on to poorer nations, who experience the negative effects of climate change at a disproportionate rate. Because nations like the U.S are responsible (directly or indirectly) for a large amount of emissions and are in a privileged position to make huge changes in mitigating climate change, I think we bear a significant responsibility to do what we can. 

2) Who is ultimately responsible for climate change?
    -  One could argue that there are individuals, groups, businesses, and countries that contribute more to global emissions than others, but ultimately we should all feel compelled to take action. Climate change is something that affects everyone, and so mitigating it has to be a concerted, collective effort. 

- After watching the “Who’s to Blame for Climate Change?” video was there anything that struck you or would cause you to change your original answers?:
    - I think the explanation in the video is pretty much aligned with my original answer. The video does a good job pointing out how there's a lot of signaling done by wealthy countries who point to their own efforts to produce green energy and reduce emissions, and who look at other poorer countries who don't necessarily have the capacity to do the same as inferior to them. I think it's important for individual countries to focus on what is in their own power to change, and not necessarily what other countries are doing (or not doing). The bottom line is that everyone is responsible and needs to do the best that they can.
