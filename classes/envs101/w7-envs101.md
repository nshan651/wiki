# ENVS 101 W7

## Water Quality

- Fly fishing and "matching the hatch":
    - Matches the kind of fly (bait) to what is found in the habitat
- Insect life can be a useful proxy for the health of the water:
    - Some insects are more tolerant to pollution
- Even a small change in the pH level can cause huge changes to the environment 

## Water Usage

- Most of our water use is **not** direct
- Our diet is one place where our water usage is highest

## Agriculture

- Pros and cons to ag.
- Pros:
    - Provides food
    - Surplus of reliable food
    - Economies of scale allow others to focus on other things
    - Specialization
    - More choices
- Cons:
    - Lots of resources required -- land, water, etc.
    - Environmental impacts -- runoff, pesticides, etc.
    - Disrupts habitats 
- Agronomy:
    - Brings in multiple disciplines to improve agriculture

## Agricultural Footprint

- More than 30% of terrestrial net primary production

## Important elements of Agroecosystems

- Nitrogen:
    - 78% of atmosphere but less than 0.003% in crust
    - Nitrogen fixation - Nitrogen entering the biosphere
        - $N_2$ gas into ammonia ($NH_3$)
        - Bacteria and **lightning strikes**
    - Nitrogen fixing microbes
        - Microbes associated with legumes
- Crop rotations:
    - Important because crops like legumes are nitrogen fixing, which enriches the soil for other crops
- Denitrification:
    - Returns to atmosphere, bacteria
- Human impacts:
    - Nitrogen can now be synthesized by humans, a major improvement to farming
    - Haber-Bosch process -- increases Nitrogen fixation rate by 200%
- Phosphorous:
    - Main sources:
        - Mining of sedimentary rocks
        - Guano harvest
- Dynamic homeostasis - the ability of an ecosystem to maintain stable levels of key process:
    - Agroecosystems are prone to nutrient loss
- Stages of Agroecosystems:
    - Harvest
    - Continual disturbance
    - Irrigation
    - Low biodiversity
