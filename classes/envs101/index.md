# ENVS 101 Index

* Presentation + worksheet:
    * Worksheet will be done separately, presentation is done with a partner
    * 2023-02-03 -- community ecology

## Notes

* [Week 1](w1-envs101)
* [Week 2](w2-envs101)
* [Week 3](w3-envs101)
* [Week 4](w4-envs101)
* [Week 6](w6-envs101)
* [Week 7](w7-envs101)
* [Week 8](w8-envs101)
* [Week 9](w9-envs101)
* [Week 10](w10-envs101)

## Projects

* [Semester Project](project-envs101)
* [Group Project](group-envs101)

## Assignments 

* [Presentation Notes](pres-envs101)
* [Project Proposal](proposal-envs101)
* [Assignment20](assignment20)
* [Podcast1](podcast1-envs101)
* [Podcast2](podcast2-envs101)
* [Assignment21](assignment21-envs)
* [Assignment22](assignment22-envs)
* [class32](class32-envs101)
* [class33](class33-envs101)
* [class34](class34)
* [class35](class35)
* [class36](class36)

## Presentation

[presentation](presentation-envs101)
