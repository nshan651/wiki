# ENVS 101 W1

## Intro to ENVS

* Cyanobacteria mass extinction:
    * Occurred over millions of years
    * Different from the "sixth" age of humans; we have the agency to make choices about how we impact the climate
* The three P's:
    * People:
        * Social variables dealing with community, education, equity, etc.
    * Planet:
        * Environmental variables relating to natural resources
    * Profit:
        * Economic variables dealing with the bottom line and cash flows

## Uncertainty and Science

* Two main reasons for uncertainty in science:
    * Ignorance -- a limited understanding of earth's systems
    * Complexity -- ecosystems can be extremely complex in their parts and interactions
* Science -- is the process that asks and answers questions about the world around us in order to increase knowledge and decrease uncertainty
* The scientific method:
    * Results are subject to review and are constantly being refined and re-evaluated
    * Does not happen in a clear, linear fashion
* The precautionary principle:
    * When there is reasonable evidence that an action or policy may place human health or the environment at risk, one should error on the side of caution rather than preceding
* Sustainability will not come about from science alone:
    * Also requires politics, policy, law, poetry, literature, history, etc.
    * I.e. help from every other discipline

## The 17 Sustainable Development Goals

1. No poverty
2. Zero hunger
3. Good health and well-being
4. Quality education
5. Gender equality
