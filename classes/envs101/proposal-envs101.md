---
title: "ENVS 101: Project Proposal"
author: "Nick Shannon"
date: 2023-03-23
---

# Blog Post

> 1) In a few sentences, summarize what you will be doing for your final project, and what exactly you will turn in.

For my project, I will be writing an online blog post on the consequences of industrial farming methods for both humans and animals. I will add supporting graphics, figures, and images to go along with it.

As far as the length, I'm thinking 3000 words (6 pages?) sounds like a good amount, but I am open to changing that if need be. I think that re-reading Animal Liberation, researching, writing, and formatting the website will easily meet the 10-hour requirement.

> 2) Have you made any changes or adjustment to your original proposal? (This might be longer if you made big changes)

I decided to narrow the focus of my project to the consequences and benefits of modern industrial farming and animal husbandry. I will only be referring to chapter 3 of "Animal Liberation", which goes into great detail on this area. I don't think I will be focusing on the ethical dimension of the book anymore. Although it's really interesting and the main crux of the book, I'd prefer to talk about the environmental aspects of the book rather than the philosophy of animal cruelty. 

As for the format, I'd like to put it on my personal website in the form of a blog post. I think it's a really great medium for sharing and an opportunity to get creative with the presentation. My goal is to make it both entertaining and informative so that it can be enjoyed by a wide audience. 

> 3) What have you done to begin work for your project (hours?)? Have you made any progress or hit any unexpected obstacles?

I started re-reading Animal Liberation, and I am decently far into it. I've also been working on the design portion of the website itself. All told, I've spent about 3 hours on the project so far.

> 4) What is your 'game plan' to put in your 10 hours in working on this project and producing a final product of some type to turn in? How can you pace this so you are not giving yourself more work to do at the end of the semester around finals time?

I'd like to aim to write around 850 words per week, which will put me on pace to finish with about a week-and-a-half to spare. I will also work on wrapping up the visual design component by the end of the week so I can focus soley on writing.

> 5) What have you learned so far?

We talked about some of the more disturbing farming methods that Singer also mentions in class, but one that we didn't discuss is veal cattle farming. Veal cattle aren't tiny baby calves, farmers figured out crafty ways to keep huge 450-500 pound bull calves' meat tender by depriving them of movement and calcium-rich diets.
