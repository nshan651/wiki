---
title: "ENVS 101: Humans and Meat"
author: "Nick Shannon"
date: 2023-03-16
---

## Ecological footprint:

- Currently, how many of each type of animals do humans keep?
    - Chickens: 23 billion
    - Cows: 1.5 billion
    - Pigs: 1 billion
    - Sheep: 1 billion
        - Note: It's kind of unclear whether the video means 1 billion pigs and sheep EACH or in total. Based on other sources, it seems like it should be each, so I'll assume that.
- What percent of the Earth’s farmland is used for livestock?:
    - 83%
- What percent of global freshwater consumption goes to meat and dairy production?
    - 27%
- How many of the calories in feed are lost while being converted into meat? Why?:
    - About 97% of calories are lost in the conversion.
    - Much of the lost calories goes into the growth of the livestock, as well as through heat and the animal's basic metabolic functions
- is this a lot lower than standard energy lost between trophic levels?
    - Yes, the energy lost between trophic levels is much greater than energy lost in the conversion of livestock feed to animal protein. This is due to the process of converting feed into meat products involves multiple trophic levels, each with its own energy loss.
- How many more people could be nourished if we all just ate the stuff we feed to animals?
    - 3.5 billion.
- What percent of total greenhouse gas emissions caused by humans are created by the meat industry:
    - 15%.

## Animal Treatment:

- Globally, how many animals do humans kill to eat every day? How many a year?:
    - 200,000,000 every day 
    - 74,000,000,000 per year
- How is it that dairy cows are able to keep producing milk?:
    - They are bred continuously, then removed from calves.
- Up to what percent of antibiotic usage in the US goes towards livestock?:
    - 80%.
- How do we keep chickens from attacking each other in some close quarters?:
    - They are de-beaked and de-taloned.
- What happens to baby male chickens?:
    - They are gassed and then pulverized.

## What you can do:

- How many meat free days per week is a good number to start with?:
    - 1 day per week.
- Who should you try to buy meat from?:
    - Buy from a trusted producer with a good track record (If financially viable).
- What types of meat are better to order because of their higher feed conversion to meat?:
    - Chicken and pigs.
- How much food does the average American throw out per day?:
    - 1 pound of meat.

## Personal reaction to this video:

- What is your personal reaction to watching this video about production and consumption?:
    - I think factory farming methods are quite shocking and in many cases inhumane, but I am also surprised by how well the meat industry has been able to keep their methods under wraps. Most ordinary consumers either are unaware of these systems or simply choose to ignore them. I'm optimistic that as awareness improves, people will be inclined to make changes to their consumption habits. Even a small change like reducing meat consumption by one day per week makes a huge difference.
