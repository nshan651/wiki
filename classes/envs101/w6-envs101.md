# ENVS 101 W6

## The Importance of Water

- 4 Characteristics of Water:
    - Metabolism
    - High heat capacity
    - Solubility
    - Different states (solid, liquid, gas)
- Water is also an ecosystem service for humans:
    - Provisioning services
    - Regulating services
    - Supporting services
    - Cultural services
- Consumptive use:
    - Ex) generation of hydroelectric; disposal of wastewater
- Non-consumptive use:
    - Ex) Irrigation, cooking
- Power generation requires a **lot** of water
- Domestic water use is among the smallest proportion of water use:
    - Dwarfed by industrial and agricultural
- Transpiration:
    - When plants cause water to evaporate
- Less than 3% of the earth's water is fresh water:
    - Much of it is in ground water and ice

## Watersheds and Water Flow

- Continental Divide:
    - Separation that dictates where drainage flows to (Atlantic or Pacific)
    - Along the Rocky Mountain range
- Groundwater Characteristics:
    - From rainwater that does not evaporate or run off
    - Percolates through rock tot he water table
    - Recharge zone -- area from which water enters water table from surface
    - Discharge zone -- groundwater flows to surface-usually via seep or spring
- Ogallala aquifer

## More on Water

- Combined sewer overflow (CSO):
    - When storm water combines with waste water and overwhelms sewer infrastructure 
- Impairments of fresh water:
    - Pathogens:
        - Usually from sewage discharge and agricultural waste
    - Nutrients
    - Metals
