# ENVS 101 W2

## Evolution & Biodiversity

- Four conditions for Nat. Sel.:
    - Variation in trait within the population
    - Trait is determined by genetics and is heritable
        - Plastic traits don't count. Only refers to traits that can be built up within an animal's lifetime
        - i.e. stronger legs
    - Individuals with certain traits have higher fitness than others
        - **Fitness** is the ability of an organism to survive and reproduce
    - There is some competition for survival and reproduction among the population
- Three types of NS:
    - Directional selection
    - Stabilizing selection
    - Disrupting selection
