---
title: "ENVS 101: NASA Climate Change Facts"
author: "Nick Shannon"
date: 2023-04-04
---

# Evidence of Climate Change

Start by going to the portion of the website on the [Evidence](https://climate.nasa.gov/evidence/) of Climate Change. Read through the page first, and then go back and answer the questions.

- For millennia, what was the amount of atmospheric carbon dioxide that wasn't exceeded (reference the graph)?:
    - 300 ppm
- What level are we at now?:
    - We are currently at ~420 ppm
- How long ago was the abrupt end of the last ice age (in the text)?:
    - 11,700 years ago
- How is the current warming trend different from the previous ones?:
    - The current warming trend is different because it is clearly influenced by human activities
- How long ago was the heat trapping nature of carbon dioxide and other gasses demonstrated?:
    - We have demonstrated their heat-trapping nature since the mid-19th century 
- Who first recognized Earth’s greenhouse effect? :
    - John Tyndall was the first to recognize and prove the greenhouse effect.
- Isn’t it crazy that we’ve known about the greenhouse effect since the 1860’s?  
    - [X] YES.
- What are some of the sources of ancient evidence about how the Earth's temperature is increasing?:
    - Tree rings, ocean sediment, coral reefs, and layers of sedimentary rock
- How many times fast is the current rate of warming happening compared to the average rate of ice-age-recover warming?:
    - 10 times faster
- How many times faster is human activity increasing carbon dioxide than it did from natural sources after the last ice age?:
    - 250 times faster
- List out the nine main sources of evidence that show how our climate is changing:
    - Global temperature is rising
    - The ocean is getting warmer
    - The ice sheets are shrinking    
    - Glaciers are retreating
    - Snow cover is decreasing
    - Sea level is rising
    - Arctic sea ice is declining
    - Extreme events are increasing in frequency
    - Ocean acidification is increasing
What is one thing that strikes you from this ‘evidence of climate change’ page?
    - It is surprising to me that we have known about the greenhouse effect for so long, and yet it seems like we didn't grasp its ramifications until recently. At this point, so much damage has already been done by human-accelerated climate change.

# Causes of Climate Change

Go to the part of the website about [Causes](https://climate.nasa.gov/causes/) of Climate Change

- True or False: The greenhouse effect is unnatural and is dangerous to life on planet earth.:
    - False, it's essential to life on earth.
- What are the four molecularly depicted gasses that contribute to the greenhouse effect?:
    - Carbon dioxide 
    - Methane
    - Nitrous oxide
    - Chlorofluorocarbons (CFCs)
- Which one of these is produced by soil cultivation?:
    - Nitrous oxide
Which of these is the most abundant greenhouse gas?:
    - Of the four listed, carbon dioxide (although water vapor generally is more common)
- Which one of these can be caused by ruminant digestion?:
    - Methane
- Which one of these is produced by volcanic eruptions?:
    - Carbon dioxide
- What is the fifth gas listed that contributes to the Greenhouse Effect?:
    - Water vapor
- Has the Sun’s solar variability played a role in the Earth’s past climate changes?:
    - Yes, the sun's solar variability has had a hand in Earth's previous climate change events, such as the Little Ice Age
- What are the three lines of evidence that our current global warming cannot be explained by solar variability?:
    - Since 1750, the average amount of energy from the Sun either remained constant or increased slightly
    - If a more active Sun did indeed cause the warming, scientists would expect warmer temperatures in all layers of the atmosphere, not just in the upper atmosphere
    - Climate models including solar irradiance changes can't reproduce the observed temperature trend over the past century without including the rise in greenhouse gases
- What is one thing that strikes you from this ‘causes of climate change’ page?:
    - One thing that is striking is the feedback loop of increased water vapor in the atmosphere, due to the greenhouse effect. The warmer the planet gets, the more water vapor is present. It is not a direct cause of climate change, its indirect effects are interesting.

# Vital Signs

Go the the [Vital](https://climate.nasa.gov/vital-signs/carbon-dioxide/) Signs page and start with Carbon Dioxide:

- What is the latest CO2 measurement in parts per million?:
    - 419 ppm (as of Feb. 2023)
- What was the CO2 measurement 10 years ago in April of 2013?:
    - 395.79 ppm
- What is a temperature anomaly?:
    - It is how much warmer or colder than the long-term average the temp over a unit of time is
- Using the graph on that page, what was the average temperature anomaly in 1960?:
    - -0.03 C
- What was the average temperature anomaly in 2021?:
    - 0.85 C
- Go to Arctic Sea Ice extent tab- what year had the lowest sea ice extent in the satellite record?:
    - 2012 did, with 3.39 million sq km
- Go to Ice Sheets- what is the average rate in decline of the amount of Greenland ice sheet mass per year?:
    - The average rate of decline for Greenland mass variation sits at 273 billion metric tons per year since 2003
- Go to Sea Level- according to the webpage what are the two factors related to global warming that ultimately lead to sea level rise?:
    - The added water from melting ice sheets and glaciers
    - The expansion of seawater as it warms
- Go to Ocean Heat Content- according to the webpage (under ‘why ocean heat matters’) what are five effects of ocean warming?:
    - Sea level rising due to thermal expansion
    - Coral bleaching
    - Accelerated melting of major ice sheets
    - Intensified hurricanes
    - Changes in ocean health and biochemistry

# Effects of Climate Change

Go to the [Effects](https://climate.nasa.gov/effects/) of Climate Change page

- What is meant by the line “Global climate change is not a future problem”?:
    - Climate change is already having pervasive, widespread effects on the environment
- What are some of the changes happening faster than scientists had previously assessed?:
    - Droughts, wildfires, and extreme rainfall
- Type out the 9 future effects of climate change in the United States (be sure to click on and read through each):
    - Sea level will rise 1-8 feet by 2100
    - Climate changes will continue through this century and beyond
    - Hurricanes will become stronger and more intense
    - More droughts and heat waves
    - Longer wildfire season
    - Changes in precipitation patterns
    - Frost-free season and growing season will lengthen
    - Global temperatures will continue to rise
    - Arctic is very likely to become ice-free
- Pick out your home region from the list from the U.S. Regional Effects list. Notice that these are effects already seen and recorded, not just predictions.  (Northeast; Northwest; Southwest, etc):
    - Midwest
    - There will be extreme heat, heavy downpours, and flooding that will affect infrastructure, health, agriculture, forestry, transportation, air and water quality, and more
    - Climate change will also worsen a range of risks to the Great Lakes

# EPA's Climate Change Impacts by State

Go to the [EPA’s page](https://19january2017snapshot.epa.gov/climate-impacts/climate-change-impacts-state_.html)on Climate Change by State and click on your home state (use IL if not from the US): 
    - Minnesota
- How much has the average temperature of your state changed in the past century?:
    - It has warmed one to three degrees (F)
- What is one thing that really strikes you from this fact sheet?:
    - The Lake and River flooding is particularly striking. As the land of 10,000 lakes (more than that, technically), there are a lot of towns and cities that are vulnerable to flooding. For example, my home town is right on the Mississippi, where flooding is becoming increasingly more severe.

# Scientific Consensus

Go to the [Scientific Consensus](https://climate.nasa.gov/scientific-consensus/) page:

- According to the graph at the top of the page, what are the names of the four different data sets used to make this temperature change graph?:
    - NASA Goddard Institute for Space Studies
    - Hadley Center/Climate Research Unit
    - NOAA National Center for Environmental Information
    - Berkeley Earth
- In your opinion, would you say the four sets of data show a convincing trend?:
    - Yes, absolutely. The four datasets are unambiguously in agreement
- Skim through the statement on climate change from the 18 scientific associations. Does it seem like there is scientific consensus that human caused climate change is happening?:
    - Yes, it is nearly universal consensus. AAAS reports that about 97% of climate scientists agree that climate change is human-accelerated
- What do you think are reasons people are still doubtful or skeptical about this?:
    - There are a number of reasons for skepticism. There is a large proliferation of mis- and disinformation online, as more and more people find themselves stuck in echo chambers and epistemic bubbles.

# FAQ’s

Go to the “[Frequently Asked Questions](https://climate.nasa.gov/faq/)” page. Read the summary and detailed answer and then summarize in your own words…

- What’s the difference between global warming and climate change?:
    - Global warming is the long-term temperature increase of Earth, whereas climate change refers to a broader range of changes in addition to temperature, such as changes to sea levels, glaciers, ice melt, and other ecosystem effects
- What’s the difference between weather and climate?:
    - Weather refers to local changes in our immediate environment, on a short timescale
    - Climate refers to long-term averages of weather patterns
- Under “Is it too late to prevent climate change” (detailed answer)- What is the difference between ‘mitigation’ and ‘adaptation’?:
    - Mitigation involves reducing the amount of greenhouse gases entering the atmosphere
    - Adaptation is learning to live with and adapt to the changes to the climate that have already occurred
 

# Brief Reflection

- After navigating the NASA Climate Change website and filling out this worksheet, what are your current feelings regarding the significance of Climate Change in the planet and humanity’s future?:
    - Climate change is without a doubt the greatest threat humanity has ever faced. I think if we are going to slow or even prevent future catastrophe, there will need to be a concerted effort between Nations. There also needs to be a lot of co-operation and awareness at the individual level.
