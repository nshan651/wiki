---
title: "ENVS 101: Is It Too Late To Stop Climate Change?"
author: "Nick Shannon"
date: 2023-03-30
---

- https://www.youtube.com/watch?v=wbR-5mHI6bo&t=478s

# Is it too late to stop Climate Change?

- Of the four factors that can explain our collective CO2 emissions, which two explain why emissions are still rising?
    1) Higher population growth.
    2) Economic growth.

- Which two explain how we can stop that?
    1) Energy intensity.
    2) Emissions per Energy unit produced.

- According to the UN- what year will the human population level off, and how many humans will there be?:
    - According to the UN, the global population will level off at about 11 billion in 2100.

- A programmer in the US has the equivalent CO2 footprint of how many farmers in Uganda?:
    - 50 farmers in Uganda.

- List the three reasons that increasing energy efficiency alone will not be enough to 
    1) Direct rebound effects - The phenomenon where the more efficient something becomes (like energy), the more it will be used.
    2) Indirect rebound effects - When a person saves money in one area because of increased efficiency, they tend to spend that money elsewhere.
    3) Finally, when something is optimized for efficiency, it becomes increasingly more difficult and expensive to make that thing more efficient.

- The video says we need to use the real leverage we have today with today’s technology. It then lists 5 things we can do extremely quickly. List those 5:
    1) We can leave nuclear power plants online longer.
    2) We can cut subsidies to the fossil fuel industry and funnel them into renewables.
    3) We can price carbon emissions harshly, and increase prices each year.
    4) We can enforce strict standards for energy efficiency.
    5) We can focus on inventing new and better technology.

- The video states: “Neither innovation nor the alternatives we’re using today alone can solve rapid climate change.” What does the video say “could do it”?:
    - Innovation and the decisive move away from fossil fuels where possible today could solve the climate change problem.

- What is one thing new you learned from watching this video that you will take with you?:
    - I thought the part about one US programmer using the same emissions as 50 Ugandan farmers was interesting. As a programmer myself, energy consumption in tech is something I have thought a lot about. Specifically, there seems to be a huge problem with high energy consumption in the training of artificial intelligence models and in crypto currency mining. I think that going forward, this will be a huge concern that must be addressed.
