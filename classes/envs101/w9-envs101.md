# ENVS 101 W9

## Energy

- Goals:
    - Transition _away_ from fossil fuels
    - Work toward electrifying as much as possible
    - Nuclear will most likely play a role
- Transition to clean energy:
    - Fossil fuel breakdown:
        - 33% oil
        - 27% coal
        - 24% nat. gas
    - Clean energy:
        - 16% of total energy production
- Why electrify:
    - Electrical tech has the capability to utilize clean energy
- The role of nuclear:
    - Non-renewable, but negligible greenhouse gas emissions
- Limitations of renewables:
    - Consistency
    - Storage
