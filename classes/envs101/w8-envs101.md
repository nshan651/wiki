# ENVS 101 W8

## Factory Farming Methods

### Statistics

- 83% of the Earth's farmland is used for livestock
- 27% of global freshwater goes into meat and dairy progression
- 97% of calories in feed are lost while being converted into meat
- 3.5 billion additional people could be nourished if we ate the nutrients we feed to animals

### Animal Treatment

- 200,000,000 animals killed to eat every day
- Dairy calves are bred continuously and removed from calves
- 80% of antibiotic usage in the US goes towards livestock
- De-beaking prevents chickens from pecking each other

## GMOs

- Trophic vs conversion efficiency:
    - Trophic efficiency:
        - Biomass of the organisms (i.e. cows)
    - Conversion:
        - Biomass of derived products (i.e. steak)
- CAFOs:
    - Concentrated animal feeding operations
    - Large industrial pig and cattle farms
- Huge problem with CAFOs -- waste management:
    - Manure rich in nutrients
    - Pathogens
    - Methane production
- Environmental impacts:
    - Land use
    - Transmission of disease
    - Impact to human diets
- Top 6 most common crops:
    - Corn, wheat, soybeans, potatoes, barley
- Maximizing resources often tends to minimize genetic diversity
- Many species have become extremely productive from breeding, hybridization, and clone
- Seed banks exist to preserve older strains for future analysis and use

### Three Main Types of GMOs

1) Genetically modified organisms to produce their own pesticide
1) Those that are resistant to common pesticides (round-up ready)
1) Added features, such as Beta-carotene inserted golden rice
