---
title: "ENVS 101: Hydrogen Power"
author: "Nick Shannon"
date: 2023-03-22
---

# Hydrogen Power 

- Doc: https://docs.google.com/document/d/1As20zpwQuK522zR-aluZJ4iE99BV5AhW4KePhzuEzOA/edit
- Slides: https://docs.google.com/presentation/d/1FHqw1oFnVZh5OxKVIVD8tQzpq-fTUAj3Ii77wNTy9gA/edit#slide=id.p
- Group Hydrogen (not in textbook): Nick Shannon, Virginia Lambert, Nora Lee Reinhardt, Annelise Huang

- The purpose of this project is for you and your group to do a ‘deep dive’ into one form of energy (either renewable or non-renewable). In doing so, you will educate yourself deeply into the details of the technology around this energy source, the pros and cons of it, the issues of fairness and justice around your energy type for meeting the needs of the future, and a ‘case study’ or example of where your energy type is being used. You will be educating yourself in part to be able to then educate your classmates on this topic. You will use a slideshow to present to the class.
- Each group will provide a two page Study Guide to provide to the class. 
- Each member should prepare the most important information from their portion of the presentation, to be added to the others’ in your group and formatted into a single page study guide for the class. You do not want a study guide that is too cluttered, so part of the strategy is thinking about the most important take-home points from your portion of the research and presentation. These will be due to the day of your presentations, and should be emailed to Br Mackey in electronic, PDF form.


## Step 1: 
 
- Student 1 (Nick Shannon) - How is this energy source being produced? (sources and production; 2.5 minutes):

### Where does the energy source come from?

- Hydrogen is a clean energy source that only produces water
- Hydrogen is an energy carrier that can be used to store and transport energy produced from other sources

#### Production Methods


### Where is the energy source most abundant in the world?
### How is it harvested?
### Basic technology involved
### How efficient is this energy (energy conversion efficiency)
### What is it mostly used for (electricity generation; heating/cooling, ect)? 

### Sources

- [eia.gov](https://www.eia.gov/energyexplained/hydrogen/production-of-hydrogen.php)
- [energy.gov](https://www.energy.gov/eere/fuelcells/hydrogen-fuel-basics)
- [wikipedia](https://en.wikipedia.org/wiki/Hydrogen_production)

## Step 2

- Turn your study guide section into slides for the presentation:
    - Present the answers above, but include photos/graphics, examples, etc. Start with the textbook but then expand to other resources to get a fuller picture of the answers. You will be the ‘expert’ on your section, and it will be your job to teach your fellow students. You are teaching and helping your fellow students to understand, not just reading off information.
- *Be sure to include an introduction and conclusion, and to make sure there is a smooth transition throughout the presentation and presenters.
