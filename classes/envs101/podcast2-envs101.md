---
title: "ENVS 101: Driving a Transport Revolution"
author: "Nick Shannon"
date: 2023-03-28
---

https://omny.fm/shows/the-clean-energy-revolution/driving-a-transport-revolution

# Episode 2: Driving a Transport Revolution

- UK’s Parliament has made it law to get to Carbon neutrality by what year?:
    - 2050
In 2018 what % of UK’s domestic greenhouse gas emissions were contributed by transport? (min 2):
    - 28%
What was second to that, at what percent?:
    - Energy supply, 23%
How long does it take to charge your typical family electric car right now? (6 min 30 sec):
    - 40 - 60 minutes for a full charge
- What two things are inextricably linked? (min 8):
    - The health of the people and the planet
- Where there are illegal levels of air pollution, where does up to 80% of the pollution come from? (min 8:30):
    - Road transport
- What is just one way that the effects of air pollution are experienced unequally by people?:
    - People from lower household incomes tend to be more exposed to the effects of air pollution while simultaneously contributing less to it 
- Why are ports great places for charging? (14:15):
    - Ports have high power electricity on tap
- What is slightly different about maritime and sea shipping? (16:45):
    - The international dimension. The regulation surrounding maritime is governed by an international body, rather than on the level of a single country.
- Why is it in the ship owner’s best interest to make transport more efficient? (18 min):
    - It saves them money
- What percent of greenhouse gas emissions in the US come from transport? (19:45):
    - 40% of greenhouse gas emissions
- How much more air pollution are people of color in the NE US exposed to than the white residents?:
    - About 30% more air pollution
- What is the key barrier to having an EV (electric vehicle) in both the US and UK? (24:15):
    - Cost
- What are the three challenges of sustainability that Rolls Royce focuses on? (26:22):
    - Jet engine, sustainable aviation fuels, and disruptive tech such as electrification
- What was the change in daily cost of commuting to London for Graeme Cooper when he switched from taking the train to using an electric vehicle (early Nissan Leaf)? (31:45):
    - 36 pounds/day to less than 2 pounds/day electricity
- How are green vehicles getting greener? (33:15ish):
    - More electricity is made from clean sources
- What are two examples of action we can take to help make the energy transition happen? (35:45):
    - First, travel less, or find alternative way of traveling, such as by bike or on foot
    - Second, if you have to travel, consider public transport or electric vehicles if possible
- What is one thing new you learned from listening to this podcast that you will take with you?:
    - This podcast made me realize just how large of a role vehicles play in emissions. Additionally, this demonstrates that the switch to alternative fuel sources and the electrification of vehicles should be a central focus in the fight against climate change.
