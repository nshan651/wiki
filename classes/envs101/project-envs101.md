---
title: "ENVS 101: Semester Project"
author: "Nick Shannon"
date: 2023-03-23
---

# Notes

## Materials

- Here are some potential materials I can use as sources:
    - "Animal Liberation" by Peter Singer - Main text
    - "The Omnivore's Dilemma" by Michael Pollan - This nonfiction book explores the food chain from farm to plate and examines the environmental impact of industrial farming practices.
    - "Animal Factory" by David Kirby - This nonfiction book investigates the environmental and social consequences of industrial animal agriculture, including issues such as water pollution, greenhouse gas emissions, and public health concerns.
    - "Eating Animals" by Jonathan Safran Foer - This nonfiction book explores the ethics of eating meat and the environmental impact of industrial farming, including the depletion of natural resources and the destruction of habitats.
    - "Fast Food Nation" by Eric Schlosser - This nonfiction book exposes the environmental and social costs of the fast food industry, including the exploitation of workers and the environmental destruction caused by industrial agriculture.
    - "The Jungle" by Upton Sinclair - This classic novel exposes the harsh working conditions and unsanitary practices of the meatpacking industry in the early 1900s, highlighting the need for reform and regulation.
    - "Okja" - This film by Bong Joon-ho tells the story of a young girl who befriends a genetically engineered super pig and becomes a part of a campaign to save her from being slaughtered for meat.
    - "In the Black Mill" Michael Chabon
    - "Chicken Run"
