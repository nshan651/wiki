# ENVS 101 W4

## Nature Deficit Disorder 

- Nature Deficit Disorder:
    - A lack of exposure to nature has negative effects on our mental and physical health 

## Biodiversity

- Outbreeding:
    - Mating with unrelated individuals
    - Maximizes diversity
- Inbreeding:
    - Increases the chance for genetic disease and reproductive failure
- Species richness:
    - Number of kinds of Species
- Species evenness:
    - Relative parity in number of the populations of different species
- Sampling effect:
    - Larger groups of species use resources more efficiently than individuals

## Biodiversity Conservation

- Complementarity:
    - How does it come about? 
    - Different creatures find different niches
- Why biodiversity matters:
    - Diverse ecosystems have many species with varying tolerance
    - [ ] Enhances ecosystem ability to survive disturbance or change
- Drought resistance increases as species **richness** increases

## Biodiversity Loss Trends

- East and West Coasts experience the most biodiversity loss

## Causes

- Habitat loss and degradation:
    - Single greatest threat to biodiversity loss
    - 95% of North America's deciduous forests have been logged
    - Huge effects when tropical forests are cut
    - Marine ecosystems -- people like to live near coasts, which has damaged much of the shallow coast ecosystem

## Quiz Notes


0:02
More than 180 invasive species have entered the Great Lakes.
0:05
Zebra mussels and round goby arrived in the lakes
0:08
more than 30 years ago,
0:09
and the lakes are still adapting to the change.
0:12
- [Narrator] Three of the best known invasive species
0:14
in the Great Lakes arrived from the same place.
0:17
Zebra mussels, quagga mussels and round gobies all came here
0:22
from halfway across the world,
0:23
carried in ballast water by ocean-going freighters.
0:27
- We kinda got a transplanted ecosystem
0:30
from the Black and Caspian Seas.
0:32
And actually around 1988,
0:34
the first zebra mussels were picked up
0:36
in the St. Clair River.
0:38
- [Narrator] Zebra mussels quickly multiplied
0:40
and began causing problems by clogging intakes
0:43
at water treatment plants and power plants.
0:46
It was anybody's guess how the ecosystem would respond.
0:50
- [Jude] They come into a system, explode.
0:53
And then you have all the different forces
0:55
of predators, parasites, limiting food supplies,
0:59
they all come to act on the population.
1:03
- [Narrator] But even before native species could adapt
1:06
to the presence of zebra mussels,
1:08
a new aquatic invader showed up and changed things again,
1:11
the quagga mussel.
1:14
Harvey Bootsma is a professor
1:15
at the University of Wisconsin,
1:17
Milwaukee School of Freshwater Sciences.
1:20
- Initially, the zebra mussels really took off
1:23
but they were confined to the edges of the lakes
1:25
because they live on hard substrate.
1:27
But a little bit later, the quagga mussel came in.
1:30
And the quagga mussel can live in colder water
1:33
and it can live on soft bottom,
1:35
so it spread much more widely than the zebra mussels did.
1:40
And it replaced the zebra mussels.
1:42
- [Narrator] Bootsma and a team of researchers
1:44
have discovered something surprising
1:46
about mussels in Lake Michigan.
1:49
- We wanted to see if there was any potential
1:51
for controlling mussels at least at a small scale
1:55
in Lake Michigan.
1:57
So in 2016, with support from the National Park Service
2:01
in the Great Lakes Restoration Initiative,
2:04
we decided to start a mussel removal project.
2:08
- [Narrator] The research was done on a rocky reef
2:11
in Good Harbor Bay between Leland, Michigan
2:14
and Sleeping Bear National Lakeshore.
2:17
There, a team of volunteer divers spent weeks
2:20
scraping mussels off of rocks.
2:22
- The number of volunteer divers working with us,
2:26
spent several weeks that summer just going down diving
2:29
and scraping the mussels off of rocks
2:31
trying to create significantly large area
2:34
that was mussel free.
2:36
And then after that, the objective was to monitor that
2:39
to see what happened to that patch of rock.
2:42
- [Narrator] Surprisingly, four years later,
2:44
there are still almost no mussels in the area.
2:48
The research team suspected round gobies
2:50
might be responsible.
2:52
- [Harvey] We think what's happening is the mussel larva
2:55
or what are called veligers,
2:57
settle on surfaces
2:58
where they grow into hard shelled mussels.
3:02
If mussels try to recolonize these rocks,
3:04
the gobies are eating them before they can grow
3:07
to any significant size.
3:09
- [Narrator] To test that hypothesis,
3:10
the researchers put in devices called Hester Dendi Traps,
3:14
which have spaces in them that are big enough for mussels
3:17
but too small for gobies.
3:19
- [Narrator] And sure enough,
3:21
we're seeing mussels in those traps now.
3:23
So it does seem like the gobies are instrumental
3:26
in keeping the mussels from recolonizing the rocks
3:29
that we cleared some time ago.
3:31
- [Narrator] And taking the mussels away,
3:32
had other effects too.
3:34
- Where we remove the mussels,
3:36
we've also seen a huge drop in other invertebrates
3:38
because they just don't have any where to hide anymore.
3:42
So arthropods are a big one.
3:44
Another one is isopods.
3:46
Fish love to eat them if they can get at them.
3:49
There's actually more of them now than there used to be
3:51
because the mussel beds provide lots of food for them
3:55
and they provide hiding places for them.
3:57
- [Narrator] But once the mussel beds were gone,
3:59
the fish could gobble up on the invertebrates,
4:01
while the gobies munched on mussel larva.
4:04
- Ironically, an invasive species, the round goby,
4:08
it may be helping to control
4:10
another invasive species, the quagga mussel,
4:13
by feeding on them.
4:14
- [Narrator] Round gobies come from the same place
4:16
as zebra mussels.
4:18
And arrived in the Great Lakes around at the same time.
4:21
Gobies can displace native fish
4:23
partly because of their spawning habits.
4:25
- [Jude] One of their successes
4:27
is that males guard the nest.
4:29
As a result of that, they get a high fertility rate.
4:32
And females can spawn I think every two weeks.
4:36
And so they start in May
4:38
and they spawn all the way to September.
4:40
Most fish, they spawn in the spring.
4:42
If they get a bad spring,
4:43
they lose the whole larval fish cohort
4:46
for that particular time, but not round gobies.
4:49
If they're spawning for four months,
4:51
one of those times, they're gonna hit the perfect storm
4:54
of good conditions for their survival,
4:56
and produce a lot of larval round gobies.
5:00
And then the second big reason is,
5:03
quagga and zebra mussels
5:05
and that's a tremendously huge food supply for them.
5:11
- [Narrator] For years, native species ignored the gobies
5:13
but things have changed.
5:14
Today, natives across the basin are adjusting
5:17
to their presence in surprising ways,
5:20
and around goby have become a daily special
5:22
on the Great Lakes menu.
5:24
- Once they became abundant, fish walleyes, northern pike
5:28
and smallmouth bass.
5:30
Smallmouth bass particularly really zeroed in on them.
5:36
They're well known for eating crayfish.
5:38
Their diet's really full of crayfish
5:40
and other benthic fish like darters.
5:43
So the gobies were a natural for them.
5:46
It took a while but they figured out how to catch 'em.
5:49
- [Narrator] Native species like sculpin hide from bass
5:52
by freezing and blending in.
5:53
So that's what the bass are used to.
5:56
But gobies have a different way to avoid being eaten,
5:59
and it took the bass by surprise.
6:01
- [Jude] Because they have this strange behavior
6:04
that when you scare 'em,
6:06
they just kinda shoot out in one direction or another.
6:10
That's one of the interesting things,
6:12
that there was definitely a learning curve
6:14
for these predators in how to catch these round gobies,
6:19
and they learned how to do it.
6:20
- [Narrator] And smallmouth bass
6:22
aren't the only native predator to start feeding on gobies.
6:24
- The other one that has recently been found
6:28
to eat a lot of round gobies are the lake trout.
6:31
We've been seeing some really big changes in their diets
6:34
as they're shifting from fish like mottled sculpins
6:37
and the alewives to round gobies.
6:40
- Another interesting story is the Lake Erie water snake,
6:43
which previously was endangered.
6:45
And they've made a real comeback
6:47
because they love to eat gobies as well.
6:51
So those are really interesting questions
6:53
as we're going forward.
6:55
What's the new Lake Michigan going to look like?
6:57
And are there fish that are able
7:00
to actually take advantage of this new situation?
7:03
- [Narrator] The story of the mussel and goby invasion
7:05
of the Great Lakes is still unfolding,
7:07
but the impact has already been huge.
7:10
The mussels have filtered the lakes,
7:12
removing algae and absorbing industrial waste,
7:15
making the waters clearer.
7:17
- [Jude] They're shifting back
7:18
to more of the state they were
7:20
before man interfered and changed everything.
7:25
And so we're certainly getting close to that state.
7:30
- [Narrator] What it all means for the lakes
7:32
is still unknown.
7:34
- [Harvey] Lake Michigan is like a moving target right now.
7:36
We're trying to understand it.
7:38
But the way the lake works
7:39
is changing while we're trying to understand
7:41
how the lake works.
