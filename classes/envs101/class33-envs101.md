---
title: "ENVS 101: Class 33 Assignment"
author: "Nick Shannon"
date: 2023-04-18
---

# In-Class Assignment

- Discuss with your partner what has struck you from the first half of this video. Write down your answer below:
    - One thing that really struck me was how many emissions are produced by building/maintaining roads, especially with respect to building cars. Electric vehicles are brought up a lot in the conversation around a sustainable future, but the issue of roads is a larger, even more difficult problem that needs more attention.

- After watching the second half of the video (solutions vs expenses; what can YOU actually do?), discuss with your partner what struck you and write your answers below.:
    - It was a little surprising to me when I first learned about the efforts of big oil and others to shift the blame from large corporations onto individual consumers, even though a small minority of wealthy corporations and individuals make up the majority of carbon emissions. The unfortunate reality is that our own personal emissions matter very little in the grand scheme of global greenhouse emissions. However, it is important to align our behaviors with our values, because it creates strong incentives for others around us to follow suite. We need to have unified alignment so that our leaders can implement effective strategies on behalf of the majority.
