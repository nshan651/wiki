# ENVS 101 W3

## Survivorship Curve

- Types I, II, and III survivorship:
    - High survivorship over lifetime, equal survivorship over lifetime, and low survivorship over lifetime
    - Ex.) 
        - Humans(T1)
        - Maple trees, sea turtles (T3)
        - Bald eagle(T2)
    - Type 2 is a relatively equal survivorship across lifetime

## Population Biology

- How do population sizes grow?:
    - Population sizes often increase exponentially as a function of the size of the population so that the larger the population, the larger the degree of increase
    - Individuals grow by addition, populations grow multiplicatively
- How are population sizes limited?:
    - Environmental resistance slows down growth
    - Once a population's growth levels off, it is said to have reached its **carrying capacity**
- Carrying capacity exhibits a logistic growth curve
- $EnvironmentalResistance = ExponentialCurve - LogisticCurve$
- A third possibility: carrying capacity is reached too quickly and outstrips resources, thus dramatically decreasing the population
- $Population_{growth} = (birth_r + immigration_r) - (death_r + emmigration_r)$

## Periods of Human Population Growth

- Three periods:
    - Pre-agricultural period
    - Agricultural period
    - Industrial period
- Population is increasing due to a *decrease* in the death rate
- Factors of population:
    - Immigration/emmigration
    - Birth rate/death rate

## History in US

1. Pre-transition:
    - birth/death rates mostly the same
2. Transitional stage
    - Lowered death rate, rapid population growth
3. Fertility transition:
    - Birth rates decline, population growth slows
4. Stability transition:
    - Low birth and death rates, population growth very slow

## Ecology

- Competition for resources:
    - Intraspecific -- between species
    - Interspecific -- within a species
- Exploitation vs interference competition:
    - Exploits another species without directly interfering
    - Interference competition -- directly competing
- Competitive exclusion principle:
    - If two species are competing for resources, one can drive the other to extinction
    - No two organisms can occupy the same niche
- Niches:
    - Could be space, time, food specialization
    - Kind of like how capital markets function (efficient market hypothesis??)
- Example of coexisting within the same niche:
    - Warblers (~12 different species)
