# COMP 371 W1

## Intro to Scala

* Multiversal equality:
    * Only allows values of the same type to be compared
    * Promotes type safety
* Explicit nulls:
    * Must specify that something *can* be null

## Map Example

```
val words = Seq("hello", "hi") // Sequence of strings
words.map(x => x.length) // Maps each string to its length
```
 
* Example of higher-order functions

## Dev Tools

* SDK Man - tool for managing multiple SDKs
