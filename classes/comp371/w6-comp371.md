# COMP 371 W6

* Map every word in the queue with the length of each word:
    * Remove the word that gets dequed
* No need to re-call the map every time the queue slides over

## Project-2a

#### EC

* Make ./topwords have defaults 10, 6, and 1000
    * Should be positional
    * Will use defaults if no args are passed
    * Look into a couple scala libraries for arg parsing

### Notes

* Watch C6a and C6b on panopto for more
* Scala version of pythonic iteration:

```scala
for line <- scala.io.something() do
```

* Problems with simple/
    * Input is directly tied to the output
* In queue example, each update is a queue
* In ours, is either a frequency map or the sorted representation of the words
* Only modular versions are testable
* `extends` and `with`:
    * `extends` is first keyword, with is `second`
* Running occurs in the LinesCountImperativeModular
* NOTE: main method must occur in an object

## Functional Programming

* Seq is an immutable version of List
