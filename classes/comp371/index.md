# COMP 371 Index

### Zoom Info

Join Zoom Meeting
https://luc.zoom.us/j/85443596061?pwd=SVF4dDcrVmt6SjlJVjFQNmRoZytrZz09

Meeting ID: 854 4359 6061
Passcode: 965126

### Textbooks

* Scala for the Impatient, 2nd Edition
* Programming Scala, 3rd Edition
* Scala Cookbook, 2nd Edition
* Engineering a Compiler, 2nd Edition

## Notes

[Week 1](w1-comp371)
[Week 2](w2-comp371) 
[Week 3](w3-comp371) 
[Week 6](w6-comp371) 
[Week 12](w12-comp371)
