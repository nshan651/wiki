# COMP 371 W3

* Differences between Java and Scala with OOP:
    * Case classes
    * Pattern matching 
* Structural recursion - defining a type in terms of itself, usually involves aggregation and variation
* Scala has type inference
* `List("hello", "world", "hi", "howdy").map(s => s.length)`:
    * Higher order function that maps a series of values on top of others
