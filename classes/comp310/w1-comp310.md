# COMP 310 W1

## Basics of Loading Into Memory

* Memory addresses and registers
* Fetch -> Decode -> Execute -> Repeat
* System BIOS:
    * Persistent memory holding important OS info
* System layer:

| OS (Located at `100000`) |
|----------------------------|
| Bootloader                 |
| MBR (Or GPT)               |
| Bios (Or UEFI)             |
|                            |
|                            |

* Grabs data from hard disc
* Hard disc is a linear array of sectors:
    * Each sector is a block of 512 bytes
* First sector is the Master Boot Record (MBR)
* MBR on the second sector loads the bootloader, which in turn runs the OS
* The original stack is in 16-bit mode, which eventually transitions to the 64-bit (or 32-bit) OS
* Hexcode to communicate back to BIOS from MBR:
    * `0x7C00`
    * Useful to verify that the MBR is valid (and not a virus)
* MBR is basically a raw memory image
* Linux format is ELF (Executable Liner File)
* ELF is a data structure that's part of a file
* Has a header with info about where to find code, variables, and addresses
* Also has a data section
* Data contains code and variables
* Variables are further separated into initialized and uninitialized:
    * Uninitialized variables don't need an address pointing to them (yet)
    * Constant initialized vars go into *RO Data* (read-only)
* Bootloader is capable of reading ELF files later on in the disk
* OS (ELF) file is usually stored later in memory
* BIOS and MBR are both encoded as raw binary; ELF is the first piece of actually intelligent code
* OS is located at `100000`

## The Bootloader

* 8086 ASM
    * Has registers AX, BX, CX, DX, S1, and D1
* printf in 8086
```
MOV S1, STR
INT 0x13 # Interrupt 16 (prints a character to the screen)
HLT
```
