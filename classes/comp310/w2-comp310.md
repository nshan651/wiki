# COMP 310 W2

## Malloc

* `malloc`
    * Short for "memory allocate"
    * Allocates space on disc for an object
* The heap:
    * A block of memory used to satisfy malloc requests
    * Only malloc is allowed access to the heap
* Example:

```c
void main() {
    int size = getSizeOnDisc();
    char xarr = malloc(size); // Starts at 0xA0000

    // Some more code...
    char xarr2 = malloc(64) // 0xA1000
    
    // More stuff...
    char xarr3 = malloc(1024) // 0xA10F0
    
    free(xarr) // Frees up space at array xarr
    free(xarr2) 
    free(xarr3)
}
```

* Malloc implementation
```c
char heap[1024x1024];
char* counter = heap;

// Linked list representation of the heap
struct block {
   struct block* next; 
   struct block* prev; 
   int size;
};

// Pointer to the first block of the heap
struct block* start = heap;
start -> next = NULL;
start -> size = 1024x1024;

// If malloc request < size of heap - size of block metadata (12 bytes), then reserve that space in memory

// Put address on the heap at the next available space
int malloc(int size) {
    counter+=size;
    return counter-size;
}

int free() {
}
```
