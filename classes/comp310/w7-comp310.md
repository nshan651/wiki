# COMP 310 W7

## Writing a Filesystem

* Tree-like structure
* 512 disc sectors further separated into *blocks*
    * *fat32* filesystem calls them clusters...
* Each block is 4k long
* Most blocks are reserved for actual data storage
* Blocks are subdivided into sectors
* Files spanning multiple blocks:
    * It takes "seek time" for the read head to read each block
    * Will come at a cost of speed
    * Problematic for magnetic discs
    * Less so for flash memory, but still slows things down

## Inodes

* What is an inode?:
    * FS data structure
    * A low-level filename
    * Pointer from where you find it
* Stores a list of blocks in the filesystem showing where each file lives
* One inode per file, but a file can have multiple inodes depending on size
* An inode has a list of 16 blocks (128 / 8)
* Inode structure (2 bytes)

| Inode        |
| ------------ |
| Mode         |
| VID          |
| Size         |
| Time         |
| Ctime        |
| Mtime        |
| Block Array  |

* Disk fragmentation:
    * When memory blocks for programs are stored in a non-contiguous way
    * Means that disk will have to jump around to read blocks
* Data block bitmap:
    * Shows if block is in use or not 
* Note - no file name in the inode:
    * Only contains low-level info

## The Superblock

* Superblock:
    * 4kb in size
    * Stores # of inodes, # of data blocks
    * Stores a list of child directories
* Inode table
    * Stores the name and the inode number
* Inodes are associated with directories **and** files
* Data for a directory is...a list of inode data about other files and directories

## Partitions

* Different programs to install:
    1. Installed programs
    2. OS programs
    3. Docs
    4. Recovery Info
* Filesystem does not necessarily need to go *immediately* at the beginning of the disk:
    * As long as it is partitioned properly

## Disk Ordering

* First 2048 bytes

## Finding "/bin/bash"

1. Read superblock (4kb block):
    * Look for table in the superblock for list of subdirectories
    * Find the required inode number
2. Read inode table from the disk:
    * Inode table takes up 2 blocks
3. DIR: read data block for the inode
    * For inode -> #SB + #Inode Bmap Blocks + # Data Bmap Blocks + # Inode tbl blocks
* Problem:
    * If OS is mid-write, you can potentially lose the inode and thus lose the file

## Journaling

* First writes the inode locations in the date block table so that you can get to and from data
* You can use the journal to replay the transaction and fix the consistency

## Optimizations

* Load the inode table into memory once so that it is more-easily accessible

## Caching

* Maintain disk contents in cache
* Take data lazily out of RAM cache and write to disk as it is able
