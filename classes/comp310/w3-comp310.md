# COMP 310 W2


## OS Repo Ins-and-Outs

* `os-name` for hw1

### Makefile

* In `os-name`:
    * `cp Makefile.[platform] Makefile`
    * Where `[platform]` is either M1 or x86
* HACK: multiboot header const **MUST** live in .rodata zero:
    * Solution that Neil added `--section-start=.rodata=0` flag to linker in `Makefile`
* Make sure to have `mtools` and `screen` installed for 

### hello.c

* `hello.c` is where hw1 will be
* Keep lines 15 and 16, everything else in main can probably be deleted

### Summary

* `rootfs.img`:
    * Recipe to create disc image to boot off of
* Create OS -> create disc img to boot off -> boots off of it and runs qemu

## LinkedList Refresher

* Update new node pointers first
* n1 -> n2 -> n3 -> n4 
  * Inserting n5

```c
tmp = n2.next;
n3.prev = n5;
n5.prev = n2;
n5.next = n3;
```

* malloc - node for every byte

## More on Malloc

* Each node is **12 bytes** (4 bytes each for next, prev, and size)
 
```c

struct node {...};

struct node listmem[5]; // 60 bytes; 5*12
char listmem[60]; // Each char is 1 byte

```

```c 

int *arry_ints = listmem;
arry_ints[0] = -1; // modifies the contents that the pointer points to
...
arry_ints = 0xDEADBEEF; // Changes where the pointer points to
```

## Tips

* `int *x` refers to an address in memory
* We can treat a pointer **as an array**
* Will modify the contents

## Declarations and Statements

* Create variables using type and name of var `int *x;` 
* Using `*` in a statement will derefrence 

## HW1
* declare and init pointer
* Treat like an array
