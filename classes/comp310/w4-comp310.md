# COMP 310 W4

## I/O & Interrupts

* This week, implement a polling keyboard driver in the operating system
* Notice I/O port types:
    * `0x60` - R/W access - data port
    * `0x64` - R access - status register
    * `0x64` - W access - command register 
* PS/2 status register:
    * Tells us whether a user pressed a key or not
* Check the least significant bit (LSB)
    * Use a bitwise `&` for this. i.e. `number & 1`
```
  10011001
& 00000001
----------
  10011001
```

```c
int status = inb(0x64)
if (status & 1)
    // read data register
else
    // nothing
```

## Interrupts, Exceptions, and Event Handling

* Useful for determining why computers perform poorly under load
* Dumb way to handle events - a state machine:
    * if event, then handle event
    * Literally Windows95
* CPU user mode(user process) and supervisor modes(ISR):
    * Keyboard handler is a special function that is a hardwired link from the user process and ISR
    * User process is execution level 0, ISR is execution level 1
* It's important to plan around these function calls in software development
* Creates a stack frame:
    * Return address (pushed by caller), stack pointer, local variables
* Problem: How do we know what function to call when an I/O event happens?
* Solution: Vector table
* The vector table will have a list of function names and their relevant hardware device name
* If the hard-coded vector table isn't appealing, use an interrupt controller
* Interrupt Controller (IC):
    * Inputs -> IC logic -> send signal to relevant vector number
* This way there's some programmability/flexibility with use of the vector table

## Virtual Memory

* You can't move the address of the program at runtime:
    * Already been assigned to the compiler
* Compiler chooses where in memory a program will be assigned
* Cannot hard-code addresses
* Three solutions (basically only #3 is used):
    1. Relative addressing
    2. Segmentation
    3. Paged memory management
1. Compiler sets aside dedicated space for code, globals, heap, stack, and the rest of the program addresses
    * Register (A4, for example) will be used as a base address for the OS
    * Requires agreement between compiler and OS
2. Segmentation:
    * Segment register will have base addr of the program loaded into the OS
    * Tricks program into thinking it's accessing one address, but actually it goes to an in-between seg reg that points to the physical address
    * Program addresses can be the same, but together with seg reg's they will have a unique physical addr
3. Memory management unit (MMU) 
    * Converts virtual to physical addresses using hash functions 
    * Virtual/physical addresses are made block-wise, not address-wise
        * A block of addresses is known as a **page**
* MMU steps:
    * Take a virtual address
    * Apply a hash function to it, becomes an index to hash table
    * What is corresponding phys address
    * Nuke lower order three nibles
    * Replace them with the least significant nibles of the virtual address
* Translation lookaside buffer (TLB)
    * Table that stores a bunch of virtual to physical address buffers
    * TLB is **inside** the CPU
* Problem: what do we do when a virtual address is not in the TLB???
* Solution: Tree off in main memory:
* Page table (1024-tree):
    * 1024-tree
    * Each node has 1024 children
    * Stores all virtual to physical mappings 
    * If virtual mapping is not in TLB, we need to look for it in the page table
    * Kick one of the virtual addresses out of the TLB, replace it with the needed address found in the page table
        * Usually removes one that was used the longest ago

