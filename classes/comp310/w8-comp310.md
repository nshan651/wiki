# COMP 310 W8

## FAT16 Filesystem

* Check out FAT filesystem wikipedia page!
* Also OS dev articles

## Creating Our Own FATFS

```
[nick@arch ~/git/Course-Archive/comp310/os-nshan651/fat-test] (main)$ dd if=/dev/zero of=disk.img bs=1M count=16
16+0 records in
16+0 records out
16777216 bytes (17 MB, 16 MiB) copied, 0.0118819 s, 1.4 GB/s

[nick@arch ~/git/Course-Archive/comp310/os-nshan651/fat-test] (main)$ ls -lh
total 16M
-rw-r--r-- 1 nick wheel 16M Oct 17 12:51 disk.img
```

## Verifying With Hexdump Command

```
[nick@arch ~/git/Course-Archive/comp310/os-nshan651/fat-test] (main)$ hexdump -C disk.img | less
00000000  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
01000000
(END)
```

## Partitioning the disk

* Run `fdisk disc.img`
* All explained in the Gentoo partitioning guide (based-af)
* Go through fdisk setup and select all defaults
* End result should look like this...

```
Device     Boot Start   End Sectors Size Id Type
disk.img1        2048 32767   30720  15M 83 Linux
```

* Command to create fat fs (same as the one that creates root.img in Makefile!):
    * `mkfs.vfat --offset 2048 -F16 disk.img`
* Run `hexdump -C disk.img | less` again to inspect
* Note that superblock starts at `0x100000`
 
```
Partition type
   p   primary (0 primary, 0 extended, 4 free)
00000000  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
000001b0  00 00 00 00 00 00 00 00  ae 0d 91 2e 00 00 00 20  |............... |
000001c0  21 00 83 0a 08 02 00 08  00 00 00 78 00 00 00 00  |!..........x....|
000001d0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
000001f0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 55 aa  |..............U.|
00000200  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
00100000  eb 3c 90 6d 6b 66 73 2e  66 61 74 00 02 04 04 00  |.<.mkfs.fat.....|
00100010  02 00 02 00 78 f8 20 00  20 00 02 00 00 00 00 00  |....x. . .......|
00100020  00 00 00 00 80 00 29 f3  8d 1c 8a 4e 4f 20 4e 41  |......)....NO NA|
00100030  4d 45 20 20 20 20 46 41  54 31 36 20 20 20 0e 1f  |ME    FAT16   ..|
00100040  be 5b 7c ac 22 c0 74 0b  56 b4 0e bb 07 00 cd 10  |.[|.".t.V.......|
00100050  5e eb f0 32 e4 cd 16 cd  19 eb fe 54 68 69 73 20  |^..2.......This |
00100060  69 73 20 6e 6f 74 20 61  20 62 6f 6f 74 61 62 6c  |is not a bootabl|
00100070  65 20 64 69 73 6b 2e 20  20 50 6c 65 61 73 65 20  |e disk.  Please |
00100080  69 6e 73 65 72 74 20 61  20 62 6f 6f 74 61 62 6c  |insert a bootabl|
00100090  65 20 66 6c 6f 70 70 79  20 61 6e 64 0d 0a 70 72  |e floppy and..pr|
001000a0  65 73 73 20 61 6e 79 20  6b 65 79 20 74 6f 20 74  |ess any key to t|
001000b0  72 79 20 61 67 61 69 6e  20 2e 2e 2e 20 0d 0a 00  |ry again ... ...|
001000c0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
001001f0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 55 aa  |..............U.|
00100200  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
```

## Anatomy of the Boot Sector

* Note: Check this out (https://wiki.osdev.org/FAT)
* FAT boot sector:
    * Bytes per sector = 512
    * Cluster = block:
        * 4 reserved sectors per cluster
    * Superblock contains:
        * Boot sector + 3 reserved sectors
    * Each fat table is 1 cluster long
    * Then root directories begin
    * Total sectors in the file system
    * Num sectors per cluster = 4
    * fs_type = FAT16 (check fs_type to verify this is the case when testing)

## Adding Data to the Disk

* Mount the disk image on `/mnt` directory:
    * i.e. `/mnt/disc`
* Setting a loop-back device to point to our disc image
* `sudo losetup /dev/loop1 disk.img -o 1048576`
    * NOTE: Will be slightly different on ubuntu-based systems

## Mounting the FS

* `sudo mount -oloop,offset=1048576 disk.img /mnt/disk`
* Formula to point you to the beginning of data region:
    * (Part offset) + (# Reserved sectors) + (# Fat tables * # Sectors per cluster * # Clusters per fat) + (RDE * sizeof(RDE) / Bytes per sector)
