# COMP 310 W9

## Processes Execution

* Thread of execution
* Memory space
* Matrix multiplication:
    * $a1b1 + a2b2 + a3b3...$
    * $O(n^3)$ time
    * How do we make this faster...
* Parallelization:
    * Divide up the operations through the use of workers, who perform them in parallel
    * Workers operate the same instructions on different data
    * Calculate independent results, combine into a final result
* Features of parallel operations:
    * One copy of the code shared between multiple registers
    * This way, we also only need one copy of input and output data
    * Two different threads of execution that are running simultaneously
* Processes can only write to a dedicated space to memory:
    * They have their own page table
* Two bad things can happen if processes tries to write outside assigned memory space:
    * Computer restarts
    * Page fault handler can reroute it to a safe address space

## Processes

* Process tree:
    * Reads process off hard disc
    * This is the init processes
    * OS itself will never explicitly read a process again after the init process
* On Linux, reads from `/etc/init.d/` directory:
    * These are the files that get loaded
* Fork:
    * Init system will create an identical copy child process also called init:
        * Only difference is the physical addresses and process IDs
    * Child init calls `exec`
* Exec replaces the child init process with a binary
* Forking is weird, we won't be doing this, we'll just directly exec
