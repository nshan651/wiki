# COMP 310 W5

* TLB stack:
    * VADDR -> TLB -> PADDR
    * ^ MMU encompasses this process
* What is cache?
    * CPU is ~5x faster than the memory system 
* Slow because there are so many connections and possibilities
* Solution - memory caching:
    * L1 Cache - 32k @ 2GHz
    * L2 Cache - 256k @ 1GHz
    * L3 Cache - 2M @ 2MHz
* Additional latency (how is it faster when we have all these extra bottlenecks?)
* Answer: there's a lot of redundancy in code execution. This means that caches are generally able to perform better, unless there is pathological code that jumps around in memory a lot
* Store all page translations inside TLB, in the CPU
* Also levels in the TLB

## Address Translation

* Some 32-bit address issued by program (assigned by compiler)
* VADDR (Virtual Address):
    * Comprised of offset (first three bits), the index, and the tag
* Offset (12 bits) or 4kb pages
* We need 2 bits to index into a 4-element array:
    * Subsequently, 3-bits to access 8-element array TLB
* The typical TLB is 64-256 elements long
* Therefore, the index will be **bit 4**
* Example address:
    * `0x8004477c`
    * `77c` is the 12-bit offset
    * `4` will be the index
    * Remaining bits elements will be the tag
    * `80044` is the Virtual page number (VPN)

## Inside the TLB

* TLB:
    
| Index | Tag   | PPN  | ValidBit  |
| ----- | ----- | ---- | --------- |
| 0     |       |      |           |
| 1     |       |      |           |
| 2     |       |      |           |
| 3     |       |      |           |
| 4     | 8005  | 8017 | 0         |
| 5     |       |      |           |

* Process for lookups:
    1. Split ADDR into offset, idx, tag 
    2. Look in TLB at index
    3. Compare tag in TLB to tag in VADDR
    4. If it is not found, look in the tree
* The page frame allocator is an operator between page table and TLB

## Page Table

VA -> APP -> SPP -> MP

* Bits for each section:
    * Offset = $log_2(PAGE_SIZE)$
    * Index = $log_2(NUM_ROWS_TABLE)$
* Least significant bit:
    * 8 = 1000
        * LSB 3
    * c = 1100
        * LSB 2
    * Basically the first one going from left to right (starting with count 0)
* Address translation:
    * f(VPN) -> PPN
    * Some function translates from virtual addr to physical addr
* How to handle this in practice:
    * Array containing every virtual to physical address page mapping
* Page table features:
    * 1024 directory numbers
    * 1024 pages

## Example

* Given address `80070101`
* VPN is `80070`, offset is `101`
* Higher 10 bits becomes directory number, lower 10b becomes page in directory
* Index of directory number is `0x200` (512 in decimal)
* Index in by the amount to the page table (subset of the directory + page table)
    * Note that this is a different thing. Page table is directory + ptable collectively but also the standalone page table
* Index into page table using page number (lower 10b)
* pseudo code:

```
*pg = dir[D#]
ppn = pg[P#]
```

## TLB

* TLB is normally supposed to be understood to the programmer
    * But, you should know how the hardware works
* Virtual paging **must** be understood

## Page Table

* The anatomy of the page table
* All of the pointers in the page table are **physical addresses**
* Root of the table, CR3:
    * Can't use this
    * Userland programs cannot access it
* CR3 points to the base of an array of some addresses in memory
    * Each element of the array has its own virtual addr
* Takes bits above the lower 3 nibbles which becomes the VPN (virtual page number)
* VPN is split into DE (upper half) and PTE lower half
* Directory entry points to **another** physical address 

### Example

```c
uint32_t *pg_tbl = dir[511];
uint32_t *ppn = pg_tbl[968]
```

* Addresses are aligned to 4kb, buuuut...There are a lower 12bits that are used for other things
    * These are flags
* Most elements in the tree are not used
    * Thus, most flags will not be flipped
* Important bits: present bit, read/write, and frame:
    * Just set r/w to 1 so that you can create pages
* 
