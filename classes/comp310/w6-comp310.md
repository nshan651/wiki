# COMP 310 W6

## Hard Drives and SSDs

* HDDs stored on a rotating magnetic platter:
    * Had to be read 512b at a time
* SSDs stored on a digital chip:
    * Fast, flash memory similar to what is found in the CPU
    * Can technically read 1b at a time, but most follow the HDD convention of 512b
* Stepping through tree data structure on paper:
    * `/ -> /bin -> /bin/ls`
    * How can we do this in 512b chunks of memory? Which data structure would you use?
* Simple tree structure:
    * Fine if you have a couple children per node, becomes a problem with many children
    * Solution - max number of children per node of the tree 
* Study done on computer files:
    1. Most common file size is small (~2k)
    2. Average file size is growing (~200k)
    3. Most bytes on the FS are stored in large files
        * We need some way to support large files
    4. Filesystems contain ~100k files
    5. ~1/2 full
    6. Directories are typically small - less than 20 files


