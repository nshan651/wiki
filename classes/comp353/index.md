# COMP 353 Index

## Notes

[Week 1](w1-comp353)
[Week 2](w2-comp353)
[Week 3](w3-comp353)
[Week 4](w4-comp353)
[Week 5](w5-comp353)
[Week 6](w6-comp353)
[Week 7](w7-comp353)
[Week 8](w8-comp353)
[Week 9](w9-comp353)
[Week 11](w11-comp353)
[Week 12](w12-comp353)

## Final Review

[Final Review](fr-comp353)

## HW

[Group Project 1](gp1-comp353)
[hw1](hw1-comp353)

## In-Class Exercises

[exercise1](exercise1-comp353)
