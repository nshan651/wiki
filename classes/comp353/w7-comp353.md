# COMP 353

## Midterm Study

### Student Example

1. Student taking at least 1 course

```sql

-- Question 1
select distinct sid
from takes;

-- Question 2
select sid, count(*)
from takes
group by sid
having count(*) = 2;

-- Question 3
select sid, count(*)
from takes 
group by sid
having count(*) = (
    select count(*) from course
);

-- Question 4
select sid, count(*)
from takes 
group by sid
having count(*) = (
    select count(*) from course
);

-- BONUS: Students taking every online course
select t.sid, count(*)
from takes t, course c
where c.cid = t.cid and c.modality = online
group by t.sid
having count(*) = (
    select count(cid) from course 
);

-- Question 5
```

```sql
select distinct m.groupCode, m.groupName
from musicalGroup m, recordingLabel r, cd
where m.groupCode = cd.groupCode and cd.labelID = r.labelID and r.labelLocation = 'Detroit'
group by m.groupCOde, m.groupName
having count(*) = (
    select count(*) 
    from recordingLabel
    where labelLocation = 'Detroit'
);
```

## Midterm Format

* High-level review of data base general concepts 
* ER-modeling
* Relational Schemas
* SQL (MOST IMPORTANT TOPIC)
