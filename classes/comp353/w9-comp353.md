# COMP 353 W9

## Other SQL Concepts

* Undesirable properties of join:
    * Loss of information
* Superkey formal definition:
    * Let R be a relational scheme
    * A subset K of R is a superkey of R...
    * If, in any legal relation r(R) all pairs $t_1$ and $t_2$ tuples in r $s.t.t_1 \neq t_2$
        * $t_1[K] \neq t_2[K]$
    * **"No two tuples in any legal relation r(R) may have the same value on attribute set K"**
* Functional dependencies:
    * Property of the meaning or semantics of the attributes in a relational schema
    * Used to provide a set of constraints on the attributes of a relation schema that must hold at all times
* Example: Functional dependencies and company training courses
    * ID -> name, sal
    * CRSID -> cName, crsname, inst, length
    * ID, CRSID -> Date (takes relation)
    * ID -> DNUM{emp} (works)
    * DNUM -> ID{MGR} (manages)

## Rules of Inference for FDs

* Rules of inference:
    1. Reflexivity
        * `If Y identifies X, then X identifies Y`
    2. Augmentation
        * `If X -> Y, then WX -> WY`
    3. Transitivity
        * `If X -> Y & Y -> Z, then X -> Z`
    4. Union
        * `If X -> Y & X -> Z, then X -> YZ`
    5. Decomposition
        * `If X -> YZ, then X -> Y &...`
    6. Pseudo-transitivity
        * `...`
* First (3) are known as Armstrong's axioms (complete)
    * One can infer the rest of the rules from just these three

## More on Formal Logic

* Lossless-join decomposition:
    * True if at least one of the following holds...
        * $(R_1 \cap R_2) \rightarrow R_1\ in\ F^+$
        * $(R_1 \cap R_2) \rightarrow R_2\ in\ F^+$
* Dependency preservation:
    * "Update validation without computation of joins"
    * All of the original knowledge is still preserved

## Normal Forms

* Unormalized:
    * A table that is not necessarily one of atomic values
* 1NF:
    * Happens when we have all atomic values 
* 2NF:
    * Is dependent on the whole of every candidate key
