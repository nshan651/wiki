# COMP 353 W6

## Aggregation Statistics

* Includes max, min, sum, avg, and count

## Group By and Having Heywords

* Group by:
    * Indicates how to order the output
* Having:
    * Acts like a filter
    * Ex.)

```sql
having max(score) >= 10
```

## Anatomy of a Query

1. SFW:
    * Select, from, where
2. Grouping
3. Having

## Create Table

```sql
create table t
(col1 varchar(24) primary key,
col2 varchar(6))
```

```sql
-- An example of majors and teaches table creation
create table cse_majors
(
id      varchar(24)     primary key,
name    varchar(6)      not null,
class   varchar(2)
        check (class in ('FR', 'SO', 'JR', 'SR', 'GR'))
);

create table teaches
(
name    varchar(24)
        references cse_profs
        on update (...)
crsid   varchar(6)
        (...)
primary key (name,crsid)
);
```

* Also can have table constraints
* Attribute constraints:
    * Attribute constraints themselves
    * Check constraints
    * Column reference
    * Action
* DML (data manipulation language):
    * Includes insert and delete statements

## Database Privileges:

* SELECT - an authorized db user must have the select table privileges to access any column of a table
* INSERT - can restrict to certain attributes
* UPDATE - can restrict to certain attributes
* DELETE 
* REFERENCE - can control who can access what

### Granting Privileges

* Example:

```sql
grant privilege-list
on table-list
to user-list
-- [with grant option]
```

### Revoking Privileges

* When switching projects (or getting fired :/), useful to revoke privileges
* Example:

```sql
revoke privilege-list
on table-list
from user-list
```

## Example Problems

```sql

-- Query 1

-- Query 2
select dnum, avg(sal), count(*)
from emp
group by dnum

-- Query 3
select dnum, avg(sal), count(*) as numEmp
from emp
group by dnum
having numEmp > 1;

-- Query 4
select cname, clength as maxL
from course
where clength = (
    select max(clength)
    from course);
    
-- Query 5
select count(T.crsid), E.dnum 
from emp
where numCourse = (
    select count(*)
    from takes T, course C
    where T.crsid = C.crsid
)
group by dnum
```
