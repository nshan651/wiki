# COMP 353 

## 2022-10-17

### In-Class Exercises

#### Exercise 1

```sql
-- 1. List the ids of employees in the “Research” department that earn more than $75,000
select id
from emp E, dept D
where E.dnum=D.dnum and D.dname='Research' and E.sal>75000;

-- 2. List the names of the employees who took the course with id ‘DB01’ on June 25, 1990
select E.id
from emp E, takes T
where E.id=T.id and T.crsid='DB01' and T.date='25Jun90';

-- 3. Who is the manager of the employee 'John Smith'? Give the manager name and department name
select E.name, D.dname
from emp E, dept D
where E.dnum=D.dnum and 
      E.id = (select D.mgrid
              from emp E, dept D
              where E.dnum=D.dnum and 
              E.name='John Smith');
```

#### Exercise 2

```sql
-- a. List the names of the employees who work for department ‘Dept 1’ and have a salary greater than 64000.

SELECT E.name 
FROM emp E, dept D
WHERE E.dnum=D.dnum and E.sal>64000 and D.dname='Dept 1';

-- b. What is the average salary in each department? Print the department number, average salary and number of employees.

SELECT E.dnum, avg(E.sal),count(*) 
FROM emp E
GROUP BY E.dnum;

-- c. What is the average salary in each department with more than 1 employee? Print the department number, average salary and number of employees.

SELECT E.dnum, avg(E.sal),count(*) 
FROM emp E
GROUP BY E.dnum;
HAVING count(*)>1;

-- d. Find the course(s) with the maximum length. Print course names and lengths.

SELECT C.cname,C.length 
FROM course C 
WHERE C.length=(SELECT max(length) FROM course);

-- e. Find the number of courses taken by the employees of each department. Print the department number and number of courses. Count each instance of an employee taking a class. 

SELECT  E.dnum, count(*)
FROM emp E, takes T
WHERE T.id =E.id
GROUP BY E.dnum;

-- Extra:
-- 1. Get the courses taken by all employees
SELECT E.name
FROM Emp E
WHERE (SELECT count(*) FROM Course) =
      (SELECT count (*) 
              FROM Takes T
              WHERE T.id=E.id);
             
SELECT E.name
FROM Emp E, Takes T WHERE T.id=E.id
GROUP BY E.id, E.name 
HAVING COUNT(*) = (SELECT COUNT(*) FROM Course)             

-- 2. Get the courses that have not been taken by any employee
(SELECT C.crsid FROM Course C)
EXCEPT
(SELECT t.crsid FROM Takes T);
```
