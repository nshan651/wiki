# COMP 353 Final Review

* BCNF decomposition algorithm:
    * BCNF decomp guarantees loss-less decomp
    * However, not every BCNF decomposition is dependency preserving
* Undesirable properties:
    * Repetition of information
    * Inability to represent certain information
    * Loss of information
* Desirable properties:
    * No repetition
    * Dependency preservation
    * Lossless-join decomposition
