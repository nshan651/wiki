# COMP 353 W3

## Mapping Between ER Diagram and Relational Database

* Cannot have two records that contain the same value for the key
* For entities that share characteristics, choosing a candidate key involves thinking about how the relationship should be represented (1:1, 1:M, M:M)
* Primary keys cannot be null
* If a table has multiple primary keys, a uniqueness constraint can be used on the candidates that don't comprise the primary key
* Primary and foreign keys combine to form a relationship 
* Referential integrity constraint breakdown:
    * A set of attributes $F_k$ in $R_1$ is a foreign key of $R_1$ if
        1. The attributes of $F_k$ have the same domain as the primary key attrs $P_k$ if the relation scheme $R_2$ that $F_k$ references
        2. Let $t_1$ be a tuple of $r_1$, either
            * $t_1[F_k]$ is null or
            * $t_1[F_k]=t_2[P_k]$ for some $t_2$ or $r_2$
* In order to enforce a mandatory constraint on entity A between relationship AB, you must add the candidate key for B to A:
    * We select this approach because it is more **compact**
    * The number of entries in B is greater than or equal to the number of entries in B because B is optionally constrained by A
* To general solutions in a 1:1 relationship:
    1. The above method of adding the optional foreign key as an attribute of the mandatory one
    2. Create a new entity relationship with the primary keys of each
* 1:M Mappings:
    1. Create a new relationship
        * In a 1:M scenario, the primary key should be chosen from the **many** side
    2. Add foreign key to **many** side
* M:M Mappings:
    * Only one option - create a new entity relationship with primary keys from each
