# COMP 353

## Group Project 1

### Notes

* 3. ERD - just had a few minor suggestions
    * Rename customer 'Name' attribute to 'CName' to keep it consistent
    * I would make all IDs include the full entity name, just because it removes some ambiguity and is more explicit. Like 'Cid' -> 'CustomerID', etc.
    * It's a little unclear what 'Bank' represents in the Payment entity. I would change it to BankName or something to avoid confusion.
    * This is more personal preference, but I feel like the relationships should be present tense verbs. I.e. customer places order, order has product.
* 4. ERD uncaptured constraints:
    * I think the first two make sense, but can't the customer place many orders since the (0, N) constraint is on the customer side?
* Everything else looks great from what I can see!
