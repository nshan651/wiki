# COMP 353 W1

* Anshu - MS in CS - Traveled to 10 states and India
* Sanmeet - MS in IT - 2 hour commute everyday

## Intro to Databases

* Challenges of traditional file processing systems:
    * Data redundancy and inconsistency
        * Pro: facilitates access, con: takes up extra storage
    * Difficulty in accessing data
    * Lack of concurrency control (multi-user)
    * Lack of security (beyond that of the OS)
    * Integrity constraints

## Benefits of Data Management Systems

* Provides data abstraction between physical and conceptual level:
    * Moreover, we can have different logical views *on top of* the conceptual level
* physical vs logical:
    * Physical is the ability to modify the physical scheme without causing the program to be re-written
    * Logical is the ability to modify the conceptual scheme without causing app to be re-written

## Databases vs Filesystems 

* Self-contained
* Insulation between program and data
* Data abstraction
* Support of multiple views

## History of Data Models

* 1960s - network model (graphs)
* Late 60s - hierarchical model (trees)
* 1970 - relational model (tables)
* 1975 - entity-relationship model
* 1980s-1990s - object-oriented databases (objects)

### MAP REDUCE PAPER

## Database Terminology

* Languages:
    * Definition languages
    * Dictionary (catalog) 
    * Data manipulation language
    * Query language
    * Query

## Users of a Database

* Database admin (DBA) 
* Application programmers
* Business Users
* End-users

## ER Diagrams

* ER Diagram -> Relational Schema -> DB creation
* Entity:
    * Type is a distinct object
    * Entity extension - set of entities of the same type
* Relationship:
    * Type is an association among several entities
    * Instance is a set of relationships of the same type
* Simple but limited notation has a single number:
    * "Up to n"
    * Doesn't allow for a minimum enforcement, i.e. at least 1
* Participation constraint and cardinality ratio:
    * Cardinality - allowable amount of instances
    * Participation - whether participation is optional or mandatory
* Can be shown as min/max pairs 
* Keys:
    * Super (composite) key - set of one or more attributes which together allow for unique identification
    * Candidate key - superkey where no proper subset is a superkey
    * Primary key - A single key designated as the main way of identifying entities
* Strong vs week entities:
    * Strong entities have primary key, weak entities do not
