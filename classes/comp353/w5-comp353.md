# COMP 353

## JOIN Clause

* `JOIN` clause:
    * returns the intersection of two tables based on primary keys

## More on SQL

* SQL is a declarative language
* Intermediate tables:
    * Views and snapshots
* Views:
    * A relation that is not explicitly stored in the database
    * Made visible to a user as a "virtual relation"
* Snapshot:
    * A materialized view
    * Materialized once at the time the "create snapshot" command is issued
* Nested subqueries - set comparison:
    * Sometimes a set operation is easier!
    * However, nested subqueries are also very important

### Demo

```sql
select col1, col2, 'aaa' as "extraCol" from t1;

-- Map (+) operator over columns
select col1+col2 from t1;
```

* There are also many functions that you can apply to tables
* An example of the `where` clause

```sql
select * from t1
where col2 = 200; -- We don't use '=='
```

* Nested queries example: exists/not exists

```sql
/* 
Exists evaluates to FALSE if the sub-query 
evaluates as empty; TRUE if otherwise
*/
select name
from cse_profs P
where not exists
    (select *
    from teaches T
    where T.name = P.name);
```

* This is very inefficient: there is an inner query that is run for every case of the outer query
* In order to connect disparate tables, you need to include the connections between intermediate tables:
    * A -> B -> C -> D
    * All connections must be shown
* Ex.) Rough way of selecting the courses that begin with `"COMP"`

```sql
select P.name, P.office, C.crstitle
from P, C, T
where P.name = T.name & C.crsid = T.crsid 
-- Pseudo code: substring(C.crstitle, 3) = 'COMP';
```

## Performance Considerations

* Sometimes it's worth sacrificing performance for readability, at least when the ultimate goal is visualizing

## Aggregation

```
count(*) -- Most often used with a '*'
min()
max()
avg()
sum()
```

### Demo 2

* Selecting number of students average age of students by level

```sql
select lvl, avg(age)
from S
group by lvl; 
```
