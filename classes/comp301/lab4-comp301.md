# COMP 301 Lab 4

- Range Force modules

1) [X] Legal Considerations for a Pentest

2) [X] Situational Awareness

3) [X] Pentest Reporting and Delivery

4) [X] IAM – Principle of Least Privilege

5) [X] Common General Programming Errors

6) [X] Protect Data in Use

7) [X] Visual Spoofing

8) [X] Visual Spoofing Challenge

9) [X] Practical Secure Logging (you can skip the last task “Mitigate the problem” to get 75%)
