# COMP 301 W1

## Threat Model

* Who is the attacker?
* What are its resources?
* What is the goal?
* Why?
* What are your resources?
* Out of scope attacks?
* Any assumptions?

## Goals

* The "CIA":
    * Confidentiality:
        * "Need to know"
        * Access controls
        * Existence of data can also breach confidentiality
    * Integrity:
        * Protects data + origin of data
        * Provides correctness and trustworthiness of data
        * Prevention (attackers cannot modify data)
        * Detection (attackers can modify data but easy to detect modification)
    * Availability:
        * Reliability -- fault tolerance
        * Dependability
        * Denial of service (Dos)
            * Malicious or accidental
    * (Bonus) Accountability:
        * Every action can be traced
        * Audit
        * Assurance
* Attacks:
    * Passive -- snooping or eavesdropping
    * Active:
        * Modification
        * Spoofing/masquerading
    * Repudiation:
        * Deny you did something
    * Delay of messages
* Goals are related:
    * Failure of one goal can lead to failures elsewhere

## Security Assessment

* Security goals:
    * Audits are necessary for companies to ensure these goals are achieved
* Security by obscurity:
    * Hiding details of a system does NOT make the system secure!

## Laws of Cybersecurity

* Laws:
    * Import/export laws:
        * Cryptographic key limitations for foreign countries
    * HIPAA
    * FERPA
    * GDPR/CCPA
    * Patriot Act (internet monitoring)
    * CISA (cybersecurity information sharing act)
* Human issues:
    * Social engineering
    * Errors

## Common Threats

* Common kinds of threats:
    * Malware
    * Misuse of data, resources, or services
    * User errors
    * External hackers
    * Internal actors
    * Environmental/natural disasters
    * Social engineering
    * System/component failures
    * Financial collapse
    * Theft

## Risk
 
* Measuring risk:
    * Risk = threat * vulnerability * consequences
* Risk analysis:
    * Adding security increases cost => worth the risk reduction?
    * ROSI: return on security investment
    * Gordon-Loeb: 37% of loss for security investment:
        * To account for a 10 million loss => spend 3.7 million on security
        * Once it goes over this, it starts to no longer be cost-effective
* Threat => risk:
    * When threats can be realized (although improbable) => risk
* What do we do with risk?:
    * Elimination
    * Avoidance
    * Acceptance
    * Reduction

### Activity

* Evaluate the four security goals, a realistic threat model, and risk analysis for:
    * Lock security:
        * Is it pickable? What is it protecting?
        * Increase complexity, change locks frequently, make sure users are using it properly
    * Credit card (electronic payment) use
    * LUC services (Sakai, grades, Locus, etc.)
