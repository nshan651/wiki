# COMP 301 -- Lab 2

## 0. Access the Server

![access](/home/Downloads/comp301/access.jpg)

## 1. Symmetric Encryption

### 1.1 File Creation and Encryption

- What is the plaintext file size?:
    - 12 bytes
- What is the ciphertext file size?:
    - 16 bytes
- Why?
    - The encryption algorithm adds overhead to the size of the original data
- Are the two cipher texts the same? Are their sizes the same?:
    - The contents of the cipher texts are different, but the sizes are the same

### 1.2 Identify the Weakness of ECB

- Compare the two ciphertexts. What do you see?:
    - Both encrypted keys start with `inPo`, but then it seems like when it gets to the words "you" or "Pika", it begins to differ. It continues to be different, even though the plaintext following the two words isn't changed.

### 1.3 Encrypting and Decrypting Files Properly With GPG

- What are the contents of the message?:
    - "Everyone loves a conspiracy", right? - Dan rown, The Davinci Code. So it goes that Jon Snow knows nothing, Luke knows who his father is, Scottie knows how to beam anybody's preciousss, and nobody knows the rules of this club. 

## 2. Public Keys

### 2.1 Keypair Creation and Export

```sh
 gpg --full-generate-key;
 gpg --export -a “<Your LUC email address>” > <USERNAME>.pub;
 cp <USERNAME>.pub /usr/local/src/lab2/public-keys/<USERNAME>.pub
```

### 2.2 Retrieve and Verify a Message from the Instructor

```sh
gpg --import /usr/local/src/lab2/public-keys/chantin.pub;
gpg --list-public-keys;
gpg --edit-key chantin@cs.luc.edu;
gpg --verify /usr/local/src/lab2/instructor-message.txt.gpg;
```

- Take a screenshot showing the verification was successful:
![verify](/home/nick/Downloads/comp301/gpg-verify.jpg)

- Decrypting it:
![decrypted](/home/nick/Downloads/comp301/gpg-decrypted.jpg)

### 2.3 Send and Receive Secure Messages with GPG

```sh
vim nshannon-message.txt;
gpg --encrypt --sign --armor –-recipient chantin@cs.luc.edu <username>-message.txt;
cp <username>-message.txt.asc /usr/local/src/lab2/requests/<username>-message.txt.asc;
ls -l /usr/local/src/lab2/responses/;
```

```sh
cp /usr/local/src/lab2/responses/<username>-response.txt.asc ~/lab2;
cd ~/lab2;
gpg --decrypt <username>-response.txt.asc;
```

- What is the comment?
    - 

## 3. Hashing

### 3.1 File Creation and Hashing

```sh
openssl list -digest-algorithms;
openssl list -digest-algorithms | grep “^SHA3-”;
echo “hello world” > myFile1;
cat myFile1;
# Create the second file as follows:
echo “Hello world” > myFile2;
cat myFile2;
# Calculate the MD5 hash as follows:
openssl dgst -md5 myFile1;
openssl dgst -md5 myFile2;
# Calculate the SHA512 hash as follows:
openssl dgst -sha512 myFile1;
openssl dgst -sha512 myFile2;
```

- Do you notice anything when you hash the two different files using the same algorithm?
    - Despite the only difference in the plain text being that the second file has a capital "Hello", the outputs are both extremely different, to the point where it's hard to see any similarities or patterns
- Do you notice anything when using different hashing algorithms?
    - Yes, MD5 and SHA-512 produce different length digits. MD5 creates a 32 digit integer, whereas SHA-512 produces 128-digits

### 3.2 Find the Right File

```sh
cp /usr/local/src/lab2/files.tar.gz .;
tar -xzf files.tar.gz;
cd files;
```

- What is the file?:
    - `file12`

## 4. RangeForce Lab

### Completion Status

![completed1](/home/nick/Downloads/comp301/completed1.jpg)
![completed2](/home/nick/Downloads/comp301/completed1.jpg)

14 Blue
