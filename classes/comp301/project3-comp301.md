---
title: COMP 301 Project 3
author: Nick Shannon
date: 2023-04-29
---

# Part 1: Crypto (10 points)

Decode the following messages:  

- RGFubnkgRGV2aXRvIHNob3VsZCBiZSBpbiBtb3JlIG1vdmllcy4g
    - The decoded message is "Danny Devito should be in more movies."
- 2 555 555 0 222 2 7 7777 0 66 666 0 7777 7 2 222 33 7777 
    - The code maps to a numberpad
    - The phrase is "ALL CAPS NO SPACES"
- Hanes  ng lu se da noehr entehutn fhl os?I’ rtygo.sy eehaioihetpto
    - The phrase is encoded using a Vigenere cipher, which uses a secret key
    - The secret key is "SECRETSECRETSECRETSECRE", and the decrypted phrase is "Never gonna give you up, never gonna let you down"
 
# Part 2: Password Cracking (10 points)

Using rockyou.txt, crack the following md5 hash: f480034fee8f39994f1989df74cdd206 

- To crack the MD5 hash, I used the following script:

```bash
#!/bin/bash

# Define the target MD5 hash
target_hash="f480034fee8f39994f1989df74cdd206"

# Loop through each line of the file
while read line; do
  # Calculate the MD5 hash of the current line
  hash=$(echo -n "$line" | md5sum | awk '{print $1}')

  # Compare the calculated hash to the target hash
  if [ "$hash" = "$target_hash" ]; then
    echo "Match found: $line"
    exit 0
  fi
done < "$1"

echo "No match found."
exit 1
```

- This compares the MD5 hash values of each line with the target hash.
- The decoded word is "pisces"

# Part 3: OSINT (10 points) 

Find the city and state where the picture in myBrothersDog.jpeg was taken.  

- We can dump the image exif data using `identify -verbose myBrothersDog.jpeg`
- Given the following GPS data:
    - exif:GPSLatitude: 33/1, 28/1, 4819/100
    - exif:GPSLatitudeRef: N
    - exif:GPSLongitude: 112/1, 3/1, 4802/100
    - exif:GPSLongitudeRef: W
- We can conclude that the photo was taken in Phoenix, Arizona, USA

# Part 4: Forensics (10 points)

Find the answer hidden in what-is-the-ultimate-question.gif.  

- The "ultimate answer" is found on frame 50 of 74. It is red text that reads "The answer [ineligible] 42 is the answer to the ultimate question of life, the universe, and everything"
- I found this by using the stenography tool stegsolve, which has a feature that allows you to inspect a gif frame-by-frame.

# Part 5: Traffic Analysis (10 points)

Look at the wireshark.pcapng file.

a) How many incorrect logins are there?

    - There are 1577 total incorrect logins
    - `tshark -r wireshark.pcapng -Y "ftp.response.code == 530" -T fields -e frame.number | wc -l`

b) What is the correct username and password?

    - The correct username is *cooluser*, and the password is *HappyStPatricks*
    - We can find this by filtering based on response code 230, which indicates a successful login (`ftp.response.code == 230`)

# Part 6: Log Analysis (10 points)

Look at log1 

a) How many packages were installed (not upgraded/updated)? 
 
    - 61 packages were installed
    - We can find that using the command: `cat log1.txt | grep -i "status installed" | wc -l`
b) What is the Operating System on that machine? 

    - The machine appears to be running Ubuntu
    - This is indicated by the dbus user session
c) What is the version number of ssh on that machine?

    - The software version number of ssh installed is version `7.6`
    - The package-specific version is indicated as `-4ubuntu0.5`

# Part 7: Exploitation (10 points)

What is the flag in Exploitation.py?  

- The flag is "COMP301-4529"
- I found this by determining each character independently by looping through all 128 ascii character for each character of the 12-character flag.

# Part 8: Scanning (10 points)

You might need to use the class server (same IP address as Lab2, Project1, Exam1)
- I do not have access to campus wifi, so I use `nmap` to show information about the server

1) There is a secret flag at IP address 192.168.101.223. Can you find it? The flag starts with *proj3*
2) There are two web (http) servers running at 192.168.101.223. What are the port numbers? 
    - port 8008
3) What is the service running on the highest UDP port number? 
4) There is a DNS server running on a TCP port. What port is it? 
    
    - Port 8008
5) What is the lowest UDP port number that is open? 
    
    - port 113
6) What is the lowest TCP port number that is open?

    - The lowest open TCP port appears to be port 2000 (sip service)

*Hint: if you are using the provided server, use -privileged*

# Part 9: Hidden picture (10 points)

What is the picture hidden in the picture stego.jpg?

- Frame 2 using stegosolve shows that the hidden image is the One Ring from Lord of the Rings

# Part 10: Steganography (10 points)

What is the secret message in the picture luc.jpg?

- The secret message is "The worst thing that happens to you may be the best thing for you if you don't let it get the best of you."
- I found this by extracting the image using the CLI tool `steghide`

# Extra Credit 

Scan the following code using your smartphone to
get to the challenge

- RickRolled!
