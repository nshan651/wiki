---
title: "COMP 301: Lab 11"
author: "Nick Shannon"
date: 2023-04-15
---

# Problem 1 RangeForce Modules (5 points)

- Introduction to Social Engineering

![Lab 11](/home/nick/dl/comp301/lab11.png) 

# Problem 2 OSINT (15 points)
- Go to https://net.educause.edu/whois.htm and look for the owner of CS.LUC.EDU
    - What did you find?:
        - The whois lookup doesn't return any matches for a CS.LUC.EDU domain. I assume that it isn't found because it is a subdomain used for the computer science department, but this tool is only for searching domains and not subdomains.
        ![CS.LUC.EDU](/home/nick/dl/comp301/CS-LUC-EDU-1.png) 
    - What happens if you look for LUC.EDU domain?:
        - Searching "LUC.EDU" returns several pieces of information about the domain. It includes information about the registrant, administrative and technical contacts, named server, and domain record information.
        ![LUC.EDU](/home/nick/dl/comp301/LUC-EDU.png) 
- Go to https://dnsdumpster.com/ and look up CS.LUC.EDU
    - What kinds of servers and services show up? Can you identify the operating systems, application versions and other information?:
        - When looking at the long list of host records, it appears that many of the services are running ubuntu. Many of the host records appear to be inactive, but several belong to Loyola faculty. 
        - The name and location of the services are mostly Loyola University Chicago, United States, but there are a few that are associated with a cybersecurity firm called Akamai Technologies
        - One service that I recognize is Linode, a cloud-hosting service. There is also a host associated with the Pearson publishing service
    - What email service is that domain using?:
        - It appears that the five MX (mail service) records that are listed are all utilizing the ASP.NET server-side framework and are handled by Google's email service
        - Additionally, I found that the number before each of the records indicates its relative priority. For example, the main MX record for the domain, ASPMX.L.GOOGLE.COM. has a priority of 1, which is the highest priority, whereas the other alternatives have priorities of 5 and 10, which indicate a lower priority level
        ![MX Records](/home/nick/dl/comp301/mx-records.png)

*Note: If you submit only pages of screenshot, you will lose points. You can submit screenshot and I also want to see a short description (1-2 sentences) of what you found*

# Problem 3 More OSINT (10 points)

- Look at the provided picture, Picture1

For this problem, we can use the ImageMagick tool to inspect the image metadata. The command I used was `identify -verbose Lab11-Picture1.jpg`.
    - What camera was used to take the picture?:
        - An Apple iPhone 5 was used to take the picture.
    - When was the picture taken?:
        - The picture was originally taken on 2021:11:24 12:00:00
    - Where was the picture taken?:
        - The gps longitude and latitude coordinates are:
            - Lat: 38/1, 53/1, 65001/1250 Long: 77/1, 2/1, 2751/250 
            - This translates to 38.883334° N 77.040111° W
            - The location in Washington D.C., very close to the White House

# Problem 3 extra credit for all students (50 points) 

- What is the name of what is shown in the picture? How did you find the name? Please provide enough details so I know you are not guessing the name. Please do not do anything I wouldn’t do.:
    - I believe that the cat in this photo is Willow, the White House cat adopted by the Biden family. I cannot find the specific image, but I suspect that it was either taken 1) on a private social media account, and/or 2) was posted before the Bidens got the cat.
    - This seems to match the date of the image, November 24th, 2021, which seems to be before she was taken in by the Bidens.
    - I tried several reverse image searches, which did not result in anything. I then tried several search queries, and found that the Bidens' tabby cat looks quite similar to the cat in the photo.
 
*Note: this might not be feasible and I am open to give partial credit for a robust attempt.*

# Problem 4: 10 points if you can find the address of the Picture2

- The image is of the United States Bullion Depository (also known as Fort Knox)
- The address is 397 Redmar Blvd, Fort Knox, KY, 40121.
- I found this by using GoogleLens to perform a reverse image search.

# (graduate students: 10 points) Problem 5: (just for fun for undergraduate)

- You are in pursuit of a hacker who is escaping to Canada! When the car is turned off, it sends out
- a ping to the server. 
- The first ping was at 3030 N. Broadway, Chicago, IL
- The second ping was at 11211 120th Ave, Pleasant Prairie, WI
- The third ping was at 595 S. Taylor Drive, Sheboygan, WI
- Send the address where the hacker will stop next to law enforcement. What is that address? How did you find the address?

*Note: there might a few answers – I am looking more on how you found the answer*

The hacker is likely driving a Tesla, given that all three pings have a Tesla supercharger nearby. Given this information, we need to find where the next charging station is en route to Canada. It seems like the driver isn't confident with going more than 150 miles without stopping, given that if they were, they could have made it all the way to Green Bay from Pleasant Prairie. Instead, they chose to stop in Sheboygan. Given this information, I think the next spot they will stop is Green Bay, because the next necessary charging station after that is in Escanaba, which is too far. The closest point in Canada along the driver's route is Sault Ste. Marie.
