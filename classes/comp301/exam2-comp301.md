---
title: "COMP 301 Exam 2"
author: "Nick Shannon"
date: 2023-05-01
---

# Cover Page

Name: _Nick Shannon_______________________________

Date  : _2023-05-01_________________________________

By signing below, I agree to the following:

0) I may not copy or distribute this exam to anyone.
1) I may not discuss this exam with other students. I may not seek assistance from anyone 
(except the instructor and TA). I can only access Sakai, Panopto, lecture notes. I can use the 
Internet to help you understand some things, but I cannot look for the answer.
2) This exam is open-book, open-notes, open-computer. However, I may not look for answers on 
the Internet or other similar media (class notes not included). You can use the Internet for parts of
the questions (e.g. to do the scanning) but you cannot search for answers.
3) If I violate any part of this agreement, I will receive a 0 in this exam. Violations may also be 
reported to the Dean of Students.
4) If I am stuck on a challenge, I will ask the instructor for help. I will not ask other students, 
bots, the Internet (such as but not limited to stackoverflow, reddit, chegg) or look for answers on 
the Internet. (Remember: the instructor can search for questions as well!)
5) Submit in PDF

Signature or Initials: _N.S.___________________________________________

# Problem 1 (30 points – 5 points per question)

- 1.1 Can a system provide integrity without confidentiality?
    - [X] True
    - [ ] False
- 1.2 You have a file that you suspect could be malware. What should you do?
    - [ ] Email to Eric and ask him to check
    - [X] Upload to VirusTotal
    - [ ] Download and install on your computer to see what it does
    - [ ] Share with your friend to see what their fancy antivirus says
- 1.3 Which of the following is NOT a cybersecurity design paradigm?
    - [ ] Open Design
    - [X] Security at all costs
    - [ ] Separation of privilege
    - [ ] Least Privilege
- 1.4 What is a hash?
    - [ ] Something you eat at breakfast with eggs and bacon/sausage
    - [X] Set of alphanumeric characters that is generated from a one-way function
    - [ ] A way to review things
    - [ ] Part of the #tag
- 1.5 What is robots.txt?
    - [ ] A bunch of popular robot names
    - [ ] Something you should not access since you are not a robot
    - [X] A file that web crawlers look at
    - [ ] The file that powers Skynet
- 1.6 What is a Tor (The Onion Router) hidden server? 
    - [ ] Something hidden under a stack of onions
    - [X] A server that can only be access through the Tor network
    - [ ] A server that nobody can access because it is hidden
    - [ ] A server hidden to serve only illegal content

# Problem 2 (10 points)

To destroy a Star Destroyer heading towards the secret Rebel base, Luke Skywalker infiltrates the ship and asks R2D2 to connect to one of the terminals and have the ship auto-destruct in five minutes. How is that possible (meaning why can’t R2D2 do this from outside the ship) and what can the Empire do to prevent this type of attack? 

In other words (if you have no idea who R2D2 is), why does an adversary need to be at a local terminal in the server room of a company to deploy an attack? Why can’t the adversary do this remotely? And what can the company do to prevent this type of attack? 

Bullet points are okay/preferred but I am looking for a precise answer – you will lose points if you write some vague answer.

- R2D2 needs to be physically present to access the terminal, because connecting remotely would require a network connection that R2 does not have.
- The Start Destroyer likely has security measures in place such as firewalls and other access controls. These would make it more difficult to bypass remotely.
- There are also likely to be physical security measures in place, such as stormtroopers guarding the premise. Although it would be convenient to bypass this physical security, this is likely the only way to gain terminal access.

In order to prevent this type of attack, the Empire could

- Improve physical security defenses
- Implement more elaborate access controls/authentication
- System monitoring/logging to track suspicious behavior
- Employee training for those with privileged access

# Problem 3 (60 points – 10 points per Part)

Make sure to include step-by-step screenshots/details instead of just the answer

## Part 1: Crypto 

Decode the following message: 

VkdocGN5QmxlR0Z0SUhkaGN5QjFibVY0Y0dWamRHVmtiSGtnYUdGeVpBPT0=

- The message appears to be a base64-encoded string
- Using the online tool CyberChef, the decoded message reads "This exam was unexpectedly hard"

![Decrypted](/home/nick/dl/comp301/part1-crypto.png)

\newpage

## Part 2: Metadata 

Where was the picture metadata.JPG taken?  

- We can use the command `identify -verbose metadata.JPG > metadata.txt` to dump the metadata
- According to the property `Iptc4xmpCore:Location: Antartica`, the photo was taken in Antartica

## Part 3: Traffic Analysis 

Look at the wireshark.pcapng file.

a) What is the webserver software and version number installed?
    - The server is an Apache version 2.4.52 running Ubuntu
    
b) If you look at the beginning of the packet capture, you’ll see a lot of requests and “failed” responses (RST/Reset packets). What software/process do you think is running? (hint: It’s the same source and destination IP address, so take a look at the port numbers)
    - It appears that there is some sort of port scanning software running on the network, given that the process is sending packets to different ports on the system. This could be to test for open and potentially vulnerable ports.

## Part 4: Log Analysis 

Look at log2:

a) How many GET requests were there? 
    - 522 GET requests
    - We can count them by running `grep -c "GET" log2`
    
b) How many unique IP addresses accessed that website? 
    - 97 unique IPs
    - We can use the command `awk '{print $1}' log2 | sort | uniq | wc -l`
        - Note that we need to sort the output to group by duplicates
        
c) Whose website does this log belong to?
    - The website is https://chantin.cs.luc.edu/~chantin/
    - We can see multiple requests to the /~chantin part of the url

## Part 5: OSINT

Which university did Eric graduate from?

    - B.A. from Macalester College and Ph.D. from the University of Minnesota.

## Part 6: Forensics

Find the flag is the picture forensics.jpg. The flag starts with COMP301

- The flag is COMP301-MEOW-401
- Using Foremost, we find a jpg and png attached
    - The png reads "One does not simply walk away and leave their computer unlocked"
    - The jpg has the flag!
