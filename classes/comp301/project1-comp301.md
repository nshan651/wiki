---
title: "COMP 301: Project 1"
author: "Nick Shannon"
date: "2023-02-18"
---

# Project 1

### To begin, here is the project directory I use and the aliases of the three accounts:

```sh
#!/bin/sh

# TTP => nshannon
# Client(c) => nshannon2
# Server(s) => nshannon3

PROJ_DIR=/usr/local/src/project1/project1-nshannon
```

### Generate keypairs for each of the users
 
```sh
# ===== Generate Key Pairs =====
# SSH into server
ssh nshannon@192.168.101.200
cd $PROJ_DIR
# Generates and exports keys
gpg --full-generate-key
gpg --export -a “ttp-nick” > ttp-nick.pub
# Server
ssh nshannon3@192.168.101.200
gpg --full-generate-key
gpg --export -a “ttp-nick” > ttp-nick.pub
# Client
ssh nshannon2@192.168.101.200
gpg --full-generate-key
gpg --export -a “ttp-nick” > ttp-nick.pub
```

### Import public key of TTP for C

```sh 
ssh nshannon2@192.168.101.200
gpg --import $PROJ_DIR/ttp.pub
```

### TTP Signs public key for S
 
```sh
ssh nshannon@192.168.101.200
gpg --import $PROJ_DIR/server.pub
gpg --list-keys
gpg --edit-key server-nick
# trust (4)
gpg --sign-key server-nick
```

### Download signed key of S
 
```sh
# Login to client
ssh nshannon2@192.168.101.200
# import server pub
gpg --import $PROJ_DIR/server.pub
gpg --edit-key server.pub
gpg --verify server.pub
cd ~/project1
echo "Hello" > hello.txt
gpg --encrypt --sign --armor –-recipient server@luc.edu hello.txt
mv hello.txt.asc $PROJ_DIR
```

### S decrypts message and sends back a list of supported algos

```sh
ssh nshannon3@192.168.101.200
cd ~/project1
cp $PROJ_DIR/hello.txt.asc ~/project1/
gpg --decrypt hello-nick.txt.asc > hello-nick.txt
# Select algorithms and append to file
echo "AES-128-CBC" >> hello-nick.txt
# Re-encrypt it
gpg --encrypt --sign --armor –-recipient client@luc.edu hello-nick.txt
```

### C selects one of these algorithms, uses symm key, and makes hash
 
```sh
ssh nshannon2@192.168.101.200
cd ~/project1
# Create symmetric key (16 bytes because it is a 128 bit algo
openssl rand -hex 16 > key-nick.txt
# Encrypt 
openssl enc -aes-128-cbc -e -in ready-nick.txt -out ready-nick.enc -K $(cat key-nick.txt) -iv $(cat key-nick.txt)
# Create hash
openssl dgst -sha256 ready-nick.txt > ready-nick.hash
cp ready-nick* key-nick.txt $PROJ_DIR
```

### S verifies and sends their own response

```sh
ssh nshannon3@192.168.101.200
cd ~/project1
cp $PROJ_DIR/ready-nick* $PROJ_DIR/key-nick.txt .
# Decrypt
openssl aes-128-cbc -d -in ready-nick.enc -out ready-nick.txt -K $(cat key-nick.txt) -iv $(cat key-nick.txt)
echo "Ready 2!" > ready2-nick.txt
# Encrypt the new message and create a new hash
openssl enc -aes-128-cbc -e -in ready2-nick.txt -out ready2-nick.enc -K $(cat key-nick.txt) -iv $(cat key-nick.txt)
openssl dgst -sha256 ready2-nick.txt > ready2-nick.hash
cp ready2-nick* $PROJ_DIR
```

### C verifies and decrypts

```sh
ssh nshannon2@192.168.101.200
cd ~/project1
cp $PROJ_DIR/ready2-nick* .
openssl aes-128-cbc -d -in ready2-nick.enc -out ready2-nick.txt -K $(cat key-nick.txt) -iv $(cat key-nick.txt)
# Move all files to proj_dir
mv "*nick*" $PROJ_DIR
```

# Screenshots

![keygen](/home/nick/Downloads/gpg-keygen.png)
![import](/home/nick/Downloads/gpg-import.png)
![encrypt](/home/nick/Downloads/gpg-encrypt.png)
![ready1-1](/home/nick/Downloads/ready1-1.png)
![ready1-2](/home/nick/Downloads/ready1-2.png)
![ready2](/home/nick/Downloads/ready2.png)
