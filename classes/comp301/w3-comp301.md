# COMP 301 W3

## Encryption Algorithms

- Types:
    - MD4 (broken)
    - MD5 (broken)
    - SHA-1 (broken)
    - SHA-2/SHA-256
    - Whirlpool
    - SHA-3 (new)
- Hashing vs encryption:
    - Faster than encryption
    - Cannot reverse a hash
    - Don't have to deal with import/export laws as with encryption
- HMAC:
    - used in SSL/TLS
    - Https

## Additional Security Measures

- Salting:
    - hash(salt || password)
    - Way of changing the hash
    - Prevents hackers who breach an enterprise to reverse the passwords and breach the db

## Digital Signatures

- Digital signatures:
- Secret key is not a signature:
    - Why? Because the secret key can be stolen (even though hacker doesn't know the hash function
- Request is sent, then it is signed and returned
- You are only signing the hash, not the data itself 
- Private key encrypts hash by being signed
- Public key of the other party decrypts and verifies the signature
- Digital Signature Standard (DSS)
    - Used as part of the Digital Signature Algorithm (DSA)
