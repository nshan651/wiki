# COMP 301 Index

* Zoom: https://luc.zoom.us/j/84355077350?pwd=YmYvTkJZNVRHN0wxSmRsUE5IZVBtdz09&from=addon

## Notes

* [Week 1](w1-comp301)
* [Week 2](w2-comp301)
* [Week 3](w3-comp301)

## Labs

- [Lab 2](lab2-comp301)
- [Lab 3](lab3-comp301)
- [Lab 4](lab4-comp301)
- [Lab 5](lab5-comp301)
- [Lab 11](lab11-comp301)

## Exams

- [Exam 1](exam1-comp301)
- [Exam 2](exam2-comp301)
- [Exam 2 EC](examEC-comp301)

## Projects

- [Project 1](project1-comp301)
- [Project 2](project2-comp301)
- [Project 3](project3-comp301)

## Misc

- [Tiger Team](tiger-team)
- [SQL Notes](sql-notes)
