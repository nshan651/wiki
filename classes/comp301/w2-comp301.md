# COMP 301 W2

## Crypto 101

* Creates a secsure channel between two entities
* NO information leakage
* Algorithm is done in public:
    * No need for security by obscurity
    * The key **used** is secret, but the set of all keys can be public

## Ciphers

* Transpositional ciphers:
    * Permute the characters
* Substitution ciphers
    * Replace each character with a different
* Product cipher:
    * Applies above multiple times for further complexity 

## Terminology

* TODO: Add terms from slides

## Cryptosystem

* Fast to encrypt/decrypt
* Low overhead
* Hard to break

## Stream vs Block Cipher

* Block Cipher (64,128,256, ++ bit):
    * Converts plain text into cipher text by taking plain texts block at a time
    * Simple complexity
    * Reverse encryption is hard
    * Slow
* Stream cipher (8 bit):
    * Converts plain text into cipher text by taking 1 byte at a time
    * Fast and easy to reverse
    * Much more complex
    * Uses 8 bits

## Attacks

* Goal: find key K
* Ciphertext-only attack:
    * Only know ciphertext
    * Very difficult, requires lots of compute power
* Know-plaintext attack:
        * Atleast one pair of plaintext/ciphertext pairing
* Chosen-ciphertext attack:
    * Can input some plaintext to obtain ciphertext

## Cyptanalysis

* Mathematics:
    * Discrete log problem
* Statistics:
    * Letter e is the most common in English language
* Brute-force:
    * Try every possible combination

## OTP

* One time password (OTP):
    * Random key as long as sent message
    * Cipher = key XOR message
    * Provably unbreakable and secure
        * This is because every time a new message is sent, a different key is generated
* Caveats:
    * Random key must be as long as message you want to send
    * Random key cannot be reused
    * Hard to generate a completely random key everytime

## Hashing

* Also known as message digests and one-way transformations
* Relies on the following:
    * Computing hash of a message is easy
    * Given hashed val, no way to determine the original message
    * Very difficult to find two messages with the same hash
* Can be used for integrity
* Hash is stored with a **salt**, or key
* This will ensure that passwords can be stored safely

## XOR

* AKA bitwise exclusive OR
* Bit-based operation over two binary numbers
* Reversible operation as long as you have one of the original numbers
