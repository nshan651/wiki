---
title: "COMP 301 Exam 2 EC"
author: "Nick Shannon"
date: 2023-05-02
---

1. What is a type of attack in which an attacker intercepts communication between two parties in 
order to steal information? 
    - [X] Man-in-the-middle attack 
    - [ ] Denial of Service attack 
    - [ ] SQL injection attack 
    - [ ] Social engineering attack
2. What is a type of attack that floods a network with traffic in order to make it unavailable to legitimate users? 
    - [ ] Man-in-the-middle attack 
    - [X] Denial of Service attack 
    - [ ] SQL injection attack 
    - [ ] Social engineering attack
3. What is multi-factor authentication? 
    - [X] A method of authentication that uses multiple forms of identification 
    - [ ] A method of authentication that uses only one form of identification 
    - [ ] A method of authentication that requires a password and a PIN 
    - [ ] None of the above
4. What is a firewall? 
    - [ ] A physical barrier that blocks unauthorized access to a network 
    - [X] A software program that monitors and controls incoming and outgoing network traffic 
    - [ ] A device that encrypts network traffic to protect it from interception 
    - [ ] None of the above

5. What is a type of attack in which an attacker tricks a user into revealing sensitive information?
    - [ ] Man-in-the-middle attack 
    - [ ] Denial of Service attack 
    - [X] Phishing attack 
    - [ ] Social engineering attack

6. What is encryption? 
    - [X] The process of converting plain text into a code to prevent unauthorized access 
    - [ ] The process of converting code into plain text to make it readable 
    - [ ] The process of hiding information in plain sight 
    - [ ] None of the above

7. What is a vulnerability assessment? 
    - [X] The process of identifying and prioritizing vulnerabilities in a system 
    - [ ] The process of fixing vulnerabilities in a system 
    - [ ] The process of testing a system's performance
    - [ ] None of the above

8. What is social engineering? 
    - [ ] The process of exploiting vulnerabilities in software 
    - [X] The process of tricking people into revealing sensitive information 
    - [ ] The process of intercepting network traffic 
    - [ ] None of the above

9. What is the principle of least privilege? 
    - [X] The principle that a user should have only the necessary access to perform their job functions
    - [ ] The principle that a user should have access to all system resources 
    - [ ] The principle that a user should have access to more resources than they need
    - [ ] None of the above

10. What is the process of making sure that a message has not been altered or tampered with in transit? 
    - [ ] Confidentiality 
    - [X] Integrity 
    - [ ] Availability 
    - [ ] None of the above

11. What is the difference between symmetric and asymmetric encryption?
    - [X] Symmetric encryption uses one key for both encryption and decryption, while asymmetric encryption uses two keys. 
    - [ ] Symmetric encryption uses two keys for encryption and decryption, while asymmetric encryption uses one key. 
    - [ ] Symmetric encryption and asymmetric encryption are the same thing.
    - [ ] None of the above.
