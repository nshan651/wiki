---
title: "Comp 301: Exam 1"
author: "Nick Shannon"
date: 2023-02-24
---

# Signature

Name: __Nicolas Shannon________________________

Date  : 2023-02-24____________________________________________

By signing below, I agree to the following.
 
0) I may not copy or distribute this exam to anyone.
1) I may not discuss this exam with other students. I may not seek assistance from anyone 
(except the instructor and TA). I can only access Sakai, Panopto, lecture notes. I can use the 
Internet to help understand some things, but I cannot look for the answer.
2) This exam is open-book, open-notes, open-computer. However, I may not look for answers on 
the Internet or other similar media (class notes not included).
3) If I violate any part of this agreement, I will receive a 0 in this exam. Violations may also be 
reported to the Dean of Students.
4) If I am stuck on a challenge, I will ask the instructor for help. I will not ask other students, 
bots, the Internet (such as but not limited to stackoverflow, reddit, chegg, etc.) or look for 
answers on the Internet. (Remember: the instructor can search for questions as well!
answers on the Internet. (Remember: the instructor can search for questions as well!
5) Submit in PDF
Signature or Initials: N.S.____________________________________________

# Problem 1

- 1.1 Why should you not use ECB mode? 
    - [X] a) Same plaintext encrypt to same ciphertext
    - [ ] b) Tux (Linux Penguin) is visible
    - [ ] c) Same plaintext encrypt to different ciphertext
    - [ ] d) Different plaintext encrypt to same ciphertext
- 1.2 Signature is encryption with the private key.
    - [ ] a) True
    - [X] b) False
- 1.3 Should your public key be made? 
    - [ ] a) private
    - [X] b) public
    - [ ] c) trusted
    - [ ] d) revoked
- 1.4 Is it okay to re-use the same symmetric key later on in a future session?
    - [ ] a) Yes
    - [X] b) No
- 1.5 Look at the following snippet of code. Assume the code compiles
```c
char[] func(int sizeString, String s) {
    char arrayCharacters[] = new char[sizeString];
    for (int i = 0; i < s.length(); i++) {
        arrayCharacters[i] = s.get(i);
    }
    return arrayCharacters;
}
char[] characters = func(5, “hello”);
```
- Which of the following statement is correct?
    - [ ] a) Code is secure
    - [X] b) Code is insecure as there is a vulnerability

# Problem 2

- Can a system provide confidentiality without integrity? Justify your answer or provide an example.
    - Yes, A system can provide confidentially without integrity by encrypting the data to prevent unauthorized access, but not verifying the integrity of the data to ensure that it has not been tampered with.
    - For example, a medical database could encrypt patient records to provide confidentiality, but that encrypted data could still be modified if the system does not have the necessary checks in place to ensure data integrity.

# Problem 3

Sam and Frodo are disguised as orcs. To prevent them from fighting each other, they need a mechanism to determine that they are close to each other. They agree to use a shared key K and a keyed hash algorithm to use in a protocol as follows:
 
- Frodo → Sam: m and n, where n = HASH_K(m)
- Sam: Receives m and n; checks that HASH_K(m) = n. If equal, then the other party is Frodo.
- Sam → Frodo: p = HASH_K(n)
- Frodo: Receives p; checks that HASH_K(HASH_K(m)) = p. If equal, then the other party is Sam.
- The main idea is that if only Sam and Frodo knows secret key K, only they will be able to calculate a correct HASH.
- After a while, Sam and Frodo discover that sometimes Sauron is able to fool Frodo into believing he is Sam. Assuming that Sauron cannot use any out-of-this-world attacks such as magic, what is a simple way for him to impersonate Sam?

In this example, Sauron can perform a man-in-the-middle attack where he intercepts the messages exchanged between Sam and Frodo and modifies them in a way that allows him to impersonate either one of them. In order to address this vulnerability and successfully deliver the One Ring to Mt. Doom, Sam and Frodo can use a mechanism such as a public-key infrastructure (PKI) in order to provide additional assurance that the message was genuinely from the other party.

# Problem 4

- SSH to the Lab2/Project1 server: 192.168.101.200
- What is the plaintext for the message at /usr/local/src/Exam1/Problem4.enc? You know the key used starts with 0000000000000000000000000000000 (that is 31 zeroes) and the algorithm is aes-128-ecb
    - It is not possible to decrypt the message because it cannot be verified. I tried every possible hex value as the final 32nd digit using a shell script, but none worked.

```sh
#!/bin/bash

# Set the input and output file names
input_file="Problem4.enc"
output_file="Problem4.txt"

# Define the key prefix (31 zeroes)
key_prefix="0000000000000000000000000000000"

# Loop through every possible hex digit for the final digit of the key
for digit in {0..9} {a..f}; do
  # Generate the full key by appending the current digit to the prefix
  key="$key_prefix$digit"
  # Attempt to decrypt the file using the current key
  openssl aes-128-ecb -d -in "$input_file" -out "$output_file" -K "$key" >/dev/null 2>&1
  # Check if the decryption was successful
  if [ $? -eq 0 ]; then
    echo "Decryption successful with key: $key"
    exit 0
  fi
done

# If no key was successful, print an error message
echo "Unable to decrypt file with any possible key."
exit 1
```

- To verify the integrity of Problem4.enc, I am telling you that the SHA-256 value is 808b8fe9245e0eca09434671a5244d9669c748660907f0f2a7c3587a0325a288.
    - Is that correct? Why or why not?
    - No. The hash after running `openssl dgst -sha256 Problem4.enc`is `SHA256(Problem4.enc)= f7ddd9df479ae964c5cfdbee864a01f86b0e273adffebeb152c72f01fccdd750`. This does not match the given hash, so the message integrity cannot be verified. 
- You should have my public key from Lab 2. Encrypt (do NOT sign), using my public key, the following message “This exam took me X minutes to complete”, replace X with your own number. Copy the encrypted message to ~/Exam1/<USERNAME>/Problem4.txt.gpg
- Make sure you are logged in to your original account from Lab2, not the other 2 accounts created.
