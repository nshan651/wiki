---
title: "COMP 301: Project 2"
author: "Nick Shannon"
date: 2023-03-28
---

# One.pcapng

- What is the IP and MAC address of the client? (hex in colon-separated format)
    - The client is the address making the requests, in this case they are sending information about their username and password.
    - IP is 192.168.100.111
    - MAC is 00:0c:29:1e:7a:da
- What is the IP and MAC address of the server? (hex in colon-separated format):
    - The server is the address responding with the information. In this case, the server is responding with the results of the login info specified by the client.
    - IP is 192.168.100.70
    - MAC is 00:0c:29:41:bc:8b
- What protocol is being used? (not IP or TCP... name the application):
    - The user is using File Transfer Protocol (FTP) to interact with what is presumably a remote VMware server. We can see in packet numbers 14-16, the user asks what kind of system is used, to which the server responds "UNIX Type: L8"
- What was the directory that was created?:
    - /home/JohnSmith/Passwords
- Attach a screenshot of the picture downloaded from the server.:
    - To get the image, we can use the filter `ftp-data`
    - Then we right click the packet containing the image, and select Follow->TCP Stream
    - Finally, we can view the contents as raw binary and save as a jpg to inspect the image 
    
    ![Loyola](/home/nick/dox/wiki/classes/comp301/loyola.jpg)
    ![Decoded](/home/nick/dox/wiki/classes/comp301/decoded.png)
 
- What are the usernames?:
    - The usernames are: Alex, Abraham, Armin, Alejandro
    
# Two.pcapng

- What protocol is being used? (not IP or TCP ... name the application):
    - The protocol being used is TELNET
- What is the username used to login to the server?:
    - JohnSmith
- What is the associated password for that username?:
    - password123
- What time did the user log in to the server?":
    - The user logged in on Wednesday, March 25 , 07:56:54 UTC 2020

# Three.pcapng

- There is a DNS lookup to a book publisher – what is the domain name?:
    - home.pearsonvue.com
- What is the IP address of the local webserver?:
    - The local webserver is 192.168.100.120
    - We know that the client is 192.168.100.70 because that is where the requests are coming from. If we search "http.host" and look at a list of hosts, we can see that some of them include ocsp (cert authority), sakai, and detectportal.firefox. However, the only host that appears to resolve as an ip is the aforementioned addres, so this must be the local webserver.
- What is the audio file downloaded?:
    - We can use the search feature (set to string) to look for file extensions like .mp3 and .wav
    - The audio file is: NeverGiveUp.mp3 (RickRolled!)
- There are two redirections happening – what are they redirecting to?:
    - We can find this by using the filter `http.response.code >= 300 && http.response.code <= 399`
    - One of the redirections is to https://www.google.com/gmail/ and the other is to https://sakai.luc.edu
- There is a famous picture downloaded – what is it?:
    - It is a picture of Baby Yoda (babyyoda.jpg)
    
    ![Baby Yoda](/home/nick/dox/wiki/classes/comp301/babyyoda.jpg)
    
