# COMP 363 Week 7 Notes

## Memoization

* Abstract way to separate and re-use computations 

* Especially useful with recursive sequences
    - Ex.) Pathfinding: eliminate redundant visits to already-visited "spaces" 

* Implementation in JS
    - Create a wrapper function that gives a temporary cache for re-use

## Divide & Conquer

* Base case should be as simple as possible 

* Divide until base case is reached

* Great practical example: tiling
    - Basically finds greatest common divisor of a square/rectangular plot
        -- Idea: animated program based on this...? 
    - Probably used in window managers with snap-to or tiling feature

* Another use of memoization is in calculating exponents
    - Skip even powers

## Quick Sort

* Faster than selection sort 

* Uses a pattern of divide and conquer

* May define base cases for sorting, i.e. zero or one elements in the array

* Quicksort algorithm:
    1. Define *pivot*, and sort the sub-arrays
    2. All elements less than pivot are moved before, greater are moved after   
    3. Recursively call quicksort on these two sub-arrays (defining a new pivot for each), repeat until fully-sorted

* Although the pivot can be any index, the median element is the most common choice


