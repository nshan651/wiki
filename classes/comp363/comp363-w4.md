# COMP 363 - Week 4 Notes

## Systems & Memory

* Necessary to consider how an OS and application handle and use memory

* Memory management can be considered broadly as follows:
    - Ensure each process has enough memory
    - Different memory types must be organized efficiently
    - Start by managing memory boundaries for different processes
        - Processes are a fundamental component of Unix-based operating systems

## Process & Memory Usage

* Child processes are assigned an address memory space

* Each process will see their memory space as a contiguous logical unit

* Such memory addresses might actually be separate across a system
    - disparate addressed memory spaces for each program

* Separate memory stores and addresses organized into a contiguous group per process

* Benefit is efficient use of memory space

* No need for pre-assigned large chunks of memory

* The **kernel** controls access for a process to memory addresses
    - Controlling conversion of assigned virtual addresses
    - Converts to a physical address in the system's hardware memory

## States of a Process

* Each child process may have a related state

* State may be monitored by the kernel

* Kernel may then switch processes relative to an update in a process' state

## Process Manager

* Manages processes in a system; controlled by the system's kernel
    - Process creation and termination
    - Resource allocation and protection
    - Cooperation with device manager to implement I/O
    - Implementation of address space
    - Process scheduling

* States and processes operate in a similar fashion to asyncronous programming
    - Promises/change in state

* States:
    - Hold - holds the state in limbo until it receives a signal to continue
    - Ready - running state
    - Finish - finished process

* OS design is where many CS paradigms are derived from

## Process Scheduler

* The kernel chooses the next process to run on the CPU
    - Much of the computation nowadays is outsourced to the GPU

* Context switch 
    - Informed by the required and available process properties
    - Also determined by the nature of the process

* Example algorithms included:
    - First-come, first-serve
        - Non-preemptive
        - Insufficient for an interactive system with multiple jobs to handle
    - Shortest job next
        - Non-preemptive
    - Priority scheduling
        - Non-preemptive
    - Round robin
        - Pre-determines time slices (time quantums) that each process can use to complete processes
        - When time expired, they are reassigned until they recieve a new quantum
    - Multi-level priority queue (min heap)

* Common to assign a *front-facing* process over one running in the background

* Common issues:
    - Deadlock: occurs when no processes are able to run
    - Livelock: occurs when multiple processes compete to heavily over shared resources
    - Starvation: one process gets all the resources and the others have to wait

* Tightly vs loosely coupled code
    - First is very dependent on many other pieces of code
        - Also only works within a specific context
    - Loosely coupled is more like a stand-alone module

* The OS can be broken up into loosely coupled modules

## Design Patterns to Look Up 

* Observer

* Pub sub

* Proxy pattern

* Collation
    - Assembly of written info in a standard order

## JavaScript Language Semantics

* Arrays are built into JS collections
    - Exist as both built-in Array constructors and literals
    - Literals are more common

* Arrays in JS are mutable by default 
    - Dynamically sizeable

* If we delete an array item, we leave a hole at the position of type undefined

* Splice method
    - Removes a single item at position n
    - Also returns an array of deleted items

* Better to use than basic insert/delete

* Iterators in JS
    - ForEach loop

* Array mapping
    - Create a new array based on the items in an existing array
    - Creates a Map data structure (akin to python dictionaries)

* JS has a Map collection in addition to implicit array mapping 
    - Slightly different implementation

* Comparators in JS
    - Can subtract one value from the other and use the sign as a proxy for conditional logic
    - Also can use string compare method, does a similar operation

* Collation
    - Google docs example with many people

* Built-in search method called *find*

* Use *filter* to return all matches in an array

* Indexing
    - indexOf
    - lastIndexOf
    - findIndex

## Recursion 

* Recursion is another way to perform complex operations
    - There are times where recursion is a simpler solution
    - Can be slower than iteration

* Different kinds of recursion

* Common application of recursion
    - Parsing json data of n depth
        - Extremely useful when you are uncertain of the depth
    - Also useful for working with linked lists

* 
