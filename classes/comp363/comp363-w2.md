# COMP 363 - Week 2 Notes

* Algorithm: All code that accepts input and returns an output is considered an algorithm

* Imperative programming
    - Describes computation in terms of instructions that change the program/data state
    - Common example with JavaScriptand direct DOM manipulation
        - DOM (Document Object Model) allows us to interface directly with the underlying HTML and XML documents

* Declarative programming
    - Specifies what ht eprogram should accomplish without describing *how* to use it
    - ex) React JS

## Specification, verification, and performance analysis

* Specification
    - Formalize essential details
    - May be based on representation of associatied data
    - Customarily need to define relationship between inputs and outputs of the algorithm
    - For simple problems, it will be easy to determine whether a particular algorithm will always/never work

* Verification
    - The fact that an algorithm satisfies its specification may not be as obvious
    - Need to consider formal verification
    - testing on a few particular inputs *may* be enough to show that the algorithm is incorrect

* Performance 
    - Efficiency or performance of a given algorithm may relate to the defined resources it requires
    - Commonly depends on input size
    - Commonly acted as a useful driving force for development of new data structures and algorithms
    - Efficiency will be considered in more detail in the course

* 
