# COMP 363 - Week 3 Notes 

## Testing & Optimizing Algorithms 

* One way is to customize the data structure
    - Use the one that suits the problem

* Standard libraries must cover as many use cases as possible
    - Sometimes it is necessary to customize to suit specific needs

## Linked lists and arrays 

* For linked lists, we need to know addressable location in memory to retrieve item at position n

* Most useful when we need to access one item at a time

|           | arrays | lists
| reading   | O(1)   | O(n) 
| insertion | O(n)   | O(1)

## Linked list JS example 

* To delete a node at position n for a singly linked list
    - Must keep track of previous node as well as current, so that we can connect prev to next after deletion

```
if ( (this(head) === null) || (index < 0) {
    // Throw an error
    // Note that '===' is the strict equals (checks both value AND type)
}
```

* Promises in JS
    - A promise of some future return
    - Concept that is commonly used in asynchronous programming

* In JS, an object is not iterable by default
    - Must specify a custom iterator using JS *Symbol.iterator*
    

## Features of JS 

* Language constructs
    - Data consumers
    - Data sources

* Data consumers
    - for-of loops over values
    - spread() operator inserts values into Arrays or function calls
    - May consume values from a variety of data sources

* Iterators vs iterables 
    - Iterable is an object that can be iterated over

* Standard built-in traversal operations
    - destructuring via an array pattern
    - for-of loop
    - Array.from()
    - Spread operator

* Other options
    - Set and Map constructors
    - Default iterables provided by Promise methods
        - Promise.all
        - Promise.race
        - yield*for generator iterables

* JS custom iterator 
    - Generator allows for a "stepped" traversal
    ```
    *[Symbl.iterator]() {
        let current = this(head)
        while current !=== null) {
            // Pause and resume the generator
            yield current.data;
            current = current.next;
        }
    }
    ```
* Benefits of custom structure
    - Easy to test/connect testing pipeline
    - Easy to add features

* Length of the linked list
    - Once it is known, it becomes statically stored in cache
    - Very simple example of memoization
        - Memoization is when calculations are stored in an intermediary variable
        - Reduces unnecessary computation
    - Makes sense to store it in head/tail (because it is constant time to access then)

* Arrays are common due to their support for easy *random* access of data items

* Random vs sequential access

## Snake game

* "Snake" is a linked list of nodes
* When green square is reached, that becomes the head
* Each node contains coordinates as data points
* When two given nodes' coordinates are equal, a collision has occured

