# COMP 363 Week 8 Notes

## Symbol Tables

* Key-value pairing

* Associative array abstraction
    - Associate ine value with each key

## Binary Search Tree

* Non-linear insertion

* Similar to a doubly-linked list in nature

* Left, right, and data pointers

* Unique in that it orders nodes by value

* Problem: balanced vs unbalanced bst
    - Unbalanced makes the search and insert times linear

* Tree traversal
    - To determine size 
    - May need to flatten the tree to an array or map

* traversal
    - inorder: left, root, right
    - preorder:
    - postorder:

* Inorder only works when left and right links actually exist
    - Designed to reduce null node references to a bare minimum
   
* Height vs Depth
    - Root has max height, 0 Depth
    - Leaves have min height, max depth (or max-1)
    - *Height is min leaf to node*
    - *Depth is root to node*

* Deletion can get complicated for a BST
    - This is due to the necessity of balancing the underlying tree

* Balanced BST
    - Self balancing to logorithmic height

* Worst-case is O(log(n)) for all operations

* Also monitor the parent node as passed node will need to be removed from its parent

* Tree rotations
    - Tree invariant is a property you impose that it must meet after every operation
    - Tree rotations are what ensure the above to be satisfied

* In-order predecessor: value before deleted node

* In-order successor: value after deleted node


