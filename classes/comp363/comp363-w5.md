# COMP 363 - Week 5 

## Stacks

*segue -> seg*

* Stacks commonly represented as a modified, restricted array

* Data may only be added/removed from the top of the structure

* LIFO

* Can lead to stale data
    - Data that sits in memory without being used (stuck at the bottom)

* Push/pop

## The call stack

* Maintained inside PC's RAM by the OS

* Data from one procedure to another
    - Each new procedure receives the former's paramters, return address, and local variables

* Cautious when trying to modify the behavior of the call stack
    - Usually a separate stack structure is maintained

* When the top procedure is finished, it returns control to the previous procedure

* Key to understand how a particular language uses its call stack 
    - Important to know the underlying structure of the programming language

* Call stack records and checks order of execution

* Segmanted memory allocation (SMA)
    
* With recursion, it is guaranteed to have the same scope (within the function)

* Ideally, variables are only *live* within scope 

* Stack provides context for ordered execution of code

* Interpreted vs compiled

* System architecture
    - How they process memory
    - Memory management

* Call stack
    - Check to see how they are implemented
    - How they are executed
    - Event management

* Event-driven architecture

## Server-side considerations

* Node.js
    - Useful for speed
    - Uses an event-based architecture, rather than common thread-based model
    - All I/O is asyncronous
