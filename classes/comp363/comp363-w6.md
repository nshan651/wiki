# COMP 363 Week 6 Notes

## Quiz 

1. Which of the following is a consideration in determining the efficiency or performance of a defined algorithm?
    **A**. instance size 
    B. execution clearance
    C. new data structures
    D. progression of the algorithm
    
    Answer key: **A**

## Node JS: A Conceptual Model for Processing

* Process Model:
    - client sends http request
    - Event loop then informed by the host OS
    - Long running jobs continue to run on assigned worker threads
    - Responses are sent from the non-blocking workers back to the main event loop
    - Event loop returns any results back to the client

## Non-Blocking Architecture

* (Very) Rough Diagram

    Non-Blocking worker    -<----------Client
        |                  |           |
        | _____<<_ Event Loop ___>>___ |   

* Node supports asynch by default
    - But, also supports synchronous patterns as well

* JavaScript was built to handle blocking problem

* Event-based architecture
    - Event creator -> event manager -> event consumer

## Callbacks

* Common way to pass I/O

* In JS, the *callback* will need to run when a task is completed

* Common practice in most languages is to send a standard query to a database
    - Downside to this is that the program waits for a response to continue running

* Major reason why Node.js is easily scalable for large server-driven apps

## More on the call stack

* Call stack is especially important to look at in event-driven architecture
    - Any given function can call subsequent branching events
    - Can very quickly get out of sync

* The call stack essentially functions as a **record** of function calls

* Tail call optimization (TCO)
    - Predict the bounds of necessary memory to perform the calculation, regardless of the input size
    - Tail call: last thing a function does

        --- !!con - tail recurse optimization musical

## Stack/call stack from scratch

* Known limitation: lack of access to elements *not* at the top of the stack

    --- Celine Dion

* Why don't we use tail recursion all the time...?
    - Certain cases where there are clear downsides
    - Tradeoff between predictable memory bounds (maybe can get away without t-rec), and unpredictable bounds (*should use t-rec*)

* A pragmatic implementation
    - In JS, makes sense to use an array
    - Instantiate a basic stack object through default constructor
    - Functions
        - peek, pop, push, size
    - Also include a reset function that will clear the object
        - This allows the instantiated object to be re-used

* As we develop the stack's prototype methods...
    - We can add further restrictions and controls
    - Clear definitions on uses of the DS
    - Also define what is returned, the context, usage...

* More on prototypes
    - **Not** the same as classes
    - Prototypes have more functionality/extensibility than classes

* Prototypes are a staple of functional programming

* Take advantage of prototype chaining by essentially creating an overall parent/child relationship
    - But...has the advantage of using everything in between

* Stacks are still susceptible to misuse based on the underlying array structure
    - Solution: use a proxy which restricts and controls access to the stack

* Proxy
    - Can use a construct trap, which intercepts calls
    - Another option is to use a Reflect API to define defaults for handlers
    - Every time the constructor is passed to the proxy object, it needs to go through the handler
        - The handler method can support logging, type checks, etc.
        - Anything that ensures safety before the proxy is actually useable

* If proxy cannot be instantiated, the prototype usage doesn't matter
    - Never even reaches that point

## Fibonacci in JS

* Not tail recursive, will blow up the call stack very quickly
