# COMP 339 W8

## Assignment Notes Review

- Check for the number of collisions
- Filter out "noise words" in English:
    - Short/common words
- Word (integer) count based on our bloom filter

### Word Counting

- Hamlet words, index to an array
- `v[bloom(hash)]++`
- Value will be word count
- *Make sure all values of the array are intialized to zero*
- words -> numbers -> counts 

## Notes

- Use FigShare for final project:
    - Create associated project DOI

## Tips on C++

- **spdlog** instead of `printf()`
- Consult AwesomeCPP
- Elegant I/O with fmt and scnlib (input)
- Use improved foreach syntax
- Look into unions and arrays
- Automatic varibale type inference (auto)
- Variadic templates for increased parametric code reuse
- Improved libraries for time, atomics, regex, etc.
- Threading/concurrency libraries (beyond pthreads)

### Functional Programming in C++

- lambda expressions as a way of defining anonymous functions
    - Lambda as a closure
- Use lambdas for arrays of hash functions
- Captures allow lambdas to access variables from the surrounding scope

```cpp
#include <algorithm>
#include <cmath>

void abssort(float* x, unsigned n)
/** An example of a lambda */
{
    std::sort(x, x + n,
        // Lambda begins
        [](float a, float b) {
            return (std::abs(a) < std::abs(b));
        }
    };
}
```
