# COMP 339 W5

## CLI Args

- Use CLI11 C++ lib for better args
- George's shell implementation + more on github
- Logging:
    - Checkout spdlog C++ package
    - Better than using cout or cerr
    - Can turn them on and off
- Header-only:
    - Means that there are no external dependencies to install
    - Very lightweight
- Make sure to use C++17 going forward:
    - Add the following flag to gcc
    - `g++ -std=c++17 other_args`
- Look at trapezoid integration example program
- GoogleTest unit testing
- Building CMake:
    - `cmake -S . -B build`
    - Specify source and build dirs
- If you want to change replit cmake, need to change .replit hidden file
- **Set up git with replit**
- `Shift Factor: 2 Errors: 0 rlanld dawdnal dlakwndk`
- Make encoder pipe-able into decoder and vice-versa
- Go into GoogleMVA repo given in replit projects
- Add `run:` command for in .replit to execute unix commands
- Additional CMakeLists.txt for `src` directory
- Try `transform(line.begin(), line.end(), ::toupper)`
- Use C++17 random generation based off hardware specs
