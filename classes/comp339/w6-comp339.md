# COMP 339 W6

## Sliding Word Problem

- `aspell en`:
    - 126,657 words
- Hamlet:
    - 34,927
- Make sure that every word that appears in the text is also in the dictionary:
    - Including all the old English found in Hamlet
- Dictionaries do **not** play well with GPU parallelization:
    - Arrays are a great choice
- Bit set:
    - Boolean array
- Bloom filters:
    - Use a bit set 
    - Stores stores a hash of the values rather than the elements themselves
    - Does not store the key in the bucket, simply marks it as filled
    - https://brilliant.org/wiki/bloom-filter/
    - We will use this to determine whether a word is found in the dictionary
- The goal is to find the minimum number of hash functions needed to make it work
- Competition: find the best hash function that gives the best combo of `k` and `m`
- Try using lambda functions for this:

```cpp
/* An example lambda function */
auto lambda_function = [capture-list](variables) -> return_type {
    // body...
    return body;
};
```

- Note: new project will go in the same replit directory
- Min and Max:
    - Max should have **zero** false positives
- Perform binary search on candidate hashes:
    - `(min+max)/2`
- Note: Bloom filter must be in C++, but simulation can be a script
- **Make everything parametric**
- George's word sliding program in go:
    - https://github.com/gkthiruvathukal/wordcount-sliding-go
    - Also a version in python
- Ignore list for the filter (words like "the")
- Make it case sensitive (Hamlet, hamlets, Hamlet's, etc.)
- Use a circular queue

## Summary

- Bloom filter
- Sliding word counter using the bloom filter

## Principles of Distributed Systems

- Key principles of distributed systems:
    - Self-organizing
    - Resource sharing
    - Openness
    - Concurrency
    - Scalability
    - Fault tolerance (reliability)
    - Transparency
- Keys to fault tolerance:
    - Redundancy
- Principles of transparency:
    - Access: remote and local objects to be accessed using identical operations
    - Location: location of resources hidden
    - Migration: resources can be moved without changing names
    - Replication: users cannot tell how many copies exist
    - Concurrency: multiple users can share resouces automatically
    - Parallelism: activities can happen in parallel without user knowing
    - Failure: concealment of faults
