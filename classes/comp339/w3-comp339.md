# COMP 339 W3

## Intro to Intel OneAPI AI Toolkit

- Distributed systems are usually heterogeneous, as it is generally more cost-effective
- Difficult to write code that enables access to parallel resources in a modern heterogeneous system
- Code optimization is hard due to vendor lock:
    - CUDA for NVIDIA GPUs, other libraries for ASICs, AMD, etc.
- Latency vs throughput:
    - Data can be rapidly available or have high transmission rate
    - Trade-off

## SYCL

- Industry standard super library wrapper

## Intel OneAPI

- oneAPI implements SYCL through their Data Parallel C++ compiler (DPC++)
- DPC++ is just a compiler and collection of libraries for C++ optimized for parallelization:
    - Uses LLVM and Clang

## Level Zero

- Existing and future frameworks can take advantage of oneAPI by targeting the hardware abstraction layer (lvl 0):
    - Allows you to write optimized code once and choose that device or devices to run on

### Activity: Caeser Cipher in C++

- Several options:
    - C++ Map
    - Arrays and modulo
- Assignment details:
    - Large file list of encrypted words to read from
    - Each line has a sentence with a different shift value
    - Goal is to find the shift key of each line
    - Given a dictionary of words
    - `spell` command tells you if something is spelled correctly
- Advanced tips:
    - frequency analysis -- look at the most frequently occurring characters in the cipher text
