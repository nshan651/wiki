# COMP 339 W9

## Bloom Filter

### TODO

- **Make sure to use Teams for communication**
- Use vector<bool>
- Use sqlite3
    - Run the Teams sqlite3 program and find the 8 duplicates
    - Also try it for different hashes besides md5
- Graph number of collisions instead of false probabilities
- Add `->check` to CLI++11 options for input validatation
- [Modern C++](https://unoapi.cs.luc.edu/15-modern-c%2B%2B/modern-cpp.html#fmt)
- [Code examples](https://github.com/gkthiruvathukal/unoapi-dpcpp-examples)
- Sign up for DevCloud
- Use auto, const, and constexpr
- Use fmt and spdlog
- Look into templates with C++

## UnoAPI Examples
 
- Use unsigned longs for most operations
