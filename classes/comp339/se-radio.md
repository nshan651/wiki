---
title: "COMP 339: SE-Radio Podcast"
author: "Nick Shannon"
date: 2023-04-19
---

# Episode 203: Leslie Lamport

As per your recommendation, I listened to the episode featuring Leslie Lamport, which did not disappoint. Toward the beginning of the episode, Lamport explains his Paxos algorithm, which he first proposed in 1989. It's interesting that distributed computing is modeled as a consensus-based voting system -- almost like a political system of sorts. 

One thing that struck me as odd was how he introduced Paxos in the original paper with a fictionalized story. It seems like this didn't really land -- even the interviewer was confused. I think this can work, and I love the concept of complex ideas with simple metaphors or stories, but I feel like it can feel a little frustrating and heavy-handed. In *Godel, Escher, Bach*, for example, Douglas Hofstader does something similar by beginning every chapter with a short dialogue sequence between Achilles and the Tortoise. It mostly works and some parts are genuinely hilarious, but I found myself getting annoyed by the end. I can totally see how this concept would be confusing and frustrating for academics trying to read his paper, especially if the gimmick wasn't explained upfront.

I really like Lamport's point about how the most important part of mathematics, at least for programmers, is in learning how to think precisely and rigorously. I think that as a programmer, the best thing that you can do for yourself is to focus on the essence of a complex problem and filter out all of the extra cruft. His point to "avoid any language that is all syntax and no semantics", is really illustrative of this. It's really easy to get bogged down with things like implementation details, syntactic sugar, containerization, deployment, configuration files, and a host of other worries. Many of these things are elements of the software engineering process, but they are peripheral to the problem itself.
