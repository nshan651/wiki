# COMP 339 W7

## Notes

- Mark collisions when they occur
- Benchmark should be zero collisions
- Use lambdas to create a list of hash functions
- Try to do it with just 2 hash functions

## Bloom Filter

- Get it to an integer program:
    - List of words -> List of integers on integer program
- `list of words -> list of integers -> list of counts`

## Distributed Networking

- Naming
- Name resolution:
    - "Resolved" when it is translated into a form to be used to invoke an action on the resource
    - i.e) internet communication id
- Name space for each type of resource:
    - Must be resolvable to communicaton ids
    - Frequently accessed resources are resolved by a resource manager for efficiency
- Unix filesystem:
    - Hierarchical name space - each part is resolved relative to current context

## Communication

- Communcation normally involved - transfer of data from sender to receiver
- Synchronous or blocking - sender waits for receiver to execute a receive operation
- Asynchronous or non-blocking

## Types of Communication

- Client-server:
    - Message passing operations
    - Remote procedure call (RPC)
- Group multicast:
    - An important primitive for distributed systems
- Function shipping:
    - Fallen out of favor. Running code on the server poses some security issues
- Performance of distributed systems depends critically on comm. performance!
