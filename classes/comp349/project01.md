# Project 01: SigFox

## Notes

* Network packet - formatted unit of data carried by a packet-switching network
    * Consists of control information and user data (payload)
* Frame - single data transmission unit in networking
    * Acts as a simple container for a single network packet

### SigFox

* Founded (2010) by Ludovic Le Moan and Christophe Fourtet
* A low power, long range global IoT network offering end-to-end connectivity
* Useful for connecting to low power objects such as wearable devices and electronic meters. 
* Uses radio tech based on Ultra Narrow-Band (UNB) which communicates on unlicensed ISM bands
* Usecase specs
    * 140 msgs per object per day
    * Payload size for each msg is 12 bytes
    * Wireless throughput up to 100 bits per second
* A base station can handle 1 mil connected objects; can be scaled to support more by increasing density of base stations
* Base station density: 30-50km rural, 3-10km urban
    * Travels much further in line of sight
* Pilot testing roll-out in the Czech Republic

### Core Principles of the Standard

> 1. The provision of a minimal channel for transferring small messages, that would complement other communication protocols (WiFi, Bluetooth, Satellite, 3G, 4G, 5G, ADSL, Fibre…)
> 2. Setting up a back-up channel in case the main communication links fail, or in cases where a network goes down after a natural disaster or malicious act
> 3. Increasing and guaranteeing the security of networks and exchanges, to stabilize our digitalised economy
> 4. Simplifying access to different networks to increase adoption
> 5. Reducing energy consumption linked to telecommunications networks

### Use cases

* Wearables, asset tracking (Dregvaite, 2016, p. 680), electric meters
* Majority is in M2M sensors

### Other

* SigFox wants to be the premier global LoT network as the core behind every device.
* This means that all data collected on the network will come back to SigFox servers
* Super risky. Would be very bad if thargument for SigFoxey disappeared. Also a huge privacy concern

#### Citations & Links

Fascinating blog: https://www.survivingwithandroid.com/sigfox-protocol-network-architecture-iot-protocol-stack/
https://www.survivingwithandroid.com/sigfox-protocol-network-architecture-iot-protocol-stack/

https://www.sigfox.com/en/news/sigfox-presents-2017-results-and-2018-roadmap
https://www.sigfox.com/en/sigfox-story
https://www.sigfox.com/en/coverage
https://techcrunch.com/2022/01/27/sigfox-the-french-iot-startup-that-had-raised-more-than-300m-files-for-bankruptcy-protection-as-it-seeks-a-buyer/
https://circuitdigest.com/article/what-is-sigfox-basics-architecture-and-security-features
Duty Cycle: https://csrc.nist.gov/glossary/term/Duty_Cycle
High Demand for Tech: https://www.datacenterdynamics.com/en/news/sigfox-nine-companies-in-running-to-buy-iot-firm/
Buyout by UnaBiz: https://www.mobileworldlive.com/featured-content/top-three/unabiz-brings-sigfox-into-the-fold
High standby time: https://www.rfwireless-world.com/Terminology/SIGFOX-technology-basics.html

 Al, Khaldoun, et. al. "Mobile and Wireless Networks." P. 241. Wiley & Sons, Incorporated, John, 2016.
 Augustin, Aloÿs, et. al. "A Study of LoRa: Long Range &amp; Low Power Networks for the Internet
	 of Things." MDPI AG, 2016.
 Dregvaite, Giedre, and Damasevicius, Robertas. "Information and Software Technologies."
	 Springer, 2016.
