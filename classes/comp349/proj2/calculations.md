# COMP 349 Project 02

## Remote Facility Calculations

### Antenna Height

* Given:
    - $d_1=4.3$ mi, $d_2=5.2$ mi, and $D=9.5$ mi
    - Height of obstruction ($H_3$) is $38$'
    - Maximum right tower (plus building) height is $100$', max right tower height is $185$'
* Assumptions:
    - We will use $5.0$ GHz as the frequency
    - Add an $10$' to antenna height as an additional buffer
* Calculations:
    - Freznel Zone (FZ1)$=H_1=0.6 \cdot 72.1 \cdot \sqrt{\frac{4.3\cdot5.2}{(4.3+5.2)\cdot5}}=29.6808$'
    - Curvature of the Earth$=H_2=\frac{(5.2\cdot 2)^2}{6}=18.0267$'
    - $H_3=38$'
    - $H_4=10$'
    - Height of antenna$=H_1+H_2+H_3+H_4=95.7075$'

### Antenna Length 

* The optimal transmission and conversion efficiency is achieved when the antenna length is $1/4$ the wavelength. [1],[2]
* $\lambda=c/f$
    - Where $\lambda=$ Wavelength (m), $c=$ Speed of light (m/s), and $f=$ Frequency
* $\lambda=299,792,458/5.0=0.06$m$=0.1970$'
* Antenna length$=(1/4)\cdot0.1970=0.04925$'

