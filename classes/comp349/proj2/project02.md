# COMP 349 Project 02

## Connection Type

I chose to use the 802.11n protocol because of its long indoor range and high data stream rate. I chose to go with a bandwidth of 40MHz, the highest that 802.11n supports. The company requires that the connection be at least 100Mbps, but would like to scale up. 802.11n allows for data rates of up to 600Mbps using the 40MHz.

Although A 2.4GHz frequency would cover a larger range, the building is relatively small and has complete coverage using 5GHz. The building is 132'x78', which means that the maximum indoor connection range of ~230' completely covers the building area.

<img src="/home/nick/Downloads/indoor.jpg" width=300px style="margin-left:50px; transform:rotate(-90deg);"> 
<figcaption> Fig.1: The building diagonal is well within the range of the indoor signal radius </figcaption>

## Design & Materials

##### Antennas

For the antennas I opted to use parabolic dishes because of their extended range compared to yagis. The dish itself will be made out of sheet metal. The length of the antenna is inversely proportional to the frequency, so the dish itself will be smaller. [1]

##### Wiring & Cables

I used insulated copper wiring to divert lightning strikes from the rod to the grounding. For cable protection, I used coax cables connected to routine lightning sure protectors.

##### Lightning Rods

I used two lightning rods, one on each of the towers. Each is $15$' tall. The rods are made out of copper due to its conductivity.

##### Ground Plates

The copper wire is attached to the tower leg, which runs down to its length to the grounding, where it descends underground to a ground rod clamp.

##### Cable Building Entry 

I opted to use a cable box as my method of building entry. Inside are two drip loops (one before the ground block and one after), as well as an insulated entry into the building.

## Remote Facility Calculations

### Antenna Height

* Given:
    - $d_1=4.3$ mi, $d_2=5.2$ mi, and $D=9.5$ mi
    - Height of obstruction ($H_3$) is $38$'
    - Maximum right tower (plus building) height is $100$', max right tower height is $185$'
* Assumptions:
    - We will use $5.0$ GHz as the frequency
    - Add an $10$' to antenna height as an additional buffer
* Calculations:
    - Freznel Zone (FZ1)$=H_1=0.6 \cdot 72.1 \cdot \sqrt{\frac{4.3\cdot5.2}{(4.3+5.2)\cdot5}}=29.6808$'
    - Curvature of the Earth$=H_2=\frac{(5.2\cdot 2)^2}{6}=18.0267$'
    - $H_3=38$'
    - $H_4=10$'
    - Height of antenna$=H_1+H_2+H_3+H_4=95.7075$'

### Antenna Length 

* The optimal transmission and conversion efficiency is achieved when the antenna length is $1/4$ the wavelength. [1],[2]
* $\lambda=c/f$
    - Where $\lambda=$ Wavelength (m), $c=$ Speed of light (m/s), and $f=$ Frequency
* $\lambda=299,792,458/5.0=0.06$m$=0.1970$'
* Antenna length$=(1/4)\cdot0.1970=0.04925$'
 
## RF Wireless Bridge Diagram

<img src="/home/nick/Downloads/rf-tower.jpg" width=300px style="margin-left:50px; transform:rotate(-90deg);">
<figcaption> Fig.2: The complete diagram for a RF bridge connection </figcaption>

## Further Considerations

* Sway Protection
    - Out of the three main cellular tower types (monopole, self-supporting, and guyed), both towers appear to be of the self supporting variety
    - This type supports 100-400 ft towers, so there is no need to add guy pole support

## Sources

[1] https://www.rfwireless-world.com/calculators/parabolic-dish-antenna-calculator.html

[2] https://www.everythingrf.com/rf-calculators/frequency-to-wavelength

