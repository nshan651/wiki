# COMP 349 Project 02

## Design & Materials

##### Antennas

For the antennas I opted to use parabolic dishes because of their extended range compared to yagis. The dish itself will be made out of sheet metal. The length of the antenna is inversely proportional to the frequency, so the dish itself will be smaller. [1]

##### Wiring & Cables

I used insulated copper wiring to divert lightning strikes from the rod to the grounding. For cable protection, I used coax cables connected to routine lightning sure protectors.

##### Lightning Rods

I used two lightning rods, one on each of the towers. Each is $15$' tall. The rods are made out of copper due to its conductivity.

##### Ground Plates

The copper wire is attached to the tower leg, which runs down to its length to the grounding, where it descends underground to a ground rod clamp.

##### Cable Building Entry 

I opted to use a cable box as my method of building entry. Inside are two drip loops (one before the ground block and one after), as well as an insulated entry into the building.

