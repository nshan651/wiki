# COMP 349 Session 05

## Outdoor Wifi

* LOS considerations
    - Take into account curve of earth
    - Keep Freznel zone - ellipse between the two points
    - Focus on FZ1: 0-90 degrees out of phase

## Curvature of the Earth

* Must keep 60% of FZ1 clear
* Component meanings
    - H1: Freznel zone (60% of it)
    - H2: Curvature of the earth
    - H3: Actual height of the obstruction
    - H4: Growth space (biological component, i.e. forests)
* Simple calculations for midpoint, complex if not
* For H2 complex calc, use whichever *d* is longer
* Higher freq, small ellipse and vice versa

## Other Design Components

* Towers and masts
* Sway protection
* Lightning protection
    - Arrestors
    - Ground field
* Cable entry
    - Drip loop to shed moisture

