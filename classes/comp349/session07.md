# COMP 349 Seesion 07

## 802.11 Layer

* Inside the frame
    - Frame control (header)
        - Info/metainf, such as protocol, type, data, protected, order, etc.
        - 16 bits
    - Duration/ID (NAV)
* Data has IP, NLH, App, Payload, trailing info
* Management frames part of SIFS Delay
    - High priority
* Control frames used for when we want to control traffic flows
* Data Frames (DIFS Delay)
    - First part is payloads and actions
    - Second part is the responses (usually null)

## Bridges vs Mesh Networks
    - Bridge is a more traditional AP to client replacement for Ethernet, allows connection from AP to client
    - Mesh network is more flexible, allows an AP to dynamically connect other APs to wired in order to maintain connectivity
    - Mesh performance is reduced
 
## Duration ID (NAV)

* Used in three different ways:

1. Nav value - varies from field to field
    - DCF
    - 32,768 ($2^14-1$)
2. Frames transmitted during contention-free period 
3. PS-Poll Frames
    - Power save

## Address Fields

* Destination Addi - MAC of end station
* Source - mac of sending station
* Receiver address - MAC of processing frame wireless station
* Transmission address - MAC of wireless station that sent the frame (only used in bridging)

## Frame Itself

* 802.11 MAC -> IP header -> NLL (ICP, UDP, etc.) -> APP -> Payload -> Translayers
* Higher-level protocol data to be transmitted
* Payload is the data that we ultimately want to transmit
* Frame check sequence (FCS)

