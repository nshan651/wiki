# COMP 349 Session 08

## Antennas

* Isotropic Antenna
    - Theoretical device
* All other antennas are measured against this antenna
* "perfect" antenna that propagates signals in all directions
* The ultimate goal is to get close to this type of coverage

## Measuring Antenna Performance

* Dielectric constant
    - Measurement of how well an antenna allows electrons to flow through its material (and radiate out of its insulators)
    - Permeability of materials with respect to frequency
* Voltage standing wave radio
    - Measurement of impedance mismatch between the transmission line and its load
* Antenna gain
    - Measure of how energy is directed from the antenna (dBi or dBd)
