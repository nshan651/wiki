# COMP 349 Exam 01

## 802.11 Standard

* The base specification includes 802.11 MAC and several other physical layers:
    - FHSS
    - DSSS
    - HR/DSSS
    - OFDM

* Four major physical components of 802.11
    - Distribution center
    - Access point
    - Wireless medium
    - Station
