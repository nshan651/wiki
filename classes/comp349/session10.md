# COMP 349 Session 10

## 802.11a PHY

* Based on OSDM
* Encodes a single tx into multiple carriers
* Similar to FDM (Frequency Division Multiplexing)

## Carrier Multiplexing

* Binding multiple frequencies together
* Carefully selects channels that overlap but do not interfere with eachother
* Orthogonality
    - Allows to distinguish independent items
    - Uses *fast Fourier transformation* to code the data

## Fast Fourier Transformation

* Divides waveform
* Takes time-varying signal and converts it
* Generates set of freq-domain components that make up the signal
* Restore the waveform on the receiver

## Guard Bands / Guard Time Reasoning

* Way to deal with inter-symbol interference (noise between encoded symbols)
* Fourier transform is a way to prevent this
* Biggest issue with guard time:
    - Transmitting nothing during guard period results in loss of orthogonality

## PMD Order of Ops

1. Choose TX rate
2. TX preamble
3. TX PLCP Header
4. Create data field of packet
5. Scramble the data
6. Encode data with Convolutional coder (FEC)
7. Split into blocks
8. Modulate (QAM)
9. Repeat till no remaining (padding last block to the fixed size)
10. ACK sent at supported rate for all to hear
    
