# COMP 349 Session 09

* Chipping process
    - DSSS uses a chipping sequence to spread the data across the spectrum
    - Chipping stream = pseudo-random noise codes
* Barker word
    - Allows partial transmissions to be decoded (inferred)
* Trading performance for reliability

## Differential (Binary) Phase Shift Keying

* Encodes data in phase shift changes of the transmitted data

## PLCP (Physical Layer Convergence Protocol)

* Order of the stack:
    - PLCP | 802.11 MAC | IP | TCP/UDP/IM | APP | Payload | FCS

* Clear Channel Mode (CCM)
    - Different modes between DPSK and DQSK
    - States based on dB noise
