# COMP 349 Project 03

## Facility Markup

<img src="/home/nick/Downloads/project03/MyCo-first-floor.png" width="800px"/> 
<figcaption> Fig.1: MyCo Facility L1 </figcaption>
<br>
<img src="/home/nick/Downloads/project03/MyCo-second-floor.png" width="800px"/> 
<figcaption> Fig.2: MyCo Facility L2 </figcaption>
<br>
<img src="/home/nick/Downloads/project03/network-logical-revised.png" width="400px"/> 
<figcaption> Fig.3: Network Logic Revised </figcaption>
<br>

### Markup Notes

* Each number corresponds to that AP's channel designation
    * The channels I used are 36, 52, 100, 136
* For all APs (except reception area AP), we will place them at 12' for optimal coverage
* Reception desk AP:
    * This area is an atrium with no ceiling, meaning it has access to the floor above
    * We'll use a single AP to provide coverage to both the first-floor reception area and the second-floor tables by placing it at 15'

## Physical Hardware Characteristics

* APs

| Name                         | Standard    | Frequency | Channel Width | Power       | Link Speed |
|------------------------------|-------------|-----------|---------------|-------------|------------|
| Cisco Catalyst 9130AX Series | 802.11ac W2 | 5 Ghz     | 80 Mhz        | 13.4W (PoE) | 1 Gbps     |

* Switches

| Name            | Uplink Speed             |
|-----------------|--------------------------|
| Meraki MS390-48 | 2x40G or 4/8x10G uplinks |

* I replaced the existing switches with new ones that support mGig and POE:
    * The new backbone will be 10 Gb between the switches rather than 1 Gb
* I will use the existing edge router, firewall, and network cabling (which already supports PoE)
* For the APs, I chose the Cisco Catalyst 9130AX Series 
    * Most of the Cisco 802.11ac W2 APs are at end-of-sale, so I wanted something future-proof
    * Can be configured to use 802.11ac W2 on PoE with only 13.4W power draw, up to 160MHz channel width. I will use the 80 MHz channel
    * The AP also has support for 802.11ax using PoE+, which will make it easier for the company to upgrade in the future
* 20 APs:
    * With 185 current users and an upper limit of 250 users within the next three years, the density of APs should easily be able to accommodate traffic
    * Average of `9.25:1` users per AP for 185 users, `12.5:1` users per AP for 250 users
* As far as antennas, the new hardware I installed have built-in antennas, so I won't bother installing an external antenna

## Network Characteristics
 
* The network uses 802.11ac wave 2 @5Ghz
* The backbone supports 10 Gb between switches and ~1 Gbps connectivity per access point
    * 10 Gb switches per floor / 10 APs per floor (technically 11 on L1 and 9 on L2) = ~1 Gbps connection
* For the channel designation, I utilized 5 out of the 6 available channels for 5GHz:
    * 36, 52, 100, 136, 149
* The network utilizes dynamic VLAN assignment to provide connection for multiple classes of users, and to segment and separate them based on access level:
    * Guests (and IoT/BYODs of employees) should use 172.18.128.0/17 
    * Employees will use 172.18.0.0/17
        * Employees will be further subdivided based on whichever of the 9 internal departments they are a part of
        * Treated as trusted entities that have access to whatever internal resources their department requires
* I will use 802.1x for the authentication protocol on the staff network, due to its ability to dynamically configure network landing-points
* For the guest network, I will a WPAx protected network
* PAP as a password system
* As per MyCo's request, the current AD infrastructure will continue to be used 
* Because many of the staff wish to leverage the convenience of wireless coverage, I opted for a dense deployment:
    * This especially makes sense given the abundance of whiteboards with aluminum in the first-floor cubicles that may affect coverage, as well as the large conference rooms on the second floor that must be able to accommodate many devices at a time.

