# COMP 349 Session 11

## 802.11 Highspeed PHY

* In order to maintain backwards compatability, all headers must be transmitted at the *lowest* rate
* Payload can still be transmitted using fasted possible speed

## Super-G

* Non-standards based, came before 802.11bg
* Hardware data compression
* Proprietary Super-G solutions eventually became integrated into 802.11n PHY

## 802.11n PHY

* Both MIMO 
* Both use 40MHz channels

## MIMO

* Each antenna has its own RF chain (connected to own radio)
* Improves throughput

## Operational Modes

* Mixed Mode (MM)
    * Full support for legacy clients
    * 20 MHz wide channel width
    * 50 Mbps 
* Greenfield (GF)
    * Not backwards compatible
    * PLCP format
    * Highest levels of throughput

## Increasing Throughput

* All that really changes are 3 things:
    * Channel size
    * Modulation complexity
    * TX

