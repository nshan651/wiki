# COMP 349 Session 03

## Physical Layer

* 802.11 MAC (Layer 1)
    - Subset of datalink layer
    - Modulation and demodulation between wireless devices
    - Interface between MAC and other wireless devices
    - Signal sensors
    - Rate shifting

## DSSS

* Direct Sequence Spread Spectrum
* 1-2 Mbps (basically)
* Barker sequence (chipping) allows partial sequences that arrive to be decoded
* Reduces interference by multiplying transmitted value across a larger frequency range

## FHSS

* Not a major player in 802.11 anymore
* Not really important

* Packet (IP Layer 3), Frame (Layer 2)

## OFDM

* Latest standard
* 6-45 Mbps
