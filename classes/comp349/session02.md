# COMP 349 Session 02

## Networking Standards

* Standards provide a way of guaranteeing interoperability
* 2.4 GHz range is universally set aside in most countries

### Issues 

* Channelization
* Frequency ranges (especially 5GHz range)
* Power output

### Early Adopters in Networking

* 802.11 standard was originally based on 900MHz
    * Opened up to 2.4GHz because of overcrowding
* Couple things companies look for
    * Tech maturity
    * Stability of basic design
    * Interoperability
        * I.e. backwards compatibility with existing infrastructure

### Developing Standards

* Ways new tech is identified
    * Publicly developed, privately, R&D
* Interest grows
* Developers and independent organizations will contribute
* Standards move to being finalized by the IEEE
    - Process is long and arduous
    - Finally becomes ratified into a new standard

### IEEE Process to Publish a Standard

* Sponsor selection - person or group that oversees the dev process 
* Cannot be any overlap; no existing work already being done
* Request authorization from IEEE 
    * Project Authorization Request (PAR)
    * Follows specific template
* PAR reviewed and accepted/rejected
* A working group is formed
    * Must include a chair, vice chair, secretary and treasurer
    * Operates like a company
    * Group develops their portion of the technical document
* Once complete, polling of different groups begns (No one gets more than 50%)
    * Companies, gov'n't, societies, consultants, educational institutions, users, NERDS
    * 30-90 day process
    * Draft is finally provided for review
* RevCom of IEEE reviews balloting process (not the contents of the standards)
* IEEE then ratifies and publishes the standard

### The Wi-Fi Alliance

* Wifi (Wireless Fidelity)
* Competing standards chart (48:00)

