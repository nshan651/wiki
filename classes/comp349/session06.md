# COMP 349 Session 06

* ACK (acknowledgements) function link quality
    - Positive ACK is only sign of success
* Hidden node problem:
    - Not every node can communicate with every other in a network
    - Hard to detect collisions associated with hidden nodes
* Nav = transmission (TX) timer
* RTS with empty queue = Clear to Send (CTS)
* RTS with queue members = add clients to back of queue, send CTS to node at front of queue
* Note: "Other" is what the client sees
