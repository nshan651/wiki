# ENGL 273

## Kingston -- The Woman Warrior

* Maxine Hong Kingston:
    * Born 1940 in Stockton, Kingston
    * Eldest of six American-born kids of Chinese immigrant parents
    * Father, scholar, immigrated to New York in 1924
    * Mother remained in China until 1939
    * Graduated from UC Berkeley in 1962
    * Moved to Hawaii in 1967 to escape the violence of the anti-war movement

## Story

* Telling this to her daughter as a cautionary tale
* Part I: No-name woman:
    * Story of her imaged aunt who drowned herself and her baby after being expelled from the village for adultery
    * Examines traditional Chinese ideals of womanhood
    * Emphasizes reality and importance of ghosts in her life
* Part II: White TIgers:
    * Imagines her young self called to the mountains to train as a woman warrior with supernatural fighting ability
    * Returns to her less epic real childhood, talking about suffering and sexism
    * Her family and community routinely dismiss girls
* Part III: Shaman:
    * Story of Kingston's mother, Brave Orchid
    * Husband is away in America when she loses her first two children
    * Puts herself through medical school and returns home as a doctor
    * Has run-ins with ghosts:
        * Exorcises a hairy creature from a school dorm
        * Espcapes a man-ape
    * Orchid is presented as a person with great power to bridge the mythical and the scientific
* Part IV: At the Western Palace:
    * Jumps to Brave Orchid's life as a California immigrant
    * Precedes her sister, Moon Orchid's own immigration
    * Her sister is abandoned by her husband who immigrates before her, but Brave schemes to reunite them and oust the husband's second wife
    * Leaves a dramatic impression on Kingston
* Part V: A song for a barbarian reed pipe:
    * Kingston's own childhood and struggles with communication
    * Struggles to speak as a child, takes it out on another Chinese girl who has even more difficulty communicating
    * Eventually finds her voice and stands up to her parents, but in the process loses contact with her herritage
    * Ends things with the story of a Chinese noblewoman who is kidnapped by barbarians and sings a song in her own language
    
