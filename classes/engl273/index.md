# ENGL 273 Index

## Notes

* [Achebe -- Things Fall About](achebe-engl273) 
* [Kingston -- The Woman Warrior](kingston-engl273)
* [Gordimer -- July's People](gordimer-engl273)
* [Exam 1](e1-engl273)
* [Final](final-engl273)
* [Research Paper](rp-engl273)
* [RP Notes](rp-notes-engl273)

## Required Texts

1. Achebe, Chinua (Nigeria/USA). Things Fall Apart (1958) (Norton critical edition); ISBN:
978-0393932195
2. Rhys, Jeanne (Dominica, UK). Wide Sargasso Sea (1966) (Norton Critical edition); ISBN:
978-0393960129
1. Kingston, Maxine Hong (USA). The Woman Warrior (1976); ISBN: 978-0679721888
2. Gordimer, Nadine (South Africa). July's People (1981); ISBN: 978-0140061406
3. Hamid, Mohsin (Pakistan, UK). The Reluctant Fundamentalist (2007)

## Course Schedule

* Jan. 3 and 4: 
    * Introduction to the course, literary concepts and terminology, and critical methods
    * research sources and MLA format and documentation
    * Achebe, Nigeria, and Things Fall Apart
* Jan. 5 and 6:
    * Rhys, the West Indies, and Wide Sargasso Sea
* Jan. 7:
    * Research paper discussion
    * Midterm exam
    * Kingston, China and USA, and The Woman Warrior
* Jan. 9:
    * Kingston and The Woman Warrior continued
* Jan. 10 and 11:
    * Gordimer, South Africa, and July’s People
* Jan. 12:
    * Hamid, Pakistan and USA, and The Reluctant Fundamentalist
* Jan. 13:
    * Final exam
    * Research paper due
