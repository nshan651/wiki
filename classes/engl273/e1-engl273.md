# ENGL 273 Exam

## Scratch Notes

* The portrayal of colonialism in Achebe's *Things Fall Apart* is significant in both his own life and the cultural context during which the novel takes place.

## Part I
 
* Write an essay in which you critically analyze and assess (and propose a thesis about) the portrayal of colonialism in Chinua Achebe’s Things Fall Apart. First, you should briefly explain the significance of this theme in Achebe’s life and in the cultural context about/out of which he is writing. Next, analyze/assess in detail the ways in which this theme intersects with issues of race, ethnicity, nation, religion, gender, and even the rewriting of earlier canonical British texts, among others. What are the key techniques that Achebe uses to portray the theme of colonialism in the novel?

1. Exam Q1:

<u>
In Chinua Achebe's *Things Fall Apart*, the theme of colonialism is significant in both his own life as well as the cultural context during which the novel takes place. Through the portrayal of the complex and sophisticated Igbo culture and society, as well as the depiction of the colonizers as ignorant and motivated by greed, Achebe effectively challenges the portrayal of Africa and Africans in European literature and exposes the realities of colonialism as a process of exploitation and oppression. Througout the text, Achebe uses an array of techniques, including characters, symbols, and imagery, in order to offer a nuanced critique of colonialism and its effects on African societies.
</u>

Achebe, born in Nigeria in 1930, was a vocal critic of colonialism and its damaging effects on African society. In his writings, he challenged the European portrayal of Africans by giving voice to the experiences of colonized peoples. The novel was published at a significant time for Nigeria, just two years before its independence from Britain. It was seen as a response to the dominant narrative presented by European colonizers.

One of the key techniques that Achebe uses is the portrayal of the Igbo culture and society as a complex and sophisticated one that is rich in traditions and values. This challenges the European depiction of African cultures as primitive and inferior to European culture. The character Okonkwo, whom the story centers around, is a proud and independent man of the Umuofia who values hard work and the role of men as leaders of their families and communities. Despite not receiving much help from his father, Okonkwo is able to find great success in yam farming and wrestling. Over time he builds up great wealth and status in the village, as well a strong family and many friends. Honorary titles are very important to Okonkwo. Okonkwo's pride in his culture is symbolized by the way in which he bears his title, "The Cat."

As Okonkwo and his community come into contact with the colonizers in the second part of the novel, they are forced to contend with the rapidly changing world around them and the erosion of their usual way of life. The first missionary to arrive in Umuofia, Mr. Brown, is depicted good-natured and sincere yet ignorant to the local customs. This is exemplified with his theological discussions with village leaders about the Christian God versus the many gods worshipped by the people of Umuofia. To the people of the village, he "was a man of good heart, but he did not understand" the Igbo people and their beliefs (Achebe 134).

Although the church and the Igbo people are able to peacefully coexist for a time, the arrival of Mr. Smith, who represents the interests of European trading companies, makes this tenuous peace impossible. Mr. Smith and the colonizers are portrayed as being motivated by greed and a desire to exploit the resources of the colonized peoples. The colonizers' greed and indifference to the suffering of the Igbo people exposes the grim realities of colonialism as a process of exploitation and oppression - a stark contrast to the European view of colonizers as benevolent and enlightened.

The theme of gender is also closely tied to the themes of colonialism and cultural change. The Igbo people live in a patriarchal society, where men hold positions of power and women are expected to adhere to traditional gender roles. Men are expected to be strong, industrious, and successful, while women are expected to be submissive and to fulfill their domestic duties. Ekwefi, one of Okonkwo's wives, is expected to bear children and to perform household chores, whereas Okonkwo and his sons are expected to do the "men's tasks" of farming the yams and leading. Even the strong independent female characters of the novel are subject to these roles. When Okonkwo's daughter, Enzima, shows signs of strength and intelligence, he repeatedly says to himself that he wishes she had been born as a man.
    


The traditional gender roles of the Igbo people are challenged with the arrival of the European colonizers, who seek to promote gender equality and the idea that men and women are seen as equals to God. The promise of equality is most attractive to people who have low social standing in the village, including women and the *osu*, the lowest class of people who are exiled to a specific part of town.

Overall, the portrayal of colonialism in *Things Fall Apart* serves to highlight the ways in which this force can lead to the destabilization of traditional cultural values and practices. By examining the intersection of various identities and through the use of various literary techniques, Achebe effectively conveys the destructive impacts of colonialism and its lasting effects.

2. Exam Q4:

In "Wide Sargasso Sea" by Jean Rhys, the passage that describes Antoinette's dream of seeing Tia at the pool at Coulibri is significant to the rest of the novel because it represents the character's growing realization of her own agency and her desire to escape the constraints placed on her by colonialism, gender, and race. This passage reflects the major themes of the novel through the use of symbols and imagery, as well as through the techniques of stream of consciousness and unreliable narration.

One of the main themes the excerpt explores is the impacts of colonialism. Antoinette's dream of seeing Tia at Coulibri can be seen as a symbol for the way in which colonialism has destroyed the way of life in the Caribbean and erased the cultural identity of people like Tia. The "red sky" in Antoinette's vision can be seen as a symbol for the bloodshed and violence caused by colonialism in the Caribbean.

Antoinette's dreams also contain the theme of gender, as they are reflections of the ways in which her desires are constantly suppressed and manipulated by the men in her life. The male voice calling out "Bertha!" serves as a reminder of how Antoinette is isolated and dehumanized due to her "madness." She expresses her desires to reclaim her agency and identity as a woman in her dreams of burning down the mansion and eventually through her actions to overcome control and escape her prison.

Antoinette's sense of identity and connection to the Caribbean is depicted as being in tension with her association to the British colonial society. The dream of her childhood friend Tia demonstrates Antoinette's desire for a sense of belonging and home. These emotions are in sharp contrast to the aggressive themes of the male voice calling out and the vivid red sky, which symbolizes how her sense of being has been upended by the destruction of colonialism.

Additionally, Rhys uses stream of conscious and unreliable narration to convey Antoinette's fragile emotional state and her struggle with identity. The narration through Annete's voice helps to convey her immediate thoughts and feelings of uncertainty. The fact that she is unsure of whether or not she is dreaming demonstrates the chaos and confusion that she is experiencing.

The passage from "Wide Sargasso Sea" describing Antoinette's dream of Tia at Coulibri is significant because it captures the major themes of the novel, including, colonialism, identity, gender, nation, and race. The literary techniques, such as stream of conscious and unreliable narration, convey Antoinette's inner turmoil and her struggle with identity.
