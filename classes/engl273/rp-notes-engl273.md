# ENGL 273 Research Paper Notes

## "Things Fall Apart" as a Nigerian rewriting of colonial narratives

## Resources

* Chinua Achebe's "An Image of Africa: Racism in Conrad's Heart of Darkness" is a seminal essay that discusses the portrayal of Africa and Africans in Joseph Conrad's "Heart of Darkness" and how "Things Fall Apart" serves as a response to those representations.
    * "Heart of Darkness" displays the Western need to set up themselves as superior and to have Africa be the antithesis of the West
    * HoD has joined the pantheon of "permanent literature," literature that is firmly embedded into our scholarship and collective conscious
    * Conrad compares the banks of the Congo and its inhabitants to "A prehistoric Earth" (Achebe, 3).
    * "the fascination it holds over the Western mind: 'What thrilled you was just the thought of their humanity -- like yours -- Ugly.'" (Achebe, 3).
    * Conrad attempts disguise his own moral attitudes by telling the story through two levels of narration: through the primary narrator, Marlow, whose account is given through another mysterious figure
    * Conrad was "A thoroughgoing racist" (Achebe, 6).
    * The role of Africa and Africans in HoD is that of set pieces or props

* "Decolonizing the Mind: The Politics of Language in African Literature" by Ngugi wa Thiong'o, is a book that examines the role of language in the colonized mind, and how African writers use language to challenge colonial narratives.
    * In Kenya, English was seen as the most prestigious language and the gateway to many opportunities
    * Orature stopped in favor of English literature
    * "Thus language and literature were taking us further and further from ourselves to other selves, from our world to other worlds" (Thiong'o, 387)
    * Language can act as a means of communication, but on a deeper level it serves as a "carrier of culture." (Thiong'o, 388).
        * For English-first countries, it serves as both. For countries like Kenya, it does not carry culture in the same way that Swahili does for them.
* "Postcolonialism: An Historical Introduction" by Robert J. C. Young, is a comprehensive overview of postcolonial theory and its application to literature and culture, including a discussion of how "Things Fall Apart" fits into the postcolonial literary canon.
