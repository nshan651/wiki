# ENGL 273

## Achebe -- Things Fall Apart

* Chinua Achebe (1930-2013):
    * Born in Ogidi, Nigeria to family who were members of the Igbo ethnic group
    * Christian father
    * Studied English Lit at uni in Nigeria, graduated in 1953
    * Published *Things Fall Apart* in 1957, two years before Nigerian independence from Britain
* *Things Fall Apart*:
    * Published in English because of a confluence of factors (education was in English, unifying language of Africa/rest of the modern world)
    * Themes of Religion, Colonialism, and more
    * Critical of both, but also has a very nuanced opinion
    * Setting is in the late 1800s:
        * This is significant because it takes place around the time of the Berlin Conference and the carving up of Africa by colonizers
        * Very damaging to distinct ethnic groups that are suddenly forced together and governed as one group 

## Character/Place Guide

* Ani -- Earth God that is worshipped by the village
* Ekwefi -- Okonkwo's first wife. Almost shot and killed by Okonkwo
    * Had many of her children pass away in infancy
    * Believes she was curesed by *ogbanje*, children who die early and keep returning to the womb
* Ezinma -- Daughter of Ekwekfi
    * With the help of medicine man Okagbue, they find her *iyi-uwa* buried beneath a tree and remove the curse from her
* Umuofia -- The village where the story takes place
* Ilkemefuna -- Boy who is taken from neighboring village who waged a war with Umuofia and killed several of their people. Lives with Okonkwo
    * Killed by Okonkwo and a party of other men after a locus swarm befalls the village
* Nwoye -- Okonkwo's young son. Forms a strong brotherly relationship with the adopted Ilkemefuna
* Okonkwo -- The main character. Extremely ambitious, famous fighter with a hot temper. His father was very unsuccessful and gave him little to start out with, but Okonkwo becomes one of the most respected people in the village through his diligence and work-ethic
    * Accidentally kills a kid during a funeral celebration when he is supposed to be shooting a gun off as a salute. Banished from the village for 7 years
    * Flees to his mother's village of Mbanta
* Obierika -- Okonkwo's friend. Also a distinguished member of the village. His son is a talented fighter.
* Osu -- Outcast group of the village. The lowest class, they are exiled to a specific part of town and buried in the Evil Forest
* Enoch -- One of the early Christian converts. Desecrates a sacred tradition and starts a conflict in Umuofia

## Story Notes

* When the white missionaries come to Umuofia and build a church, the converts are men of no title, who do not have a say in the existing system of the village
* Okonkwo's son, Nwoye, becomes one of these converts and disowns his father
* To the people of the village, perhaps the most compelling part about the new religion being presented to them was the poetry and songs
* Elders do not want the missionaries in Mbanta, so they give them a piece of the Evil Forest to build their church on
* Royal Python -- the emenation of the water god. Allowed to go where it pleased

## Glossary of Words and Phrases

* (End of book)
